package br.com.concrelaje.sisproducao.basico.service;


import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.FaseProducao;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.ItemOrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.ItemPedido;
import br.com.concrelaje.sisproducao.entidades.ItemTraco;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MaterialProj;
import br.com.concrelaje.sisproducao.entidades.MaterialProjeto;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamentoFrota;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.ProdutoProjeto;
import br.com.concrelaje.sisproducao.entidades.Traco;
import br.com.concrelaje.sisproducao.entidades.view.ContratoView;


/** 
 * @author IGOR
 */
public interface ProducaoService {

	/**
	 * Salva um Produto.
	 * @param produto  
	 * @throws Exception
	 */ 

	Produto salvarProduto(Produto produto);
	/**
	 * Busca todos os produtos.
	 * @return
	 */
	List<Produto> buscarTodosProdutos();

	/**
	 * Busca produto pelo id
	 * @param Long id
	 * @return produto
	 */
	Produto buscarProdutoPorId(Long id);

	/**
	 * 
	 * @param produto
	 */
	void excluirProduto(Produto produto);


	/**
	 * 
	 * @param contrato
	 */
	Contrato salvarContrato(Contrato contrato);
	
	/**
	 * 
	 * @param produto
	 */
	void excluirContrato(Contrato contrato);

	/**
	 * Busca todos os contratos
	 * @return
	 */
	List<ContratoView> buscarTodosContratos();

	/**
	 * 
	 * @param numero
	 * @return
	 */
	List<ContratoView> buscarContratosPorFiltros(String numero);

	/**
	 * 
	 * @param id
	 * @return
	 */
	Contrato buscarContratoPorId(Long id);

	/**
	 * 
	 * @param item
	 */
	void salvarItemContrato(ItemContrato item);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<ItemContrato> buscarItensContratoPorIdContrato(Long id);

	/**
	 * 
	 * @return
	 */
	List<Traco> buscarTodosTracos();

	/**
	 * 
	 * @param traco
	 * @return
	 */
	Traco salvarTraco(Traco traco);

	/**
	 * 
	 * @param id
	 * @return
	 */
	Traco buscarTracoPorId(Long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<ItemTraco> buscarItemTracoPorIdTraco(Long id);

	/**
	 * 
	 * @param itemTraco
	 */
	void salvarItemTraco(ItemTraco itemTraco);

	/**
	 * 
	 * @param traco
	 */
	void excluirTraco(Traco traco);

	/**	 * Buscar contratos com itens contrato carregados por filtro informado;
	 * @param contrato
	 * @return
	 */
	List<Contrato> buscarContratosPorFiltros(Contrato contrato);


	/**
	 * 
	 * @param id
	 * @return
	 */
	List<ItemContrato> buscarItensContratoProdutoPorIdContrato(Long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<ItemContrato> buscarItensContratoMaterialPorIdContrato(Long id);

	/**
	 * Busca Item contrato por id.
	 * @param id
	 * @return
	 */
	ItemContrato buscarItemContratoPorId(Long id);


	List<Produto> buscarProdutosDisponiveisPorIdContratoEProduto(Long id, String produto);

	void excluirItemContrato(ItemContrato itemContrato);
	
	/**
	 * Busca produto por tipo do produto
	 * @param tipoProduto
	 * @return
	 */
	List<Produto> buscarProdutosPorTipo(EnumTipoProduto tipoProduto);
	
	/**
	 * Busca todas as ordens de produ��o
	 * @return
	 */
	List<OrdemProducao> buscarTodasOPs();
	
	
	OrdemProducao salvarOP(OrdemProducao op);
	
	OrdemProducao buscarOrdemProducaoPorId(Long id);
	
	/**
	 * Verifica se o produto tem depend�ncia com contrato
	 * @param produto
	 * @return
	 */
	boolean verificarProdutoUtilizado(Produto produto);
	
	void excluirItemTraco(ItemTraco itemTraco);
	
	/**
	 * Salva um produto projeto
	 * @param produtoProjeto
	 * @return
	 */
	ProdutoProjeto salvarProdutoProjeto(ProdutoProjeto produtoProjeto);
	
	List<FaseProducao> buscarTodasFases();
	
	OrdemProducaoFase salvarOrdemProducaoFase(OrdemProducaoFase producaoFase);
	
	List<OrdemProducaoFase> buscarOrdemProducaoFasePorOp(OrdemProducao op);
	
	void excluirOrdemProducaoFase(OrdemProducaoFase ordemProducaoFase);
	
	OrdemProducaoFase buscarOrdemProducaoFasePorId(Long id);
	
	List<FaseProducao> buscarFasesDisponiveisParaOP(OrdemProducao op);
	
	/**
	 * Exclui um produto projeto
	 * @param produtoProjeto
	 */
	void excluirProdutoProjeto(ProdutoProjeto produtoProjeto);
	
	/**
	 * Busca um produto projeto por id.
	 * @param id
	 * @return
	 */
	ProdutoProjeto buscarProdutoProjetoPorId(Long id);
	
	FaseProducao buscarFasesProducaoPorId(Long id);
	
	FaseProducao buscarFaseProducaoPorOrdem(int ordem);
	
	/**
	 * Busca a pr�xima fase que a OP ir� passar.
	 * @param op
	 * @return
	 */
	OrdemProducaoFase buscarProximaOrdemProducaoFaseParaProduzirPorOp(OrdemProducao op);

	/**
	 * Busca as fases dispon�veis para baixar da OP. Retorna as fases com data in�cio difente de nulo e com a data fim igual a nulo
	 * @param op
	 * @return
	 */
	List<FaseProducao> buscarFasesDisponiveisParaBaixarPorOp(OrdemProducao op);
	
	/**
	 * 
	 * @param codigoPeca
	 * @param faseAtual
	 * @param situacao
	 * @param sigla
	 * @param nomeProduto
	 * @param nomeObra
	 * @param dimensao1
	 * @return
	 */
	List<OrdemProducao> buscarOrdensProducaoPorFiltro(Long codigoPeca, Long faseAtual, EnumSituacaoOP situacao, String sigla, 
			String nomeProduto, String nomeObra, String dimensao1);
	
	void excluirOrdemProducao(OrdemProducao ordem);
	
	OrdemProducao buscarOrdemProducaoPorIdPeca(Long idOrdemProducao);
	
	
	MaterialProjeto salvarMaterialProjeto(MaterialProjeto materialAco);
	
	MaterialProjeto buscarMaterialProjetoPorId(Long idMaterialProjeto);
	
	void excluirMaterialProjeto(MaterialProjeto materialProjeto);
	
	/**
	 * Busca lista de itens contrato projeto com produto carregado
	 * @param contratoId
	 * @return
	 */
	List<ItemContratoProjeto> buscarItensContratoProjetoComProdutoCarregadoPorIdProjeto(Long projetoId);
	List<OrdemProducao> buscarOrdemProducaoDisponivelPorProjeto(Long idProjeto);
	
	/**
	 * Busca todas as ordens de carregamento
	 * */
	
	List<OrdemCarregamento> buscarTodasOrdensCarregamento();
	
	List<OrdemCarregamento> buscarOrdensCarregamentoPorFiltro(String contrato);
	
	List<Contrato> buscarContratoParaOrdemCarregamento();
	OrdemCarregamento salvarOrdemCarregamento(OrdemCarregamento ordemCarregamento);
	
	List<ItemContratoProjeto> buscarPecasPorContratoESituacaoOP(Long id,EnumSituacaoOP disponivel);
	
	OrdemCarregamento buscarOrdemCarregamentoPorId(Long idOC);
	
	void salvarItemOrdemCarregamento(ItemOrdemCarregamento item);
	
	List<ItemOrdemCarregamento> buscarItensOrdemCarregamentoPorOC(Long idOC);
	
	List<Produto> buscarProdutosPorTipoParaProducao(EnumTipoProduto prateleira);
	
	List<OrdemCarregamentoFrota> buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(Long id);
	
	OrdemCarregamentoFrota salvarOrdemCarregamentoFrota(OrdemCarregamentoFrota ordemCarregamentoFrota);
	
	void excluirOrdemCarregamentoFrota(OrdemCarregamentoFrota ordemCarregamentoFrota);
	
	void excluirItemOrdemCarregamento(ItemOrdemCarregamento itemOrdemCarregamento);
	
	ItemOrdemCarregamento buscarItemOrdemCarregamentoPorId(Long id);
	
	void excluirOrdemCarregamento(OrdemCarregamento ordemCarregamento);
	
	
	MaterialProj salvarMaterialProj(MaterialProj material);
	
	MaterialProj buscarMaterialProjPorId(Long id);
	
	void excluirMaterialProjeto(MaterialProj material);
	
	boolean tracoUtilizadoEmProjeto(Long idTraco);
	List<Traco> buscarTodosTracosAtivos();
	
	List<Object> buscarDadosRelatorioAco(Long materialFiltro, String contratoFiltro, String obraFiltro, String situacaoFiltro, String dataInicialFiltro, String dataFinalFiltro);
	
	List<String> buscarSituacoesProjetos();
	
	List<Object> buscarDadosRelatorioProducao(String contratoFiltro, String obraFiltro, String situacaoProjetoFiltro, String dataInicioFiltro, 
			String dataFimFiltro, String pecaFiltro, String siglaPecaFiltro);
	
	List<Pedido> buscarTodosPedidos();
	
	Pedido salvarPedido(Pedido pedido);
	
	Pedido buscarPedidoPorId(Long idPedido);
	List<Produto> buscarProdutosDisponiveisEProduto(Long id, String produto);
	
	ItemPedido salvarItemPedido(ItemPedido item);
	
	List<ItemPedido> buscarItensPedidoProdutoPorIdPedido(Long id);
	List<ItemPedido> buscarItensPedidoMaterialPorIdPedido(Long id);
	void excluirItemPedido(ItemPedido itemPedido);
	void excluirPedido(Pedido pedido);
	List<Pedido> buscarPedidosPorFiltros(String numero);
	List<Pedido> buscarPedidoParaOrdemCarregamento();
	List<Produto> buscarPecasPorPedidoESituacaoOP(Long id);
	List<ItemOrdemCarregamento> buscarItensOrdemCarregamentoProdutoPorOC(Long idOC);
	ItemPedido buscarItemPedidoPorProdutoEPedido(Long idProduto, Long idPedido);
	List<Produto> buscarProdutosPorContratoESituacaoOP(Long id);
	List<Object> buscarMateriaisPorPedidoESituacaoOP(Long id);
	List<ItemOrdemCarregamento> buscarItensOrdemCarregamentoMaterialPorOC(Long idOC);
	
	
}
