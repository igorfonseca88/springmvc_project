package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.PessoaService;
import br.com.concrelaje.sisproducao.entidades.Cliente;
import br.com.concrelaje.sisproducao.entidades.Endereco;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoCliente;
import br.com.concrelaje.sisproducao.entidades.EnumTipoPessoa;
import br.com.concrelaje.sisproducao.entidades.PessoaFisica;
import br.com.concrelaje.sisproducao.entidades.PessoaJuridica;
import br.com.concrelaje.sisproducao.entidades.Telefone;
import br.com.concrelaje.sisproducao.entidades.wrapper.ClientePessoaWrapper;

@Controller
@RequestMapping(value = "/cliente/clienteList")
@SessionAttributes("clientePessoaWrapper")
public class ListarClientes {  
    
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private PessoaService pessoaService;
	
    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarClientes(ModelMap modelMap) {
        modelMap.addAttribute("listaClientes", clienteService.buscarTodosClientes());
        return "producao/cliente/ClienteList";
    }
    
    
    @RequestMapping(value="/excluir")
    public String excluir(Cliente cliente, ModelMap modelMap) {
    	try{
    		
    		cliente = clienteService.buscarPorId(cliente.getId());
    		clienteService.excluirCliente(cliente);
    		
    		modelMap.addAttribute("success", "Cliente exclu�do com sucesso!");
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "N�o foi poss�vel excluir o cliente!");
    	}
    	
    	return listarClientes(modelMap);
    }
    
    @RequestMapping(value="/editar")
    public String editar(Cliente cliente, ModelMap modelMap) {
    	try{
    		ClientePessoaWrapper clientePessoaWrapper = new ClientePessoaWrapper();
    		cliente = clienteService.buscarPorId(cliente.getId());
    		clientePessoaWrapper.setCliente(cliente);
    		
    		
    		if(EnumTipoPessoa.FISICA.equals(cliente.getPessoa().getTipoPessoa())){
    			clientePessoaWrapper.setTipoPessoa(EnumTipoPessoa.FISICA.toString());
    			clientePessoaWrapper.setPessoaFisica((PessoaFisica)cliente.getPessoa());
    		}
    		else{
    			clientePessoaWrapper.setTipoPessoa(EnumTipoPessoa.JURIDICA.toString());
    			clientePessoaWrapper.setPessoaJuridica((PessoaJuridica)cliente.getPessoa());
    		}
    		
    		List<Telefone> telefones = pessoaService.buscarPessoaComTelefonePorIdPessoa(clientePessoaWrapper.getCliente().getPessoa().getId()).getTelefones();
    		clientePessoaWrapper.setListaTelefonesCliente(telefones);
    		
    		List<Endereco> enderecos = pessoaService.buscarPessoaComEnderecoPorIdPessoa(clientePessoaWrapper.getCliente().getPessoa().getId()).getEnderecos();
    		clientePessoaWrapper.setListaEnderecosCliente(enderecos);
    		
    		modelMap.addAttribute("clientePessoaWrapper", clientePessoaWrapper);
    		modelMap.addAttribute("listaSituacoesCliente", getListaSituacoesCliente());
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "N�o foi poss�vel editar este cliente.");
    	}
    	return "producao/cliente/ClienteForm";
    }
    
    public EnumSituacaoCliente[] getListaSituacoesCliente() {
		return EnumSituacaoCliente.values();
	}
}
