package br.com.concrelaje.sisproducao.basico;


import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.concrelaje.sisproducao.basico.service.FrotaService;
import br.com.concrelaje.sisproducao.entidades.Motorista;

@Controller
@RequestMapping(value = "/basico/motoristaList/**")
public class ListarMotoristas {

	@Autowired
	private FrotaService frotaService;

	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String listarFrotas(ModelMap modelMap) {
		modelMap.addAttribute("listaMotoristas", frotaService.buscarTodosMotoristas());
		return "basico/MotoristaList";
	}

	@RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		Motorista motorista = new Motorista();
    		motorista = frotaService.buscarMotoristaPorId(new Long(id));
    		String data = "dd/MM/yyyy";

			SimpleDateFormat formata = new SimpleDateFormat(data);
			
			motorista.setDataString(formata.format(motorista.getValidade()));
			modelMap.addAttribute("motorista", motorista);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar motorista.");
    	}
    	return "basico/MotoristaForm";
    }
	
	@RequestMapping(value="/excluir", method = RequestMethod.GET)
    public String excluir(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		
    		Motorista motorista = new Motorista();
    		motorista = frotaService.buscarMotoristaPorId(new Long(id));
    		frotaService.excluirMotorista(motorista);
    		modelMap.addAttribute("listaMotoristas", frotaService.buscarTodosMotoristas());
			modelMap.addAttribute("success", "Sucesso ao excluir motorista.");
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao excluir motorista.");
    	}
    	return "basico/MotoristaList";
    }
	

}
