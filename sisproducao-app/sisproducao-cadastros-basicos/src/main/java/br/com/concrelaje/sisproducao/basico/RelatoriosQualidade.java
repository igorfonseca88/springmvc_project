package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.QualidadeService;
import br.com.concrelaje.sisproducao.entidades.wrapper.RelatorioQualidadeWrapper;


@Controller
@SessionAttributes("relatorioQualidadeWrapper")
@RequestMapping(value = "qualidade/relatoriosQualidade/**")
public class RelatoriosQualidade {
    
	 @Autowired
	 QualidadeService qualidadeService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String init(ModelMap modelMap) {
    	RelatorioQualidadeWrapper relatorioQualidadeWrapper = new RelatorioQualidadeWrapper();
    	modelMap.addAttribute(relatorioQualidadeWrapper);
    	modelMap.addAttribute("listaTipoRelatorioQualidade", relatorioQualidadeWrapper.getListaTipoRelatorioQualidade());
    	return "qualidade/RelatoriosQualidade";
    }
    
    @RequestMapping(method = RequestMethod.POST, params={"pesquisar"})
    public ModelAndView pesquisar(@ModelAttribute("relatorioQualidadeWrapper") RelatorioQualidadeWrapper relatorioQualidadeWrapper,
    		ModelMap modelMap) {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("qualidade/RelatoriosQualidade");
    	try{
    		
    		
    		List<Object> lista = qualidadeService.buscarDadosRelatorioQualidadePorTipo(relatorioQualidadeWrapper.getFiltroTipoRelatorio());
    		modelMap.addAttribute("listaDataModel", lista);
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return mv;
    }
    
       
    
}
