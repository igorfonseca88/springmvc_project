package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.wrapper.ContratoWrapper;

@Controller
@RequestMapping(value = "contrato/contratoList/**")
@SessionAttributes("contratoWrapper")
public class ListarContratos {
    
	 @Autowired
	 private ProducaoService producaoService;
	
	 @Autowired
	 private EstoqueService estoqueService;
	
	 @Autowired
	 private ClienteService clienteService;
	
	 @Autowired
     private MessageSource messageSource;
	 
	 @Autowired
	 private DocumentoAnexoService documentoAnexoService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarContratos(ModelMap modelMap) {
    	modelMap.addAttribute("listaContratos", producaoService.buscarTodosContratos());
    	return "producao/contrato/ContratoList";
    }
    
    @RequestMapping(value="/pesquisar", method = RequestMethod.POST)
    public String pesquisar(@RequestParam("numero") String numero,ModelMap modelMap) {
    	modelMap.addAttribute("listaContratos", producaoService.buscarContratosPorFiltros(numero));
        return "producao/contrato/ContratoList";
    }
    
   @RequestMapping(value="/editar", method = RequestMethod.GET) 
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		Contrato contrato =  producaoService.buscarContratoPorId(new Long(id));
    		ContratoWrapper contratoWrapper = new ContratoWrapper();
    		contratoWrapper.setContrato(contrato);
    		contratoWrapper.setListaItemContratoProduto(producaoService.buscarItensContratoProdutoPorIdContrato(contrato.getId()));
    		contratoWrapper.setListaItemContratoMaterial(producaoService.buscarItensContratoMaterialPorIdContrato(contrato.getId()));
    		contratoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdContrato(contratoWrapper.getContrato().getId()));
    		contratoWrapper.setCliente(contrato.getCliente());
    		contratoWrapper.setListaProdutos(producaoService.buscarTodosProdutos());
    		contratoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
    		
    		modelMap.addAttribute("listaItens", contratoWrapper.getItemContrato());
    		modelMap.addAttribute("listaClientes", clienteService.buscarTodosClientes());
    		modelMap.addAttribute("contratoWrapper", contratoWrapper);
    		
    	}catch(Exception e){
    		modelMap.addAttribute("error", "Erro ao carregar contrato.");
    		e.printStackTrace();
    	}
    	return "producao/contrato/ContratoForm";
    }
   
   @RequestMapping(value="/excluir")
   public String excluir(Contrato contrato, ModelMap modelMap) {
	   try{
   		contrato = producaoService.buscarContratoPorId(contrato.getId());
   		producaoService.excluirContrato(contrato);
   		
   		modelMap.addAttribute("success", "Contrato exclu�do com sucesso!");
   		
   		}catch(Exception e){
   		e.printStackTrace();
   		modelMap.addAttribute("error", "N�o foi poss�vel excluir o contrato!");
   		}
   		return listarContratos(modelMap);
   }
}
