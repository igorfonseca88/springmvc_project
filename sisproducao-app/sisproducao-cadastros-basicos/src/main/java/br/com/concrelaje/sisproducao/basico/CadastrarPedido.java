package br.com.concrelaje.sisproducao.basico;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Cliente;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoContrato;
import br.com.concrelaje.sisproducao.entidades.ItemPedido;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.wrapper.PedidoWrapper;

@Controller
@RequestMapping(value = "/pedido/pedidoForm/**")
@SessionAttributes("pedidoWrapper")
public class CadastrarPedido { 
	@Autowired
	private ProducaoService producaoService;

	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private DocumentoAnexoService documentoAnexoService;
	
	@Autowired
	private EstoqueService estoqueService;
	



	@RequestMapping(value = "/init", method = RequestMethod.GET)
	public String cadastrarPedido(Model model) {
		model.addAttribute(new PedidoWrapper());
		model.addAttribute("listaClientes", clienteService.buscarTodosClientes());
		return "producao/pedido/PedidoForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarPedido(@ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, @ModelAttribute("idCliente") String idCliente, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try {
			
						
						
			if(SimpleValidate.isNullOrBlank(pedidoWrapper.getPedido().getNumero())) {
				mv.addObject("alert", "O número é um campo obrigatório");
				mv.setViewName("producao/pedido/PedidoForm");
				return mv;
			}
			
			
			
			if(pedidoWrapper.getPedido().getId() == null){
				pedidoWrapper.getPedido().setDataCadastro(new Date());
				pedidoWrapper.getPedido().setSituacao(EnumSituacaoContrato.EM_ELABORACAO);
			}
			
			Cliente cliente = new Cliente();
			if(pedidoWrapper.getCliente() != null && pedidoWrapper.getCliente().getId() != null ){
				cliente = clienteService.buscarPorId(pedidoWrapper.getCliente().getId());
				pedidoWrapper.getPedido().setCliente(cliente);
			}
			

			Pedido pedido = producaoService.salvarPedido(pedidoWrapper.getPedido());
			
			pedidoWrapper.setPedido(pedido);

			
			mv.addObject("success", "Pedido cadastrado com sucesso.");
			pedidoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			mv.addObject(pedidoWrapper);
			mv.addObject("listaClientes", clienteService.buscarTodosClientes());
			

		} catch (Exception e) {
			mv.addObject("error", "Não foi possível salvar o pedido.");
			e.printStackTrace();
		}

		mv.setViewName("producao/pedido/PedidoForm");
		return mv;
	}

	
	@RequestMapping(method = RequestMethod.GET)
	public String selecionarCliente(@ModelAttribute("id") String idCliente,	@ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, ModelMap modelMap) {

		Cliente cliente = clienteService.buscarPorId(new Long(idCliente));
		
		pedidoWrapper.setCliente(cliente);
		modelMap.addAttribute("listaClientes", clienteService.buscarTodosClientes());
		modelMap.addAttribute(pedidoWrapper);
		return "producao/pedido/PedidoForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"pesquisarCliente"})
    public ModelAndView pesquisarCliente(@RequestParam("cliente") String cliente,ModelMap modelMap) {
		ModelAndView mv = new ModelAndView();
    	modelMap.addAttribute("listaClientes", clienteService.buscarClientePorNome(cliente));
    	modelMap.addAttribute("openModal", "sim");
    	mv.setViewName("producao/pedido/PedidoForm");
    	return mv;
    }
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"finalizar"})
	public ModelAndView finalizar(@ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, BindingResult bindingResult) {
		ModelAndView mv = new ModelAndView();
		
		try {
			
			if(SimpleValidate.isNullOrEmpty(producaoService.buscarItensPedidoProdutoPorIdPedido((pedidoWrapper.getPedido().getId())))){
				mv.addObject("alert", "Para finalizar o pedido é necessário que exista pelo menos um produto adicionado.");
				mv.setViewName("producao/pedido/PedidoForm");
				return mv;
			}
			
						
			pedidoWrapper.getPedido().setSituacao(EnumSituacaoContrato.FINALIZADO);
			producaoService.salvarPedido(pedidoWrapper.getPedido());
			
			Pedido pedido = producaoService.buscarPedidoPorId(pedidoWrapper.getPedido().getId());
			pedidoWrapper.setPedido(pedido);
			mv.setViewName("producao/pedido/PedidoForm");
			mv.addObject("success", "Pedido finalizado com sucesso.");
			mv.addObject("pedidoWrapper", pedidoWrapper);
			
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao finalizar contrato.");
		}
		
		return mv;
	}
	
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST, params={"upload"})
	public String upload(HttpServletRequest request, @ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, Model model) {
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile multipartFile = multipartRequest.getFile("file");
		
		
		try {
			
			DocumentoAnexo documento = new DocumentoAnexo();
			
			documento.setPedido(pedidoWrapper.getPedido());
			documento.setDataCadastro(new Date());
			documento.setDescricao(pedidoWrapper.getDocumentoAnexo().getDescricao());
			documento.setImagem(multipartFile.getBytes());
			documento.setFilename(multipartFile.getOriginalFilename());
			documento.setType(multipartFile.getContentType());
			documento.setTipoDocumento(pedidoWrapper.getDocumentoAnexo().getTipoDocumento());
			
			documentoAnexoService.salvarDocumentoAnexo(documento);
				
			Pedido pedido = producaoService.buscarPedidoPorId(pedidoWrapper.getPedido().getId());
			pedidoWrapper.setPedido(pedido);
			pedidoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdPedido(pedidoWrapper.getPedido().getId()));
			pedidoWrapper.setDocumentoAnexo(null);
			model.addAttribute("listaClientes", clienteService.buscarTodosClientes());
			model.addAttribute("pedidoWrapper", pedidoWrapper);
			model.addAttribute("success", "Documento incluído com sucesso.");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			model.addAttribute("error", "Erro ao cadastro documento.");
		}catch (IOException e){
			e.printStackTrace();
			model.addAttribute("error", "Erro ao cadastro documento.");
		}
		
		
		return "producao/pedido/PedidoForm";
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("id") String idDocumento, Model model) throws Exception {
			 DocumentoAnexo documentoAnexo = documentoAnexoService.buscarPorId(new Long(idDocumento));
			 response.setContentType(documentoAnexo.getType());
			 response.setContentLength(documentoAnexo.getImagem().length);
			 response.setHeader("Content-Disposition","attachment; filename=\"" + documentoAnexo.getFilename() +"\"");
			
			FileCopyUtils.copy(documentoAnexo.getImagem(), response.getOutputStream());
			
			return null;
    }
	
	@RequestMapping(value = "/excluirProduto")
	public ModelAndView excluirProduto(@ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, ItemPedido itemPedido,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(pedidoWrapper.getListaItemPedidoProduto())){
				producaoService.excluirItemPedido(itemPedido);
				pedidoWrapper.getListaItemPedidoProduto().remove(itemPedido);
			}
			pedidoWrapper.setListaItemPedidoProduto(producaoService.buscarItensPedidoProdutoPorIdPedido(pedidoWrapper.getPedido().getId()));

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("producao/pedido/PedidoForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirDocumento")
	public ModelAndView excluirDocumento(@ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, DocumentoAnexo documento,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(pedidoWrapper.getListaDocumentoAnexo())){
				documentoAnexoService.excluirDocumento(documento);
				pedidoWrapper.getListaDocumentoAnexo().remove(documento);
			}
			pedidoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdPedido(pedidoWrapper.getPedido().getId()));

			mv.addObject("success", "Documento excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o documento.");
			e.printStackTrace();
		}
		mv.setViewName("producao/pedido/PedidoForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirMaterial")
	public ModelAndView excluirMaterial(@ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, ItemPedido itemPedido,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(pedidoWrapper.getListaItemPedidoMaterial())){
				producaoService.excluirItemPedido(itemPedido);
				pedidoWrapper.getListaItemPedidoMaterial().remove(itemPedido);
			}
			pedidoWrapper.setListaItemPedidoMaterial(producaoService.buscarItensPedidoMaterialPorIdPedido(pedidoWrapper.getPedido().getId()));

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("producao/pedido/PedidoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"reabrir"})
	public ModelAndView reabrir(@ModelAttribute("pedidoWrapper") PedidoWrapper pedidoWrapper, BindingResult bindingResult) {
		ModelAndView mv = new ModelAndView();
		
		try {
			pedidoWrapper.getPedido().setSituacao(EnumSituacaoContrato.REABERTO);
			producaoService.salvarPedido(pedidoWrapper.getPedido());
			
			Pedido pedido = producaoService.buscarPedidoPorId(pedidoWrapper.getPedido().getId());
			pedidoWrapper.setPedido(pedido);
			pedidoWrapper.setListaItemPedidoProduto(producaoService.buscarItensPedidoProdutoPorIdPedido(pedidoWrapper.getPedido().getId()));
			pedidoWrapper.setListaItemPedidoMaterial(producaoService.buscarItensPedidoMaterialPorIdPedido(pedidoWrapper.getPedido().getId()));
			pedidoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdPedido(pedidoWrapper.getPedido().getId()));
			pedidoWrapper.setCliente(pedido.getCliente());
			mv.addObject("pedidoWrapper", pedidoWrapper);
			mv.addObject("listaClientes", clienteService.buscarTodosClientes());
			mv.addObject("sucesso", "Pedido reaberto com sucesso.");
			mv.setViewName("producao/pedido/PedidoForm");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao reabrir contrato.");
		}
		
		
		return mv;
	}
	
	
}
