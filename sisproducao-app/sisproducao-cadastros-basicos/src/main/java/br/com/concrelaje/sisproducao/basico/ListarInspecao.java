package br.com.concrelaje.sisproducao.basico;


import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.QualidadeService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.Inspecao;
import br.com.concrelaje.sisproducao.entidades.wrapper.InspecaoWrapper;

@Controller
@RequestMapping(value = "qualidade/inspecaoList/**")
@SessionAttributes("inspecaoWrapper")
public class ListarInspecao { 
    
	 @Autowired
     private QualidadeService qualidadeService;
	  
	@Autowired
	private UsuarioService usuarioService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarInspecao(ModelMap modelMap) {
    	
    	String idPeca = (String) RequestContextHolder.currentRequestAttributes().getAttribute("idPecaSessao",RequestAttributes.SCOPE_SESSION);
    	
    	if(!SimpleValidate.isNullOrBlank(idPeca)){
    		modelMap.addAttribute("listaInspecao", qualidadeService.buscarInpecoesPorFiltro(new Long(idPeca), null,null,null));
    	}
    	else{
    		modelMap.addAttribute("listaInspecao", qualidadeService.buscarInpecoesPorFiltro(null,null,null,null));
    	}
    	
    	modelMap.addAttribute("inspecaoWrapper", new InspecaoWrapper());
    	
        return "qualidade/InspecaoList";
    }
    
    @RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"limpar"})
    public String limpar(ModelMap modelMap) {
    	
    	String idPeca = (String) RequestContextHolder.currentRequestAttributes().getAttribute("idPecaSessao",RequestAttributes.SCOPE_SESSION);
    	
    	if(!SimpleValidate.isNullOrBlank(idPeca)){
    		modelMap.addAttribute("listaInspecao", qualidadeService.buscarInpecoesPorFiltro(new Long(idPeca), null,null,null));
    	}
    	else{
    		modelMap.addAttribute("listaInspecao", qualidadeService.buscarInpecoesPorFiltro(null,null,null,null));
    	}
    	
    	modelMap.addAttribute("inspecaoWrapper", new InspecaoWrapper());
    	
        return "qualidade/InspecaoList";
    }
    
    @RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"pesquisar"})
    public ModelAndView pesquisar(@ModelAttribute("inspecaoWrapper") InspecaoWrapper inspecaoWrapper, 
    		 ModelMap modelMap) {
    	
    	
    	ModelAndView mv = new ModelAndView();
    	Long codPeca = null;
    	if(!SimpleValidate.isNullOrBlank(inspecaoWrapper.getCodigoPecaFiltro())){
    		codPeca = new Long(inspecaoWrapper.getCodigoPecaFiltro());
    	}
    	
    	if(SimpleValidate.isNullOrEmpty(qualidadeService.buscarInpecoesPorFiltro(codPeca, null, inspecaoWrapper.getNomeObraFiltro(), inspecaoWrapper.getNumeroContratoFiltro()))){
    		mv.addObject("alert", "Pesquisa não trouxe resultados.");
    	}
    	
    	modelMap.addAttribute("listaInspecao", qualidadeService.buscarInpecoesPorFiltro(codPeca, null, inspecaoWrapper.getNomeObraFiltro(), inspecaoWrapper.getNumeroContratoFiltro()));
    	mv.setViewName("qualidade/InspecaoList");
        return mv;
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
			InspecaoWrapper inspecaoWrapper = new InspecaoWrapper();
			Inspecao inspecao = qualidadeService.buscarInspecaoPorId(new Long(id));
			inspecaoWrapper.setInspecao(inspecao);

			String hora = "hh:mm";
			String data = "dd/MM/yyyy";
			String horaInicio = "", horaFinal= "" ;

			SimpleDateFormat formata = new SimpleDateFormat(hora);
			horaInicio = formata.format(inspecao.getDataInicio());
			if(inspecao.getDataFim() != null){
				horaFinal = formata.format(inspecao.getDataFim());
			}
			
			formata = new SimpleDateFormat(data);
			
			inspecaoWrapper.setDataString(formata.format(inspecao.getDataCadastro()));

			inspecaoWrapper.setHorarioInicial(horaInicio);
			inspecaoWrapper.setHorarioFinal(horaFinal);
			
			inspecaoWrapper.setRespostaInspecaos(qualidadeService.buscarRespostaInspecaoPorIdInspecao(inspecaoWrapper.getInspecao().getId()));
			inspecaoWrapper.setListaChecklist(qualidadeService.buscarTodosChecklistsAtivo());
			inspecaoWrapper.setListaUsuarios(usuarioService.buscarTodosUsuarios());
			modelMap.addAttribute(inspecaoWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar inspeção.");
    		return this.listarInspecao(modelMap);
    	}
    	return "qualidade/InspecaoForm";
    }
    
            
}
