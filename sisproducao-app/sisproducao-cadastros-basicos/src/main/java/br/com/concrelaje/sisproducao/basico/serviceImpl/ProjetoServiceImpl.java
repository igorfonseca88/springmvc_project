package br.com.concrelaje.sisproducao.basico.serviceImpl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
  
import br.com.concrelaje.sisproducao.basico.service.ProjetoService;
import br.com.concrelaje.sisproducao.entidades.ProdutoProjeto;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.view.ProjetoView;

@Service("projetoService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class ProjetoServiceImpl implements ProjetoService{
    
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public Projeto salvarProjeto(Projeto projeto) {
		if(projeto.getId() == null){
			projeto.setDataCadastro(new Date());
		}
        return entityManager.merge(projeto);
    }

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void excluirProjeto(Projeto projeto) {
        entityManager.remove(entityManager.merge(projeto));
    }
	
	@SuppressWarnings("unchecked")
	@Override
    public List<Projeto> buscarProjetos() {
        return entityManager.createQuery("FROM Projeto ").getResultList();
    }
	
	@SuppressWarnings("unchecked")
	@Override	
    public List<ProjetoView> buscarProjetosView() {
        return entityManager.createQuery("FROM ProjetoView ").getResultList();
    }
	
	@SuppressWarnings("unchecked")
	@Override
    public List<ProjetoView> buscarProjetosViewPorIdContrato(Long idContrato) {
		Query query = entityManager.createQuery("select p from ProjetoView p where p.idContrato = :idContrato ");
		query.setParameter("idContrato", idContrato);
		return query.getResultList();
    }
	
	@Override
    public ProjetoView buscarProjetoViewPorIdProjeto(Long idProjeto) {
		Query query = entityManager.createQuery("select p from ProjetoView p where p.id = :idProjeto ");
		query.setParameter("idProjeto", idProjeto);
		return (ProjetoView) query.getSingleResult();
    }
	
	@Override
	public Projeto buscarPorId(Long id) {
		Query query = entityManager.createQuery("select p from Projeto p where p.id = :id ");
		query.setParameter("id", id);
		return (Projeto) query.getSingleResult();
	}
	
	//TODO
	// busca os acessorios e tamb�m os materiais
	// n�o alterei a assinatura do m�todo para n�o ter muita altera��o
	@Override
	public Projeto buscarProjetoComAcessoriosPorIdProjeto(Long idProjeto) {

		try{
			StringBuilder hql = new StringBuilder("");

			hql.append("select p from Projeto p left join fetch p.acessorios left join fetch p.materiais left join fetch p.materiaisProjeto where p.id = :idProjeto ");
			
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("idProjeto", idProjeto);
			
			return (Projeto) query.getSingleResult();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Projeto buscarProjetoCarregadoPorIdProjeto(Long idProjeto) {

		try{
			StringBuilder hql = new StringBuilder("");

			hql.append("select p from Projeto p  left join fetch p.materiais left join fetch p.materiaisProjeto where p.id = :idProjeto ");
			
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("idProjeto", idProjeto);
			
			return (Projeto) query.getSingleResult();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProdutoProjeto> buscarAcessoriosPorIdProjeto(Long idProjeto) {

		try{
			StringBuilder hql = new StringBuilder("");

			hql.append("select p from ProdutoProjeto p  where p.projeto.id = :idProjeto ");
			
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("idProjeto", idProjeto);
			
			return  query.getResultList();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Integer countItensContratoProjetoPorIdProjeto(Long idProjeto) {

		try{
			StringBuilder hql = new StringBuilder("");

			hql.append("select p from Projeto p where p.id = :idProjeto ");
			
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("idProjeto", idProjeto);
			return ((Projeto) query.getSingleResult()).getItensContratoProjeto().size();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Projeto buscarProjetoComAcessoriosPorIdPeca(Long idPeca) {
		try {
			StringBuilder hql = new StringBuilder("");

			hql.append("select p from Projeto p left join fetch p.acessorios left join fetch p.materiais left join fetch p.materiaisProjeto where p.id in (SELECT pp.projeto.id from ItemContratoProjeto pp where pp.id = :idPeca) ");

			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("idPeca", idPeca);

			return (Projeto) query.getSingleResult();
		} catch (NoResultException exception) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}

