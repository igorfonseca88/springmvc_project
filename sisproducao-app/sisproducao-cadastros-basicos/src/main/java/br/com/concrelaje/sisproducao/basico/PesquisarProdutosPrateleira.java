package br.com.concrelaje.sisproducao.basico;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.ItemPedido;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.Produto;


@Controller
@RequestMapping(value = "modal/pesquisaProdutoPrateleira/**")
@SessionAttributes("pedido")
public class PesquisarProdutosPrateleira {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private EstoqueService estoqueService;
	
	@Autowired
	private ClienteService clienteService;
	
	 @Autowired
     private MessageSource messageSource;
	 

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String pesquisar(@ModelAttribute("idPedido") String idPedido, ModelMap modelMap) {
    	Pedido pedido = producaoService.buscarPedidoPorId(new Long(idPedido));
    	modelMap.addAttribute("pedido", pedido);
    	modelMap.addAttribute("listaProdutos", producaoService.buscarProdutosDisponiveisEProduto(new Long(idPedido), null));
    	return "producao/modal/PesquisaProdutosPrateleira";
    }
    
    @RequestMapping(value="/filtrar", method = RequestMethod.POST)
    public String filtrar(@RequestParam("produto") String produto, @ModelAttribute("pedido") Pedido pedido,  ModelMap modelMap) {
    	modelMap.addAttribute("listaProdutos", producaoService.buscarProdutosDisponiveisEProduto(pedido.getId(), produto));
    	return "producao/modal/PesquisaProdutosPrateleira";
    }
    
    
    @RequestMapping(value = "/incluirProduto", method = RequestMethod.POST)
	public ModelAndView incluirProduto(@ModelAttribute("pedido") Pedido pedido, @ModelAttribute("checados") String checados, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try {
			
			String s[] = checados.split("@");  
			
			if(s.length >1){
				
				for(int i = 1; i< s.length; i++){
					String arrProd[] = s[i].split("#");
					Produto produto = producaoService.buscarProdutoPorId(new Long(arrProd[0]));
					
					ItemPedido item = new ItemPedido();
					item.setPedido(pedido);
					item.setDataCadastro(new Date());
					item.setProduto(produto);
					item.setQuantidade(new Integer(arrProd[1]));
					producaoService.salvarItemPedido(item);

				}
				
			}
		
			mv.setViewName("producao/modal/PesquisaProdutosPrateleira");
			mv.addObject("sucesso", "Produto incluído com sucesso.");

		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao incluir produto.");
		}
		
		mv.addObject("listaProdutos", producaoService.buscarProdutosDisponiveisEProduto(pedido.getId(), null));
		
		return mv;
	}
    
    
       
    
}
