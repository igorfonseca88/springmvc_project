package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;

@Controller
@RequestMapping(value = "/basico/tipoDocumentoList")
public class ListarTipoDocumento {
    
	@Autowired
	DocumentoAnexoService anexoService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarTipoDocumentos(ModelMap modelMap) {
        modelMap.addAttribute("listaTipoDocumento", anexoService.buscarTodosTiposDocumento());
        return "basico/TipoDocumentoList";
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(TipoDocumento tipoDocumento, ModelMap modelMap) {
    	try{
    		 tipoDocumento = anexoService.buscarTipoDocumentoPorId(tipoDocumento.getId());
    		modelMap.addAttribute("tipoDocumento", tipoDocumento);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar tipo de documento.");
    	}
    	return "basico/TipoDocumentoForm";
    }
    
    @RequestMapping(value="/excluir")
    public String excluir(TipoDocumento tipoDocumento, ModelMap modelMap) {
    	try{
    		
    		tipoDocumento = anexoService.buscarTipoDocumentoPorId(tipoDocumento.getId());
    		anexoService.excluirTipoDocumento(tipoDocumento);
    		
    		modelMap.addAttribute("success", "Tipo de documento exclu�do com sucesso!");
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "N�o foi poss�vel excluir o tipo de documento!");
    	}
    	
    	return listarTipoDocumentos(modelMap);
    }
}
