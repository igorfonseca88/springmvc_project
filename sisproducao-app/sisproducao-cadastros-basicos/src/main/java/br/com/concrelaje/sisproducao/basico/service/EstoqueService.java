package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.EnumLogicoSimNao;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.ItemMovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Setor;


/**
 * @author IGOR
 */
public interface EstoqueService { 

	/** 
	 * 
	 * @return 
	 */
	List<MovimentacaoEstoque> buscarTodasMovimentacoes();

	/**
	 * 
	 * @param movimentacaoEstoque
	 * @return
	 */
	MovimentacaoEstoque salvarMovimentacao(MovimentacaoEstoque movimentacaoEstoque);

	/**
	 * 
	 * @param id
	 * @return
	 */
	MovimentacaoEstoque buscarPorId(Long id);

	/**
	 * 
	 * @param item
	 */
	void salvarItemMovimentacaoEstoque(ItemMovimentacaoEstoque item);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<ItemMovimentacaoEstoque> buscarItensMovimentacaoEstoquePorIdMovimentacaoEstoque(Long id);

	MovimentacaoEstoque buscarMovimentacaoEstoqueComItensPorIdMovimentacaoEstoque(Long id);

	void excluirItemMovimentacaoEstoque(ItemMovimentacaoEstoque itemMovimentacaoEstoque);
	
	
	/**
	 * 
	 * @return
	 */
	List<Material> buscarTodosMateriais();
	
	/**
	 * 
	 * @param material
	 * @return
	 */
	Material salvarMaterial(Material material);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	Material buscarMaterialPorId(Long id);
	
	/**
	 * 
	 * @param material
	 */
	void excluirMaterial(Material material);
	
	/**
	 * 
	 * @return
	 */
	List<Material> buscarTodosMateriaisComEstoque();

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<Material> buscarMateriaisDisponiveisPorIdContratoENumero(Long id, String numero);
	
	void movimentarEstoque(ItemContrato item, Material material);

	List<Material> buscarMateriaisAco(EnumLogicoSimNao acoSimNao);
	
	/**
	 * Salva um Setor.
	 * @param setor  
	 * @throws Exception
	 */ 

	Setor salvarSetor(Setor setor);
	/**
	 * Busca todos os setores.
	 * @return
	 */
	List<Setor> buscarTodosSetores(); 

	/**
	 * Busca setor pelo id
	 * @param Long id
	 * @return setor
	 */
	Setor buscarSetorPorId(Long id);

	/**
	 * 
	 * @param setor
	 */
	void excluirSetor(Setor setor);

	List<Material> buscarMateriaisDisponiveisPorIdPedidoENumero(Long idPedido,String numero);

	
}



