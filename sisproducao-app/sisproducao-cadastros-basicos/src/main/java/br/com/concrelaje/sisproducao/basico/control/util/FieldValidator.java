package br.com.concrelaje.sisproducao.basico.control.util;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.concrelaje.sisproducao.entidades.wrapper.ClientePessoaWrapper;
import br.com.concrelaje.sisproducao.entidades.wrapper.UsuarioPessoaWrapper;

public class FieldValidator implements Validator{

	@Override 
	public boolean supports(Class<?> clazz) {
		return Object.class.isAssignableFrom(clazz);
	}
	 
    @Override
    public void validate(Object target, Errors errors) {
    	if (target.getClass().equals(UsuarioPessoaWrapper.class)){
    		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.cpf", "sistema.campo.obrigatorio");
    		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.nome", "sistema.campo.obrigatorio");
    		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.email", "sistema.campo.obrigatorio");
    		
        	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "usuario.username", "sistema.campo.obrigatorio");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "usuario.password", "sistema.campo.obrigatorio");
    	}

    	if (target.getClass().equals(ClientePessoaWrapper.class)){
    		ClientePessoaWrapper item = new ClientePessoaWrapper();
    		item = (ClientePessoaWrapper) target;
    		
    		if (item.getValidator().equals("Fisica")){
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.nome", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.rg", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.cpf", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.email", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cliente.situacao", "sistema.campo.obrigatorio");
    		}

    		if (item.getValidator().equals("Juridica")){
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaJuridica.nomeFantasia", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaJuridica.razaoSocial", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaJuridica.cnpj", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaJuridica.inscricaoEstadual", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaJuridica.email", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cliente.situacao", "sistema.campo.obrigatorio");
    		}

    		if (item.getValidator().equals("Telefone")){
    			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefone.tipoTelefone", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefone.numero", "sistema.campo.obrigatorio");
    		}

    		if (item.getValidator().equals("Endereco")){
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endereco.CEP", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endereco.logradouro", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endereco.bairro", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endereco.numero", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endereco.cidade", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endereco.UF", "sistema.campo.obrigatorio");
        		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endereco.tipoEndereco", "sistema.campo.obrigatorio");
    		}
    	}
    }
}
