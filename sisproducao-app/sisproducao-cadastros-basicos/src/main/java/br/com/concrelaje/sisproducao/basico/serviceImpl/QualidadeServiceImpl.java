package br.com.concrelaje.sisproducao.basico.serviceImpl;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.QualidadeService;
import br.com.concrelaje.sisproducao.entidades.Checklist;
import br.com.concrelaje.sisproducao.entidades.Inspecao;
import br.com.concrelaje.sisproducao.entidades.ItemInspecao;
import br.com.concrelaje.sisproducao.entidades.RespostaInspecao;

@Service("qualidadeService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class QualidadeServiceImpl implements QualidadeService{
    
	@PersistenceContext
    private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemInspecao> buscarItemInspecao() {
		Query query =  entityManager.createQuery("FROM ItemInspecao ii");
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ItemInspecao salvarItemInspecao(ItemInspecao itemInspecao) {
		return entityManager.merge(itemInspecao);
	}

	@Override
	public ItemInspecao buscarItemInspecaoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT ii FROM ItemInspecao ii WHERE ii.id = :id");
		query.setParameter("id", id);
		return (ItemInspecao) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Checklist> buscarTodosChecklists() {
		Query query =  entityManager.createQuery("FROM Checklist c");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Checklist> buscarTodosChecklistsAtivo() {
		Query query =  entityManager.createQuery("FROM Checklist c WHERE c.situacao = 'ATIVO'");
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Checklist salvarChecklist(Checklist checklist) {
		return entityManager.merge(checklist);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemInspecao> buscarItensDisponiveisPorChecklist(Long idChecklist) {
		
		Query query =  entityManager.createQuery(" SELECT ck.itensInspecao FROM Checklist ck WHERE ck.id = :id)");
		query.setParameter("id", idChecklist);
		List<ItemInspecao> lista =  query.getResultList();
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ii FROM ItemInspecao ii ");
		
		if(!SimpleValidate.isNullOrEmpty(lista)){
			sql.append("  WHERE ii NOT IN (:lista)  and ii.situacao = 'ATIVO'");
		}
		
		Query query2 =  entityManager.createQuery(sql.toString());
		
		if(!SimpleValidate.isNullOrEmpty(lista)){
			query2.setParameter("lista", lista);
		}
		
		return query2.getResultList();
	}

	@Override
	public Checklist buscarChecklistPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT ck FROM Checklist ck left join fetch ck.itensInspecao  WHERE ck.id = :id");
		query.setParameter("id", id);
		return (Checklist) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Inspecao salvarInspecao(Inspecao inspecao) {
		return entityManager.merge(inspecao);
	}

	@Override
	public Inspecao buscarInspecaoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT i FROM Inspecao i left join fetch i.peca  WHERE i.id = :id");
		query.setParameter("id", id);
		return (Inspecao) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvarRespostaInspecao(RespostaInspecao respostaInspecao) {
		entityManager.merge(respostaInspecao);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RespostaInspecao> buscarRespostaInspecaoPorIdInspecao(Long id) {
		Query query =  entityManager.createQuery("SELECT r FROM RespostaInspecao r  WHERE r.inspecao.id = :id");
		query.setParameter("id", id);
		return  query.getResultList();
	}

	@Override
	public RespostaInspecao buscarRespostaInspecaoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT r FROM RespostaInspecao r  WHERE r.id = :id");
		query.setParameter("id", id);
		return  (RespostaInspecao) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> buscarInpecoesPorFiltro(Long idPeca, Long idChecklist, String nomeObra, String numeroContrato) {
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ( SELECT count(r.id) FROM RespostaInspecao r WHERE r.inspecao.id = i.id and r.legenda in ('REPROVADO', 'APROVADO_APOS_REINSPECAO')), i ");
		sql.append(" FROM Inspecao i  WHERE 1=1" );
		if(idPeca != null){
			sql.append(" AND i.id IN ( SELECT r.inspecao.id FROM RespostaInspecao r WHERE r.inspecao.peca.id = :id) ");
		}
		if(idChecklist != null){
			sql.append(" AND i.checklist.id = :idChecklist ");
		}
		
		if(!SimpleValidate.isNullOrBlank(nomeObra)){
			sql.append(" AND i.peca.itemContrato.contrato.nomeObra like :nomeObra ");
		}
		
		if(!SimpleValidate.isNullOrBlank(numeroContrato)){
			sql.append(" AND i.peca.itemContrato.contrato.numero like :numero ");
		}
		
		sql.append(" ORDER BY i.dataCadastro desc ");
		
		Query query =  entityManager.createQuery(sql.toString());
		
		if(idPeca != null){
			query.setParameter("id", idPeca);
		}
		if(idChecklist != null){
			query.setParameter("idChecklist", idChecklist);
		}
		if(!SimpleValidate.isNullOrBlank(nomeObra)){
			query.setParameter("nomeObra", "%"+nomeObra+"%");
		}
		if(!SimpleValidate.isNullOrBlank(numeroContrato)){
			query.setParameter("numero", "%"+numeroContrato+"%");
		}
		return  query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirItemInspecao(ItemInspecao itemInspecao) {
		entityManager.remove(entityManager.merge(itemInspecao));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RespostaInspecao> buscarRespostaInspecaoPorIdItemInspecao(Long idItemInspecao) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ri FROM RespostaInspecao ri  WHERE 1=1");
		sql.append(" AND ri.itemInspecao.id IN ( SELECT ii.id FROM ItemInspecao ii WHERE ii.id = :id) ");
		Query query = entityManager.createQuery(sql.toString());

		query.setParameter("id", idItemInspecao);
		
		return  query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirChecklist(Checklist checklist) {
		entityManager.remove(entityManager.merge(checklist));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> buscarDadosRelatorioQualidadePorTipo(String filtroTipoRelatorio) {
		StringBuffer buffer = new StringBuffer();
		
		if(filtroTipoRelatorio.equals("NUMERO_REPROVACOES_POR_OBRA")){
			buffer.append("select COUNT(ri.II_ID) as num_reprovacoes, c.CT_NOME_OBRA "); 
			buffer.append(" from tb_inspecao i "); 
			buffer.append(" join TB_ITEM_CONTRATO_PROJETO icp on i.IP_ID = icp.IP_ID ");
			buffer.append(" join TB_ITEM_CONTRATO ic on ic.IC_ID = icp.ITEMCONTRATO_ID ");
			buffer.append(" join TB_CONTRATO c on c.CT_ID = ic.CONTRATO_ID ");
			buffer.append(" join TB_RESPOSTA_INSPECAO ri on ri.INSPECAO_ID = i.IN_ID ");
			buffer.append(" where ri.II_LEGENDA in ('APROVADO_APOS_REINSPECAO', 'REPROVADO') ");
			buffer.append(" group by c.CT_NOME_OBRA ");
		}
		else if (filtroTipoRelatorio.equals("NUMERO_REPROVACOES_POR_PECA")){
			buffer.append("select COUNT(ri.II_ID) as num_reprovacoes, PROD.PR_NOME "); 
			buffer.append(" from tb_inspecao i "); 
			buffer.append(" join TB_ITEM_CONTRATO_PROJETO icp on i.IP_ID = icp.IP_ID ");
			buffer.append(" join TB_ITEM_CONTRATO ic on ic.IC_ID = icp.ITEMCONTRATO_ID ");
			buffer.append(" join TB_CONTRATO c on c.CT_ID = ic.CONTRATO_ID ");
			buffer.append(" join TB_RESPOSTA_INSPECAO ri on ri.INSPECAO_ID = i.IN_ID ");
			buffer.append(" join TB_PRODUTO PROD on PROD.PR_ID = ic.PRODUTO_ID ");
			buffer.append(" where ri.II_LEGENDA in ('APROVADO_APOS_REINSPECAO', 'REPROVADO') ");
			buffer.append(" group by PROD.PR_NOME ");
		}
		else if (filtroTipoRelatorio.equals("NUMERO_REPROVACOES_POR_GAP")){
			buffer.append("select COUNT(ri.II_ID) as num_reprovacoes, i.IN_GAP "); 
			buffer.append(" from tb_inspecao i "); 
			buffer.append(" join TB_ITEM_CONTRATO_PROJETO icp on i.IP_ID = icp.IP_ID ");
			buffer.append(" join TB_ITEM_CONTRATO ic on ic.IC_ID = icp.ITEMCONTRATO_ID ");
			buffer.append(" join TB_CONTRATO c on c.CT_ID = ic.CONTRATO_ID ");
			buffer.append(" join TB_RESPOSTA_INSPECAO ri on ri.INSPECAO_ID = i.IN_ID ");
			buffer.append(" where ri.II_LEGENDA in ('APROVADO_APOS_REINSPECAO', 'REPROVADO') ");
			buffer.append(" group by i.IN_GAP ");
		}
		
		Query query = entityManager.createNativeQuery(buffer.toString());
		
		return query.getResultList();
	}

}

