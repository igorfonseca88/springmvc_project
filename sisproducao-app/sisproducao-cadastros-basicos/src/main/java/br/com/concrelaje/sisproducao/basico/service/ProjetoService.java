package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.ProdutoProjeto;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.view.ProjetoView;

/**
 * @author WILLIAM
 */
public interface ProjetoService {

	/** 
	 * Salva um projeto.
	 * @param projeto
	 * @throws Exception
	 */ 
	Projeto salvarProjeto(Projeto projeto); 
	
	/**
	 * Exclui um projeto.
	 * @param projeto
	 * @throws Exception
	 */
	void excluirProjeto(Projeto projeto);
	
	/**
	 * Busca todos os projetos.
	 * @return
	 */
	List<Projeto> buscarProjetos();
	
	/**
	 * Busca projeto por id
	 * @param id
	 * @return
	 */
	
	Projeto buscarPorId(Long id);
	
	/**
	 * Busca todos os dados da View ProjetoView 
	 * @return
	 */
	List<ProjetoView> buscarProjetosView();
	
	/**
	 * Busca projetos por idContrato
	 * @param idContrato
	 * @return
	 */
	List<ProjetoView> buscarProjetosViewPorIdContrato (Long idContrato);
	
	/**
	 * Busca projetoview por id projeto
	 * @param idProjeto
	 * @return
	 */
	ProjetoView buscarProjetoViewPorIdProjeto(Long idProjeto);
	
	/**
	 * Busca projeto por id com lista de acessorios carregada
	 * @param idProjeto
	 * @return
	 */
	Projeto buscarProjetoComAcessoriosPorIdProjeto(Long idProjeto);
	
	/**
	 * Contador de itens contrato projeto por id do projeto
	 * @param idProjeto
	 * @return
	 */
	Integer countItensContratoProjetoPorIdProjeto(Long idProjeto);

	Projeto buscarProjetoComAcessoriosPorIdPeca(Long long1);

	List<ProdutoProjeto> buscarAcessoriosPorIdProjeto(Long idProjeto);

	Projeto buscarProjetoCarregadoPorIdProjeto(Long idProjeto);
}

