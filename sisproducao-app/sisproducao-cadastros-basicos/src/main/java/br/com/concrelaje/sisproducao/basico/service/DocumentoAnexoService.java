package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;

/**
 * @author IGOR 
 */
public interface DocumentoAnexoService {

	/**
	 * 
	 * @param documento 
	 */
	void salvarDocumentoAnexo(DocumentoAnexo documento);
	
	/**
	 * 
	 * @param documento
	 */
	void excluirDocumento(DocumentoAnexo documento);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<DocumentoAnexo> buscarDocumentosAnexoPorIdContrato(Long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	DocumentoAnexo buscarPorId(Long id);
	
	/**
	 * Busca anexo por id do projeto informado.
	 * @param id
	 * @return
	 */
	List<DocumentoAnexo> buscarDocumentosAnexoPorIdProjeto(Long id);

	List<TipoDocumento> buscarTodosTiposDocumento();

	TipoDocumento buscarTipoDocumentoPorId(Long id);

	/**
	 * 
	 * @param documento
	 */
	void salvarTipoDocumento(TipoDocumento tipoDocumento);
	
	/**
	 * 
	 * @param documento
	 */
	void excluirTipoDocumento(TipoDocumento tipoDocumento);

	List<DocumentoAnexo> buscarDocumentosAnexoPorIdOrdemCarregamento(Long id);

	List<DocumentoAnexo> buscarDocumentosAnexoPorIdPedido(Long id);

}



