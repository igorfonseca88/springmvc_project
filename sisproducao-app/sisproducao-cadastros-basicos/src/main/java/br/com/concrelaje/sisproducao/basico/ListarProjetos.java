package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.ProjetoService;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoSimNao;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoProjeto;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.ProdutoProjeto;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.Traco;
import br.com.concrelaje.sisproducao.entidades.view.ProjetoView;
import br.com.concrelaje.sisproducao.entidades.wrapper.ProjetoWrapper;

@Controller
@RequestMapping(value = "/projeto/projetoList")
@SessionAttributes("projetoWrapper")
public class ListarProjetos {
    
	@Autowired
	private ProjetoService projetoService;
	
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private DocumentoAnexoService documentoAnexoService;
	
	@Autowired
	private EstoqueService estoqueService;
	
    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarProjetos(ModelMap modelMap) {
        modelMap.addAttribute("listaProjetos", projetoService.buscarProjetosView());
        return "producao/projeto/ProjetoList";
    }
    
    @RequestMapping(value="/complementarProjeto")
    public String complementarProjeto(Projeto projeto, ModelMap modelMap) {
    	try{
    		ProjetoWrapper projetoWrapper = new ProjetoWrapper();
    		ProjetoView projetoView = new ProjetoView();
    		Long idProjeto = projeto.getId();
    		
    		projeto = projetoService.buscarProjetoCarregadoPorIdProjeto(idProjeto);
    		// buscar acessorios
    		List<ProdutoProjeto> acessorios = projetoService.buscarAcessoriosPorIdProjeto(idProjeto);
    		projeto.setAcessorios(acessorios);
    		
    		projetoView = projetoService.buscarProjetoViewPorIdProjeto(idProjeto);
    		
    		projetoWrapper.setProjeto(projeto);
    		projetoWrapper.setProjetoView(projetoView);
    		
    		Integer countItensContratoProjeto = projetoService.countItensContratoProjetoPorIdProjeto(idProjeto);
    		projetoWrapper.setCountItensContratoProjeto(countItensContratoProjeto);
    		projetoWrapper.setHabilitarAprovarReprovar(projeto.getSituacao() != null && projeto.getSituacao().equals(EnumSituacaoProjeto.AGUARDANDO_APROVACAO));
    		projetoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(projetoWrapper.getProjeto().getId()));
    		projetoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
    		projetoWrapper.setListaItensContratoProjeto(producaoService.buscarItensContratoProjetoComProdutoCarregadoPorIdProjeto(idProjeto));
    		
    		
    		// busca as ops geradas para as pe�as e verifica se tem alguma com a situa��o Dispon�vel
    		if(!SimpleValidate.isNullOrEmpty(producaoService.buscarOrdemProducaoDisponivelPorProjeto(idProjeto))){
    			projetoWrapper.setVisualizar(false);
    		}
    		
    		
    		modelMap.addAttribute("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
    		modelMap.addAttribute("listaProdutos", getListaProdutos());
    		modelMap.addAttribute("listaMateriaisAco", getListaMateriaisAco());
    		modelMap.addAttribute("listaMateriais", getListaMateriais());
    		modelMap.addAttribute("projetoWrapper", projetoWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "N�o foi poss�vel abrir este projeto para complementar dados.");
    	}
    	return "producao/projeto/ComplementarProjetoForm";
    }
    
    @RequestMapping(value="/visualizarProjeto")
    public String visualizarComplementarProjeto(ModelMap modelMap) {
    	try{
    		ProjetoWrapper projetoWrapper = new ProjetoWrapper();
    		ProjetoView projetoView = new ProjetoView();
    		
    		
			String idPeca = (String) RequestContextHolder.currentRequestAttributes().getAttribute("idPecaSessao",RequestAttributes.SCOPE_SESSION);
    		Projeto projeto = projetoService.buscarProjetoComAcessoriosPorIdPeca(new Long(idPeca));
    		projetoView = projetoService.buscarProjetoViewPorIdProjeto(projeto.getId());
    		
    		projetoWrapper.setProjeto(projeto);
    		projetoWrapper.setProjetoView(projetoView);
    		
    		Integer countItensContratoProjeto = projetoService.countItensContratoProjetoPorIdProjeto(projeto.getId());
    		projetoWrapper.setCountItensContratoProjeto(countItensContratoProjeto);
    		projetoWrapper.setHabilitarAprovarReprovar(projeto.getSituacao() != null && projeto.getSituacao().equals(EnumSituacaoProjeto.AGUARDANDO_APROVACAO));
    		projetoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(projetoWrapper.getProjeto().getId()));
    		projetoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
    		
    		// busca as ops geradas para as pe�as e verifica se tem alguma com a situa��o Dispon�vel
    		if(!SimpleValidate.isNullOrEmpty(producaoService.buscarOrdemProducaoDisponivelPorProjeto(projetoWrapper.getProjeto().getId()))){
    			projetoWrapper.setVisualizar(false);
    		}
    		
    		modelMap.addAttribute("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
    		modelMap.addAttribute("listaProdutos", getListaProdutos());
    		modelMap.addAttribute("listaMateriaisAco", getListaMateriaisAco());
    		modelMap.addAttribute("listaMateriais", getListaMateriais());
    		modelMap.addAttribute("projetoWrapper", projetoWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Nao foi possivel visualizar o projeto.");
    	}
    	return "producao/projeto/ComplementarProjetoForm";
    }
    
    
    @RequestMapping(value="/excluir")
    public String excluir(Projeto projeto, ModelMap modelMap) {
    	try{
    		projeto = projetoService.buscarPorId(projeto.getId());
    		projetoService.excluirProjeto(projeto);

    		modelMap.addAttribute("success", "Projeto exclu�do com sucesso!");

    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "N�o foi poss�vel excluir o projeto!");
    	}
    	return listarProjetos(modelMap);
    }
    
     
	public List<Produto> getListaProdutos(){
		return producaoService.buscarProdutosPorTipo(EnumTipoProduto.PADRAO);
	}
	
	public List<Material> getListaMateriaisAco(){
		return estoqueService.buscarMateriaisAco(EnumLogicoSimNao.S);
	}
	
	public List<Material> getListaMateriais(){
		return estoqueService.buscarMateriaisAco(EnumLogicoSimNao.N);
	}
	
	public List<Traco> getListaTracos(Projeto projeto){
		
		
		List<Traco> tracos = producaoService.buscarTodosTracosAtivos();
		
		if(projeto != null && projeto.getTraco() != null){
			if(!tracos.contains(projeto.getTraco())){
				tracos.add(projeto.getTraco());
			}
		}
		return tracos;
	}
}
