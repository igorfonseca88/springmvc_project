package br.com.concrelaje.sisproducao.basico.serviceImpl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ItemContratoProjetoService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoProjeto;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;

@Service("itemContratoProjetoService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class ItemContratoProjetoServiceImpl implements ItemContratoProjetoService{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ItemContratoProjeto salvarItemContratoProjeto(ItemContratoProjeto itemContratoProjeto) {
		if(itemContratoProjeto.getId() == null){
			itemContratoProjeto.setDataCadastro(new Date());
		}
		return entityManager.merge(itemContratoProjeto);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemContratoProjeto> buscarItensContratoProjetoPorFiltro(ItemContratoProjeto itemContratoProjeto) {

		StringBuilder hql = new StringBuilder("select icp from ItemContratoProjeto icp where 1 = 1 and icp.situacao <> 'CANCELADO'");

		if(itemContratoProjeto.getItemContrato().getId() != null){
			hql.append(" and icp.itemContrato.id = " + itemContratoProjeto.getItemContrato().getId());
		}
		
		if(!SimpleValidate.isNullOrBlank(itemContratoProjeto.getSigla())){
			hql.append(" and icp.sigla = '" + itemContratoProjeto.getSigla()+"' ");
		}
		
		
		hql.append(" order by icp.sequencial ASC ");
		
		Query query =  entityManager.createQuery(hql.toString());

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemContratoProjeto> buscarItensContratoProjetoParaProducao(ItemContratoProjeto itemContratoProjeto, Contrato contrato) {

		StringBuilder hql = new StringBuilder("select icp from ItemContratoProjeto icp where 1 = 1 and icp.projeto.situacao = '"+EnumSituacaoProjeto.APROVADO+"' ");

		if(itemContratoProjeto != null && itemContratoProjeto.getItemContrato() != null){
			if(itemContratoProjeto.getItemContrato().getId() != null){
				hql.append(" and icp.itemContrato.id = " + itemContratoProjeto.getItemContrato().getId());
			}
			
			if(!SimpleValidate.isNullOrBlank(itemContratoProjeto.getSigla())){
				hql.append(" and icp.sigla = '" + itemContratoProjeto.getSigla()+"' ");
			}
		}
		
		if(contrato != null){
			if(!SimpleValidate.isNullOrBlank(contrato.getNumero())){
				hql.append(" and icp.itemContrato.contrato.numero = '" + contrato.getNumero()+"' ");
			}
			
			if(!SimpleValidate.isNullOrBlank(contrato.getNomeObra())){
				hql.append(" and icp.itemContrato.contrato.nomeObra like '" + contrato.getNomeObra()+"%' ");
			}
		}
		
		hql.append(" and icp.id not in (select op.itemContratoProjeto.id from OrdemProducao op WHERE op.itemContratoProjeto.id is not null ) ");
		
		hql.append(" order by icp.id ASC ");
		
		Query query =  entityManager.createQuery(hql.toString());

		return query.getResultList();
	}
	
	@Override
	public ItemContratoProjeto buscarPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT icp FROM ItemContratoProjeto icp WHERE icp.id = :id");
		query.setParameter("id", id);
		try {
			return (ItemContratoProjeto) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}