package br.com.concrelaje.sisproducao.basico.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.ParametroConfiguracaoService;
import br.com.concrelaje.sisproducao.entidades.ParametroConfiguracao;

@Service("parametroConfiguracaoService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class ParametroConfiguracaoServiceImpl implements ParametroConfiguracaoService {
	
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ParametroConfiguracao salvar( ParametroConfiguracao parametroConfiguracao) {
		return entityManager.merge(parametroConfiguracao);
	}

	@Override
	public ParametroConfiguracao buscarPorId(Long id) {
		Query query = entityManager.createQuery("select pc from ParametroConfiguracao pc where pc.id = :id");
		query.setParameter("id", id);
		return (ParametroConfiguracao) query.getSingleResult();
	}

}
