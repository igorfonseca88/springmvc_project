package br.com.concrelaje.sisproducao.basico.serviceImpl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.Usuario;

@Service("usuarioService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class UsuarioServiceImpl implements UsuarioService{

	@PersistenceContext
	private EntityManager entityManager; 

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Usuario salvarUsuario(Usuario usuario) {
		if(usuario.getId() == null){
			usuario.setDataCadastro(new Date());
		}
		return entityManager.merge(usuario);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirUsuario(Usuario usuario) {
		entityManager.remove(entityManager.merge(usuario));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> buscarTodosUsuarios() {
		return entityManager.createQuery("FROM Usuario").getResultList();
	}

	@Override
	public Usuario buscarPorId(Long id) {
		Query query = entityManager.createQuery("select u from Usuario u left join fetch u.papeis where u.id = :id ");
		query.setParameter("id", id);
		return (Usuario) query.getSingleResult();
	}

	@Override
	public Usuario buscarUsuarioPorCpf(String cpf) {
		Usuario usuario = new Usuario();
		try{

			StringBuilder hql = new StringBuilder("");
			hql.append(" select u from Usuario u left join fetch u.papeis where u.pessoa.id = ( ");
			hql.append(" select pf.id from PessoaFisica pf where pf.cpf = :cpf ) ");

			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("cpf", cpf);
			usuario = (Usuario) query.getSingleResult();

		}catch (NoResultException exception){
			usuario = null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}

	@Override
	public Usuario buscarUsuarioPorLogin(String login) {
		Usuario usuario = new Usuario();
		try{

			StringBuilder hql = new StringBuilder("");
			hql.append(" select u from Usuario u where u.username = :login ");

			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("login", login);
			usuario = (Usuario) query.getSingleResult();

		}catch (NoResultException exception){
			usuario = null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}
}

