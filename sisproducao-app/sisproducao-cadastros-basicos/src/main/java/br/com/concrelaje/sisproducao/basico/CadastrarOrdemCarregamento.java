package br.com.concrelaje.sisproducao.basico;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView; 


import br.com.concrelaje.sisproducao.basico.control.util.DateFacility;
import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.FrotaService;
import br.com.concrelaje.sisproducao.basico.service.ParametroConfiguracaoService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.EnumModalidade;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOC;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.ItemOrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.ItemPedido;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamentoFrota;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.ParametroConfiguracao;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.OrdemCarregamentoWrapper;


@Controller
@RequestMapping(value = "/ordemCarregamento/ordemCarregamentoForm/**")
@SessionAttributes("ordemCarregamentoWrapper")
public class CadastrarOrdemCarregamento {
	@Autowired
	private ProducaoService producaoService; 
	
	@Autowired
	private FrotaService frotaService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private DocumentoAnexoService documentoAnexoService;
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private ParametroConfiguracaoService configuracaoService;
	

	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarOC(Model model) {
		OrdemCarregamentoWrapper ordemCarregamentoWrapper = new OrdemCarregamentoWrapper();
		OrdemCarregamento ordemCarregamento = new OrdemCarregamento();
		ordemCarregamento.setSituacao(EnumSituacaoOC.EM_ELABORACAO);
		ordemCarregamentoWrapper.setOrdemCarregamento(ordemCarregamento);
		ordemCarregamentoWrapper.setListaContrato(producaoService.buscarContratoParaOrdemCarregamento());
		ordemCarregamentoWrapper.setListaPedido(producaoService.buscarPedidoParaOrdemCarregamento());
		ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarTodasFrotas());
		ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
		ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
		model.addAttribute(ordemCarregamentoWrapper);
		return "producao/ordemCarregamento/OrdemCarregamentoForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			
			if(ordemCarregamentoWrapper.getOrdemCarregamento().getId() == null){
				ordemCarregamentoWrapper.getOrdemCarregamento().setDataCadastro(new Date());
				ordemCarregamentoWrapper.getOrdemCarregamento().setSituacao(EnumSituacaoOC.EM_ELABORACAO);
			}
			ordemCarregamentoWrapper.getOrdemCarregamento().setTipo(ordemCarregamentoWrapper.getTipo());
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.getOrdemCarregamento().setPedido(null);
			}else{
				ordemCarregamentoWrapper.getOrdemCarregamento().setContrato(null);
			}
			
			ordemCarregamentoWrapper.getOrdemCarregamento().setDataCarregamento(DateFacility.formataData(ordemCarregamentoWrapper.getDataCarregamentoString()));
			ordemCarregamentoWrapper.getOrdemCarregamento().setDataPrevSaida(DateFacility.formataData(ordemCarregamentoWrapper.getDataPrevistaSaidaString()));
			
			String arrayHorInicial[] = ordemCarregamentoWrapper.getHorarioInicial().split(":");
			String arrayHorFinal[] = ordemCarregamentoWrapper.getHorarioFinal().split(":");

			if (arrayHorInicial.length > 1) {
				
				if(new Integer(arrayHorInicial[0]) > 24 || new Integer(arrayHorInicial[1]) > 59){
					mv.addObject("error", "Horario inicial é invalido.");
					mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
					return mv;
				}
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(ordemCarregamentoWrapper.getOrdemCarregamento().getDataCadastro());
				cal.set(Calendar.HOUR_OF_DAY, new Integer(arrayHorInicial[0]));
				cal.set(Calendar.MINUTE, new Integer(arrayHorInicial[1]));
				cal.set(Calendar.SECOND, 0);
				ordemCarregamentoWrapper.getOrdemCarregamento().setDataInicio(cal.getTime());
			}

			if (arrayHorFinal.length > 1) {
				
				if(new Integer(arrayHorFinal[0]) > 24 || new Integer(arrayHorFinal[1]) > 59){
					mv.addObject("error", "Horario final é invalido.");
					mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
					return mv;
				}
				
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(ordemCarregamentoWrapper.getOrdemCarregamento().getDataCadastro());
				cal2.set(Calendar.HOUR_OF_DAY, new Integer(arrayHorFinal[0]));
				cal2.set(Calendar.MINUTE, new Integer(arrayHorFinal[1]));
				cal2.set(Calendar.SECOND, 0);
				ordemCarregamentoWrapper.getOrdemCarregamento().setDataFim(cal2.getTime());
			}
			
			
			OrdemCarregamento ordemCarregamento = producaoService.salvarOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			ordemCarregamentoWrapper.setOrdemCarregamento(ordemCarregamento);
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaContrato(new ArrayList<Contrato>());
				ordemCarregamentoWrapper.getListaContrato().add(ordemCarregamento.getContrato());
			}else{
				ordemCarregamentoWrapper.setListaPedido(new ArrayList<Pedido>());
				ordemCarregamentoWrapper.getListaPedido().add(ordemCarregamento.getPedido());
			}
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
			ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());

			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamento.getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamento.getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamento.getPedido().getId()));
			}
			mv.addObject("ordemCarregamentoWrapper", ordemCarregamentoWrapper);
			mv.addObject("success", "Ordem salva com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Nao foi possivel salvar o Ordem.");
			e.printStackTrace();
		}

		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}
	
	
	@RequestMapping(value = "/acoesFormPecas", method = RequestMethod.POST,  params={"incluirPeca"})
	public ModelAndView incluirPeca(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		try{
			
			ItemOrdemCarregamento item = ordemCarregamentoWrapper.getItemOrdemCarregamento();
			item.setDataCadastro(new Date());
			item.setOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.getItemOrdemCarregamento().setProduto(null);
			}else{
				ordemCarregamentoWrapper.getItemOrdemCarregamento().setItemContratoProjeto(null);
			}
			
			
			producaoService.salvarItemOrdemCarregamento(item);
			
			ordemCarregamentoWrapper.setItemOrdemCarregamento(new ItemOrdemCarregamento());
			
			ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
			ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			
			ordemCarregamentoWrapper.setListaCarregamento(producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoMaterial(producaoService.buscarItensOrdemCarregamentoMaterialPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));

			mv.addObject("ordemCarregamentoWrapper", ordemCarregamentoWrapper);
			
			mv.addObject("success", "Item adicionado com sucesso!");

		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel incluir uma pe�a.");
			e.printStackTrace();
		}

		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesFormPecas", method = RequestMethod.POST,  params={"incluirPecaPedido"})
	public ModelAndView incluirPecaPedido(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		try{
			
			ItemOrdemCarregamento item = ordemCarregamentoWrapper.getItemOrdemCarregamento();
			item.setDataCadastro(new Date());
			item.setOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.PEDIDO)){
				ItemPedido itemPedido = producaoService.buscarItemPedidoPorProdutoEPedido(ordemCarregamentoWrapper.getItemOrdemCarregamento().getProduto().getId(), ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId());
				item.setItemPedido(itemPedido);
				
			}
			ordemCarregamentoWrapper.getItemOrdemCarregamento().setItemContratoProjeto(null);
			
			producaoService.salvarItemOrdemCarregamento(item);
			
			ordemCarregamentoWrapper.setItemOrdemCarregamento(new ItemOrdemCarregamento());
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			ordemCarregamentoWrapper.setListaCarregamento(producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoProduto(producaoService.buscarItensOrdemCarregamentoProdutoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoMaterial(producaoService.buscarItensOrdemCarregamentoMaterialPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
    		mv.addObject("ordemCarregamentoWrapper", ordemCarregamentoWrapper);
			
			mv.addObject("success", "Item adicionado com sucesso!");

		}catch(Exception e){
			mv.addObject("error", "Nao foi possivel incluir uma peça.");
			e.printStackTrace();
		}

		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}
	
	
	
	@RequestMapping(value = "/acoesFormPecas", method = RequestMethod.POST,  params={"incluirMaterial"})
	public ModelAndView incluirMaterial(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		try{
			
			ItemOrdemCarregamento item = ordemCarregamentoWrapper.getItemOrdemCarregamento();
			item.setDataCadastro(new Date());
			item.setOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			
			//if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.PEDIDO)){
				//ItemPedido itemPedido = producaoService.buscarItemPedidoPorProdutoEPedido(ordemCarregamentoWrapper.getItemOrdemCarregamento().getProduto().getId(), ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId());
				//item.setItemPedido(itemPedido);
				
		//	}
			ordemCarregamentoWrapper.getItemOrdemCarregamento().setItemContratoProjeto(null);
			ordemCarregamentoWrapper.getItemOrdemCarregamento().setProduto(null);
		
			
			producaoService.salvarItemOrdemCarregamento(item);
			
			ordemCarregamentoWrapper.setItemOrdemCarregamento(new ItemOrdemCarregamento());
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			ordemCarregamentoWrapper.setListaCarregamento(producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoProduto(producaoService.buscarItensOrdemCarregamentoProdutoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoMaterial(producaoService.buscarItensOrdemCarregamentoMaterialPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
    		mv.addObject("ordemCarregamentoWrapper", ordemCarregamentoWrapper);
			
			mv.addObject("success", "Item adicionado com sucesso!");

		}catch(Exception e){
			mv.addObject("error", "Nao foi possivel incluir uma peça.");
			e.printStackTrace();
		}

		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}
	
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"carregar"})
	public ModelAndView carregar(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, BindingResult bindingResult) {
		ModelAndView mv = new ModelAndView();
		
		try {
			
			ordemCarregamentoWrapper.getOrdemCarregamento().setSituacao(EnumSituacaoOC.A_CARREGAR);
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.getOrdemCarregamento().setPedido(null);
			}else{
				ordemCarregamentoWrapper.getOrdemCarregamento().setContrato(null);
			}
			
			OrdemCarregamento ordemCarregamento = producaoService.salvarOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			
			
			ordemCarregamentoWrapper.setOrdemCarregamento(ordemCarregamento);
			
			mv.addObject("ordemCarregamentoWrapper", ordemCarregamentoWrapper);
			
			mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
			mv.addObject("success", "Ordem enviada para o carregamento com sucesso.");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao alterar ordem.");
		}
		
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"finalizar"})
	public ModelAndView finalizar(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, BindingResult bindingResult) {
		ModelAndView mv = new ModelAndView();
		
		try {
			
			
			// buscar as OPs dos Itens para marcar a situação da OP como expedida 
			
			for (ItemOrdemCarregamento item : ordemCarregamentoWrapper.getListaCarregamento()) {
				OrdemProducao ordem = producaoService.buscarOrdemProducaoPorIdPeca(item.getItemContratoProjeto().getId());
				
				if(ordem != null){
					
					List<OrdemProducaoFase> opFaseList = producaoService.buscarOrdemProducaoFasePorOp(ordem);
					
					for (OrdemProducaoFase ordemProducaoFase : opFaseList) {
						if(ordemProducaoFase.getFaseProducao().getId() == ordem.getFase().getId()){
							OrdemProducaoFase producaoFase = ordemProducaoFase;
							
							producaoFase.setDataFim(new Date());
							producaoFase.setDataInicio(ordemCarregamentoWrapper.getOrdemCarregamento().getDataCarregamento());
							Usuario usuario = usuarioService.buscarUsuarioPorLogin(SecurityContextHolder.getContext().getAuthentication().getName());
							producaoFase.setUsuario(usuario);
							producaoFase.setRevisao(item.getItemContratoProjeto().getProjeto().getRevisao());
							producaoService.salvarOrdemProducaoFase(producaoFase);
						}
					}
					
					ordem.setSituacao(EnumSituacaoOP.EXPEDIDA);
					producaoService.salvarOP(ordem);
					
				}
			}
			
			ordemCarregamentoWrapper.getOrdemCarregamento().setSituacao(EnumSituacaoOC.FINALIZADO);
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.getOrdemCarregamento().setPedido(null);
			}else{
				ordemCarregamentoWrapper.getOrdemCarregamento().setContrato(null);
			}
			
			OrdemCarregamento ordemCarregamento = producaoService.salvarOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			ordemCarregamentoWrapper.setOrdemCarregamento(ordemCarregamento);
			
			mv.addObject("ordemCarregamentoWrapper", ordemCarregamentoWrapper);
			
			mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
			mv.addObject("success", "Ordem finalizada com sucesso.");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao finalizar ordem.");
		}
		
		return mv;
	}
	
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST, params={"upload"})
	public ModelAndView upload(HttpServletRequest request, @ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, Model model) {

		ModelAndView mv = new ModelAndView();

		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile multipartFile = multipartRequest.getFile("file");

			DocumentoAnexo documento = new DocumentoAnexo();

			documento.setTipoDocumento(ordemCarregamentoWrapper.getDocumentoAnexo().getTipoDocumento());
			documento.setOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			documento.setDataCadastro(new Date());
			documento.setDescricao(ordemCarregamentoWrapper.getDocumentoAnexo().getDescricao());
			documento.setImagem(multipartFile.getBytes());
			documento.setFilename(multipartFile.getOriginalFilename());
			documento.setType(multipartFile.getContentType());

			documentoAnexoService.salvarDocumentoAnexo(documento);
			
			
			ordemCarregamentoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setDocumentoAnexo(new DocumentoAnexo());
			
			ordemCarregamentoWrapper.setListaContrato(new ArrayList<Contrato>());
			ordemCarregamentoWrapper.getListaContrato().add(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato());
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
			ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			mv.addObject("success", "Documento anexado com sucesso.");
			mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e){
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("id") String idDocumento, Model model) throws Exception {
		DocumentoAnexo documentoAnexo = documentoAnexoService.buscarPorId(new Long(idDocumento));
		response.setContentType(documentoAnexo.getType());
		response.setContentLength(documentoAnexo.getImagem().length);
		response.setHeader("Content-Disposition","attachment; filename=\"" + documentoAnexo.getFilename() +"\"");

		FileCopyUtils.copy(documentoAnexo.getImagem(), response.getOutputStream());

		return null;
	}
	
	@RequestMapping(value = "/excluirDocumento")
	public ModelAndView excluirDocumento(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, DocumentoAnexo documento,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(ordemCarregamentoWrapper.getListaDocumentoAnexo())){
				documentoAnexoService.excluirDocumento(documento);
				ordemCarregamentoWrapper.getListaDocumentoAnexo().remove(documento);
			}
			ordemCarregamentoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			ordemCarregamentoWrapper.setListaContrato(new ArrayList<Contrato>());
			ordemCarregamentoWrapper.getListaContrato().add(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato());
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
			ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			

			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			mv.addObject("success", "Documento excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Nao foi possivel excluir o documento.");
			e.printStackTrace();
		}
		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirVeiculo"})
	public ModelAndView incluirVeiculo(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		try{
			
			OrdemCarregamentoFrota ordemCarregamentoFrota = new OrdemCarregamentoFrota();
			ordemCarregamentoFrota.setFrota(ordemCarregamentoWrapper.getVeiculo());
			ordemCarregamentoFrota.setOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			
			producaoService.salvarOrdemCarregamentoFrota(ordemCarregamentoFrota);
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			ordemCarregamentoWrapper.setListaCarregamento(producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoProduto(producaoService.buscarItensOrdemCarregamentoProdutoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoMaterial(producaoService.buscarItensOrdemCarregamentoMaterialPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			ordemCarregamentoWrapper.setListaOrdemCarregamentoFrota(producaoService.buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			
    		mv.addObject("ordemCarregamentoWrapper", ordemCarregamentoWrapper);
			
			mv.addObject("success", "Ve�culo adicionado com sucesso!");

		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel incluir um ve�culo.");
			e.printStackTrace();
		}

		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}

	@RequestMapping(value = "/excluirVeiculo")
	public ModelAndView excluirVeiculo(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, OrdemCarregamentoFrota ordemCarregamentoFrota,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(ordemCarregamentoWrapper.getListaOrdemCarregamentoFrota())){
				ordemCarregamentoWrapper.getListaOrdemCarregamentoFrota().remove(ordemCarregamentoFrota);
				producaoService.excluirOrdemCarregamentoFrota(ordemCarregamentoFrota);
			}
			ordemCarregamentoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			ordemCarregamentoWrapper.setListaContrato(new ArrayList<Contrato>());
			ordemCarregamentoWrapper.getListaContrato().add(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato());
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
			ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			ordemCarregamentoWrapper.setListaOrdemCarregamentoFrota(producaoService.buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));

			mv.addObject("success", "Veiculo excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Nao foi possevel excluir o ve�culo.");
			e.printStackTrace();
		}
		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"gerarNotaFiscal"})
    public ModelAndView doSendEmail(HttpServletRequest request, @ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper) {
		
		ModelAndView mv = new ModelAndView();
		try{
			
				
			ParametroConfiguracao parametroConfiguracao = new ParametroConfiguracao();
			parametroConfiguracao = configuracaoService.buscarPorId(new Long(1));
	        
		
			
			if(parametroConfiguracao!= null && !SimpleValidate.isNullOrBlank(parametroConfiguracao.getSmtpHost())
					&& !SimpleValidate.isNullOrBlank(parametroConfiguracao.getSmtpPort())
					&& !SimpleValidate.isNullOrBlank(parametroConfiguracao.getSmtpUser())
					&& !SimpleValidate.isNullOrBlank(parametroConfiguracao.getSmtpPass()) 
					&& !SimpleValidate.isNullOrBlank(parametroConfiguracao.getRecipienteNF()) ){
				
			
				
				
				// takes input from e-mail form
		        String recipientAddress = parametroConfiguracao.getRecipienteNF();
		        String subject = "Gerar Nota Fiscal - Ordem Carregamento de número "+ ordemCarregamentoWrapper.getOrdemCarregamento().getId();
		        String msg = "Solicitação de geração de nota fiscal para a Ordem de Carregamento "+ ordemCarregamentoWrapper.getOrdemCarregamento().getId()+". " +
		        		"\n\n Acesso através do link " +
		        		parametroConfiguracao.getUrl()+ "/sisproducao/ordemCarregamento/ordemCarregamentoList/editar.html?id="+ ordemCarregamentoWrapper.getOrdemCarregamento().getId();
		         
		        Properties props = new Properties();  
		        props.put("mail.host", "concrelaje.com.br");   
		        //props.put("mail.smtp.host", "smtp2.locaweb.com.br");  
		        props.put("mail.smtp.host", parametroConfiguracao.getSmtpHost());  
		        props.put("mail.store.protocol", "pop3");  
		        props.put("mail.transport.protocol", "smtp");  
		        props.put("mail.smtp.auth", "true");  
		        props.put("mail.smtp.port", parametroConfiguracao.getSmtpPort());  //587  
		        final String user = parametroConfiguracao.getSmtpUser();
		        final String pass = parametroConfiguracao.getSmtpPass();
		        Authenticator auth = new Authenticator()  
	            {  
	               public PasswordAuthentication getPasswordAuthentication()  
	               {   
	                  return new PasswordAuthentication(user, pass);  
	               }  
	            };    
		        Session session = Session.getDefaultInstance(props,auth);         
		          
		        Message message = new MimeMessage(session);  
		          
		        try {  
		              
		            Address toAddress = new InternetAddress(recipientAddress);  
		            message.addRecipient(Message.RecipientType.TO, toAddress);  
		            InternetAddress from = new InternetAddress(user);
		            message.setFrom(from);
		            
		            message.setSubject(subject);  
		            message.setText(msg);
		            message.saveChanges(); // implicit with send()  
		              
		            Transport transport = session.getTransport("smtp");  
		            transport.connect();    //da o erro aqui  
		            transport.sendMessage(message, message.getAllRecipients());  
		            transport.close();  
		              
		            mv.addObject("success", "E-mail enviado com sucesso com a solicitação e geração de nota fiscal!");
		        } catch (AddressException e) {  
		            // TODO Auto-generated catch block  
		            e.printStackTrace();  
		            mv.addObject("error", "E-mail não foi enviado.");
		        }catch (MessagingException e) {   
		                System.out.println("Email nao pode ser enviado! " + e.getMessage());  
		                e.printStackTrace();  
		                mv.addObject("error", "E-mail não foi enviado.");
		        }    
		       
		       
		        
		        
			}
			else{
				mv.addObject("error", "E-mail não foi enviado devido a falta de parâmetros de configuração!");
			}
			
	        
	        // altera situação 
	        
	        ordemCarregamentoWrapper.getOrdemCarregamento().setSituacao(EnumSituacaoOC.GERAR_NOTA);
	        
	        if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.getOrdemCarregamento().setPedido(null);
			}else{
				ordemCarregamentoWrapper.getOrdemCarregamento().setContrato(null);
			}
			
			OrdemCarregamento ordemCarregamento = producaoService.salvarOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento());
			ordemCarregamentoWrapper.setOrdemCarregamento(ordemCarregamento);
			
			
	        
		}catch(Exception e){
			e.printStackTrace(); 
			mv.addObject("error", "Erro ao executar a ação de Enviar para nota fiscal.");
		}
         
		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
    }
	
	
	@RequestMapping(value = "/excluirPeca")
	public ModelAndView excluirPeca(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, ItemOrdemCarregamento itemOrdemCarregamento,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(ordemCarregamentoWrapper.getListaCarregamento())){
				producaoService.excluirItemOrdemCarregamento(itemOrdemCarregamento);
				ordemCarregamentoWrapper.setListaCarregamento(producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
				
			}
			
			if(!SimpleValidate.isNullOrEmpty(ordemCarregamentoWrapper.getListaCarregamentoProduto())){
				producaoService.excluirItemOrdemCarregamento(itemOrdemCarregamento);
				ordemCarregamentoWrapper.setListaCarregamentoProduto(producaoService.buscarItensOrdemCarregamentoProdutoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
				
			}
			ordemCarregamentoWrapper.setListaCarregamentoMaterial(producaoService.buscarItensOrdemCarregamentoMaterialPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			ordemCarregamentoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			ordemCarregamentoWrapper.setListaContrato(new ArrayList<Contrato>());
			ordemCarregamentoWrapper.getListaContrato().add(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato());
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
			ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			ordemCarregamentoWrapper.setListaOrdemCarregamentoFrota(producaoService.buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));

			mv.addObject("success", "Peça excluida com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir a peça.");
			e.printStackTrace();
		}
		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesFormPecas", method = RequestMethod.POST)
	public ModelAndView excluirPecasChecadas(@ModelAttribute("ordemCarregamentoWrapper") OrdemCarregamentoWrapper ordemCarregamentoWrapper, @ModelAttribute("checados") String checados, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
		
			String s[] = checados.split("@");  
			
			if(s.length >1){
				
				for(int i = 1; i< s.length; i++){
					ItemOrdemCarregamento itemOrdemCarregamento = new ItemOrdemCarregamento();
					itemOrdemCarregamento = producaoService.buscarItemOrdemCarregamentoPorId(new Long(s[i]));
					producaoService.excluirItemOrdemCarregamento(itemOrdemCarregamento);
				}
			}
			ordemCarregamentoWrapper.setListaCarregamento(producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoProduto(producaoService.buscarItensOrdemCarregamentoProdutoPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaCarregamentoMaterial(producaoService.buscarItensOrdemCarregamentoMaterialPorOC(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			ordemCarregamentoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			ordemCarregamentoWrapper.setListaContrato(new ArrayList<Contrato>());
			ordemCarregamentoWrapper.getListaContrato().add(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato());
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
			ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			
			
			if(ordemCarregamentoWrapper.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamentoWrapper.getOrdemCarregamento().getPedido().getId()));
			}
			
			ordemCarregamentoWrapper.setListaOrdemCarregamentoFrota(producaoService.buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));

			mv.addObject("success", "Peças excluidas com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir as peças.");
			e.printStackTrace();
		}
		mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoForm");
		return mv;
	}
	
	
	public EnumModalidade[] getListaModalidade() {
		return EnumModalidade.values();
	}

}
