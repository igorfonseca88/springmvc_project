package br.com.concrelaje.sisproducao.basico;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Cliente;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoContrato;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.wrapper.ContratoWrapper;

@Controller
@RequestMapping(value = "/contrato/contratoForm/**")
@SessionAttributes("contratoWrapper")
public class CadastrarContrato { 
	@Autowired
	private ProducaoService producaoService;

	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private DocumentoAnexoService documentoAnexoService;
	
	@Autowired
	private EstoqueService estoqueService;
	



	@RequestMapping(value = "/init", method = RequestMethod.GET)
	public String cadastrarContrato(Model model) {
		model.addAttribute(new ContratoWrapper());
		model.addAttribute("listaClientes", clienteService.buscarTodosClientes());
		return "producao/contrato/ContratoForm";
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarContrato(	@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper,	@ModelAttribute("idCliente") String idCliente,	BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try {
			
			/*if(contratoWrapper.getCliente().getId() == null){
				mv.addObject("alert", "O cliente é um campo obrigatório.");
				mv.setViewName("producao/contrato/ContratoForm");
				return mv;
			}
			
			if(contratoWrapper.getContrato().getObjeto() == null){
				mv.addObject("alert", "O objeto é um campo obrigatório.");
				mv.setViewName("producao/contrato/ContratoForm");
				return mv;
			}*/
			
			if(SimpleValidate.isNullOrBlank(contratoWrapper.getContrato().getNomeObra())){
				mv.addObject("alert", "O nome da obra é um campo obrigatório.");
				mv.setViewName("producao/contrato/ContratoForm");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(contratoWrapper.getContrato().getNumero())) {
				mv.addObject("alert", "O n�mero do contrato � um campo obrigat�rio");
				mv.setViewName("producao/contrato/ContratoForm");
				return mv;
			}
			
			
			
			if(contratoWrapper.getContrato().getId() == null){
				contratoWrapper.getContrato().setDataCadastro(new Date());
				contratoWrapper.getContrato().setSituacao(EnumSituacaoContrato.EM_ELABORACAO);
			}
			
			Cliente cliente = new Cliente();
			if(contratoWrapper.getCliente() != null && contratoWrapper.getCliente().getId() != null ){
				cliente = clienteService.buscarPorId(contratoWrapper.getCliente().getId());
				contratoWrapper.getContrato().setCliente(cliente);
			}
			

			Contrato contrato = producaoService.salvarContrato(contratoWrapper.getContrato());
			
			contratoWrapper.setContrato(contrato);

			
			mv.addObject("success", "Contrato cadastrado com sucesso.");
			contratoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			mv.addObject(contratoWrapper);
			mv.addObject("listaClientes", clienteService.buscarTodosClientes());
			

		} catch (Exception e) {
			mv.addObject("error", "N�o foi poss�vel salvar o contrato.");
			e.printStackTrace();
		}

		mv.setViewName("producao/contrato/ContratoForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirProduto"})
	public ModelAndView incluirProduto(@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try {
			
			Produto produto = producaoService.buscarProdutoPorId(contratoWrapper.getProduto().getId());

			ItemContrato item = new ItemContrato();
			item.setContrato(contratoWrapper.getContrato());
			item.setDataCadastro(new Date());
			item.setProduto(produto);
			item.setQuantidade(contratoWrapper.getItemContrato().getQuantidade());
			producaoService.salvarItemContrato(item);


			contratoWrapper.setListaItemContratoProduto(producaoService.buscarItensContratoProdutoPorIdContrato(contratoWrapper.getContrato().getId()));
			mv.addObject("listaClientes", clienteService.buscarTodosClientes());
			
			mv.addObject("sucesso", "Produto incluído com sucesso.");
			

		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao incluir produto.");
		}
		
		mv.setViewName("producao/contrato/ContratoForm");
		return mv;
	}

	
	@RequestMapping(method = RequestMethod.GET)
	public String selecionarCliente(@ModelAttribute("id") String idCliente,	@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, ModelMap modelMap) {

		Cliente cliente = clienteService.buscarPorId(new Long(idCliente));
		
		contratoWrapper.setCliente(cliente);
		modelMap.addAttribute("listaClientes", clienteService.buscarTodosClientes());
		modelMap.addAttribute(contratoWrapper);
		return "producao/contrato/ContratoForm";
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST, params={"upload"})
	public String upload(HttpServletRequest request, @ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, Model model) {
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile multipartFile = multipartRequest.getFile("file");
		
		
		try {
			
			DocumentoAnexo documento = new DocumentoAnexo();
			
			documento.setContrato(contratoWrapper.getContrato());
			documento.setDataCadastro(new Date());
			documento.setDescricao(contratoWrapper.getDocumentoAnexo().getDescricao());
			documento.setImagem(multipartFile.getBytes());
			documento.setFilename(multipartFile.getOriginalFilename());
			documento.setType(multipartFile.getContentType());
			documento.setTipoDocumento(contratoWrapper.getDocumentoAnexo().getTipoDocumento());
			
			documentoAnexoService.salvarDocumentoAnexo(documento);
				
			Contrato contrato = producaoService.buscarContratoPorId(contratoWrapper.getContrato().getId());
			contratoWrapper.setContrato(contrato);
			contratoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdContrato(contratoWrapper.getContrato().getId()));
			contratoWrapper.setDocumentoAnexo(null);
			model.addAttribute("listaClientes", clienteService.buscarTodosClientes());
			model.addAttribute("contratoWrapper", contratoWrapper);
			model.addAttribute("success", "Documento incluído com sucesso.");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			model.addAttribute("error", "Erro ao cadastro documento.");
		}catch (IOException e){
			e.printStackTrace();
			model.addAttribute("error", "Erro ao cadastro documento.");
		}
		
		
		return "producao/contrato/ContratoForm";
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("id") String idDocumento, Model model) throws Exception {
			 DocumentoAnexo documentoAnexo = documentoAnexoService.buscarPorId(new Long(idDocumento));
			 response.setContentType(documentoAnexo.getType());
			 response.setContentLength(documentoAnexo.getImagem().length);
			 response.setHeader("Content-Disposition","attachment; filename=\"" + documentoAnexo.getFilename() +"\"");
			
			FileCopyUtils.copy(documentoAnexo.getImagem(), response.getOutputStream());
			
			return null;
    }
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"finalizar"})
	public ModelAndView finalizar(@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, BindingResult bindingResult) {
		ModelAndView mv = new ModelAndView();
		
		try {
			
			if(SimpleValidate.isNullOrEmpty(producaoService.buscarItensContratoProdutoPorIdContrato(contratoWrapper.getContrato().getId()))){
				mv.addObject("alert", "Para finalizar o contrato é necessário que exista pelo menos um produto adicionado.");
				mv.setViewName("producao/contrato/ContratoForm");
				return mv;
			}
			
			if(contratoWrapper.getContrato().getObjeto() == null){
				mv.addObject("alert", "O objeto é um campo obrigatório.");
				mv.setViewName("producao/contrato/ContratoForm");
				return mv;
			}
			
			contratoWrapper.getContrato().setSituacao(EnumSituacaoContrato.FINALIZADO);
			producaoService.salvarContrato(contratoWrapper.getContrato());
			
			Contrato contrato = producaoService.buscarContratoPorId(contratoWrapper.getContrato().getId());
			contratoWrapper.setContrato(contrato);
			mv.setViewName("producao/contrato/ContratoForm");
			mv.addObject("success", "Contrado finalizado com sucesso.");
			mv.addObject("contratoWrapper", contratoWrapper);
			
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao finalizar contrato.");
		}
		
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"reabrir"})
	public ModelAndView reabrir(@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, BindingResult bindingResult) {
		ModelAndView mv = new ModelAndView();
		
		try {
			contratoWrapper.getContrato().setSituacao(EnumSituacaoContrato.REABERTO);
			producaoService.salvarContrato(contratoWrapper.getContrato());
			
			Contrato contrato = producaoService.buscarContratoPorId(contratoWrapper.getContrato().getId());
			contratoWrapper.setContrato(contrato);
			contratoWrapper.setListaItemContratoProduto(producaoService.buscarItensContratoProdutoPorIdContrato(contratoWrapper.getContrato().getId()));
			contratoWrapper.setListaItemContratoMaterial(producaoService.buscarItensContratoMaterialPorIdContrato(contratoWrapper.getContrato().getId()));
			contratoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdContrato(contratoWrapper.getContrato().getId()));
			contratoWrapper.setCliente(contrato.getCliente());
			mv.addObject("contratoWrapper", contratoWrapper);
			mv.addObject("listaClientes", clienteService.buscarTodosClientes());
			mv.setViewName("producao/contrato/ContratoForm");
			mv.addObject("sucesso", "Contrado reaberto com sucesso.");
			
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao reabrir contrato.");
		}
		
		
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"pesquisarCliente"})
    public ModelAndView pesquisarCliente(@RequestParam("cliente") String cliente,ModelMap modelMap) {
		ModelAndView mv = new ModelAndView();
    	modelMap.addAttribute("listaClientes", clienteService.buscarClientePorNome(cliente));
    	modelMap.addAttribute("openModal", "sim");
    	mv.setViewName("producao/contrato/ContratoForm");
    	return mv;
    }
	
	@RequestMapping(value = "/excluirProduto")
	public ModelAndView excluirProduto(@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, ItemContrato itemContrato,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(contratoWrapper.getListaItemContratoProduto())){
				producaoService.excluirItemContrato(itemContrato);
				contratoWrapper.getListaItemContratoProduto().remove(itemContrato);
			}
			contratoWrapper.setListaItemContratoProduto(producaoService.buscarItensContratoProdutoPorIdContrato(contratoWrapper.getContrato().getId()));

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("producao/contrato/ContratoForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirDocumento")
	public ModelAndView excluirDocumento(@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, DocumentoAnexo documento,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(contratoWrapper.getListaDocumentoAnexo())){
				documentoAnexoService.excluirDocumento(documento);
				contratoWrapper.getListaDocumentoAnexo().remove(documento);
			}
			contratoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdContrato(contratoWrapper.getContrato().getId()));

			mv.addObject("success", "Documento excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o documento.");
			e.printStackTrace();
		}
		mv.setViewName("producao/contrato/ContratoForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirMaterial")
	public ModelAndView excluirMaterial(@ModelAttribute("contratoWrapper") ContratoWrapper contratoWrapper, ItemContrato itemContrato,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(contratoWrapper.getListaItemContratoMaterial())){
				producaoService.excluirItemContrato(itemContrato);
				contratoWrapper.getListaItemContratoMaterial().remove(itemContrato);
			}
			contratoWrapper.setListaItemContratoMaterial(producaoService.buscarItensContratoMaterialPorIdContrato(contratoWrapper.getContrato().getId()));

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Nao foi possivel excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("producao/contrato/ContratoForm");
		return mv;
	}
}
