package br.com.concrelaje.sisproducao.basico;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.Material;


@Controller
@RequestMapping(value = "modal/adicionarMaterial/**")
@SessionAttributes("contrato") 
public class AdicionarMateriais {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private EstoqueService estoqueService;
	
	
	@Autowired
	private ClienteService clienteService;
	
	 @Autowired
     private MessageSource messageSource;
	 

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String pesquisar(@ModelAttribute("idContrato") String idContrato, ModelMap modelMap) {
    	Contrato contrato = producaoService.buscarContratoPorId(new Long(idContrato));
    	modelMap.addAttribute("contrato", contrato);
    	modelMap.addAttribute("listaMateriais", estoqueService.buscarMateriaisDisponiveisPorIdContratoENumero(new Long(idContrato), null));
    	return "producao/modal/AdicionarMateriais";
    }
    
    @RequestMapping(value="/filtrar", method = RequestMethod.POST)
    public String filtrar(@RequestParam("numero") String numero, @ModelAttribute("contrato") Contrato contrato,  ModelMap modelMap) {
    	modelMap.addAttribute("listaMateriais", estoqueService.buscarMateriaisDisponiveisPorIdContratoENumero(contrato.getId(), numero));
    	return "producao/modal/AdicionarMateriais";
    }
    
    
    @RequestMapping(value = "/incluirMaterial", method = RequestMethod.POST)
	public ModelAndView incluirMaterial(@ModelAttribute("contrato") Contrato contrato, @ModelAttribute("checados") String checados, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try {
			
			String s[] = checados.split("@");  
			
			if(s.length >1){
				
				for(int i = 1; i< s.length; i++){
					String arrMat[] = s[i].split("#");
					Material material = estoqueService.buscarMaterialPorId((new Long(arrMat[0])));
					
					ItemContrato item = new ItemContrato();
					item.setContrato(contrato);
					item.setDataCadastro(new Date());
					item.setMaterial(material);
					item.setQuantidade(new Integer(arrMat[1]));

					producaoService.salvarItemContrato(item);
					
					//estoqueService.movimentarEstoque(item, material);
					
					
				}
				
			}
		
			mv.setViewName("producao/modal/AdicionarMateriais");
			mv.addObject("sucesso", "Material incluído com sucesso.");

		} catch (Exception e) {
			mv.addObject("error", "Erro ao incluir material.");
		//	e.printStackTrace();
			
		}
		 
		mv.addObject("listaMateriais", estoqueService.buscarMateriaisDisponiveisPorIdContratoENumero(contrato.getId(), null));
		
		return mv;
	}
       
    
}
