package br.com.concrelaje.sisproducao.basico;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.ItemPedido;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Pedido;


@Controller
@RequestMapping(value = "modal/adicionarMaterialPedido/**")
@SessionAttributes("pedido") 
public class AdicionarMateriaisPedido {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private EstoqueService estoqueService;
	
	
	@Autowired
	private ClienteService clienteService;
	
	 @Autowired
     private MessageSource messageSource;
	 

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String pesquisar(@ModelAttribute("idPedido") String idPedido, ModelMap modelMap) {
    	Pedido pedido = producaoService.buscarPedidoPorId(new Long(idPedido));
    	modelMap.addAttribute("pedido", pedido);
    	modelMap.addAttribute("listaMateriais", estoqueService.buscarMateriaisDisponiveisPorIdPedidoENumero(new Long(idPedido), null));
    	return "producao/modal/AdicionarMateriaisPedido";
    }
    
    @RequestMapping(value="/filtrar", method = RequestMethod.POST)
    public String filtrar(@RequestParam("numero") String numero, @ModelAttribute("pedido") Pedido pedido,  ModelMap modelMap) {
    	modelMap.addAttribute("listaMateriais", estoqueService.buscarMateriaisDisponiveisPorIdPedidoENumero(pedido.getId(), numero));
    	return "producao/modal/AdicionarMateriaisPedido";
    }
    
    
    @RequestMapping(value = "/incluirMaterial", method = RequestMethod.POST)
	public ModelAndView incluirMaterial(@ModelAttribute("pedido") Pedido pedido, @ModelAttribute("checados") String checados, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try {
			
			String s[] = checados.split("@");  
			
			if(s.length >1){
				
				for(int i = 1; i< s.length; i++){
					String arrMat[] = s[i].split("#");
					Material material = estoqueService.buscarMaterialPorId((new Long(arrMat[0])));
					
					ItemPedido item = new ItemPedido();
					item.setPedido(pedido);
					item.setDataCadastro(new Date());
					item.setMaterial(material);
					item.setQuantidade(new Integer(arrMat[1]));

					producaoService.salvarItemPedido(item);
					
					//estoqueService.movimentarEstoque(item, material);
					
					
				}
				
			}
		
			mv.setViewName("producao/modal/AdicionarMateriaisPedido");
			mv.addObject("sucesso", "Material incluído com sucesso.");

		} catch (Exception e) {
			mv.addObject("error", "Erro ao incluir material.");
		//	e.printStackTrace();
			
		}
		 
		mv.addObject("listaMateriais", estoqueService.buscarMateriaisDisponiveisPorIdPedidoENumero(pedido.getId(), null));
		
		return mv;
	}
       
    
}
