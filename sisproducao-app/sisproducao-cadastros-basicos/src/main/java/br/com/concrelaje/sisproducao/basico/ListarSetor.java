package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.entidades.Setor;

@Controller
@RequestMapping(value = "/basico/setorList")
public class ListarSetor {
    
	@Autowired
	private EstoqueService estoqueService;

	
    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarSetores(ModelMap modelMap) {
        modelMap.addAttribute("listaSetor", estoqueService.buscarTodosSetores());
        return "basico/SetorList";
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(Setor setor, ModelMap modelMap) {
    	try{
    		 setor = estoqueService.buscarSetorPorId(setor.getId());
    		modelMap.addAttribute("setor", setor);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar local de armazenamento.");
    	}
    	return "basico/SetorForm";
    }
    
    @RequestMapping(value="/excluir")
    public String excluir(Setor setor, ModelMap modelMap) {
    	try{
    		
    		setor = estoqueService.buscarSetorPorId(setor.getId());
    		estoqueService.excluirSetor(setor);
    		
    		modelMap.addAttribute("success", "Local de armazenamento excluido com sucesso!");
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Nao foi possivel excluir local de armazenamento!");
    	}
    	
    	return listarSetores(modelMap);
    }
}
