package br.com.concrelaje.sisproducao.basico;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ItemContratoProjetoService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.ProjetoService;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoSimNao;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoProjeto;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MaterialProj;
import br.com.concrelaje.sisproducao.entidades.MaterialProjeto;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.ProdutoProjeto;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.Traco;
import br.com.concrelaje.sisproducao.entidades.wrapper.ProjetoWrapper;

@Controller
@RequestMapping(value = "/projeto/complementarProjetoForm")
@SessionAttributes("projetoWrapper") 
public class ComplementarProjeto {  

	@Autowired
	private ProjetoService projetoService;

	@Autowired
	private ProducaoService producaoService;

	@Autowired
	private DocumentoAnexoService documentoAnexoService;
	
	@Autowired
	private EstoqueService estoqueService; 
	
	@Autowired
	private ItemContratoProjetoService itemContratoProjetoService;


	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarProjeto(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, @ModelAttribute("revisao")  String revisao,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		if(projetoWrapper.getProjeto().getTraco().getId() == null ){
			projetoWrapper.getProjeto().setTraco(null);
		}
		if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
			projetoWrapper.getProjeto().setMotivoReprovacao(null);
		}
		
		
		projetoWrapper.getProjeto().setRevisao( Integer.parseInt(revisao));
		projetoWrapper.setProjeto(projetoService.salvarProjeto(projetoWrapper.getProjeto()));
		projetoWrapper.setProjetoView( projetoService.buscarProjetoViewPorIdProjeto(projetoWrapper.getProjeto().getId()));

		mv.addObject("listaTracos", producaoService.buscarTodosTracosAtivos());
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("success", "Projeto salvo com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"enviar"})
	public ModelAndView enviarProjeto(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		if(projetoWrapper.getProjeto().getTraco().getId() == null ){
			projetoWrapper.getProjeto().setTraco(null);
		}
		
		if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
			projetoWrapper.getProjeto().setMotivoReprovacao(null);
		}
		
		boolean docTipoProjeto = false;
		for(DocumentoAnexo documentoAnexo : projetoWrapper.getListaDocumentoAnexo()){
			if(documentoAnexo.getTipoDocumento().getDescricao().equalsIgnoreCase("PROJETO")){
				docTipoProjeto = true;
				break;
			}
		}
		
		if(!docTipoProjeto){
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("error", "Projeto nao pode ser enviado pois, o mesmo nao contem anexo do tipo Projeto.");
			mv.setViewName("producao/projeto/ComplementarProjetoForm");
			return mv;
		}
		
		
		projetoWrapper.getProjeto().setSituacao(EnumSituacaoProjeto.AGUARDANDO_APROVACAO);
		projetoService.salvarProjeto(projetoWrapper.getProjeto());
		projetoWrapper.setHabilitarAprovarReprovar(true);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("success", "Projeto enviado com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"aprovar"})
	public ModelAndView aprovarProjeto(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
			projetoWrapper.getProjeto().setMotivoReprovacao(null);
		}
		
		if(projetoWrapper.getProjeto().getTraco().getId() == null){
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("error", "Para efetuar uma aprovação é necessário informar um traço.");
			mv.setViewName("producao/projeto/ComplementarProjetoForm");
			return mv;
		}
		
		
		
		projetoWrapper.getProjeto().setSituacao(EnumSituacaoProjeto.APROVADO);
		projetoService.salvarProjeto(projetoWrapper.getProjeto());
		projetoWrapper.setHabilitarAprovarReprovar(false);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("success", "Projeto enviado com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"okReprovar"})
	public ModelAndView confirmarReprovacaoProjeto(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("error", "Para efetuar uma reprovacao e necessario informar o campo motivo.");
			mv.setViewName("producao/projeto/ComplementarProjetoForm");
			return mv;
		}
		
		if(projetoWrapper.getProjeto().getTraco().getId() == null ){
			projetoWrapper.getProjeto().setTraco(null);
		}

		projetoWrapper.getProjeto().setSituacao(EnumSituacaoProjeto.REPROVADO);
		projetoService.salvarProjeto(projetoWrapper.getProjeto());
		projetoWrapper.setHabilitarAprovarReprovar(false);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("success", "Projeto enviado com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"cancelarReprovacaoProjeto"})
	public ModelAndView cancelarReprovacaoProjeto(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		if(projetoWrapper.getProjeto().getTraco().getId() == null ){
			projetoWrapper.getProjeto().setTraco(null);
		}

		projetoWrapper.getProjeto().setSituacao(EnumSituacaoProjeto.AGUARDANDO_APROVACAO);
		projetoWrapper.getProjeto().setMotivoReprovacao(null);
		projetoService.salvarProjeto(projetoWrapper.getProjeto());
		projetoWrapper.setHabilitarAprovarReprovar(true);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("success", "Reprovação cancelada com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	
	@RequestMapping(value = "/cancelarProducao", method = RequestMethod.POST)
	public ModelAndView cancelarProducao(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, @ModelAttribute("checados") String checados, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
		
			String s[] = checados.split("@");  
			
			if(s.length >1){
				
				for(int i = 1; i< s.length; i++){
					ItemContratoProjeto itemContratoProjeto = new ItemContratoProjeto();
					itemContratoProjeto = itemContratoProjetoService.buscarPorId(new Long(s[i]));
					
					if(itemContratoProjeto.getId() != null){
						itemContratoProjeto.setSituacao(EnumSituacaoItemContratoProjeto.CANCELADO);
						itemContratoProjetoService.salvarItemContratoProjeto(itemContratoProjeto);
						
	
						OrdemProducao ordemProducao = new OrdemProducao();
						if(producaoService.buscarOrdemProducaoPorIdPeca(itemContratoProjeto.getId())!= null){
							ordemProducao = producaoService.buscarOrdemProducaoPorIdPeca(itemContratoProjeto.getId()); 
						}
						
						if(ordemProducao.getId() != null){
							ordemProducao.setSituacao(EnumSituacaoOP.CANCELADO);
							producaoService.salvarOP(ordemProducao);
						}
					}
					
					
					
				}
				
			}
			
			
			
			mv.addObject("success", "Peça cancelada com sucesso do projeto. A quantidade foi devolvida ao contrato e poderá ser sequenciada novamente.");
			
		
		}catch(Exception e){
			mv.addObject("error", "Erro ao cancelar peça.");
			e.printStackTrace();
		}
		
		projetoWrapper.getProjeto().setMotivoReprovacao(null);
		projetoWrapper.setHabilitarAprovarReprovar(true);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		projetoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(projetoWrapper.getProjeto().getId()));
		projetoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
		projetoWrapper.setListaItensContratoProjeto(producaoService.buscarItensContratoProjetoComProdutoCarregadoPorIdProjeto(projetoWrapper.getProjeto().getId()));
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirAcessorio"})
	public ModelAndView incluirAcessorio(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		ProdutoProjeto produtoProjeto = null;

		if(projetoWrapper.getProdutoProjeto().getProduto().getId() == null){
			projetoWrapper.setProdutoProjeto(new ProdutoProjeto());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("alert", "Deve ser selecionado um acessório antes de incluir.");
			mv.setViewName("producao/projeto/ComplementarProjetoForm");
			return mv;
		}

		if(projetoWrapper.getProdutoProjeto().getQuantidade() == null || projetoWrapper.getProdutoProjeto().getQuantidade() <= 0){
			projetoWrapper.setProdutoProjeto(new ProdutoProjeto());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.setViewName("producao/projeto/ComplementarProjetoForm");
			mv.addObject("alert", "Quantidade é um campo obrigatório.");
			return mv;
		}
		for (ProdutoProjeto acessorio : projetoWrapper.getProjeto().getAcessorios()) {
			if(acessorio.getProduto().getId() == projetoWrapper.getProdutoProjeto().getProduto().getId()){
				Integer aumentarQuantidade = acessorio.getQuantidade() + projetoWrapper.getProdutoProjeto().getQuantidade();
				acessorio.setQuantidade(aumentarQuantidade);
				produtoProjeto =  producaoService.salvarProdutoProjeto(acessorio);
				break;
			}
		} 

		if(produtoProjeto == null){
			projetoWrapper.getProdutoProjeto().setProjeto(projetoWrapper.getProjeto());
			produtoProjeto =  producaoService.salvarProdutoProjeto(projetoWrapper.getProdutoProjeto());
			projetoWrapper.getProjeto().getAcessorios().add(produtoProjeto);
		}
		
		if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
			projetoWrapper.getProjeto().setMotivoReprovacao(null);
		}

		projetoWrapper.setProdutoProjeto(new ProdutoProjeto());
		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("success", "Acessório adicionado com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirMaterialAco"})
	public ModelAndView incluirMaterialAco(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		MaterialProjeto materialProjeto = null;

		if(projetoWrapper.getMaterialProjeto().getMaterial().getId() == null){
			projetoWrapper.setMaterialProjeto(new MaterialProjeto());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("listaMateriais", getListaMateriais());
			mv.addObject("alert", "Deve ser selecionado um aço antes de incluir.");
			mv.setViewName("producao/projeto/ComplementarProjetoForm");
			return mv;
		}

		projetoWrapper.getMaterialProjeto().setComprimento(new BigDecimal(projetoWrapper.getMaterialProjeto().getComprimentoString().replace(",", ".")));
		projetoWrapper.getMaterialProjeto().setPeso(new BigDecimal(projetoWrapper.getMaterialProjeto().getPesoString().replace(",", ".")));
		
		if(projetoWrapper.getMaterialProjeto().getComprimento() == null || projetoWrapper.getMaterialProjeto().getComprimento().compareTo(BigDecimal.ZERO)<0){
			projetoWrapper.setMaterialProjeto(new MaterialProjeto());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("listaMateriais", getListaMateriais());
			mv.addObject("alert", "Comprimento é um campo obrigatório.");
			return mv;
		}
		
		if(projetoWrapper.getMaterialProjeto().getPeso() == null || projetoWrapper.getMaterialProjeto().getPeso().compareTo(BigDecimal.ZERO)<0){
			projetoWrapper.setMaterialProjeto(new MaterialProjeto());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("listaMateriais", getListaMateriais());
			mv.addObject("alert", "Peso é um campo obrigatório.");
			return mv;
		}
		
		for (MaterialProjeto materialAco : projetoWrapper.getProjeto().getMateriais()) {
			if(materialAco.getMaterial().getId() == projetoWrapper.getMaterialProjeto().getMaterial().getId()){
				materialAco.setComprimento(materialAco.getComprimento().add(projetoWrapper.getMaterialProjeto().getComprimento()));
				materialAco.setPeso(materialAco.getPeso().add(projetoWrapper.getMaterialProjeto().getPeso()));
				materialProjeto =  producaoService.salvarMaterialProjeto(materialAco);
				break;
			}
		} 

		if(materialProjeto == null){
			projetoWrapper.getMaterialProjeto().setProjeto(projetoWrapper.getProjeto());
			materialProjeto =  producaoService.salvarMaterialProjeto(projetoWrapper.getMaterialProjeto());
			projetoWrapper.getProjeto().getMateriais().add(materialProjeto);
		}
		
		if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
			projetoWrapper.getProjeto().setMotivoReprovacao(null);
		}

		projetoWrapper.setMaterialProjeto(new MaterialProjeto());
		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("listaMateriais", getListaMateriais());
		mv.addObject("success", "Aço adicionado com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	
	
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirMaterial"})
	public ModelAndView incluirMaterial(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		MaterialProj materialProj = null;

		if(projetoWrapper.getMaterialProj().getMaterial().getId() == null){
			projetoWrapper.setMaterialProj(new MaterialProj());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("listaMateriais", getListaMateriais());
			mv.addObject("alert", "Deve ser selecionado um material antes de incluir.");
			mv.setViewName("producao/projeto/ComplementarProjetoForm");
			return mv;
		}

		projetoWrapper.getMaterialProj().setQuantidade(new BigDecimal(projetoWrapper.getMaterialProj().getQuantidadeString().replace(",", ".")));
		
		if(projetoWrapper.getMaterialProj().getQuantidade() == null || projetoWrapper.getMaterialProj().getQuantidade().compareTo(BigDecimal.ZERO)<0){
			projetoWrapper.setMaterialProjeto(new MaterialProjeto());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("listaMateriais", getListaMateriais());
			mv.addObject("alert", "Quantidade é um campo obrigatorio.");
			return mv;
		}
		
				
		for (MaterialProj material : projetoWrapper.getProjeto().getMateriaisProjeto()) {
			if(material.getMaterial().getId() == projetoWrapper.getMaterialProj().getMaterial().getId()){
				material.setQuantidade(material.getQuantidade().add(projetoWrapper.getMaterialProj().getQuantidade()));
				materialProj =  producaoService.salvarMaterialProj(material);
				break;
			}
		} 

		if(materialProj == null){
			projetoWrapper.getMaterialProj().setProjeto(projetoWrapper.getProjeto());
			materialProj =  producaoService.salvarMaterialProj(projetoWrapper.getMaterialProj());
			projetoWrapper.getProjeto().getMateriaisProjeto().add(materialProj);
		}
		
		if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
			projetoWrapper.getProjeto().setMotivoReprovacao(null);
		}

		projetoWrapper.setMaterialProj(new MaterialProj());
		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("success", "Material adicionado com sucesso.");
		mv.addObject("listaMateriais", getListaMateriais());
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	


	@RequestMapping(value = "/excluirAcessorio", method = RequestMethod.GET)
	public ModelAndView excluirAcessorio(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, ProdutoProjeto produtoProjeto,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		produtoProjeto = producaoService.buscarProdutoProjetoPorId(produtoProjeto.getId());
		producaoService.excluirProdutoProjeto(produtoProjeto);
		projetoWrapper.getProjeto().getAcessorios().remove(produtoProjeto);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("listaMateriais", getListaMateriais());
		mv.addObject("success", "Acessório excluído com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirMaterialAco", method = RequestMethod.GET)
	public ModelAndView excluirMaterialAco(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, MaterialProjeto materialProjeto,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		materialProjeto = producaoService.buscarMaterialProjetoPorId(materialProjeto.getId());
		producaoService.excluirMaterialProjeto(materialProjeto);
		projetoWrapper.getProjeto().getMateriais().remove(materialProjeto);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("listaMateriais", getListaMateriais());
		mv.addObject("success", "Aço excluido com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirMaterial", method = RequestMethod.GET)
	public ModelAndView excluirMaterialAco(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, MaterialProj material,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		material = producaoService.buscarMaterialProjPorId(material.getId());
		producaoService.excluirMaterialProjeto(material);
		projetoWrapper.getProjeto().getMateriaisProjeto().remove(material);

		mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
		mv.addObject("listaProdutos", getListaProdutos());
		mv.addObject("listaMateriaisAco", getListaMateriaisAco());
		mv.addObject("listaMateriais", getListaMateriais());
		mv.addObject("success", "Material excluido com sucesso.");
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST, params={"upload"})
	public ModelAndView upload(HttpServletRequest request, @ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, Model model) {

		ModelAndView mv = new ModelAndView();

		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile multipartFile = multipartRequest.getFile("file");

			DocumentoAnexo documento = new DocumentoAnexo();

			documento.setTipoDocumento(projetoWrapper.getDocumentoAnexo().getTipoDocumento());
			documento.setProjeto(projetoWrapper.getProjeto());
			documento.setDataCadastro(new Date());
			documento.setDescricao(projetoWrapper.getDocumentoAnexo().getDescricao());
			documento.setImagem(multipartFile.getBytes());
			documento.setFilename(multipartFile.getOriginalFilename());
			documento.setType(multipartFile.getContentType());

			documentoAnexoService.salvarDocumentoAnexo(documento);
			
			if(SimpleValidate.isNullOrBlank(projetoWrapper.getProjeto().getMotivoReprovacao())){
				projetoWrapper.getProjeto().setMotivoReprovacao(null);
			}

			projetoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(projetoWrapper.getProjeto().getId()));
			projetoWrapper.setDocumentoAnexo(new DocumentoAnexo());
			mv.addObject("listaTracos", getListaTracos(projetoWrapper.getProjeto()));
			mv.addObject("listaProdutos", getListaProdutos());
			mv.addObject("listaMateriaisAco", getListaMateriaisAco());
			mv.addObject("listaMateriais", getListaMateriais());
			mv.addObject("success", "Documento anexado com sucesso.");
			mv.setViewName("producao/projeto/ComplementarProjetoForm");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e){
			e.printStackTrace();
		}
		return mv;
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("id") String idDocumento, Model model) throws Exception {
		DocumentoAnexo documentoAnexo = documentoAnexoService.buscarPorId(new Long(idDocumento));
		response.setContentType(documentoAnexo.getType());
		response.setContentLength(documentoAnexo.getImagem().length);
		response.setHeader("Content-Disposition","attachment; filename=\"" + documentoAnexo.getFilename() +"\"");

		FileCopyUtils.copy(documentoAnexo.getImagem(), response.getOutputStream());

		return null;
	}
	
	@RequestMapping(value = "/excluirDocumento")
	public ModelAndView excluirDocumento(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, DocumentoAnexo documento,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(projetoWrapper.getListaDocumentoAnexo())){
				documentoAnexoService.excluirDocumento(documento);
				projetoWrapper.getListaDocumentoAnexo().remove(documento);
			}
			projetoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(projetoWrapper.getProjeto().getId()));

			mv.addObject("success", "Documento excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Nao foi possivel excluir o documento.");
			e.printStackTrace();
		}
		mv.setViewName("producao/projeto/ComplementarProjetoForm");
		return mv;
	}

	public List<Produto> getListaProdutos(){
		return producaoService.buscarProdutosPorTipo(EnumTipoProduto.PADRAO);
	}
	
	public List<Material> getListaMateriaisAco(){
		return estoqueService.buscarMateriaisAco(EnumLogicoSimNao.S);
	}
	
	public List<Material> getListaMateriais(){
		return estoqueService.buscarMateriaisAco(EnumLogicoSimNao.N);
	}
	
	public List<Traco> getListaTracos(Projeto projeto){
		
		
		List<Traco> tracos = producaoService.buscarTodosTracosAtivos();
		
		if(projeto != null){
			if(!tracos.contains(projeto.getTraco())){
				tracos.add(projeto.getTraco());
			}
		}
		return tracos;
	}

}
