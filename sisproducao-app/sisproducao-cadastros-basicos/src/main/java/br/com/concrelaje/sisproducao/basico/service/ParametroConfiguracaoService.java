package br.com.concrelaje.sisproducao.basico.service;

import br.com.concrelaje.sisproducao.entidades.ParametroConfiguracao;

/**
 * @author IGOR
 */
public interface ParametroConfiguracaoService {

	ParametroConfiguracao salvar(ParametroConfiguracao parametroConfiguracao);

	ParametroConfiguracao buscarPorId(Long id); 
 
	

}



