package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.concrelaje.sisproducao.basico.service.QualidadeService;
import br.com.concrelaje.sisproducao.entidades.ItemInspecao;

@Controller
@RequestMapping(value = "qualidade/itemInspecaoList/**")
public class ListarItensInspecao { 
    
	 @Autowired
     private QualidadeService qualidadeService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarItensInspecao(ModelMap modelMap) {
        modelMap.addAttribute("listaItens", qualidadeService.buscarItemInspecao());
        return "qualidade/ItemInspecaoList";
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		ItemInspecao itemInspecao = qualidadeService.buscarItemInspecaoPorId(new Long(id));
    		modelMap.addAttribute("itemInspecao", itemInspecao);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar item inspeção.");
    	}
    	return "qualidade/ItemInspecaoForm";
    }
        
}
