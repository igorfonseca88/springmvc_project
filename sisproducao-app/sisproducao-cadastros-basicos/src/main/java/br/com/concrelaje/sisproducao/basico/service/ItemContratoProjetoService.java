package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;

/**
 * @author WILLIAM
 */
public interface ItemContratoProjetoService {

	/** 
	 * Salva um item do contrado do projeto  
	 * @param itemContratoProjeto
	 * @return
	 */
	ItemContratoProjeto salvarItemContratoProjeto(ItemContratoProjeto itemContratoProjeto);
	
	/**
	 * Busca itens contrato projeto por filtros passados por parametro
	 * @param itemContratoProjeto
	 * @return
	 */
	List<ItemContratoProjeto> buscarItensContratoProjetoPorFiltro(ItemContratoProjeto itemContratoProjeto);

	
	/**
	 * Busca itens contrato projeto por filtros passados por parametro para produ��o
	 * @param itemContratoProjeto
	 * @param contrato
	 * @return
	 */
	List<ItemContratoProjeto> buscarItensContratoProjetoParaProducao(ItemContratoProjeto itemContratoProjeto, Contrato contrato);

	ItemContratoProjeto buscarPorId(Long id);

}

