package br.com.concrelaje.sisproducao.basico;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
 
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Traco;
import br.com.concrelaje.sisproducao.entidades.ItemTraco;
import br.com.concrelaje.sisproducao.entidades.wrapper.TracoMaterialWrapper;

@Controller
@RequestMapping(value = "/traco/tracoList")
@SessionAttributes("tracoMaterialWrapper")
public class ListarTracos {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private EstoqueService estoqueService;
	
	
    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarTracos(ModelMap modelMap) {
        modelMap.addAttribute("listaTracos", producaoService.buscarTodosTracos());
        return "producao/traco/TracoList";
    }
    
    @RequestMapping(value="/editar")
    public String editar(Traco traco, ModelMap modelMap) {
    	try{
    		TracoMaterialWrapper tracoMaterialWrapper = new TracoMaterialWrapper();
    		traco = producaoService.buscarTracoPorId(traco.getId());
    		tracoMaterialWrapper.setTraco(traco);
    		
    		List<ItemTraco> itensTraco = producaoService.buscarItemTracoPorIdTraco(traco.getId()); 
    		tracoMaterialWrapper.setListaItemTraco(itensTraco);
    		tracoMaterialWrapper.setListaMateriais(estoqueService.buscarTodosMateriais());
    		
    		// verificar se o traço está sendo utilizado em um projeto
   			tracoMaterialWrapper.setPodeEditarTraco(!producaoService.tracoUtilizadoEmProjeto(traco.getId()));
    		
    		modelMap.addAttribute("tracoMaterialWrapper", tracoMaterialWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar traço.");
    	}
    	return "producao/traco/TracoForm";
    }
    
    @RequestMapping(value="/excluir")
    public String excluir(Traco traco, ModelMap modelMap) {
    	try{
    		
    		traco = producaoService.buscarTracoPorId(traco.getId());
    		
    		// verificar se o traço está sendo utilizado em um projeto
   			if(!producaoService.tracoUtilizadoEmProjeto(traco.getId())){
   				producaoService.excluirTraco(traco);
   	    		modelMap.addAttribute("success", "Traço excluido com sucesso!");
   			}
   			else{
   				modelMap.addAttribute("error", "Traço não pode ser excluido pois já está em uso");
   			}
    		
    		
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Não foi possível excluir.");
    	}
    	
    	return listarTracos(modelMap);
    }
    
}
