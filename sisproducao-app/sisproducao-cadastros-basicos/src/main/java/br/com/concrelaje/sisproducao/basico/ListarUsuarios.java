package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.concrelaje.sisproducao.basico.service.PapelService;
import br.com.concrelaje.sisproducao.basico.service.PessoaService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.PessoaFisica;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.UsuarioPessoaWrapper;

@Controller
@RequestMapping(value = "/basico/usuarioList")
@SessionAttributes("usuarioPessoaWrapper")
public class ListarUsuarios {
    
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private PapelService papelService;
	 
	@Autowired
	private PessoaService pessoaService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarUsuarios(ModelMap modelMap) {
        modelMap.addAttribute("listaUsuarios", usuarioService.buscarTodosUsuarios());
        return "basico/UsuarioList";
    }
    
    @RequestMapping(value="/excluir")
    public String excluir(Usuario usuario, ModelMap modelMap) {
    	try{
    		usuario = usuarioService.buscarPorId(usuario.getId());
    		usuarioService.excluirUsuario(usuario);
    		
    		modelMap.addAttribute("success", "Usuário excluído com sucesso!");
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Não foi possível excluir usuário!");
    	}
    	
    	return listarUsuarios(modelMap);
    }
    
    @RequestMapping(value="/editar")
    public String editar(Usuario usuario, ModelMap modelMap) {
    	try{
    		UsuarioPessoaWrapper usuarioPessoaWrapper = new UsuarioPessoaWrapper();
    		usuario = usuarioService.buscarPorId(usuario.getId());
    		usuarioPessoaWrapper.setPessoaFisica((PessoaFisica)usuario.getPessoa());
    		usuarioPessoaWrapper.setUsuario(usuario);
    		usuarioPessoaWrapper.setListaPapeis(papelService.buscarTodosPapeis());
    		modelMap.addAttribute("usuarioPessoaWrapper", usuarioPessoaWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Não foi possível editar usuário!");
    	}
    	return "basico/UsuarioForm";
    }
}
