package br.com.concrelaje.sisproducao.basico;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.FrotaService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.EnumModalidade;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOC;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.ItemOrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamentoFrota;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.wrapper.OrdemCarregamentoWrapper;

@Controller
@RequestMapping(value = "ordemCarregamento/ordemCarregamentoList/**")
@SessionAttributes("ordemCarregamentoWrapper")
public class ListarOrdemCarregamento {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private FrotaService frotaService; 
	
	@Autowired
	private DocumentoAnexoService documentoAnexoService;
	

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarOC(ModelMap modelMap) {
        modelMap.addAttribute("listaOC", producaoService.buscarTodasOrdensCarregamento());
        modelMap.addAttribute("listaSitacaoOC", this.getListaSituacaoOCs());
        return "producao/ordemCarregamento/OrdemCarregamentoList";
    }
    
    @RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"pesquisar"})
    public ModelAndView pesquisar( 
    		@RequestParam("contrato") String contrato,  		
    		ModelMap modelMap) {
    	
    	
    	ModelAndView mv = new ModelAndView();
    	if(SimpleValidate.isNullOrEmpty(producaoService.buscarOrdensCarregamentoPorFiltro(contrato))){
    		mv.addObject("alert", "Pesquisa n�o trouxe resultados.");
    	}
    	
    	modelMap.addAttribute("listaOC", producaoService.buscarOrdensCarregamentoPorFiltro(contrato));
    	
    	 modelMap.addAttribute("listaSitacaoOC", this.getListaSituacaoOCs());
    	mv.setViewName("producao/ordemCarregamento/OrdemCarregamentoList");
        return mv;
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
			OrdemCarregamentoWrapper ordemCarregamentoWrapper = new OrdemCarregamentoWrapper();
			OrdemCarregamento ordemCarregamento = producaoService.buscarOrdemCarregamentoPorId(new Long(id));
			ordemCarregamentoWrapper.setOrdemCarregamento(ordemCarregamento);

			String hora = "hh:mm";
			String data = "dd/MM/yyyy";
			String horaInicio = "", horaFinal= "" ;

			SimpleDateFormat formata = new SimpleDateFormat(hora);
			if(ordemCarregamento.getDataInicio() != null){
				horaInicio = formata.format(ordemCarregamento.getDataInicio());
			}
			if(ordemCarregamento.getDataFim() != null){
				horaFinal = formata.format(ordemCarregamento.getDataFim());
			}
			
			formata = new SimpleDateFormat(data);
			
			ordemCarregamentoWrapper.setDataCarregamentoString(formata.format(ordemCarregamento.getDataCarregamento()));
			ordemCarregamentoWrapper.setDataPrevistaSaidaString(formata.format(ordemCarregamento.getDataPrevSaida()));

			ordemCarregamentoWrapper.setHorarioInicial(horaInicio);
			ordemCarregamentoWrapper.setHorarioFinal(horaFinal);
			
			ordemCarregamentoWrapper.setListaVeiculo(frotaService.buscarFrotasDisponiveisParaOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaMotorista(frotaService.buscarTodosMotoristas());
			
			ordemCarregamentoWrapper.setTipo(ordemCarregamento.getTipo());
			 
			if(ordemCarregamento.getTipo().equals(EnumModalidade.CONTRATO)){
				ordemCarregamentoWrapper.setListaContrato(new ArrayList<Contrato>());
				ordemCarregamentoWrapper.getListaContrato().add(ordemCarregamento.getContrato());
				ordemCarregamentoWrapper.setListaPecasDisponiveis(producaoService.buscarPecasPorContratoESituacaoOP(ordemCarregamento.getContrato().getId(), EnumSituacaoOP.DISPONIVEL));
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarProdutosPorContratoESituacaoOP(ordemCarregamento.getContrato().getId()));
			}else{
				ordemCarregamentoWrapper.setListaPedido(new ArrayList<Pedido>());
				ordemCarregamentoWrapper.getListaPedido().add(ordemCarregamento.getPedido());
				ordemCarregamentoWrapper.setListaPecasProdutoDisponiveis(producaoService.buscarPecasPorPedidoESituacaoOP(ordemCarregamento.getPedido().getId()));
				ordemCarregamentoWrapper.setListaMateriaisDisponiveis(producaoService.buscarMateriaisPorPedidoESituacaoOP(ordemCarregamento.getPedido().getId()));
			}
			
			
			
			ordemCarregamentoWrapper.setListaCarregamento(producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamento.getId()));
			ordemCarregamentoWrapper.setListaCarregamentoProduto(producaoService.buscarItensOrdemCarregamentoProdutoPorOC(ordemCarregamento.getId()));
			ordemCarregamentoWrapper.setListaCarregamentoMaterial(producaoService.buscarItensOrdemCarregamentoMaterialPorOC(ordemCarregamento.getId()));
			
			ordemCarregamentoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			ordemCarregamentoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			ordemCarregamentoWrapper.setListaOrdemCarregamentoFrota(producaoService.buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(ordemCarregamentoWrapper.getOrdemCarregamento().getId()));
			
			modelMap.addAttribute(ordemCarregamentoWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar ordem de carregamento.");
    		return this.listarOC(modelMap);
    	}
    	return "producao/ordemCarregamento/OrdemCarregamentoForm";
    }
    
    @RequestMapping(value="/excluir", method = RequestMethod.GET)
    public String excluir(@RequestParam("id") String id, ModelMap modelMap) {
    	
    	
    	try{
    		OrdemCarregamento ordemCarregamento = producaoService.buscarOrdemCarregamentoPorId(new Long(id));

    		//delete documentos
    		List<DocumentoAnexo> listDocumento = documentoAnexoService.buscarDocumentosAnexoPorIdOrdemCarregamento(ordemCarregamento.getId());
    		if(!SimpleValidate.isNullOrEmpty(listDocumento)){
    			for (DocumentoAnexo documentoAnexo : listDocumento) {
					documentoAnexoService.excluirDocumento(documentoAnexo);
				}
    		}
    		
    	  	// delete veiculos
    		List<OrdemCarregamentoFrota> listOrdemCarregamentoFrota = producaoService.buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(ordemCarregamento.getId());
    		if(!SimpleValidate.isNullOrEmpty(listOrdemCarregamentoFrota)){
    			for (OrdemCarregamentoFrota ordemCarregamentoFrota : listOrdemCarregamentoFrota) {
					producaoService.excluirOrdemCarregamentoFrota(ordemCarregamentoFrota);
				}
    		}
    		    	
    		// delete itens
    		List<ItemOrdemCarregamento> listItemOrdemCarregamento = producaoService.buscarItensOrdemCarregamentoPorOC(ordemCarregamento.getId());
    		if(!SimpleValidate.isNullOrEmpty(listItemOrdemCarregamento)){
    			for (ItemOrdemCarregamento itemOrdemCarregamento : listItemOrdemCarregamento) {
					producaoService.excluirItemOrdemCarregamento(itemOrdemCarregamento);
				}
    		}
    		
    		// delete ordem
    		producaoService.excluirOrdemCarregamento(ordemCarregamento);
    		modelMap.addAttribute("success", "Ordem de carregamento excluida com sucesso.");
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao excluir ordem de carregamento.");
    		return this.listarOC(modelMap);
    	}
    	
    	return this.listarOC(modelMap);
    }
    
    public EnumSituacaoOC[] getListaSituacaoOCs() {
		return EnumSituacaoOC.values();
	}
    
}
