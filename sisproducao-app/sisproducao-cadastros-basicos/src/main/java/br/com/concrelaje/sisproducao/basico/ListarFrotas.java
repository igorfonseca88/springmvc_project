package br.com.concrelaje.sisproducao.basico;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.concrelaje.sisproducao.basico.service.FrotaService;
import br.com.concrelaje.sisproducao.entidades.Frota;

@Controller 
@RequestMapping(value = "/basico/frotaList/**")
public class ListarFrotas {

	@Autowired
	private FrotaService frotaService;

	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String listarFrotas(ModelMap modelMap) {
		modelMap.addAttribute("listaFrotas", frotaService.buscarTodasFrotas());
		return "basico/FrotaList";
	}

	@RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		Frota frota = new Frota();
    		frota = frotaService.buscarFrotaPorId(new Long(id));
			modelMap.addAttribute("frota", frota);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar checklist.");
    	}
    	return "basico/FrotaForm";
    }
	
	@RequestMapping(value="/excluir", method = RequestMethod.GET)
    public String excluir(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		
    		Frota frota = new Frota();
    		frota = frotaService.buscarFrotaPorId(new Long(id));
    		frotaService.excluirFrota(frota);
    		modelMap.addAttribute("listaFrotas", frotaService.buscarTodasFrotas());
			modelMap.addAttribute("success", "Sucesso ao excluir ve�culo.");
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao excluir checklist.");
    	}
    	return "basico/FrotaList";
    }
	

}
