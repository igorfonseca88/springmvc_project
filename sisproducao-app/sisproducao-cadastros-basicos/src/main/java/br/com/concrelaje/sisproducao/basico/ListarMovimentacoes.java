package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.ItemMovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.wrapper.MovimentacaoWrapper;

@Controller
@RequestMapping(value = "material/movimentacaoList/**")
@SessionAttributes("movimentacaoWrapper")
public class ListarMovimentacoes {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired 
	private EstoqueService estoqueService;
	
	 @Autowired
     private MessageSource messageSource;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarMovimentacoes(ModelMap modelMap) {
        modelMap.addAttribute("listaMovimentacoes", estoqueService.buscarTodasMovimentacoes());
        return "producao/material/MovimentacaoList";
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		MovimentacaoWrapper movimentacaoWrapper = new MovimentacaoWrapper();
    		
    		movimentacaoWrapper.setMovimentacaoEstoque(estoqueService.buscarPorId(new Long(id)));
    		List<ItemMovimentacaoEstoque> itens = estoqueService.buscarItensMovimentacaoEstoquePorIdMovimentacaoEstoque(movimentacaoWrapper.getMovimentacaoEstoque().getId()); 
			movimentacaoWrapper.setListaItemEstoques(itens);
    		modelMap.addAttribute("listaMateriais", estoqueService.buscarTodosMateriais());
    		modelMap.addAttribute("movimentacaoWrapper", movimentacaoWrapper);

    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar movimentação de estoque.");
    	}
    	return "producao/material/MovimentacaoForm";
    }
    
  
        
}
