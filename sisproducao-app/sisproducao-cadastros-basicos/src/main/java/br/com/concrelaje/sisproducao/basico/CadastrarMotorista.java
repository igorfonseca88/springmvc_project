package br.com.concrelaje.sisproducao.basico;



import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.DateFacility;
import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.FrotaService;
import br.com.concrelaje.sisproducao.entidades.Motorista;



@Controller
@RequestMapping(value = "/basico/motoristaForm/**")
public class CadastrarMotorista {
	@Autowired 
	private FrotaService frotaService;
	


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		model.addAttribute(new Motorista());
		return "basico/MotoristaForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("motorista") Motorista motorista, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		mv.setViewName("basico/MotoristaForm"); 
		try{
			
			if(motorista.getId() == null){
				motorista.setDataCadastro(new Date());
			}
			
			if(motorista.getTipoProprioTerceiro() == null){
				mv.addObject("error", "Campo Tipo � obrigat�rio.");
				return mv;
			}
			
			motorista.setValidade(DateFacility.formataData(motorista.getDataString()));
			
			if(SimpleValidate.isNullOrBlank(motorista.getMotorista())){
				mv.addObject("error", "Campo Nome � obrigat�rio.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(motorista.getNumeroRegistro())){
				mv.addObject("error", "Campo Número do Registro � obrigat�rio.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(motorista.getTipo())){
				mv.addObject("error", "Campo Categoria � obrigat�rio.");
				return mv;
			}
			
			if(motorista.getValidade() == null){
				mv.addObject("error", "Campo Validade � obrigat�rio.");
				return mv;
			}
			
			motorista = frotaService.salvarMotorista(motorista);
			
			mv.addObject("success", "Motorista salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel salvar o Motorista.");
			e.printStackTrace();
		}

		
		return mv;
	}
	
	
}
