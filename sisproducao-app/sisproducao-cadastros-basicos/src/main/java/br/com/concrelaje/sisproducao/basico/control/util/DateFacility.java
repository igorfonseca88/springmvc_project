package br.com.concrelaje.sisproducao.basico.control.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;



public class DateFacility {

	public static Date formataData(String data) throws Exception {   
        if (data == null || data.equals(""))  
            return null;   
          
        Date date = null;  
        try {  
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
            date = formatter.parse(data);  
        } catch (ParseException e) {              
            throw e;  
        }  
        return date;  
    }
	
	public static String ajustaDataSql( String data) {
        if (!SimpleValidate.isNullOrBlank(data)) {
            String[] dataDividida = data.split("/");
            return dataDividida[2] + "-" + dataDividida[1] + "-" + dataDividida[0];
        }
        return null;
    }
}
