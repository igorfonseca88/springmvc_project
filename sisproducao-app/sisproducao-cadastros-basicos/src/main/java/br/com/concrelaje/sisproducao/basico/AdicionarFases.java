package br.com.concrelaje.sisproducao.basico;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.FaseProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.wrapper.OPWrapper;


@Controller
@RequestMapping(value = "modal/adicionarFase/**")
@SessionAttributes("opWrapper")
public class AdicionarFases {
    
	@Autowired
	private ProducaoService producaoService;
	

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String pesquisar(@ModelAttribute("idOp") String idOp, @ModelAttribute("opWrapper") OPWrapper opWrapper, ModelMap modelMap) {
    	opWrapper.setListaFaseProducao(producaoService.buscarFasesDisponiveisParaOP(opWrapper.getOp()));
    	return "producao/modal/AdicionarFases";
    }
    
    
    @RequestMapping(value = "/incluirFase", method = RequestMethod.POST)
	public ModelAndView incluirFase(@ModelAttribute("opWrapper") OPWrapper opWrapper, @ModelAttribute("checados") String checados, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try {
			
			String s[] = checados.split("@");  
			
			if(s.length >1){
				
				for(int i = 1; i< s.length; i++){
					FaseProducao fase = producaoService.buscarFasesProducaoPorId(new Long(s[i]));
					OrdemProducaoFase producaoFase = new OrdemProducaoFase();
					producaoFase.setFaseProducao(fase);
					producaoFase.setOrdemProducao(opWrapper.getOp());
					
					producaoService.salvarOrdemProducaoFase(producaoFase);
					
				}
				
			}
		
			mv.setViewName("producao/modal/AdicionarFases");
			mv.addObject("success", "Fase incluída com sucesso.");

		} catch (Exception e) {
			mv.addObject("error", "Erro ao incluir fase.");
		    e.printStackTrace();
			
		}
		
		opWrapper.setListaFaseProducao(producaoService.buscarFasesDisponiveisParaOP(opWrapper.getOp()));
		
		return mv;
	}
       
    
}
