package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.ProjetoService;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.Setor;



@Controller
@RequestMapping(value = "/produto/produtoForm/**")
public class CadastrarProduto {
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private ProjetoService projetoService;
	
	@Autowired
	private EstoqueService estoqueService;
 

	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarProduto(Model model) {
		model.addAttribute(new Produto());
		return "producao/produto/ProdutoForm";
	}

	@RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("produto") Produto produto, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			if(SimpleValidate.isNullOrBlank(produto.getNome())){
				mv.addObject("alert", "O nome do produto e um campo obrigatorio.");
				mv.setViewName("producao/produto/ProdutoForm");
				return mv;
			}
			
			if(null == produto.getTipo()){
				mv.addObject("alert", "O tipo do produto e um campo obrigatorio.");
				mv.setViewName("producao/produto/ProdutoForm");
				return mv;
			}
			
			if(produto.getSetor().getId() == null){
				produto.setSetor(null);
			}
			Produto p = new Produto();
			if(null != produto.getId()){
				p = producaoService.buscarProdutoPorId(produto.getId());
			}
			if(p!=null && p.getProjeto()!= null){
				produto.setProjeto(p.getProjeto());
			}
			produto = producaoService.salvarProduto(produto);
			
			
			if ( (produto.getTipo().equals(EnumTipoProduto.PADRAO) || produto.getTipo().equals(EnumTipoProduto.PRATELEIRA)) && produto.getProjeto() == null) {
				Projeto projeto = new Projeto();
				projeto = projetoService.salvarProjeto(projeto);

				produto.setProjeto(projeto);
				produto = producaoService.salvarProduto(produto);
			}
			mv.addObject(produto);
			mv.addObject("listaSetor", getListaSetor());
			mv.setViewName("producao/produto/ProdutoForm");
			mv.addObject("success", "Produto cadastrado com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Nao foi possivel salvar o produto.");
			e.printStackTrace();
		}

		return mv;
	}
	
	public EnumTipoProduto[] getListaTiposProduto() {
		return EnumTipoProduto.values();
	}
	
	public List<Setor> getListaSetor(){
		return estoqueService.buscarTodosSetores();
	}
}
