package br.com.concrelaje.sisproducao.basico;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ItemContratoProjetoService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.FaseProducao;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.OPWrapper;


@Controller
@RequestMapping(value = "/op/opForm/**")
@SessionAttributes("opWrapper")
public class CadastrarOP {
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private UsuarioService usuarioService; 
	
	@Autowired
	private ItemContratoProjetoService icpService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarOP(Model model) {
		model.addAttribute("opWrapper",new OPWrapper());
		return "producao/op/OPForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"produzir"})
	public ModelAndView produzir(@ModelAttribute("opWrapper") OPWrapper opWrapper) {
		ModelAndView mv = new ModelAndView();
		
		//Armação 
		FaseProducao fase = new FaseProducao();
		OrdemProducaoFase producaoFase = new OrdemProducaoFase();
		
		
		for(int i=0; i<opWrapper.getProducaoFases().size()-1;i++){

			if(opWrapper.getProducaoFases().get(i).getFaseProducao().getOrdem() == 1){
				fase = opWrapper.getProducaoFases().get(i).getFaseProducao();
				producaoFase = opWrapper.getProducaoFases().get(i);
				continue;
			}
		}
		
		if(producaoFase!= null){
			//producaoFase.setDataInicio(new Date());
			producaoService.salvarOrdemProducaoFase(producaoFase);
		}
		
		
		opWrapper.getOp().setSituacao(EnumSituacaoOP.EM_PRODUCAO);
		opWrapper.getOp().setFase(fase);

		try {
			producaoService.salvarOP(opWrapper.getOp());
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao enviar para produ��o.");
			mv.setViewName("producao/op/ComplementarOPForm");
			return mv;
		}
		mv.addObject("success", "Pe�a enviada para produ��o com sucesso.");
		mv.setViewName("producao/op/ComplementarOPForm");
		return mv;

	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"buscar"})
	public ModelAndView buscarItensProjeto(@ModelAttribute("opWrapper") OPWrapper opWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		List<ItemContratoProjeto> listaContratoProjeto = new ArrayList<ItemContratoProjeto>();		
		List<Produto> listaProdutoProjeto = new ArrayList<Produto>();
		try{
			
			Contrato contrato = new Contrato();
			
			if(opWrapper.getTipoBusca().equalsIgnoreCase("CONTRATO")){
				contrato.setNumero(opWrapper.getBuscarPor());
				listaContratoProjeto = icpService.buscarItensContratoProjetoParaProducao(null, contrato);

			}else if(opWrapper.getTipoBusca().equalsIgnoreCase("OBRA")){
				contrato.setNomeObra(opWrapper.getBuscarPor());
				listaContratoProjeto = icpService.buscarItensContratoProjetoParaProducao(null, contrato);
			}
			else if(opWrapper.getTipoBusca().equalsIgnoreCase("TODOS_OBRA")){
				listaContratoProjeto = icpService.buscarItensContratoProjetoParaProducao(null, null);
			}
			else if(opWrapper.getTipoBusca().equalsIgnoreCase("TODOS_PRATELEIRA")){
				listaProdutoProjeto = producaoService.buscarProdutosPorTipoParaProducao(EnumTipoProduto.PRATELEIRA);
			}

			if(!SimpleValidate.isNullOrEmpty(listaContratoProjeto)){
				opWrapper.setListaContratoProjeto(listaContratoProjeto);
			}
			else if(!SimpleValidate.isNullOrEmpty(listaProdutoProjeto)){
				opWrapper.setListaProdutoProjeto(listaProdutoProjeto);
			}
			else{
				mv.addObject("alert", "Pesquisa n�o trouxe resultados.");
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		mv.setViewName("producao/op/OPForm");
		return mv;
	}
	
	@RequestMapping(value = "/produzirItens", method = RequestMethod.POST)
	public ModelAndView produzirItens(@ModelAttribute("opWrapper") OPWrapper opWrapper,	@ModelAttribute("checados") String checados, BindingResult bindingResult) {


		ModelAndView mv = new ModelAndView();
		try {

			String s[] = checados.split("@");
			List<FaseProducao> fases = producaoService.buscarTodasFases();

			if (s.length > 1) {

				for (int i = 1; i < s.length; i++) {
					ItemContratoProjeto icp = (ItemContratoProjeto) icpService.buscarPorId(new Long(s[i]));
					
					OrdemProducao op = new OrdemProducao();
					op.setDataCadastro(new Date());
					op.setItemContratoProjeto(icp);
					op.setSituacao(EnumSituacaoOP.EM_ELABORACAO);
					Usuario usuario = usuarioService.buscarUsuarioPorLogin(SecurityContextHolder.getContext().getAuthentication().getName());
					op.setUsuario(usuario);
					
					op = producaoService.salvarOP(op);
					
					icp.setSituacao(EnumSituacaoItemContratoProjeto.EM_PRODUCAO);
					
					for(int cont = 0; cont<fases.size(); cont++){
						OrdemProducaoFase producaoFase = new OrdemProducaoFase();
						producaoFase.setFaseProducao(fases.get(cont));
						producaoFase.setOrdemProducao(op);
						
						producaoService.salvarOrdemProducaoFase(producaoFase);
					}
					
					icpService.salvarItemContratoProjeto(icp);
					
				}

			}


		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao gerar ordem de produ��o.");
    		return mv;
		}
		opWrapper.setListaContratoProjeto(null);
		mv.addObject("success", "Ordens de produ��o geradas com sucesso.");
		mv.addObject("listaOP", producaoService.buscarTodasOPs());
		mv.addObject("listaFases", producaoService.buscarTodasFases());
		mv.addObject("listaSituacaoOPs", this.getListaSituacaoOPs());
		mv.setViewName("producao/op/OPList");
		return mv;
	}
	
	
	@RequestMapping(value = "/produzirItensProduto", method = RequestMethod.POST)
	public ModelAndView produzirItensProduto(@ModelAttribute("opWrapper") OPWrapper opWrapper,	@ModelAttribute("checados") String checados, BindingResult bindingResult) {


		ModelAndView mv = new ModelAndView();
		try {

			String s[] = checados.split("@");
			List<FaseProducao> fases = producaoService.buscarTodasFases();

			if (s.length > 1) {

				for (int i = 1; i < s.length; i++) {
					Produto produto = (Produto) producaoService.buscarProdutoPorId(new Long(s[i]));
					
					OrdemProducao op = new OrdemProducao();
					op.setDataCadastro(new Date());
					op.setProduto(produto);
					op.setSituacao(EnumSituacaoOP.EM_ELABORACAO);
					Usuario usuario = usuarioService.buscarUsuarioPorLogin(SecurityContextHolder.getContext().getAuthentication().getName());
					op.setUsuario(usuario);
					
					op = producaoService.salvarOP(op);
					
					for(int cont = 0; cont<fases.size(); cont++){
						OrdemProducaoFase producaoFase = new OrdemProducaoFase();
						producaoFase.setFaseProducao(fases.get(cont));
						producaoFase.setOrdemProducao(op);
						
						producaoService.salvarOrdemProducaoFase(producaoFase);
					}
					
				}

			}


		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "Erro ao gerar ordem de produ��o.");
    		return mv;
		}
		opWrapper.setListaContratoProjeto(null);
		mv.addObject("success", "Ordens de produ��o geradas com sucesso.");
		mv.addObject("listaOP", producaoService.buscarTodasOPs());
		mv.addObject("listaFases", producaoService.buscarTodasFases());
		mv.addObject("listaSituacaoOPs", this.getListaSituacaoOPs());
		mv.setViewName("producao/op/OPList");
		return mv;
	}
	
	
	
	public EnumSituacaoOP[] getListaSituacaoOPs() {
		return EnumSituacaoOP.values();
	}
	
	@RequestMapping(value = "/excluirFase", method= RequestMethod.GET)
	public ModelAndView excluirFase(OrdemProducaoFase ordemProducaoFase, @ModelAttribute("opWrapper") OPWrapper opWrapper,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			ordemProducaoFase = producaoService.buscarOrdemProducaoFasePorId(ordemProducaoFase.getId());
			if(!SimpleValidate.isNullOrEmpty(opWrapper.getProducaoFases())){
				producaoService.excluirOrdemProducaoFase(ordemProducaoFase);
				opWrapper.getProducaoFases().remove(ordemProducaoFase);
			}
			opWrapper.setProducaoFases(producaoService.buscarOrdemProducaoFasePorOp(opWrapper.getOp()));

			mv.addObject("success", "Fase excluida com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel excluir a fase.");
			e.printStackTrace();
		}
		mv.setViewName("producao/op/ComplementarOPForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"cancelar"})
	public ModelAndView cancelarOp(@ModelAttribute("opWrapper") OPWrapper opWrapper,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			opWrapper.getOp().setSituacao(EnumSituacaoOP.CANCELADO);
			opWrapper.getOp().getItemContratoProjeto().setSituacao(EnumSituacaoItemContratoProjeto.CANCELADO);
			producaoService.salvarOP(opWrapper.getOp());
			icpService.salvarItemContratoProjeto(opWrapper.getOp().getItemContratoProjeto());
			
			
			mv.addObject("success", "Ordem produ��o cancelada com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel cancelar a Ordem de produ��o.");
			e.printStackTrace();
		}
		mv.setViewName("producao/op/ComplementarOPForm");
		return mv;
	}
	
}
