package br.com.concrelaje.sisproducao.basico.service;

import br.com.concrelaje.sisproducao.entidades.Endereco;

/**
 * @author TBERBET 
 */
public interface EnderecoService {

	/**
	 * Exclui endere�o.
	 * @return
	 */
	void excluir(Endereco endereco); 
}
