package br.com.concrelaje.sisproducao.basico;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Setor;



@Controller
@RequestMapping(value = "/material/materialForm/**")
public class CadastrarMaterial {
	@Autowired
	private EstoqueService estoqueService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarMaterial(Model model) {
		model.addAttribute(new Material());
		model.addAttribute("listaSetor", getListaSetor());
		return "producao/material/MaterialForm";
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarMaterial(@ModelAttribute("material") Material material, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			if(material.getId() == null){
				material.setDataCadastro(new Date());
			}
			else{
				material.setDataCadastro(estoqueService.buscarMaterialPorId(material.getId()).getDataCadastro());
			}
			
			if(material.getSetor().getId() == null){
				material.setSetor(null);
			}
			
			material = estoqueService.salvarMaterial(material);

			mv.setViewName("producao/material/MaterialForm");
			mv.addObject("success", "Material cadastrado com sucesso.");
			mv.addObject(material);
			mv.addObject("listaSetor", getListaSetor());

		}catch(Exception e){
			mv.addObject("error", "Nao foi possvel salvar o material.");
			e.printStackTrace();
		}

		return mv;
	}
	
	public List<Setor> getListaSetor(){
		return estoqueService.buscarTodosSetores();
	}
}
