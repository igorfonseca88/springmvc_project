package br.com.concrelaje.sisproducao.basico;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.ParametroConfiguracaoService;
import br.com.concrelaje.sisproducao.entidades.ParametroConfiguracao;



@Controller
@RequestMapping(value = "/basico/parametroConfiguracaoForm/**")
public class CadastrarParametroConfiguracao {
	@Autowired
	private ParametroConfiguracaoService paramService;
	


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		
		ParametroConfiguracao parametroConfiguracao = paramService.buscarPorId(new Long(1));
		
		model.addAttribute(parametroConfiguracao);
		return "basico/ParametroConfiguracaoForm";
	}
	
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("parametroConfiguracao") ParametroConfiguracao parametroConfiguracao, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		mv.setViewName("basico/ParametroConfiguracaoForm");
		try{
			ParametroConfiguracao param = new ParametroConfiguracao();
			param = parametroConfiguracao;
			paramService.salvar(param);
			
			mv.addObject("success", "Parâmetro salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível salvar o Parâmetro.");
			e.printStackTrace();
		}

		
		return mv;
	}
	
	
	
	
}
