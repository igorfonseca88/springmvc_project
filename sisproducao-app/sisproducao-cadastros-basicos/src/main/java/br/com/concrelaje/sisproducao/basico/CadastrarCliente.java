package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.FieldValidator;
import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.EnderecoService;
import br.com.concrelaje.sisproducao.basico.service.PessoaService;
import br.com.concrelaje.sisproducao.basico.service.TelefoneService;
import br.com.concrelaje.sisproducao.entidades.Cliente;
import br.com.concrelaje.sisproducao.entidades.Endereco;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoCliente;
import br.com.concrelaje.sisproducao.entidades.EnumTipoEndereco;
import br.com.concrelaje.sisproducao.entidades.EnumTipoPessoa;
import br.com.concrelaje.sisproducao.entidades.EnumTipoTelefone;
import br.com.concrelaje.sisproducao.entidades.Pessoa;
import br.com.concrelaje.sisproducao.entidades.Telefone;
import br.com.concrelaje.sisproducao.entidades.wrapper.ClientePessoaWrapper;

@Controller
@RequestMapping(value = "/cliente/clienteForm")
@SessionAttributes("clientePessoaWrapper")
public class CadastrarCliente {  

	@Autowired
	private PessoaService pessoaService;

	@Autowired
	private ClienteService clienteService; 
	
	@Autowired
	private TelefoneService telefoneService;
	
	@Autowired
	private EnderecoService enderecoService;
	
	@Autowired
	private FieldValidator fieldValidator;

	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarCliente(Model model) {
		model.addAttribute(new ClientePessoaWrapper());
		model.addAttribute(getListaSituacoesCliente());
		model.addAttribute(getListaTiposEndereco());
		model.addAttribute(getListaTiposTelefone());
		return "producao/cliente/ClienteForm";
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST)
	public ModelAndView carregarCamposFormulario(@ModelAttribute("clientePessoaWrapper") ClientePessoaWrapper clientePessoaWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		mv.setViewName("producao/cliente/ClienteForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarCliente(@ModelAttribute("clientePessoaWrapper") ClientePessoaWrapper clientePessoaWrapper, BindingResult bindingResult) {
		
		ModelAndView mv = new ModelAndView();
		
		if(EnumTipoPessoa.FISICA.toString().equals(clientePessoaWrapper.getTipoPessoa())){
			clientePessoaWrapper.setValidator("Fisica");
			
			fieldValidator.validate(clientePessoaWrapper, bindingResult);
			
			if(bindingResult.hasFieldErrors("pessoaFisica.nome") || bindingResult.hasFieldErrors("pessoaFisica.rg") || bindingResult.hasFieldErrors("pessoaFisica.cpf") || 
					bindingResult.hasFieldErrors("pessoaFisica.email") || bindingResult.hasFieldErrors("cliente.situacao")){
				mv.setViewName("producao/cliente/ClienteForm");
				return mv;
			}
		}
		else{
			clientePessoaWrapper.setValidator("Juridica");
			
			fieldValidator.validate(clientePessoaWrapper, bindingResult);
			
			if(bindingResult.hasFieldErrors("pessoaJuridica.nomeFantasia") || bindingResult.hasFieldErrors("pessoaJuridica.razaoSocial") || bindingResult.hasFieldErrors("pessoaJuridica.cnpj") || 
					bindingResult.hasFieldErrors("pessoaJuridica.inscricaoEstadual") || bindingResult.hasFieldErrors("pessoaJuridica.email") || bindingResult.hasFieldErrors("cliente.situacao")){
				mv.setViewName("producao/cliente/ClienteForm");
				return mv;
			}
		}
		
		try{

			if(clientePessoaWrapper.getCliente().getId() == null){

				if(EnumTipoPessoa.FISICA.toString().equals(clientePessoaWrapper.getTipoPessoa())){
					clientePessoaWrapper.getPessoaFisica().setTipoPessoa(EnumTipoPessoa.FISICA);
					clientePessoaWrapper.getCliente().setPessoa(clientePessoaWrapper.getPessoaFisica());
				}
				else{
					clientePessoaWrapper.getPessoaJuridica().setTipoPessoa(EnumTipoPessoa.JURIDICA);
					clientePessoaWrapper.getCliente().setPessoa(clientePessoaWrapper.getPessoaJuridica());
				}
			}

			Cliente cliente = clienteService.salvarCliente(clientePessoaWrapper.getCliente());
			//Cliente atualizado com id.
			clientePessoaWrapper.setCliente(cliente);

			mv.addObject("success", "Cliente salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível salvar cliente.");
			e.printStackTrace();
		}

		mv.setViewName("producao/cliente/ClienteForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirTelefone"})
	public ModelAndView incluirTelefone(@ModelAttribute("clientePessoaWrapper") ClientePessoaWrapper clientePessoaWrapper, BindingResult bindingResult) {

		clientePessoaWrapper.setValidator("Telefone");
		fieldValidator.validate(clientePessoaWrapper, bindingResult);
		
		ModelAndView mv = new ModelAndView();
		
		if(bindingResult.hasFieldErrors("telefone.numero") || bindingResult.hasFieldErrors("telefone.tipoTelefone")){
			mv.setViewName("producao/cliente/ClienteForm");
			return mv;
		}
		
		Pessoa pessoa = pessoaService.buscarPessoaComTelefonePorIdPessoa(clientePessoaWrapper.getCliente().getPessoa().getId());
		
		try{
			if(!SimpleValidate.isNullOrEmpty(pessoa.getTelefones())){
				if(pessoa.getTelefones().contains(clientePessoaWrapper.getTelefone())){
					mv.addObject("alert", "O telefone informado já existe para este cliente!");
					mv.setViewName("producao/cliente/ClienteForm");
					return mv;
				}
			}
			clientePessoaWrapper.getTelefone().setPessoa(pessoa);
			pessoa.getTelefones().add(clientePessoaWrapper.getTelefone());
			pessoa = pessoaService.salvar(pessoa);
			clientePessoaWrapper.setListaTelefonesCliente(pessoaService.buscarPessoaComTelefonePorIdPessoa(pessoa.getId()).getTelefones());

			mv.addObject("success", "Telefone adicionado com sucesso!");
			
			clientePessoaWrapper.setTelefone(new Telefone());
		}catch(Exception e){
			mv.addObject("error", "Não foi possível incluir telefone");
			e.printStackTrace();
		}

		mv.setViewName("producao/cliente/ClienteForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirEndereco"})
	public ModelAndView incluirEndereco(@ModelAttribute("clientePessoaWrapper") ClientePessoaWrapper clientePessoaWrapper, BindingResult bindingResult) {

		clientePessoaWrapper.setValidator("Endereco");
		fieldValidator.validate(clientePessoaWrapper, bindingResult);
		
		ModelAndView mv = new ModelAndView();
		
		if(bindingResult.hasFieldErrors("endereco.CEP") || bindingResult.hasFieldErrors("endereco.logradouro") || bindingResult.hasFieldErrors("endereco.bairro") ||
				bindingResult.hasFieldErrors("endereco.numero") || bindingResult.hasFieldErrors("endereco.cidade") || bindingResult.hasFieldErrors("endereco.UF") ||
				bindingResult.hasFieldErrors("endereco.tipoEndereco")){
			mv.setViewName("producao/cliente/ClienteForm");
			return mv;
		}
		
		Pessoa pessoa = pessoaService.buscarPessoaComEnderecoPorIdPessoa(clientePessoaWrapper.getCliente().getPessoa().getId());

		try{
			if(!SimpleValidate.isNullOrEmpty(pessoa.getEnderecos())){
				if(pessoa.getEnderecos().contains(clientePessoaWrapper.getEndereco())){
					mv.addObject("alert", "O endere�o informado j� existe para este cliente!");
					mv.setViewName("producao/cliente/ClienteForm");
					return mv;
				}
			}
			clientePessoaWrapper.getEndereco().setPessoa(pessoa);
			pessoa.getEnderecos().add(clientePessoaWrapper.getEndereco());
			pessoa = pessoaService.salvar(pessoa);
			clientePessoaWrapper.setListaEnderecosCliente(pessoa.getEnderecos());

			mv.addObject("success", "Endere�o adicionado com sucesso!");

			clientePessoaWrapper.setEndereco(new Endereco());
		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel incluir o endere�o.");
			e.printStackTrace();
		}

		mv.setViewName("producao/cliente/ClienteForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"cancelarTelefone"})
	public ModelAndView cancelarTelefone(@ModelAttribute("clientePessoaWrapper") ClientePessoaWrapper clientePessoaWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		Pessoa pessoa = pessoaService.buscarPessoaComTelefonePorIdPessoa(clientePessoaWrapper.getCliente().getPessoa().getId());

		try{
			clientePessoaWrapper.setTelefone(new Telefone());
			clientePessoaWrapper.setListaTelefonesCliente(pessoa.getTelefones());

		}catch(Exception e){
			e.printStackTrace();
		}

		mv.setViewName("producao/cliente/ClienteForm");
		return mv;
	}
	 
	@RequestMapping(value = "/excluirTelefone")
	public ModelAndView excluirTelefone(@ModelAttribute("clientePessoaWrapper") ClientePessoaWrapper clientePessoaWrapper, Telefone telefone,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		Pessoa pessoa = pessoaService.buscarPessoaComTelefonePorIdPessoa(clientePessoaWrapper.getCliente().getPessoa().getId());

		try{
			if(!SimpleValidate.isNullOrEmpty(pessoa.getTelefones())){
				//busco o telefone para que venha com o numero carregado, para que possa ser removido da lista.
				telefone = telefoneService.buscarPorId(telefone.getId());
				telefoneService.excluir(telefone);
				pessoa.getTelefones().remove(telefone);
			}
			clientePessoaWrapper.setListaTelefonesCliente(pessoa.getTelefones());

			mv.addObject("success", "Telefone excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel excluir o telefone.");
			e.printStackTrace();
		}
		mv.setViewName("producao/cliente/ClienteForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirEndereco")
	public ModelAndView excluirEndereco(@ModelAttribute("clientePessoaWrapper") ClientePessoaWrapper clientePessoaWrapper, Endereco endereco,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		Pessoa pessoa = pessoaService.buscarPessoaComEnderecoPorIdPessoa(clientePessoaWrapper.getCliente().getPessoa().getId());

		try{
			if(!SimpleValidate.isNullOrEmpty(pessoa.getEnderecos())){
				enderecoService.excluir(endereco);
				pessoa.getEnderecos().remove(endereco);
			}
			clientePessoaWrapper.setListaEnderecosCliente(pessoa.getEnderecos());

			mv.addObject("success", "Endere�o excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel excluir o endere�o.");
			e.printStackTrace();
		}
		mv.setViewName("producao/cliente/ClienteForm");
		return mv;
	}

	public EnumSituacaoCliente[] getListaSituacoesCliente() {
		return EnumSituacaoCliente.values();
	}

	public EnumTipoTelefone[] getListaTiposTelefone(){
		return EnumTipoTelefone.values();
	}

	public EnumTipoEndereco[] getListaTiposEndereco(){
		return EnumTipoEndereco.values();
	}
}
