package br.com.concrelaje.sisproducao.basico.control.util;


public class StringFacility {

	public static String StrZeroDireita(String value, int n) {  
		  
	    String s = value.trim();  
	    StringBuffer resp = new StringBuffer();  
	  
	    int fim = n - s.length();  
	   
	    for (int x = 0; x < fim; x++)  
	        resp.append('0');  
	  
	    return s + resp;  
	  
	}  
}
