package br.com.concrelaje.sisproducao.basico;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.QualidadeService;
import br.com.concrelaje.sisproducao.entidades.Checklist;
import br.com.concrelaje.sisproducao.entidades.ItemInspecao;
import br.com.concrelaje.sisproducao.entidades.wrapper.ChecklistWrapper;

@Controller
@RequestMapping(value = "qualidade/checklistList/**")
@SessionAttributes("checklistWrapper")
public class ListarChecklist { 
    
	 @Autowired
     private QualidadeService qualidadeService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarChecklist(ModelMap modelMap) {
        modelMap.addAttribute("listaChecklist", qualidadeService.buscarTodosChecklists());
        return "qualidade/ChecklistList";
    } 
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		ChecklistWrapper checklistWrapper = new ChecklistWrapper();
    		Checklist checklist = qualidadeService.buscarChecklistPorId(new Long(id));
			checklistWrapper.setChecklist(checklist);
			checklistWrapper.setInspecao(new ItemInspecao());
			modelMap.addAttribute("checklistWrapper", checklistWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar checklist.");
    	}
    	return "qualidade/ChecklistForm";
    }
    
    @RequestMapping(value="/excluir", method = RequestMethod.GET)
    public String excluir(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		ChecklistWrapper checklistWrapper = new ChecklistWrapper();
    		if(!SimpleValidate.isNullOrEmpty(qualidadeService.buscarInpecoesPorFiltro(null,new Long(id),null,null))){
    			modelMap.addAttribute("error", "Erro ao excluir checklist, pois está associado uma inspeção.");
    			return "qualidade/ChecklistForm";
    		}
			
    		// exclui itens
    		Checklist checklist = qualidadeService.buscarChecklistPorId(new Long(id));
    		for (ItemInspecao item : checklist.getItensInspecao()) {
				qualidadeService.excluirItemInspecao(item);
			}
    		qualidadeService.excluirChecklist(checklist);
    		modelMap.addAttribute("listaChecklist", qualidadeService.buscarTodosChecklists());
			modelMap.addAttribute("checklistWrapper", checklistWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao excluir checklist.");
    	}
    	return "qualidade/ChecklistList";
    }
            
}
