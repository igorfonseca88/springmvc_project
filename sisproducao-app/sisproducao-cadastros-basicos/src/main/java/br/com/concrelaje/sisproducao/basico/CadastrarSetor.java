package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoAtivoInativo;
import br.com.concrelaje.sisproducao.entidades.Setor;



@Controller
@RequestMapping(value = "/basico/setorForm/**")
public class CadastrarSetor {
	@Autowired
	private EstoqueService estoqueService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		model.addAttribute(new Setor());
		return "basico/SetorForm";
	}
	
	@RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("setor") Setor setor, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			estoqueService.salvarSetor(setor);

			mv.setViewName("basico/SetorForm");
			mv.addObject("success", "Local de Armazenamento cadastrado com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Nao foi possvel salvar.");
			e.printStackTrace();
		}

		return mv;
	}
	
	public EnumLogicoAtivoInativo[] getListaAtivoInativos() {
		return EnumLogicoAtivoInativo.values();
	}


}
