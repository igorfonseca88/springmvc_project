package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Frota;
import br.com.concrelaje.sisproducao.entidades.Motorista;

/**
 * @author IGOR
 */
public interface FrotaService {

	List<Frota> buscarTodasFrotas(); 

	Frota salvarFrota(Frota frota); 

	Frota buscarFrotaPorId(Long id); 

	void excluirFrota(Frota frota); 

	List<Motorista> buscarTodosMotoristas();

	Motorista salvarMotorista(Motorista motorista);

	Motorista buscarMotoristaPorId(Long idMotorista);

	void excluirMotorista(Motorista motorista);

	List<Frota> buscarFrotasDisponiveisParaOrdemCarregamento(Long idOrdemCarregamento);

}



