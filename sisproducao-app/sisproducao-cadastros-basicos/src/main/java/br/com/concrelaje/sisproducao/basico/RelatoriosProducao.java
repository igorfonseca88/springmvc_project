package br.com.concrelaje.sisproducao.basico;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoSimNao;
import br.com.concrelaje.sisproducao.entidades.wrapper.RelatorioProducaoWrapper;


@Controller
@SessionAttributes("relatorioProducaoWrapper")
@RequestMapping(value = "producao/relatorios/relatoriosProducao**")
public class RelatoriosProducao  {
    
	 @Autowired
	 EstoqueService estoqueService;
	 
	 @Autowired
	 ProducaoService producaoService;

    @RequestMapping(value="/initRelAco", method = RequestMethod.GET)
    public String initRelAco(ModelMap modelMap) {
    	RelatorioProducaoWrapper relatorioProducaoWrapper = new RelatorioProducaoWrapper();
    	relatorioProducaoWrapper.setListaMaterialAcoFiltro(estoqueService.buscarMateriaisAco(EnumLogicoSimNao.S));
    	relatorioProducaoWrapper.setListaSituacaoProjetoFiltro(producaoService.buscarSituacoesProjetos());
    	modelMap.addAttribute("relatorioProducaoWrapper", relatorioProducaoWrapper);
    	
    	List<Object> lista = producaoService.buscarDadosRelatorioAco(null, null, null,null,null, null);
    	
    	BigDecimal totalComprimento = new BigDecimal(0);
    	BigDecimal totalPeso = new BigDecimal(0);
    	if(!SimpleValidate.isNullOrEmpty(lista)){
    		for (int i = 0; i < lista.size(); i++) {
    			Object[] object = (Object[]) lista.get(i);
    			totalPeso = totalPeso.add( new BigDecimal(object[12].toString()));
    			totalComprimento = totalComprimento.add( new BigDecimal(object[13].toString()));
    		}
			
    	}
    	relatorioProducaoWrapper.setTotalPesoAco(totalPeso);
    	relatorioProducaoWrapper.setTotalComprimentoAco(totalComprimento);
    	relatorioProducaoWrapper.setListaPesquisaAco(lista);
		modelMap.addAttribute("listaDataModel", lista);
    	
    	return "producao/relatorios/RelatoriosAco";
    }
    
    @RequestMapping(value="/initRelProducao", method = RequestMethod.GET)
    public String initRelProducao(ModelMap modelMap) {
    	RelatorioProducaoWrapper relatorioProducaoWrapper = new RelatorioProducaoWrapper();
    	relatorioProducaoWrapper.setListaMaterialAcoFiltro(estoqueService.buscarMateriaisAco(EnumLogicoSimNao.S));
    	relatorioProducaoWrapper.setListaSituacaoProjetoFiltro(producaoService.buscarSituacoesProjetos());
    	modelMap.addAttribute("relatorioProducaoWrapper", relatorioProducaoWrapper);
    	
    	List<Object> lista = producaoService.buscarDadosRelatorioProducao(null,null,null,null,null,null,null);
    	
    	BigDecimal totalComprimento = new BigDecimal(0);
    	BigDecimal totalPeso = new BigDecimal(0);
    	
    	BigDecimal pecaPeso = new BigDecimal(0);
    	
    	if(!SimpleValidate.isNullOrEmpty(lista)){
    		for (int i = 0; i < lista.size(); i++) {
    			Object[] object = (Object[]) lista.get(i);
    			
    			if(null == object[11]){
    				pecaPeso = BigDecimal.ZERO;
    			}else{
    				pecaPeso = new BigDecimal(object[11].toString());
    			}
    			
    			totalPeso = totalPeso.add(pecaPeso);
    			totalComprimento = totalComprimento.add( new BigDecimal(object[12].toString()));
    		}
			
    	}
    	relatorioProducaoWrapper.setTotalPesoAco(totalPeso);
    	relatorioProducaoWrapper.setTotalComprimentoAco(totalComprimento);
    	relatorioProducaoWrapper.setListaPesquisaAco(lista);
		modelMap.addAttribute("listaDataModel", lista);
    	
    	return "producao/relatorios/RelatoriosProducao";
    }
           
    @RequestMapping(value="/pesquisarAco", method = RequestMethod.POST, params={"btPesquisarAco"})
    public ModelAndView pesquisarAco(@ModelAttribute("relatorioProducaoWrapper") RelatorioProducaoWrapper relatorioProducaoWrapper,
    		ModelMap modelMap) {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("producao/relatorios/RelatoriosAco");
    	try{
    		
    		
    		List<Object> lista = producaoService.buscarDadosRelatorioAco(relatorioProducaoWrapper.getMaterialFiltro(), relatorioProducaoWrapper.getContratoFitro(), 
    				relatorioProducaoWrapper.getObraFiltro(), relatorioProducaoWrapper.getSituacaoProjetoFiltro(), 
    				relatorioProducaoWrapper.getDataInicioFiltro(), relatorioProducaoWrapper.getDataFimFiltro());
    		
    		BigDecimal totalComprimento = new BigDecimal(0);
        	BigDecimal totalPeso = new BigDecimal(0);
        	if(!SimpleValidate.isNullOrEmpty(lista)){
        		for (int i = 0; i < lista.size(); i++) {
        			Object[] object = (Object[]) lista.get(i);
        			totalPeso = totalPeso.add( new BigDecimal(object[12].toString()));
        			totalComprimento = totalComprimento.add( new BigDecimal(object[13].toString()));
        		}
    			
        	}
        	relatorioProducaoWrapper.setTotalPesoAco(totalPeso);
        	relatorioProducaoWrapper.setTotalComprimentoAco(totalComprimento);
    		
    		modelMap.addAttribute("listaDataModel", lista);
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return mv;
    }
    
    @RequestMapping(value="/pesquisarProducao", method = RequestMethod.POST, params={"btPesquisarProducao"})
    public ModelAndView pesquisarProducao(@ModelAttribute("relatorioProducaoWrapper") RelatorioProducaoWrapper relatorioProducaoWrapper,
    		ModelMap modelMap) {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("producao/relatorios/RelatoriosProducao");
    	try{
    		
    		
    		List<Object> lista = producaoService.buscarDadosRelatorioProducao(relatorioProducaoWrapper.getContratoFitro(), 
    				relatorioProducaoWrapper.getObraFiltro(), relatorioProducaoWrapper.getSituacaoProjetoFiltro(), 
    				relatorioProducaoWrapper.getDataInicioFiltro(), relatorioProducaoWrapper.getDataFimFiltro(), relatorioProducaoWrapper.getPecaFiltro(),
    				relatorioProducaoWrapper.getSiglaPecaFiltro());
    		
    		BigDecimal totalComprimento = new BigDecimal(0);
        	BigDecimal totalPeso = new BigDecimal(0);
        	if(!SimpleValidate.isNullOrEmpty(lista)){
        		for (int i = 0; i < lista.size(); i++) {
        			Object[] object = (Object[]) lista.get(i);
        			totalPeso = totalPeso.add( new BigDecimal(object[11].toString()));
        			totalComprimento = totalComprimento.add( new BigDecimal(object[12].toString()));
        		}
    			
        	}
        	relatorioProducaoWrapper.setTotalPesoAco(totalPeso);
        	relatorioProducaoWrapper.setTotalComprimentoAco(totalComprimento);
    		
    		modelMap.addAttribute("listaDataModel", lista);
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return mv;
    }
    
    @RequestMapping(value="/pesquisarAco", method = RequestMethod.POST, params={"limpar"})
    public ModelAndView limparPesquisaAco(ModelMap modelMap) {
    	ModelAndView mv = new ModelAndView();
    	RelatorioProducaoWrapper relatorioProducaoWrapper = new RelatorioProducaoWrapper();
    	relatorioProducaoWrapper.setListaMaterialAcoFiltro(estoqueService.buscarMateriaisAco(EnumLogicoSimNao.S));
    	relatorioProducaoWrapper.setListaSituacaoProjetoFiltro(producaoService.buscarSituacoesProjetos());
    	modelMap.addAttribute("relatorioProducaoWrapper", relatorioProducaoWrapper);
    	
    	List<Object> lista = producaoService.buscarDadosRelatorioAco(null, null, null,null, null,null);
    	
    	BigDecimal totalComprimento = new BigDecimal(0);
    	BigDecimal totalPeso = new BigDecimal(0);
    	if(!SimpleValidate.isNullOrEmpty(lista)){
    		for (int i = 0; i < lista.size(); i++) {
    			Object[] object = (Object[]) lista.get(i);
    			totalPeso = totalPeso.add( new BigDecimal(object[12].toString()));
    			totalComprimento = totalComprimento.add( new BigDecimal(object[13].toString()));
    		}
			
    	}
    	relatorioProducaoWrapper.setTotalPesoAco(totalPeso);
    	relatorioProducaoWrapper.setTotalComprimentoAco(totalComprimento);
    	
		modelMap.addAttribute("listaDataModel", lista);
    	
		mv.setViewName("producao/relatorios/RelatoriosAco");
		return mv;
    }
    
    @RequestMapping(value="/pesquisarProducao", method = RequestMethod.POST, params={"limpar"})
    public ModelAndView limparPesquisaProducao(ModelMap modelMap) {
    	ModelAndView mv = new ModelAndView();
    	RelatorioProducaoWrapper relatorioProducaoWrapper = new RelatorioProducaoWrapper();
    	relatorioProducaoWrapper.setListaSituacaoProjetoFiltro(producaoService.buscarSituacoesProjetos());
    	modelMap.addAttribute("relatorioProducaoWrapper", relatorioProducaoWrapper);
    	
    	List<Object> lista = producaoService.buscarDadosRelatorioProducao(null, null, null,null, null,null,null);
    	
    	BigDecimal totalComprimento = new BigDecimal(0);
    	BigDecimal totalPeso = new BigDecimal(0);
    	if(!SimpleValidate.isNullOrEmpty(lista)){
    		for (int i = 0; i < lista.size(); i++) {
    			Object[] object = (Object[]) lista.get(i);
    			totalPeso = totalPeso.add( new BigDecimal(object[11].toString()));
    			totalComprimento = totalComprimento.add( new BigDecimal(object[12].toString()));
    		}
			
    	}
    	relatorioProducaoWrapper.setTotalPesoAco(totalPeso);
    	relatorioProducaoWrapper.setTotalComprimentoAco(totalComprimento);
    	
		modelMap.addAttribute("listaDataModel", lista);
    	
		mv.setViewName("producao/relatorios/RelatoriosProducao");
		return mv;
    }
    
    
    
    @RequestMapping(value="/pesquisarAco", method = RequestMethod.POST, params={"excel"})
    public ModelAndView exportExcel(HttpServletRequest request,
			HttpServletResponse response) {
	    	
    	HSSFWorkbook workbook = new HSSFWorkbook();
    	HSSFSheet sheet = workbook.createSheet("Relatório de Aço");
    	 
    	Map<String, Object[]> data = new HashMap<String, Object[]>();
    	data.put("1", new Object[] {"Contrato", "Obra", "Sigla"});
    	data.put("2", new Object[] {1d, "John", 1500000d});
    	data.put("3", new Object[] {2d, "Sam", 800000d});
    	data.put("4", new Object[] {3d, "Dean", 700000d});
    	 
    	Set<String> keyset = data.keySet();
    	int rownum = 0;
    	for (String key : keyset) {
    	    Row row = sheet.createRow(rownum++);
    	    Object [] objArr = data.get(key);
    	    int cellnum = 0;
    	    for (Object obj : objArr) {
    	        Cell cell = row.createCell(cellnum++);
    	        if(obj instanceof Date) 
    	            cell.setCellValue((Date)obj);
    	        else if(obj instanceof Boolean)
    	            cell.setCellValue((Boolean)obj);
    	        else if(obj instanceof String)
    	            cell.setCellValue((String)obj);
    	        else if(obj instanceof Double)
    	            cell.setCellValue((Double)obj);
    	    }
    	}
    	 
    	try {
    		File file = new File("E:/temp/planilha.xls");
    		
    	    FileOutputStream out =  new FileOutputStream(file);
    	    workbook.write(out);
    	    out.close();
    	    System.out.println("Excel written successfully..");
    	   String doc = "Relatorio_Producao.xls"; 
    	   response.setHeader("Content-Disposition","attachment; filename=\"" + doc +"\"");
			return null;
    	     
    	} catch (FileNotFoundException e) {
    	    e.printStackTrace();
    	} catch (IOException e) {
    	    e.printStackTrace();
    	}
				
				return null;
					
				
			
    }
    
}
