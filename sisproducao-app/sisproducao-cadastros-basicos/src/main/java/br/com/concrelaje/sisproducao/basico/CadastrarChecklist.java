package br.com.concrelaje.sisproducao.basico;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.QualidadeService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.Checklist;
import br.com.concrelaje.sisproducao.entidades.ItemInspecao;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.ChecklistWrapper;



@Controller
@RequestMapping(value = "/qualidade/checklistForm/**")
@SessionAttributes("checklistWrapper")
public class CadastrarChecklist {
	@Autowired
	private QualidadeService qualidadeService;
	
	@Autowired
	private UsuarioService usuarioService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		model.addAttribute(new ChecklistWrapper());
		return "qualidade/ChecklistForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("checklistWrapper") ChecklistWrapper checklistWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			if(checklistWrapper.getChecklist().getId() == null){
				Usuario usuario = usuarioService.buscarUsuarioPorLogin(SecurityContextHolder.getContext().getAuthentication().getName());
				checklistWrapper.getChecklist().setUsuario(usuario);
				checklistWrapper.getChecklist().setDataCadastro(new Date());
			}
			
			Checklist checklist = qualidadeService.salvarChecklist(checklistWrapper.getChecklist());
			checklistWrapper.setChecklist(checklist);
			mv.addObject("checklistWrapper", checklistWrapper);
			mv.addObject("success", "Checklist salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível salvar o Checklist.");
			e.printStackTrace();
		}

		mv.setViewName("qualidade/ChecklistForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvarItemInspecao"})
	public ModelAndView salvarItemInspecao(@ModelAttribute("checklistWrapper") ChecklistWrapper checklistWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			checklistWrapper.getInspecao().setChecklist(checklistWrapper.getChecklist());
			checklistWrapper.getInspecao().setDataCadastro(new Date());

			qualidadeService.salvarItemInspecao(checklistWrapper.getInspecao());
			checklistWrapper.setInspecao(new ItemInspecao());
			
			Checklist checklist = qualidadeService.buscarChecklistPorId(checklistWrapper.getChecklist().getId());
			checklistWrapper.setChecklist(checklist);
			mv.addObject("checklistWrapper", checklistWrapper);
			mv.addObject("success", "Item salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível salvar o Item.");
			e.printStackTrace();
		}

		mv.setViewName("qualidade/ChecklistForm");
		return mv;
	}

	@RequestMapping(value = "/excluirItem")
	public ModelAndView excluirItem(@ModelAttribute("checklistWrapper") ChecklistWrapper checklistWrapper, ItemInspecao itemInspecao,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(checklistWrapper.getChecklist().getItensInspecao())){
				
				// busca se o item inspecao possui resposta
				
				if(!SimpleValidate.isNullOrEmpty(qualidadeService.buscarRespostaInspecaoPorIdItemInspecao(itemInspecao.getId()))){
					mv.addObject("error", "Não foi possível excluir o item, pois já está associado a uma inspeção.");
					mv.setViewName("qualidade/ChecklistForm");
					return mv;
				}
				
				qualidadeService.excluirItemInspecao(itemInspecao);
				checklistWrapper.getChecklist().getItensInspecao().remove(itemInspecao);
			}
			Checklist checklist = qualidadeService.buscarChecklistPorId(checklistWrapper.getChecklist().getId());
			checklistWrapper.setChecklist(checklist);
			mv.addObject("checklistWrapper", checklistWrapper);

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("qualidade/ChecklistForm");
		return mv;
	}
	
	@RequestMapping(value = "/editarItem")
	public ModelAndView editarItem(@ModelAttribute("checklistWrapper") ChecklistWrapper checklistWrapper, ItemInspecao itemInspecao,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			checklistWrapper.setInspecao(qualidadeService.buscarItemInspecaoPorId(itemInspecao.getId()));
		}catch(Exception e){
			mv.addObject("error", "Não foi possível editar o item.");
			e.printStackTrace();
		}
		mv.setViewName("qualidade/ChecklistForm");
		return mv;
	}
}
