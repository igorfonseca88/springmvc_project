package br.com.concrelaje.sisproducao.basico;



import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.FrotaService;
import br.com.concrelaje.sisproducao.entidades.Frota;



@Controller
@RequestMapping(value = "/basico/frotaForm/**")
public class CadastrarFrota {
	@Autowired
	private FrotaService frotaService;
	


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		model.addAttribute(new Frota());
		return "basico/FrotaForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("frota") Frota frota, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		mv.setViewName("basico/FrotaForm");
		try{
			
			if(frota.getId() == null){
				frota.setDataCadastro(new Date());
			}
			
			if(frota.getTipo() == null){
				mv.addObject("error", "Campo Tipo é obrigatório.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(frota.getVeiculo())){
				mv.addObject("error", "Campo Veículo é obrigatório.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(frota.getPlaca())){
				mv.addObject("error", "Campo Placa é obrigatório.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(frota.getMarca())){
				mv.addObject("error", "Campo Marca é obrigatório.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(frota.getModelo())){
				mv.addObject("error", "Campo Modelo é obrigatório.");
				return mv;
			}
			if(SimpleValidate.isNullOrBlank(frota.getChassi())){
				mv.addObject("error", "Campo Chassi é obrigatório.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(frota.getRenavam())){
				mv.addObject("error", "Campo Renavam é obrigatório.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(frota.getCor())){
				mv.addObject("error", "Campo Cor é obrigatório.");
				return mv;
			}
			
			if(SimpleValidate.isNullOrBlank(frota.getEstado())){
				mv.addObject("error", "Campo Estado é obrigatório.");
				return mv;
			}
			
			frota = frotaService.salvarFrota(frota);
			
			mv.addObject("success", "Veículo salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível salvar o Veículo.");
			e.printStackTrace();
		}

		
		return mv;
	}
	
	
}
