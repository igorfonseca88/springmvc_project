package br.com.concrelaje.sisproducao.basico;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.DateFacility;
import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ItemContratoProjetoService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.OPWrapper;


@Controller
@RequestMapping(value = "modal/modalBaixa/**")
@SessionAttributes("opWrapper")
public class ModalBaixarFases {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ItemContratoProjetoService itemContratoProjetoService;
	

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String init(@ModelAttribute("idOp") String idOp,@ModelAttribute("opWrapper") OPWrapper opWrapper, ModelMap modelMap) {

    	if(opWrapper.getOp().getItemContratoProjeto()!= null){
    		opWrapper.setRevisaoProjeto(opWrapper.getOp().getItemContratoProjeto().getProjeto().getRevisao());
    		opWrapper.setIdItemContratoProjeto(opWrapper.getOp().getItemContratoProjeto().getId());
    		opWrapper.setTipoPecaProduto(EnumTipoProduto.OBRA);
    	}
    	else{
    		opWrapper.setRevisaoProjeto(opWrapper.getOp().getProduto().getProjeto().getRevisao());
    		opWrapper.setIdProduto(opWrapper.getOp().getProduto().getId());
    		opWrapper.setTipoPecaProduto(EnumTipoProduto.PRATELEIRA);
    	}
		opWrapper.setListaFasesDisponiveisParaBaixar(producaoService.buscarFasesDisponiveisParaBaixarPorOp(opWrapper.getOp()));
    	
    	return "producao/modal/ModalBaixarFases";
    }
    
    @RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView baixar(@ModelAttribute("opWrapper") OPWrapper opWrapper , BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try {

				OrdemProducaoFase producaoFase = new OrdemProducaoFase();
				
				mv.setViewName("producao/modal/ModalBaixarFases");
				
				if(opWrapper.getIdFaseSelecionada() == null){
					mv.addObject("error", "O campo Fase é obrigatório.");
					return mv;
				}
				
				if(SimpleValidate.isNullOrBlank(opWrapper.getDataString())){
					mv.addObject("error", "O campo Data In�cio � obrigatório.");
					return mv;
				}
				
				if(SimpleValidate.isNullOrBlank(opWrapper.getDataBaixaString())){
					mv.addObject("error", "O campo Data Baixa � obrigatório.");
					return mv;
				}
				
				
				for(int i=0; i<=opWrapper.getProducaoFases().size()-1;i++){

					if(opWrapper.getProducaoFases().get(i).getFaseProducao().getId() == opWrapper.getIdFaseSelecionada()){
						producaoFase = opWrapper.getProducaoFases().get(i);
						break;
					}
				}
				
				// realiza a baixa da fase
				if(producaoFase!= null){
					producaoFase.setDataFim(DateFacility.formataData(opWrapper.getDataBaixaString()));
					producaoFase.setDataInicio(DateFacility.formataData(opWrapper.getDataString()));
					
					
					if(producaoFase.getDataFim().before(producaoFase.getDataInicio())){
						mv.addObject("error", "O campo Data de In�cio n�o pode ser maior que o campo Data de Baixa.");
						return mv;
					}
					
					Usuario usuario = usuarioService.buscarUsuarioPorLogin(SecurityContextHolder.getContext().getAuthentication().getName());
					producaoFase.setUsuario(usuario);
					producaoFase.setRevisao(opWrapper.getRevisaoProjeto());
					producaoService.salvarOrdemProducaoFase(producaoFase);
				}
				
				
				// busca próxima a fase producao que a peça deve ir e seta data de inicio
				OrdemProducaoFase ordemProducaoFase = producaoService.buscarProximaOrdemProducaoFaseParaProduzirPorOp(opWrapper.getOp());
				if(ordemProducaoFase!=null){
					ordemProducaoFase = producaoService.salvarOrdemProducaoFase(ordemProducaoFase);
				}
				// seta no OP a fase de produção atual
				OrdemProducao op = producaoService.buscarOrdemProducaoPorId(opWrapper.getOp().getId());
				boolean isProduzido = false;
				if(ordemProducaoFase!=null){
					op.setFase(ordemProducaoFase.getFaseProducao());
					
					// verifica se a fase anterior era Conferência e marca a peça como disponível
					if(producaoFase.getFaseProducao().getFase().equals("Conferência")){
						op.setSituacao(EnumSituacaoOP.DISPONIVEL);
						if(op.getItemContratoProjeto() != null){
							op.getItemContratoProjeto().setSituacao(EnumSituacaoItemContratoProjeto.PRODUZIDO);
						}
						isProduzido = true;
					}
				}
				else{
					op.setSituacao(EnumSituacaoOP.PRODUZIDO);
					op.setFase(null);
				}
				producaoService.salvarOP(op);
				
				// marca a peça como produzida e a partir de agora não pode mais ser cancelada.
				if(isProduzido == true){
					itemContratoProjetoService.salvarItemContratoProjeto(op.getItemContratoProjeto());
				}
				
				opWrapper.setOp(op);
				
				
				
			
			mv.addObject("success", "Fase baixada com sucesso.");

		} catch (Exception e) {
			mv.addObject("error", "Erro ao baixar fase.");
		    e.printStackTrace();
		    mv.setViewName("producao/modal/ModalBaixarFases");
		}
		
		opWrapper.setListaFasesDisponiveisParaBaixar(producaoService.buscarFasesDisponiveisParaBaixarPorOp(opWrapper.getOp()));
		
		return mv;
	}
    
    
}
