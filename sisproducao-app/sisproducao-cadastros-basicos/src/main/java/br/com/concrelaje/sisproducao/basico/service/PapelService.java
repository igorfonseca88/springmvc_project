package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Papel;

/**
 * @author WILLIAM
 */
public interface PapelService {

	/** 
	 * Busca papel por id 
	 * @param id
	 * @return
	 */
	
	Papel buscarPorId(Long id);

	/**
	 * Busca todos os papeis do sistema
	 * @return
	 */
	List<Papel> buscarTodosPapeis();




}



