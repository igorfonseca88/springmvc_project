package br.com.concrelaje.sisproducao.basico;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.control.util.FieldValidator;
import br.com.concrelaje.sisproducao.basico.service.PapelService;
import br.com.concrelaje.sisproducao.basico.service.PessoaService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.EnumTipoPessoa;
import br.com.concrelaje.sisproducao.entidades.Papel;
import br.com.concrelaje.sisproducao.entidades.Pessoa;
import br.com.concrelaje.sisproducao.entidades.PessoaFisica;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.UsuarioPessoaWrapper;

@Controller
@RequestMapping(value = "/basico/usuarioForm")
@SessionAttributes("usuarioPessoaWrapper")
public class CadastrarUsuario {
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private PessoaService pessoaService;

	@Autowired 
	private  PapelService papelService;
	
	@Autowired
	private FieldValidator fieldValidator;

	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarUsuario(Model model) {
		UsuarioPessoaWrapper usuarioPessoaWrapper = new UsuarioPessoaWrapper();
		usuarioPessoaWrapper.setListaPapeis(papelService.buscarTodosPapeis());
		model.addAttribute(usuarioPessoaWrapper);
		return "basico/UsuarioForm";
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarUsuario(@ModelAttribute("usuarioPessoaWrapper") UsuarioPessoaWrapper usuarioPessoaWrapper, BindingResult bindingResult) {

		fieldValidator.validate(usuarioPessoaWrapper, bindingResult);
		
		ModelAndView mv = new ModelAndView();
		
		if(bindingResult.hasFieldErrors("pessoaFisica.cpf") || bindingResult.hasFieldErrors("pessoaFisica.nome") || bindingResult.hasFieldErrors("pessoaFisica.email") ||
				bindingResult.hasFieldErrors("usuario.password") || bindingResult.hasFieldErrors("usuario.password")){
			mv.setViewName("basico/UsuarioForm");
			return mv;
		}
		
		try{
			Long idUsuario = usuarioPessoaWrapper.getUsuario().getId();
			Usuario	usuario = usuarioService.buscarUsuarioPorCpf(usuarioPessoaWrapper.getPessoaFisica().getCpf());
			
			// significa que esta sendo salvo um usuario ja existente e que o CPF encontrado � do usuario editado em quest�o.
			boolean edicaoCPF = usuario != null && (usuario.getId() != idUsuario);
			
			if(edicaoCPF){
				mv.addObject("alert", "Já existe um usuário com o CPF informado.");
				mv.setViewName("basico/UsuarioForm");
				return mv;
			}
			
			//usa o mesmo objeto usuario para buscar usuario por login.
			usuario = usuarioService.buscarUsuarioPorLogin(usuarioPessoaWrapper.getUsuario().getUsername());
			
			// significa que esta sendo salvo um usuario ja existente e que o LOGIN encontrado � do usuario editado em quest�o.
			boolean edicaoLogin = usuario != null && (usuario.getId() != idUsuario);
			
			if(edicaoLogin){
				mv.addObject("alert", "Já existe um usuário com o login informado.");
				mv.setViewName("basico/UsuarioForm");
				return mv;
			}
			
			if(idUsuario == null){
				usuarioPessoaWrapper.getPessoaFisica().setTipoPessoa(EnumTipoPessoa.FISICA);
			}

			usuarioPessoaWrapper.getUsuario().setPessoa(usuarioPessoaWrapper.getPessoaFisica());
			//atribui usuario ja com id gravado no banco
			usuarioPessoaWrapper.setUsuario(usuarioService.salvarUsuario(usuarioPessoaWrapper.getUsuario()));

			mv.addObject("success", "Usuário salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possivel salvar usuário.");
			e.printStackTrace();
		}

		mv.setViewName("basico/UsuarioForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"buscar"})
	public ModelAndView buscarUsuario(@ModelAttribute("usuarioPessoaWrapper") UsuarioPessoaWrapper usuarioPessoaWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		Pessoa pessoa = null;
		Usuario usuario = null;
		try{
			if(!SimpleValidate.isNullOrBlank(usuarioPessoaWrapper.getPessoaFisica().getCpf())){
				usuario = usuarioService.buscarUsuarioPorCpf(usuarioPessoaWrapper.getPessoaFisica().getCpf());
				if(usuario != null){
					pessoa = usuario.getPessoa();
					usuarioPessoaWrapper.setUsuario(usuario);
				}else{
					pessoa = pessoaService.buscarPessoaPorCpfCnpj(usuarioPessoaWrapper.getPessoaFisica().getCpf(), EnumTipoPessoa.FISICA);
				}
			}

			if(pessoa != null){
				usuarioPessoaWrapper.setPessoaFisica((PessoaFisica) pessoa);
			}

		}catch(Exception e){
			mv.addObject("error", "Não foi possível buscar usuário por CPF informado.");
			e.printStackTrace();
		}

		mv.setViewName("basico/UsuarioForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluir"})
	public ModelAndView incluirPapel(@ModelAttribute("usuarioPessoaWrapper") UsuarioPessoaWrapper usuarioPessoaWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		List<Papel> papeisUsuario = usuarioPessoaWrapper.getUsuario().getPapeis();
		try{
			if(usuarioPessoaWrapper.getPapelSelecionado().getId() == 0L ){
				mv.addObject("alert", "É necessário selecionar um papel antes de incluir!");
				mv.setViewName("basico/UsuarioForm");
				usuarioPessoaWrapper.setPapelSelecionado(null);
				return mv;
			}

			if(!SimpleValidate.isNullOrEmpty(papeisUsuario)){
				if(papeisUsuario.contains(usuarioPessoaWrapper.getPapelSelecionado())){
					mv.addObject("alert", "O papel selecionado já existe para este usuário!");
					mv.setViewName("basico/UsuarioForm");
					usuarioPessoaWrapper.setPapelSelecionado(null);
					return mv;
				}
			}else{
				papeisUsuario = new ArrayList<Papel>();
			}

			List<Papel> listaPapel = new ArrayList<Papel>();
			listaPapel.addAll(papeisUsuario);
			listaPapel.add(papelService.buscarPorId(usuarioPessoaWrapper.getPapelSelecionado().getId()));

			usuarioPessoaWrapper.getUsuario().setPapeis(listaPapel);
			usuarioService.salvarUsuario(usuarioPessoaWrapper.getUsuario());
			mv.addObject("success", "Papel incluido com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível incluir papel.");
			e.printStackTrace();
		}

		mv.setViewName("basico/UsuarioForm");
		usuarioPessoaWrapper.setPapelSelecionado(null);
		return mv;
	}

	@RequestMapping(value="/excluirPapel")
	public String excluir(@ModelAttribute("usuarioPessoaWrapper") UsuarioPessoaWrapper usuarioPessoaWrapper, Papel papel, ModelMap modelMap) {
		try{
			usuarioPessoaWrapper.getUsuario().getPapeis().remove(papel);
			usuarioService.salvarUsuario(usuarioPessoaWrapper.getUsuario());
			modelMap.addAttribute("success", "Papel excluído com sucesso!");

		}catch(Exception e){ 
			e.printStackTrace();
			modelMap.addAttribute("error", "Não foi possível excluir papel!");
		}

		return "basico/UsuarioForm";
	}
}
