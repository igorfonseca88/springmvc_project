package br.com.concrelaje.sisproducao.basico;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ItemContratoProjetoService;
import br.com.concrelaje.sisproducao.basico.service.ParametroConfiguracaoService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.ProjetoService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.ParametroConfiguracao;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.wrapper.ProjetoWrapper;

@Controller
@RequestMapping(value = "/projeto/projetoForm")
@SessionAttributes("projetoWrapper")
public class CadastrarProjeto {  

	@Autowired
	private ProducaoService producaoService;

	@Autowired
	private ProjetoService projetoService;
 
	@Autowired
	private ItemContratoProjetoService itemContratoProjetoService;
	
	@Autowired
	private ParametroConfiguracaoService configuracaoService;

	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarProjeto(Model model) {
		model.addAttribute(new ProjetoWrapper());
		return "producao/projeto/ProjetoForm";
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"buscar"})
	public ModelAndView buscarItensContrato(@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		List<Contrato> listaContratos = new ArrayList<Contrato>();		
		try{
			Contrato contrato =  new Contrato();
			if(projetoWrapper.getTipoBusca().equalsIgnoreCase("CONTRATO")){
				contrato.setNumero(projetoWrapper.getBuscarPor());
				listaContratos = producaoService.buscarContratosPorFiltros(contrato);

			}else if(projetoWrapper.getTipoBusca().equalsIgnoreCase("OBRA")){
				contrato.setNomeObra(projetoWrapper.getBuscarPor());
				listaContratos = producaoService.buscarContratosPorFiltros(contrato);
			}

			projetoWrapper.setListaContratos(listaContratos);

		}catch(Exception e){
			e.printStackTrace();
		}

		mv.setViewName("producao/projeto/ProjetoForm");
		return mv;
	}

	@RequestMapping(value = "/sequenciarItemContrato")
	public ModelAndView sequenciarItem (@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, Contrato contrato,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			Long idContrato = contrato.getId();
			List<ItemContrato> listaItemContratos = producaoService.buscarItensContratoPorIdContrato(idContrato);
			projetoWrapper.setListaItemContratos(listaItemContratos);
			projetoWrapper.setListaContratos(new ArrayList<Contrato>());
			
			projetoWrapper.setListaProjetosGerados(projetoService.buscarProjetosViewPorIdContrato(idContrato));

		}catch(Exception e){
			e.printStackTrace();
		}
		mv.setViewName("producao/projeto/ProjetoForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST, params={"gerarProjetoItemContrato"})
	public ModelAndView gerarProjetoItemContrato (@ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper, HttpServletRequest request, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			Integer quantidadeItemSelecionado = projetoWrapper.getQuantidadeItemSelecionado();
			String sigla =  projetoWrapper.getItemContratoProjeto().getSigla();
			

			if(quantidadeItemSelecionado != null && quantidadeItemSelecionado == 0){
				mv.addObject("alert", "Para gerar um projeto é necessário o preenchimento do campo quantidade.");
				mv.setViewName("producao/projeto/ProjetoForm");
				return mv;
			}

			if(SimpleValidate.isNullOrBlank(sigla)){
				mv.addObject("alert", "Para gerar um projeto é necessário o preenchimento do campo sigla sequencial.");
				mv.setViewName("producao/projeto/ProjetoForm");
				return mv;
			}

			if(projetoWrapper.getItemContratoProjeto().getItemContrato().getId() == 0){
				projetoWrapper.setQuantidadeRestante("");
				mv.addObject("alert", "Para gerar um projeto é necessário selecionar um Item Contrato.");
				mv.setViewName("producao/projeto/ProjetoForm");
				return mv;
			}

			/**
			 * Busca a lista de itens contrato por projeto que contem o item selecionado e a mesma sigla, para que seja continuado a sequencia dos itens 
			 */
			List<ItemContratoProjeto> listaItensContratoProjeto = itemContratoProjetoService.buscarItensContratoProjetoPorFiltro(projetoWrapper.getItemContratoProjeto());

			Projeto projeto = new Projeto();
			projeto.setRevisao(1);

			/**
			 * Serve para saber a partir de qual sequencia seguir
			 */
			Long sequenciaItensContratoProjeto = 0L;

			if(!SimpleValidate.isNullOrEmpty(listaItensContratoProjeto)){
				sequenciaItensContratoProjeto = listaItensContratoProjeto.get(listaItensContratoProjeto.size()-1).getSequencial();
				projeto = listaItensContratoProjeto.get(0).getProjeto();
			}else{
				projeto = projetoService.salvarProjeto(projeto);
			}
			
			ItemContratoProjeto  itemContratoProjeto = new ItemContratoProjeto();
			itemContratoProjeto.setItemContrato(projetoWrapper.getItemContratoProjeto().getItemContrato());

			/**
			 * Busca a lista de itens contrato por projeto que contem o item Contrato selecionado.
			 * Pois pode existir mais de uma sigla para o mesmo item porem não pode ultrapassar a quantidade definida no contrato.
			 */
			List<ItemContratoProjeto> listaItensContratoProjetoQuantidade = itemContratoProjetoService.buscarItensContratoProjetoPorFiltro(itemContratoProjeto);

			/**
			 * Serve para saber quantos itens existe na lista
			 */
			Integer quantidadeItensContratoProjeto = 0;
			if(!SimpleValidate.isNullOrEmpty(listaItensContratoProjetoQuantidade)){
				quantidadeItensContratoProjeto =  listaItensContratoProjetoQuantidade.size();
			}
			/**
			 * Quantidade de um determinado Item do Contrato definido no momento da cria��o do contrato.
			 */
			Integer quantidadeItem = producaoService.buscarItemContratoPorId(projetoWrapper.getItemContratoProjeto().getItemContrato().getId()).getQuantidade();

			/**
			 * verifica se a quantidade de itens selecionados n�o ultrapassou a quantidade dispon�vel. 
			 */
			if(quantidadeItem < (quantidadeItemSelecionado + quantidadeItensContratoProjeto)){
				mv.addObject("alert", "A quantidade solicitada é maior que a quantidade disponível.");
				mv.setViewName("producao/projeto/ProjetoForm");
				return mv;
			}

			/**
			 * Cria um item contrato projeto, com sigla, sequencia e associa os itens a um mesmo novo projeto; 
			 */
			for (int i = 1; i <= quantidadeItemSelecionado; i++) {
				ItemContratoProjeto itemContratoProj = new ItemContratoProjeto();
				itemContratoProj.setItemContrato(projetoWrapper.getItemContratoProjeto().getItemContrato());
				itemContratoProj.setSigla(sigla);
				itemContratoProj.setSequencial(sequenciaItensContratoProjeto + i);
				itemContratoProj.setProjeto(projeto);
				itemContratoProj.setSituacao(EnumSituacaoItemContratoProjeto.A_PRODUZIR);
				itemContratoProj = itemContratoProjetoService.salvarItemContratoProjeto(itemContratoProj);
				
				ParametroConfiguracao parametroConfiguracao = new ParametroConfiguracao();
				parametroConfiguracao = configuracaoService.buscarPorId(new Long(1));
				
				String url = parametroConfiguracao.getUrl();
				String s[] = url.split("sisproducao/");
				url = s[0]+"sisproducao/admin/PainelControle.html?id="+itemContratoProj.getId();
				itemContratoProj.setUrl(url);
				itemContratoProjetoService.salvarItemContratoProjeto(itemContratoProj);
				
				
			}

			/**
			 * Serve para atualizar a quantidade restante na tela
			 */
			String quantidadeRestante = String.valueOf(quantidadeItem - (quantidadeItemSelecionado + quantidadeItensContratoProjeto));
			projetoWrapper.setQuantidadeRestante(quantidadeRestante);
			projetoWrapper.setQuantidadeItemSelecionado(null);
			projetoWrapper.getItemContratoProjeto().setSigla("");
			

		}catch(Exception e){
			e.printStackTrace();
			mv.addObject("error", "Nao foi possivel gerar projeto para este item contrato.");
			mv.setViewName("producao/projeto/ProjetoForm");
			return mv;
		}
		mv.addObject("success", "Projeto gerado com sucesso.");
		mv.setViewName("producao/projeto/ProjetoForm");
		return mv;
	}


	@ResponseBody  
	@RequestMapping(value="/buscarQuantidade", method = RequestMethod.GET)  
	public String buscarQuantidade(HttpServletRequest request, @ModelAttribute("projetoWrapper") ProjetoWrapper projetoWrapper) {  
		Integer quantidadeRestante = 0;
		try{
			if(!SimpleValidate.isNullOrBlank(request.getParameter("itemContrato").toString())){

				ItemContrato itemContrato = producaoService.buscarItemContratoPorId(Long.parseLong(request.getParameter("itemContrato").toString()));

				//Item Contrato Projeto
				ItemContratoProjeto itemContratoProjeto = new ItemContratoProjeto();
				itemContratoProjeto.setItemContrato(itemContrato);

				List<ItemContratoProjeto> listaItensContratoProjeto = itemContratoProjetoService.buscarItensContratoProjetoPorFiltro(itemContratoProjeto);

				if(!SimpleValidate.isNullOrEmpty(listaItensContratoProjeto)){
					quantidadeRestante = itemContrato.getQuantidade() - listaItensContratoProjeto.size();
				}else{
					projetoWrapper.setQuantidadeRestante(itemContrato.getQuantidade().toString());
					return projetoWrapper.getQuantidadeRestante();
				}
			}else{
				projetoWrapper.setQuantidadeRestante("");
				return "";
			}

		}catch(NoResultException e){
			//quando n�o houver resultado na busca deixar o campo vazio.
			projetoWrapper.setQuantidadeRestante("");
			return "";
		}
		catch(Exception e){
			e.printStackTrace();
		}
		projetoWrapper.setQuantidadeRestante(quantidadeRestante.toString());
		return projetoWrapper.getQuantidadeRestante();
	}
}
