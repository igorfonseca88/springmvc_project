package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Usuario;
 
/**
 * @author WILLIAM 
 */  
public interface UsuarioService {

	/**
	 * Salva um Usuario.
	 * @param usuario 
	 * @throws Exception
	 */
	Usuario salvarUsuario(Usuario usuario); 
	 
	/**
	 * Exclui um Usuario.
	 * @param usuario
	 * @throws Exception
	 */
	void excluirUsuario(Usuario usuario);
	
	/**
	 * Busca todas os usuarios.
	 * @return
	 */
	List<Usuario> buscarTodosUsuarios();
	
	/**
	 * Busca usu�rio por id
	 * @param id
	 * @return
	 */
	Usuario buscarPorId(Long id);

	/**
	 * Busca usuario por cpf informados
	 * @param cpf
	 * @return
	 */
	Usuario buscarUsuarioPorCpf(String cpf);
	
	/**
	 * Busca usuario por login
	 * @param login
	 * @return
	 */
	Usuario buscarUsuarioPorLogin(String login);
}



