package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.ItemTraco;
import br.com.concrelaje.sisproducao.entidades.Traco;
import br.com.concrelaje.sisproducao.entidades.wrapper.TracoMaterialWrapper;

@Controller
@RequestMapping(value = "/traco/tracoForm")
@SessionAttributes("tracoMaterialWrapper")
public class CadastrarTraco {  
 
	@Autowired
	private ProducaoService producaoService; 
	
	@Autowired 
	private EstoqueService estoqueService;

 
	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarTraco(Model model) {
		model.addAttribute(new TracoMaterialWrapper());
		return "producao/traco/TracoForm";
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST)
	public ModelAndView carregarCamposFormulario(@ModelAttribute("tracoMaterialWrapper") TracoMaterialWrapper tracoMaterialWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		mv.setViewName("producao/traco/TracoForm");
		return mv;
	}

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarTraco(@ModelAttribute("tracoMaterialWrapper") TracoMaterialWrapper tracoMaterialWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{

			Traco traco = producaoService.salvarTraco(tracoMaterialWrapper.getTraco());
			tracoMaterialWrapper.setTraco(traco);
			List<ItemTraco> itensTraco = producaoService.buscarItemTracoPorIdTraco(traco.getId()); 
    		tracoMaterialWrapper.setListaItemTraco(itensTraco);
			mv.addObject("tracoMaterialWrapper", tracoMaterialWrapper);
			//mv.addObject("listaMateriais", estoqueService.buscarTodosMateriais());
			mv.addObject("success", "Traço salvo com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "N�o foi poss�vel salvar o Traço.");
			e.printStackTrace();
		}

		mv.setViewName("producao/traco/TracoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirItem"})
	public ModelAndView incluirItem(@ModelAttribute("tracoMaterialWrapper") TracoMaterialWrapper tracoMaterialWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		try{
			
			ItemTraco item = tracoMaterialWrapper.getItemTraco();
			item.setTraco(tracoMaterialWrapper.getTraco());
			
			producaoService.salvarItemTraco(item);
			
			List<ItemTraco> itensTraco = producaoService.buscarItemTracoPorIdTraco(tracoMaterialWrapper.getTraco().getId()); 
    		tracoMaterialWrapper.setListaItemTraco(itensTraco);
    		tracoMaterialWrapper.setItemTraco(new ItemTraco());
    		//mv.addObject("listaMateriais", estoqueService.buscarTodosMateriais());
    		mv.addObject("tracoMaterialWrapper", tracoMaterialWrapper);
			
			mv.addObject("success", "Item adicionado com sucesso!");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível incluir um material.");
			e.printStackTrace();
		}

		mv.setViewName("producao/traco/TracoForm");
		return mv;
	}
	
	@RequestMapping(value = "/excluirItem", method= RequestMethod.GET)
	public ModelAndView excluirItem(@ModelAttribute("tracoMaterialWrapper") TracoMaterialWrapper tracoMaterialWrapper, ItemTraco itemTraco,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();

		try{
			if(!SimpleValidate.isNullOrEmpty(tracoMaterialWrapper.getListaItemTraco())){
				producaoService.excluirItemTraco(itemTraco);
				tracoMaterialWrapper.getListaItemTraco().remove(itemTraco);
			}
			tracoMaterialWrapper.setListaItemTraco(producaoService.buscarItemTracoPorIdTraco(tracoMaterialWrapper.getTraco().getId()));

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("producao/traco/TracoForm");
		return mv;
	}
}
