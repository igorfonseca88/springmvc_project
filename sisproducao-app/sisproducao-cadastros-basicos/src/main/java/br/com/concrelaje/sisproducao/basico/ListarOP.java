package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.wrapper.OPWrapper;

@Controller
@RequestMapping(value = "op/opList/**")
@SessionAttributes("opWrapper")
public class ListarOP {
    
	@Autowired
	private ProducaoService producaoService;
	
	@Autowired 
	private EstoqueService estoqueService;
	
	@Autowired 
	private DocumentoAnexoService documentoAnexoService;
	
	 @Autowired
     private MessageSource messageSource;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarOP(ModelMap modelMap) {
    	modelMap.addAttribute("listaSituacaoOPs", this.getListaSituacaoOPs());
        modelMap.addAttribute("listaOP", producaoService.buscarTodasOPs());
        modelMap.addAttribute("listaFases", producaoService.buscarTodasFases());
        return "producao/op/OPList";
    }
    
    @RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"pesquisar"})
    public ModelAndView pesquisar(@RequestParam("codigoPeca") String codigoPeca, 
    		@RequestParam("comboFases") String comboFases, 
    		@RequestParam("comboSituacao") EnumSituacaoOP comboSituacao, 
    		@RequestParam("sigla") String sigla,
    		@RequestParam("nomeProduto") String nomeProduto,
    		@RequestParam("nomeObra") String nomeObra,
    		
    		ModelMap modelMap) {
    	Long codPeca = null;
    	Long faseAtual = null;
    	String dimensao1 = "";
    	
    	ModelAndView mv = new ModelAndView();
    	if(codigoPeca != ""){
    		codPeca = new Long(codigoPeca);
    	}
    	
    	if(comboFases != ""){
    		faseAtual = new Long(comboFases);
    	}
    	
    	
    	if(SimpleValidate.isNullOrEmpty(producaoService.buscarOrdensProducaoPorFiltro(codPeca,faseAtual,comboSituacao, sigla, nomeProduto, nomeObra, dimensao1))){
    		mv.addObject("alert", "Pesquisa não trouxe resultados.");
    	}
    	
    	modelMap.addAttribute("listaOP", producaoService.buscarOrdensProducaoPorFiltro(codPeca,faseAtual,comboSituacao, sigla, nomeProduto, nomeObra, dimensao1));
    	mv.setViewName("producao/op/OPList");
        return mv;
    }
    
    @RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"limpar"})
    public String limparPesquisa(ModelMap modelMap) {
    	modelMap.addAttribute("listaSituacaoOPs", this.getListaSituacaoOPs());
        modelMap.addAttribute("listaOP", producaoService.buscarTodasOPs());
        modelMap.addAttribute("listaFases", producaoService.buscarTodasFases());
        return "producao/op/OPList";
    }
    
    @RequestMapping(value="/complementarOP", method = RequestMethod.GET)
    public String editar(OrdemProducao op, ModelMap modelMap) {
    	try{
    		OPWrapper opWrapper = new OPWrapper();
    		
    		
    		op = producaoService.buscarOrdemProducaoPorId(op.getId());
    		opWrapper.setOp(op);
    		opWrapper.setProducaoFases(producaoService.buscarOrdemProducaoFasePorOp(op));
    		
    		modelMap.addAttribute("opWrapper", opWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Não foi possível abrir esta OP para complementar dados.");
    	}
    	return "producao/op/ComplementarOPForm";
    }
    
    @RequestMapping(value="/visualizarOP", method = RequestMethod.GET)
    public String visualizarOp(ModelMap modelMap) {
    	try{
    		
    		String idPeca = (String) RequestContextHolder.currentRequestAttributes().getAttribute("idPecaSessao",RequestAttributes.SCOPE_SESSION);
    		OPWrapper opWrapper = new OPWrapper();
    		
    		
    		OrdemProducao op = producaoService.buscarOrdemProducaoPorIdPeca(new Long(idPeca));
    		opWrapper.setOp(op);
    		opWrapper.setProducaoFases(producaoService.buscarOrdemProducaoFasePorOp(op));
    		
    		modelMap.addAttribute("opWrapper", opWrapper);
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Não foi possível abrir esta OP para complementar dados.");
    	}
    	return "producao/op/ComplementarOPForm";
    }
    
    @RequestMapping(value="/excluir", method = RequestMethod.GET)
    public String excluir(OrdemProducao op, ModelMap modelMap) {
    	try {
    		
    		OrdemProducao ordem = producaoService.buscarOrdemProducaoPorId(op.getId());
    		
    		List<OrdemProducaoFase> ordemProducaoFases = producaoService.buscarOrdemProducaoFasePorOp(op);
    		
    		for(int i=0; i < ordemProducaoFases.size(); i++){
    			producaoService.excluirOrdemProducaoFase(ordemProducaoFases.get(i));
    		}
    		
			producaoService.excluirOrdemProducao(ordem);
			modelMap.addAttribute("success", "OP excluida com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			modelMap.addAttribute("error", "Não foi possível excluir esta OP.");
		}
    	
    	modelMap.addAttribute("listaOP", producaoService.buscarTodasOPs());
        modelMap.addAttribute("listaFases", producaoService.buscarTodasFases());
    	return "producao/op/OPList";
    }
  
    public EnumSituacaoOP[] getListaSituacaoOPs() {
		return EnumSituacaoOP.values();
	}
    
    @RequestMapping(value = "/download", method = RequestMethod.GET)
	public String download(HttpServletRequest request, HttpServletResponse response, Projeto projeto, ModelMap modelMap) throws Exception {
			 List<DocumentoAnexo> documentos = documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(projeto.getId());
			 DocumentoAnexo documentoAnexo = new DocumentoAnexo();
			 
			 for(int i=0; i<documentos.size(); i++){
				 if(documentos.get(i).getTipoDocumento().getDescricao().equals("Projeto")){
					 documentoAnexo = documentos.get(i);
					 break;
				 }
			 }
			 
			 if(documentoAnexo.getId() != null){
				 response.setContentType(documentoAnexo.getType());
				 response.setContentLength(documentoAnexo.getImagem().length);
				 response.setHeader("Content-Disposition","attachment; filename=\"" + documentoAnexo.getFilename() +"\"");
				
				FileCopyUtils.copy(documentoAnexo.getImagem(), response.getOutputStream());
				return null;
			 }
			 else{
				 modelMap.addAttribute("error", "Não foi possível o download pois não há um anexo do tipo Projeto.");
			 }
			
			 modelMap.addAttribute("listaOP", producaoService.buscarTodasOPs());
		     modelMap.addAttribute("listaFases", producaoService.buscarTodasFases());
			 return "producao/op/OPList";
    }
}
