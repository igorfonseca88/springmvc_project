package br.com.concrelaje.sisproducao.basico;


import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.DateFacility;
import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ItemContratoProjetoService;
import br.com.concrelaje.sisproducao.basico.service.QualidadeService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.EnumLegenda;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoAtivoInativo;
import br.com.concrelaje.sisproducao.entidades.Inspecao;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.ItemInspecao;
import br.com.concrelaje.sisproducao.entidades.RespostaInspecao;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.ChecklistWrapper;
import br.com.concrelaje.sisproducao.entidades.wrapper.InspecaoWrapper;



@Controller
@RequestMapping(value = "/qualidade/inspecaoForm/**")
@SessionAttributes("inspecaoWrapper")
public class CadastrarInspecao { 
	@Autowired
	private QualidadeService qualidadeService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ItemContratoProjetoService itemContratoProjetoService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		String idPeca = (String) RequestContextHolder.currentRequestAttributes().getAttribute("idPecaSessao",RequestAttributes.SCOPE_SESSION);
		InspecaoWrapper inspecaoWrapper = new InspecaoWrapper();
		if(!SimpleValidate.isNullOrBlank(idPeca)){
			ItemContratoProjeto peca = itemContratoProjetoService.buscarPorId(new Long(idPeca));
			inspecaoWrapper.setInspecao(new Inspecao());
			inspecaoWrapper.getInspecao().setPeca(peca);
		}
		inspecaoWrapper.setListaChecklist(qualidadeService.buscarTodosChecklistsAtivo());
		inspecaoWrapper.setListaUsuarios(usuarioService.buscarTodosUsuarios());
		model.addAttribute(inspecaoWrapper);
		
		return "qualidade/InspecaoForm";
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("inspecaoWrapper") InspecaoWrapper inspecaoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			inspecaoWrapper.getInspecao().setDataCadastro(DateFacility.formataData(inspecaoWrapper.getDataString()));
			
			String arrayHorInicial[] = inspecaoWrapper.getHorarioInicial()
					.split(":");
			String arrayHorFinal[] = inspecaoWrapper.getHorarioFinal().split(
					":");

			if (arrayHorInicial.length > 1) {
				
				if(new Integer(arrayHorInicial[0]) > 24 || new Integer(arrayHorInicial[1]) > 59){
					mv.addObject("error", "Horário inicial é inválido.");
					mv.setViewName("qualidade/InspecaoForm");
					return mv;
				}
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(inspecaoWrapper.getInspecao().getDataCadastro());
				cal.set(Calendar.HOUR_OF_DAY, new Integer(arrayHorInicial[0]));
				cal.set(Calendar.MINUTE, new Integer(arrayHorInicial[1]));
				cal.set(Calendar.SECOND, 0);
				inspecaoWrapper.getInspecao().setDataInicio(cal.getTime());
			}

			if (arrayHorFinal.length > 1) {
				
				if(new Integer(arrayHorFinal[0]) > 24 || new Integer(arrayHorFinal[1]) > 59){
					mv.addObject("error", "Horário final é inválido.");
					mv.setViewName("qualidade/InspecaoForm");
					return mv;
				}
				
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(inspecaoWrapper.getInspecao().getDataCadastro());
				cal2.set(Calendar.HOUR_OF_DAY, new Integer(arrayHorFinal[0]));
				cal2.set(Calendar.MINUTE, new Integer(arrayHorFinal[1]));
				cal2.set(Calendar.SECOND, 0);
				inspecaoWrapper.getInspecao().setDataFim(cal2.getTime());
			}
			
			if(!SimpleValidate.isNullOrBlank(validarCampos(inspecaoWrapper))){
				mv.addObject("error", validarCampos(inspecaoWrapper));
				mv.setViewName("qualidade/InspecaoForm");
				return mv;
			}
			
				
				ItemContratoProjeto peca = itemContratoProjetoService.buscarPorId(inspecaoWrapper.getInspecao().getPeca().getId());
				
				if(peca == null){
					mv.addObject("error", "Peça não encontrada.");
					mv.setViewName("qualidade/InspecaoForm");
					return mv;
				}
				
				inspecaoWrapper.getInspecao().setPeca(peca);
				
				
			//}
			
			inspecaoWrapper.setInspecao(qualidadeService.salvarInspecao(inspecaoWrapper.getInspecao()));
			
			// gera os registros para resposta
			
			if(SimpleValidate.isNullOrEmpty(qualidadeService.buscarRespostaInspecaoPorIdInspecao(inspecaoWrapper.getInspecao().getId()))){
			
				for (ItemInspecao itemInspecao : inspecaoWrapper.getInspecao()
						.getChecklist().getItensInspecao()) {
					if (EnumLogicoAtivoInativo.ATIVO.equals(itemInspecao
							.getSituacao())) {
						RespostaInspecao respostaInspecao = new RespostaInspecao();
						respostaInspecao.setInspecao(inspecaoWrapper
								.getInspecao());
						respostaInspecao.setItemInspecao(itemInspecao);
						qualidadeService
								.salvarRespostaInspecao(respostaInspecao);
					}
				}
			}
			
			inspecaoWrapper.setRespostaInspecaos(qualidadeService.buscarRespostaInspecaoPorIdInspecao(inspecaoWrapper.getInspecao().getId()));
				
			mv.addObject("inspecaoWrapper", inspecaoWrapper);
			mv.addObject("success", "Inspeção salva com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Não foi possível salvar o Inspeção.");
			e.printStackTrace();
		}

		mv.setViewName("qualidade/InspecaoForm");
		return mv;
	}
	
	public String validarCampos(InspecaoWrapper inspecaoWrapper){
		StringBuffer msg = new StringBuffer();
		if(inspecaoWrapper.getInspecao().getDataCadastro() == null){
			msg.append("Campo Data é obrigatório.");
		}
		else if(inspecaoWrapper.getInspecao().getUsuario() == null){
			msg.append("\n\n Campo Inspecionado por é obrigatório.");
		}
		else if(inspecaoWrapper.getInspecao().getDataInicio() == null){
			msg.append("\n\n Campo Hora início é obrigatório.");
		}
		else if(inspecaoWrapper.getInspecao().getChecklist() == null){
			msg.append("\n\n Campo Checklist é obrigatório.");
		}
		else if(inspecaoWrapper.getInspecao().getPeca().getId() == null){
			msg.append("\n\n Campo Peça por é obrigatório.");
		}
		else if(inspecaoWrapper.getInspecao().getGap() == null){
			msg.append("\n\n Campo GAP por é obrigatório.");
		}
		
		return msg.toString();
	}
	
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvarCheck"})
	public ModelAndView salvarCheck(@ModelAttribute("inspecaoWrapper") InspecaoWrapper inspecaoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			boolean existeItemComRestricao = false;
			boolean existeItemComRestricaoDataUsuario = false;
			for (int i = 0; i < inspecaoWrapper.getRespostaInspecaos2().length; i++) {
				
				// - Reprovado ou Aprovado após reinspeção – Descrição do problema obrigatório
				if(inspecaoWrapper.getRespostaInspecaos2()[i].getId() != null 
						&& ((EnumLegenda.REPROVADO.equals(inspecaoWrapper.getRespostaInspecaos2()[i].getLegenda()) 
								|| EnumLegenda.APROVADO_APOS_REINSPECAO.equals(inspecaoWrapper.getRespostaInspecaos2()[i].getLegenda())))){
					
					if(SimpleValidate.isNullOrBlank(inspecaoWrapper.getRespostaInspecaos2()[i].getDescricaoProblema())){
						existeItemComRestricao = true;
						break;
					}
				}
				
				if(inspecaoWrapper.getRespostaInspecaos2()[i].getId() != null 
						&& (EnumLegenda.APROVADO_APOS_REINSPECAO.equals(inspecaoWrapper.getRespostaInspecaos2()[i].getLegenda()))){
					
					if(SimpleValidate.isNullOrBlank(inspecaoWrapper.getRespostaInspecaos2()[i].getDataReinspecao()) || inspecaoWrapper.getRespostaInspecaos2()[i].getUsuario().getId() == null){
						existeItemComRestricaoDataUsuario = true;
						break;
					}
				}
			}
			
			//if(!existeItemComRestricao && !existeItemComRestricaoDataUsuario){
				for (int i = 0; i < inspecaoWrapper.getRespostaInspecaos2().length; i++) {
					
					if(inspecaoWrapper.getRespostaInspecaos2()[i].getId() != null){
						RespostaInspecao respostaInspecao = qualidadeService.buscarRespostaInspecaoPorId(inspecaoWrapper.getRespostaInspecaos2()[i].getId());
						respostaInspecao.setDescricaoProblema(inspecaoWrapper.getRespostaInspecaos2()[i].getDescricaoProblema());
						respostaInspecao.setLegenda(inspecaoWrapper.getRespostaInspecaos2()[i].getLegenda());
						respostaInspecao.setSolucaoProposta(inspecaoWrapper.getRespostaInspecaos2()[i].getSolucaoProposta());
						respostaInspecao.setReinspecao(inspecaoWrapper.getRespostaInspecaos2()[i].getReinspecao());
						respostaInspecao.setDataReinspecao(inspecaoWrapper.getRespostaInspecaos2()[i].getDataReinspecao());
						
						if(inspecaoWrapper.getRespostaInspecaos2()[i].getUsuario()!=null && inspecaoWrapper.getRespostaInspecaos2()[i].getUsuario().getId()!= null){
							Usuario usuario = usuarioService.buscarPorId(inspecaoWrapper.getRespostaInspecaos2()[i].getUsuario().getId());
							respostaInspecao.setUsuario(usuario);
						}
						qualidadeService.salvarRespostaInspecao(respostaInspecao);
					}
				}
				inspecaoWrapper.setRespostaInspecaos(qualidadeService.buscarRespostaInspecaoPorIdInspecao(inspecaoWrapper.getInspecao().getId()));
				
				
				mv.addObject("inspecaoWrapper", inspecaoWrapper);
				mv.addObject("success", "Inspeção salva com sucesso.");
			//}
			//else{
				if(existeItemComRestricao){
					mv.addObject("error", "O campo Descrição do Problema é obrigatório para os itens do checklist com respostas REPROVADO ou APROVADO_APOS_REINSPECAO.");
				}
				else if(existeItemComRestricaoDataUsuario){
					mv.addObject("error", "Os campos Data da Reinspeção e Responsável são obrigatórios para os itens do checklist com resposta APROVADO_APOS_REINSPECAO.");
				}
				
			//}

		}catch(Exception e){
			mv.addObject("error", "Não foi possível salvar o Inspeção.");
			e.printStackTrace();
		}

		mv.setViewName("qualidade/InspecaoForm");
		return mv;
	}
	

	@RequestMapping(value = "/excluirItem")
	public ModelAndView excluirItem(@ModelAttribute("checklistWrapper") ChecklistWrapper checklistWrapper, ItemInspecao itemInspecao,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		try{
			if(!SimpleValidate.isNullOrEmpty(checklistWrapper.getChecklist().getItensInspecao())){
				
				for (ItemInspecao item : checklistWrapper.getChecklist().getItensInspecao()) {
					if(item.getId() == itemInspecao.getId()){
						checklistWrapper.getChecklist().getItensInspecao().remove(item);
					}
				}
					
				checklistWrapper.setChecklist(qualidadeService.salvarChecklist(checklistWrapper.getChecklist()));
			}
			
			mv.addObject("checklistWrapper", checklistWrapper);

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("qualidade/ChecklistForm");
		return mv;
	}
}
