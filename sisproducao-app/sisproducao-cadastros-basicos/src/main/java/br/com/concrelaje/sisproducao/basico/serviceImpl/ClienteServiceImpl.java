package br.com.concrelaje.sisproducao.basico.serviceImpl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.entidades.Cliente;

@Service("clienteService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class ClienteServiceImpl implements ClienteService{
    
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public Cliente salvarCliente(Cliente cliente) {
		if(cliente.getPessoa().getId() == null){
			cliente.setDataCadastro(new Date());
		}
        return entityManager.merge(cliente);
    }

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void excluirCliente(Cliente cliente) {
        entityManager.remove(entityManager.merge(cliente));
    }
	
	@SuppressWarnings("unchecked")
	@Override
    public List<Cliente> buscarTodosClientes() {
        return entityManager.createQuery("FROM Cliente").getResultList();
    }
	
	@Override
	public Cliente buscarPorId(Long id) {
		Query query = entityManager.createQuery("select c from Cliente c where c.id = :id");
		query.setParameter("id", id);
		return (Cliente) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> buscarClientePorNome(String nome) {
		StringBuilder sql = new StringBuilder("");
		sql.append(" SELECT c FROM Cliente c WHERE 1=1  ");
		if(!SimpleValidate.isNullOrBlank(nome)){
			sql.append(" AND ( c.pessoa.id in ( select pf.id from PessoaFisica pf where pf.nome = :nome ) OR c.pessoa.id in ( select pj.id from PessoaJuridica pj where pj.razaoSocial = :nome ) ) ");
		}
		Query query = entityManager.createQuery(sql.toString());
		if(!SimpleValidate.isNullOrBlank(nome)){
			query.setParameter("nome", nome);
		}
		return query.getResultList();
	}
	
}

