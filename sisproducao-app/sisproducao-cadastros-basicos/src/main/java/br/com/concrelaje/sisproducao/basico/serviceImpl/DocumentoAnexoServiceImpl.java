package br.com.concrelaje.sisproducao.basico.serviceImpl;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;

@Service("documentoAnexoService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class DocumentoAnexoServiceImpl implements DocumentoAnexoService{
    
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvarDocumentoAnexo(DocumentoAnexo documento) {
		entityManager.merge(documento);
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirDocumento(DocumentoAnexo documento) {
		entityManager.remove(entityManager.merge(documento));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoAnexo> buscarDocumentosAnexoPorIdContrato(Long id) {
		Query query =  entityManager.createQuery("SELECT da FROM DocumentoAnexo da WHERE da.contrato.id = :id");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoAnexo> buscarDocumentosAnexoPorIdProjeto(Long id) {
		Query query =  entityManager.createQuery("SELECT da FROM DocumentoAnexo da WHERE da.projeto.id = :id order by da.id desc");
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public DocumentoAnexo buscarPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT da FROM DocumentoAnexo da WHERE da.id = :id");
		query.setParameter("id", id);
		return (DocumentoAnexo) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TipoDocumento> buscarTodosTiposDocumento() {
		Query query =  entityManager.createQuery("SELECT td FROM TipoDocumento td ");
		return query.getResultList();
	}

	@Override
	public TipoDocumento buscarTipoDocumentoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT td FROM TipoDocumento td WHERE td.id = :id");
		query.setParameter("id", id);
		return (TipoDocumento) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvarTipoDocumento(TipoDocumento tipoDocumento) {
		entityManager.merge(tipoDocumento);
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirTipoDocumento(TipoDocumento tipoDocumento) {
		entityManager.remove(entityManager.merge(tipoDocumento));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoAnexo> buscarDocumentosAnexoPorIdOrdemCarregamento(Long id) {
		Query query =  entityManager.createQuery("SELECT da FROM DocumentoAnexo da WHERE da.ordemCarregamento.id = :id");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoAnexo> buscarDocumentosAnexoPorIdPedido(Long id) {
		Query query =  entityManager.createQuery("SELECT da FROM DocumentoAnexo da WHERE da.pedido.id = :id");
		query.setParameter("id", id);
		return query.getResultList();
	}
}