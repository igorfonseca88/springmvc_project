package br.com.concrelaje.sisproducao.basico.serviceImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.PessoaService;
import br.com.concrelaje.sisproducao.entidades.EnumTipoPessoa;
import br.com.concrelaje.sisproducao.entidades.Pessoa;

@Service("pessoaService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class PessoaServiceImpl implements PessoaService{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Pessoa salvar(Pessoa pessoa) {
		return entityManager.merge(pessoa);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluir(Pessoa pessoa) {
		entityManager.remove(entityManager.merge(pessoa));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pessoa> buscarTodos() {
		return entityManager.createQuery("FROM Pessoa ").getResultList();
	}

	@Override
	public Pessoa buscarPessoaPorCpfCnpj(String cpfCnpj, EnumTipoPessoa tipoPessoa) {

		try{
			StringBuilder hql = new StringBuilder("");

			if(EnumTipoPessoa.FISICA.equals(tipoPessoa)){
				hql.append("select pf from PessoaFisica pf where pf.cpf = :cpfCnpj ");
			}
			else{
				hql.append("select pj from PessoaJuridica pj where pj.cnpj = :cpfCnpj ");
			}
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("cpfCnpj", cpfCnpj);
			return (Pessoa) query.getSingleResult();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Pessoa buscarPessoaComTelefonePorIdPessoa(Long idPessoa) {

		try{
			StringBuilder hql = new StringBuilder("");

			hql.append("select p from Pessoa p left join fetch p.telefones where p.id = :idPessoa ");
			
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("idPessoa", idPessoa);
			return (Pessoa) query.getSingleResult();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Pessoa buscarPessoaComEnderecoPorIdPessoa(Long idPessoa) {

		try{
			StringBuilder hql = new StringBuilder("");

			hql.append("select p from Pessoa p left join fetch p.enderecos where p.id = :idPessoa ");
			
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("idPessoa", idPessoa);
			return (Pessoa) query.getSingleResult();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
