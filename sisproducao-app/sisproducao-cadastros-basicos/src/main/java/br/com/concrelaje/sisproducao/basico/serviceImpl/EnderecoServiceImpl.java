package br.com.concrelaje.sisproducao.basico.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.EnderecoService;
import br.com.concrelaje.sisproducao.entidades.Endereco;

@Service("enderecoService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class EnderecoServiceImpl implements EnderecoService{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluir(Endereco endereco) {
		entityManager.remove(entityManager.merge(endereco));
	}
}
