package br.com.concrelaje.sisproducao.basico.control.util;


import java.util.Collection;

/**
 * Class SimpleValidate. <br/>
 * Realiza validacoes simples e recorrentes na programacao Java
 * 
 * @autor : wsilva
 */ 
public class SimpleValidate {   
    
    /**
     * Devolve true se a string recebida e nulla ou vazia.<br>
     * Note para saber se a string e valida basta negar a chamada da funcao.
     * 
     * @param str
     * @return boolean - true se a string e nula ou vazia e false caso contrario.
     */
    public static boolean isNullOrBlank(String str) {
        
        if (str != null) {
            str = str.trim();
        }
        
        return (str == null || "".equals(str) || "null".equals(str));
    }
    
    /**
     * Devolve true se a Collection recebida e nulla ou vazia.<br>
     * Note para saber se a Collection e valida basta negar a chamada da funcao.
     * 
     * @param collection - collection
     * @return boolean - true se a Collection e nula ou vazia e false caso contrario.
     */
    @SuppressWarnings( "rawtypes" )
    public static boolean isNullOrEmpty(Collection collection) {        
        return (collection == null || collection.isEmpty());        
    }
}
