package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoAtivoInativo;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;



@Controller
@RequestMapping(value = "/basico/tipoDocumentoForm/**")
public class CadastrarTipoDocumento {
	@Autowired
	private DocumentoAnexoService anexoService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrar(Model model) {
		model.addAttribute(new TipoDocumento());
		return "basico/TipoDocumentoForm";
	}
	
	@RequestMapping(value="/acoesForm", method = RequestMethod.POST, params={"salvar"})
	public ModelAndView salvar(@ModelAttribute("tipoDocumento") TipoDocumento tipoDocumento, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			anexoService.salvarTipoDocumento(tipoDocumento);

			mv.setViewName("basico/TipoDocumentoForm");
			mv.addObject("success", "Tipo de Documento cadastrado com sucesso.");

		}catch(Exception e){
			mv.addObject("error", "Nao foi possvel salvar.");
			e.printStackTrace();
		}

		return mv;
	}
	
	public EnumLogicoAtivoInativo[] getListaAtivoInativos() {
		return EnumLogicoAtivoInativo.values();
	}


}
