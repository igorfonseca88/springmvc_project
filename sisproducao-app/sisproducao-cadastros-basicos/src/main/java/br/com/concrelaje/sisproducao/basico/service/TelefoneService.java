package br.com.concrelaje.sisproducao.basico.service;

import br.com.concrelaje.sisproducao.entidades.Telefone;

/**
 * @author WILLIAM
 */
public interface TelefoneService {

	/**
	 * Exclui telefone.
	 * @return
	 */
	void excluir(Telefone telefone); 
	 
	/**
	 * Busca telefone por id
	 * @param id
	 * @return
	 */
	
	Telefone buscarPorId(Long id);

}



