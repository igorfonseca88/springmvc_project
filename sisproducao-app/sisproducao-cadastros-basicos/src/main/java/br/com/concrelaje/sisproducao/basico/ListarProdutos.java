package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.ProjetoService;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoSimNao;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoProjeto;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.Setor;
import br.com.concrelaje.sisproducao.entidades.view.ProjetoView;
import br.com.concrelaje.sisproducao.entidades.wrapper.ProjetoWrapper;

@Controller
@RequestMapping(value = "produto/produtoList/**")
@SessionAttributes("projetoWrapper")
public class ListarProdutos {

	@Autowired
	private ProducaoService producaoService;

	@Autowired
	private ProjetoService projetoService;

	@Autowired
	private DocumentoAnexoService documentoAnexoService;
	
	@Autowired
	private EstoqueService estoqueService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String listarProdutos(ModelMap modelMap) {
		modelMap.addAttribute("listaProdutos", producaoService.buscarTodosProdutos());
		return "producao/produto/ProdutoList";
	}

	@RequestMapping(value="/editar", method = RequestMethod.GET)
	public String editar(@RequestParam("id") String id, ModelMap modelMap) {
		try{
			Produto produto = producaoService.buscarProdutoPorId(new Long(id));
			modelMap.addAttribute("produto", produto);
			modelMap.addAttribute("listaSetor", getListaSetor());
		}catch(Exception e){
			e.printStackTrace();
			modelMap.addAttribute("error", "Erro ao buscar produto.");
		}
		return "producao/produto/ProdutoForm";
	}

	@RequestMapping(value="/excluir", method = RequestMethod.GET)
	public String excluir(@RequestParam("id") String id, ModelMap modelMap) {
		try{
			Produto produto = producaoService.buscarProdutoPorId(new Long(id));

			try{

				//verifica se o produto est� associado a um contrato caso esteja n�o poder� ser excluido
				if(producaoService.verificarProdutoUtilizado(produto)){    			
					producaoService.excluirProduto(produto);
					modelMap.addAttribute("success", "Produto removido com sucesso.");
				}
				else{
					modelMap.addAttribute("error", "Produto nao pode ser removido pois ja esta associado a um contrato.");
				}

			}catch(Exception e){
				e.printStackTrace();
				modelMap.addAttribute("error", "Erro ao excluir produto.");
			}

			return listarProdutos(modelMap);


		}catch(Exception e){
			e.printStackTrace();
			modelMap.addAttribute("error", "Erro ao excluir produto.");
		}
		return "producao/produto/ProdutoForm";
	}

	@RequestMapping(value="/complementarProjeto", method = RequestMethod.GET)
	public String complementarProjeto(@RequestParam("id") String id, ModelMap modelMap) {
		try{
			Produto produto = producaoService.buscarProdutoPorId(new Long(id));
			if(produto.getProjeto() == null ){
			 produto =	gerarProjetoProduto(produto);
			}
			
			ProjetoWrapper projetoWrapper = new ProjetoWrapper();
			ProjetoView projetoView = new ProjetoView();
			Long idProjeto = produto.getProjeto().getId();

			Projeto projeto = projetoService.buscarProjetoComAcessoriosPorIdProjeto(idProjeto);
			projetoView = projetoService.buscarProjetoViewPorIdProjeto(idProjeto);

			if(projeto.getRevisao() == 0){
				projeto.setRevisao(1);
			}
			
			projetoWrapper.setProjeto(projeto);
			projetoWrapper.setProjetoView(projetoView);

			projetoWrapper.setCountItensContratoProjeto(1);
			projetoWrapper.setHabilitarAprovarReprovar(projeto.getSituacao() != null && projeto.getSituacao().equals(EnumSituacaoProjeto.AGUARDANDO_APROVACAO));
			projetoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdProjeto(projetoWrapper.getProjeto().getId()));
			projetoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
			
			if(produto.getTipo().equals(EnumTipoProduto.PADRAO)){
				projetoWrapper.setVisualizarAcessorios(false);
			}
			modelMap.addAttribute("listaTracos", producaoService.buscarTodosTracos());
			modelMap.addAttribute("listaProdutos", getListaProdutos());
			modelMap.addAttribute("listaMateriaisAco", getListaMateriaisAco());
    		modelMap.addAttribute("listaMateriais", getListaMateriais());
			modelMap.addAttribute("projetoWrapper", projetoWrapper);
		}catch(Exception e){
			e.printStackTrace();
			modelMap.addAttribute("error", "Nao foi possivel abrir este projeto para complementar dados.");
		}
		return "producao/projeto/ComplementarProjetoForm";
	}
	
	
	public Produto gerarProjetoProduto (Produto produto) {
		Projeto projeto = new Projeto();
		projeto = projetoService.salvarProjeto(projeto);
		produto.setProjeto(projeto);

		produto = producaoService.salvarProduto(produto);

		return produto;
	}

	public List<Produto> getListaProdutos(){
		return producaoService.buscarProdutosPorTipo(EnumTipoProduto.PADRAO);
	}
	
	public List<Material> getListaMateriaisAco(){
		return estoqueService.buscarMateriaisAco(EnumLogicoSimNao.S);
	}
	
	public List<Material> getListaMateriais(){
		return estoqueService.buscarMateriaisAco(EnumLogicoSimNao.N);
	}
	
	public List<Setor> getListaSetor(){
		return estoqueService.buscarTodosSetores();
	}

}
