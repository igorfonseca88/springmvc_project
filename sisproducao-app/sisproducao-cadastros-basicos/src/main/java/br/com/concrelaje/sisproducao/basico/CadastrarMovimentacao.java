package br.com.concrelaje.sisproducao.basico;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoMovimentacao;
import br.com.concrelaje.sisproducao.entidades.EnumModalidade;
import br.com.concrelaje.sisproducao.entidades.EnumTipoMovimentacao;
import br.com.concrelaje.sisproducao.entidades.ItemMovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Usuario;
import br.com.concrelaje.sisproducao.entidades.wrapper.MovimentacaoWrapper;



@Controller
@RequestMapping(value = "/material/movimentacaoForm/**")
@SessionAttributes("movimentacaoWrapper")
public class CadastrarMovimentacao {
	@Autowired
	private ProducaoService producaoService; 
	
	@Autowired
	private EstoqueService estoqueService;
	
	@Autowired
	private UsuarioService usuarioService;


	@RequestMapping(value="/init", method = RequestMethod.GET)
	public String cadastrarMovimentacao(Model model) {
		MovimentacaoWrapper movimentacaoWrapper = new MovimentacaoWrapper();
		
		MovimentacaoEstoque movimentacaoEstoque = new MovimentacaoEstoque();
		movimentacaoEstoque.setDataCadastro(new Date());
		movimentacaoEstoque.setSituacao(EnumSituacaoMovimentacao.EM_ELABORACAO);
		Usuario usuario = usuarioService.buscarUsuarioPorLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		movimentacaoEstoque.setUsuario(usuario);
		
		movimentacaoWrapper.setMovimentacaoEstoque(movimentacaoEstoque);
		model.addAttribute(movimentacaoWrapper);
		model.addAttribute("listaTipoMovimentacao", getListaTipoMovimentacao());
		model.addAttribute("listaModalidade", getListaModalidade());
		return "producao/material/MovimentacaoForm";
	}
	

	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"salvar"})
	public ModelAndView salvarMovimentacao(@ModelAttribute("movimentacaoWrapper") MovimentacaoWrapper movimentacaoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		try{
			
			MovimentacaoEstoque estoque = estoqueService.salvarMovimentacao(movimentacaoWrapper.getMovimentacaoEstoque());
			
			movimentacaoWrapper.setMovimentacaoEstoque(estoque);
			mv.addObject("listaMateriais", estoqueService.buscarTodosMateriais());
			mv.setViewName("producao/material/MovimentacaoForm");
			mv.addObject("success", "Movimentação cadastrado com sucesso.");
			mv.addObject(movimentacaoWrapper);

		}catch(Exception e){
			mv.addObject("error", "Nao foi possvel salvar o material.");
			e.printStackTrace();
		}

		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"incluirItem"})
	public ModelAndView incluirItem(@ModelAttribute("movimentacaoWrapper") MovimentacaoWrapper movimentacaoWrapper, BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		
		
		try{
			if(SimpleValidate.isNullOrBlank(movimentacaoWrapper.getItemMovimentacaoEstoque().getFornecedor())){
				mv.addObject("alert", "O fornecedor é obrigatório.");
				mv.setViewName("producao/material/MovimentacaoForm");
				return mv;
			}
			if(movimentacaoWrapper.getItemMovimentacaoEstoque().getQuantidade().compareTo(BigDecimal.ZERO) <= 0){
				mv.addObject("alert", "A quantidade deve ser maior que Zero.");
				mv.setViewName("producao/material/MovimentacaoForm");
				return mv;
			}
			
			if(!SimpleValidate.isNullOrEmpty(movimentacaoWrapper.getListaItemEstoques()) && movimentacaoWrapper.getListaItemEstoques().contains(movimentacaoWrapper.getItemMovimentacaoEstoque())){
				mv.addObject("alert", "O material já foi inserido.");
				mv.setViewName("producao/material/MovimentacaoForm");
				return mv;
			}
			
			ItemMovimentacaoEstoque item = movimentacaoWrapper.getItemMovimentacaoEstoque();
			item.setMovimentacaoEstoque(movimentacaoWrapper.getMovimentacaoEstoque());
			item.setMaterial(movimentacaoWrapper.getMaterial());
			
			estoqueService.salvarItemMovimentacaoEstoque(item);
			
			
			
			List<ItemMovimentacaoEstoque> itens = estoqueService.buscarItensMovimentacaoEstoquePorIdMovimentacaoEstoque(movimentacaoWrapper.getMovimentacaoEstoque().getId()); 
			movimentacaoWrapper.setListaItemEstoques(itens);
			movimentacaoWrapper.setMaterial(new Material());
			movimentacaoWrapper.setItemMovimentacaoEstoque(new ItemMovimentacaoEstoque());
			
			mv.addObject("movimentacaoWrapper", movimentacaoWrapper);
			mv.addObject("listaMateriais", estoqueService.buscarTodosMateriais());
			mv.addObject("success", "Item adicionado com sucesso!");
			

		}catch(Exception e){
			mv.addObject("error", "Não foi possível incluir o material.");
			e.printStackTrace();
		}

		mv.setViewName("producao/material/MovimentacaoForm");
		return mv;
	}
	
	@RequestMapping(value = "/acoesForm", method = RequestMethod.POST,  params={"finalizar"})
	public String finalizar(@ModelAttribute("movimentacaoWrapper") MovimentacaoWrapper movimentacaoWrapper,@ModelAttribute("id") String idMaterial,	ModelMap modelMap) {

		
		for (ItemMovimentacaoEstoque item : movimentacaoWrapper.getListaItemEstoques()) {
			// SALVA O ESTOQUE NO MATERIAL
			Material materialEstoque = estoqueService.buscarMaterialPorId(item.getMaterial().getId());
			
			BigDecimal estoqueAnterior;
			
			if(materialEstoque.getEstoque() == null){
				estoqueAnterior = new BigDecimal("0");
			}
			else{
				estoqueAnterior = materialEstoque.getEstoque();
			}
			 
			BigDecimal estoqueAtual = new BigDecimal("0");
			
			if(movimentacaoWrapper.getMovimentacaoEstoque().getTipoMovimentacao().equals(EnumTipoMovimentacao.ENTRADA)){
				estoqueAtual = estoqueAnterior.add(item.getQuantidade());
			}
			else{
				estoqueAtual = estoqueAnterior.subtract(item.getQuantidade());
			}
			
			materialEstoque.setEstoque(estoqueAtual);
			estoqueService.salvarMaterial(materialEstoque);
		}
		
		MovimentacaoEstoque movimentacaoEstoque = movimentacaoWrapper.getMovimentacaoEstoque();
		movimentacaoEstoque.setSituacao(EnumSituacaoMovimentacao.FINALIZADA);
		movimentacaoEstoque.setDataFinalizacao(new Date());
		
		movimentacaoEstoque = estoqueService.salvarMovimentacao(movimentacaoEstoque);
		
		movimentacaoWrapper.setMovimentacaoEstoque(movimentacaoEstoque);
		
		return "producao/material/MovimentacaoForm";
	}
	
		
	@RequestMapping(method = RequestMethod.GET)
	public String selecionarMaterial(@ModelAttribute("movimentacaoWrapper") MovimentacaoWrapper movimentacaoWrapper,@ModelAttribute("id") String idMaterial,	ModelMap modelMap) {

		Material material = estoqueService.buscarMaterialPorId(new Long(idMaterial));
		movimentacaoWrapper.setMaterial(material);
		modelMap.addAttribute(movimentacaoWrapper);
		return "producao/material/MovimentacaoForm";
	}
	
	@RequestMapping(value = "/excluirItem")
	public ModelAndView excluirItem(@ModelAttribute("movimentacaoWrapper") MovimentacaoWrapper movimentacaoWrapper, ItemMovimentacaoEstoque itemMovimentacaoEstoque,  BindingResult bindingResult) {

		ModelAndView mv = new ModelAndView();
		MovimentacaoEstoque movimentacaoEstoque = (MovimentacaoEstoque) estoqueService.buscarMovimentacaoEstoqueComItensPorIdMovimentacaoEstoque(movimentacaoWrapper.getMovimentacaoEstoque().getId());

		try{
			if(!SimpleValidate.isNullOrEmpty(movimentacaoEstoque.getItensMovimentacaoEstoque())){
				estoqueService.excluirItemMovimentacaoEstoque(itemMovimentacaoEstoque);
				movimentacaoEstoque.getItensMovimentacaoEstoque().remove(itemMovimentacaoEstoque);
			}
			movimentacaoWrapper.setListaItemEstoques(movimentacaoEstoque.getItensMovimentacaoEstoque());

			mv.addObject("success", "Item excluido com sucesso!");
		}catch(Exception e){
			mv.addObject("error", "Não foi possível excluir o item.");
			e.printStackTrace();
		}
		mv.setViewName("producao/material/MovimentacaoForm");
		return mv;
	}
	
	public EnumTipoMovimentacao[] getListaTipoMovimentacao() {
		return EnumTipoMovimentacao.values();
	}

	public EnumModalidade[] getListaModalidade() {
		return EnumModalidade.values();
	}
}
