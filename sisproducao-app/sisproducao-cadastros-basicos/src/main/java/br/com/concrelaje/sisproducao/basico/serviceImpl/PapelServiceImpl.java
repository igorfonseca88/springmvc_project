package br.com.concrelaje.sisproducao.basico.serviceImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.PapelService;
import br.com.concrelaje.sisproducao.entidades.Papel;

@Service("papelService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class PapelServiceImpl implements PapelService{
    
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public Papel buscarPorId(Long id) {
		Query query = entityManager.createQuery("select p from Papel p where p.id = :id");
		query.setParameter("id", id);
		return (Papel) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Papel> buscarTodosPapeis() {
		return entityManager.createQuery("FROM Papel").getResultList();
	}
}

