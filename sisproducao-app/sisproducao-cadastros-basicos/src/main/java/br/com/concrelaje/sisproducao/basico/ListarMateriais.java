package br.com.concrelaje.sisproducao.basico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoSimNao;
import br.com.concrelaje.sisproducao.entidades.EnumUnidade;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Setor;

@Controller
@RequestMapping(value = "material/materialList/**")
public class ListarMateriais { 
    
	 @Autowired
     private EstoqueService estoqueService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarMateriais(ModelMap modelMap) {
        modelMap.addAttribute("listaMateriais", estoqueService.buscarTodosMateriais());
        return "producao/material/MaterialList";
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		Material material = estoqueService.buscarMaterialPorId(new Long(id));
    		
    		if(material.getSetor() == null){
    			Setor setor = new Setor();
    			material.setSetor(setor);
    		}
    		modelMap.addAttribute("material", material);
    		modelMap.addAttribute("listaSetor", getListaSetor());
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Erro ao buscar material.");
    	}
    	return "producao/material/MaterialForm";
    }
    
    @RequestMapping(value="/excluir")
    public String excluir(Material material, ModelMap modelMap) {
    	try{
    		
    		material = estoqueService.buscarMaterialPorId(material.getId());
    		estoqueService.excluirMaterial(material);
    		
    		modelMap.addAttribute("success", "Material excluído com sucesso!");
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Não foi possível excluir o material!");
    	}
    	
    	return listarMateriais(modelMap);
    }
    
    public EnumUnidade[] getListaUnidadesMaterial() {
		return EnumUnidade.values();
	}
    
    public EnumLogicoSimNao[] getListaEnumLogicoSimNao() {
		return EnumLogicoSimNao.values();
	}
    
    public List<Setor> getListaSetor(){
		return estoqueService.buscarTodosSetores();
	}
    
        
}
