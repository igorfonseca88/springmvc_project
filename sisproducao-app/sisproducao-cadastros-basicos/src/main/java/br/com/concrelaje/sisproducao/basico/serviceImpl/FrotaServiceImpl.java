package br.com.concrelaje.sisproducao.basico.serviceImpl;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.FrotaService;
import br.com.concrelaje.sisproducao.entidades.Frota;
import br.com.concrelaje.sisproducao.entidades.Motorista;

@Service("frotaService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class FrotaServiceImpl implements FrotaService{
    
	@PersistenceContext
    private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Frota> buscarTodasFrotas() {
		Query query =  entityManager.createQuery("SELECT fr FROM Frota fr ORDER BY fr.veiculo ");
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Frota salvarFrota(Frota frota) {
		return entityManager.merge(frota);
	}

	@Override
	public Frota buscarFrotaPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT f FROM Frota f WHERE f.id = :id");
		query.setParameter("id", id);
		return (Frota) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirFrota(Frota frota) {
		entityManager.remove(entityManager.merge(frota));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Motorista> buscarTodosMotoristas() {
		Query query =  entityManager.createQuery("SELECT m FROM Motorista m ORDER BY m.motorista ");
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Motorista salvarMotorista(Motorista motorista) {
		return entityManager.merge(motorista);
	}

	@Override
	public Motorista buscarMotoristaPorId(Long idMotorista) {
		Query query =  entityManager.createQuery("SELECT m FROM Motorista m WHERE m.id = :id");
		query.setParameter("id", idMotorista);
		return (Motorista) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirMotorista(Motorista motorista) {
		entityManager.remove(entityManager.merge(motorista));		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Frota> buscarFrotasDisponiveisParaOrdemCarregamento(Long idOrdemCarregamento) {
		Query query =  entityManager.createQuery("SELECT f FROM Frota f WHERE f.id not in (select ov.frota.id FROM OrdemCarregamentoFrota ov WHERE ov.ordemCarregamento.id = :id ) ORDER BY f.veiculo ");
		query.setParameter("id", idOrdemCarregamento);
		return query.getResultList();
	}

	
}

