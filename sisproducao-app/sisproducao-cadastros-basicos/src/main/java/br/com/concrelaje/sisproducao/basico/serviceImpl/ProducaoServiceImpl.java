package br.com.concrelaje.sisproducao.basico.serviceImpl;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.control.util.DateFacility;
import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoOP;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.FaseProducao;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.ItemOrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.ItemPedido;
import br.com.concrelaje.sisproducao.entidades.ItemTraco;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MaterialProj;
import br.com.concrelaje.sisproducao.entidades.MaterialProjeto;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamentoFrota;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.ProdutoProjeto;
import br.com.concrelaje.sisproducao.entidades.Traco;
import br.com.concrelaje.sisproducao.entidades.view.ContratoView;

@Service("producaoService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class ProducaoServiceImpl implements ProducaoService{

	@PersistenceContext 
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Produto salvarProduto(Produto produto) {
		if(produto.getId() == null ){
			produto.setDataCadastro(new Date());
		}
		return entityManager.merge(produto);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> buscarTodosProdutos() {
		return entityManager.createQuery("FROM Produto p ORDER BY p.nome ").getResultList();
	}

	@Override
	public Produto buscarProdutoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT p FROM Produto p WHERE p.id = :id");
		query.setParameter("id", id);
		return (Produto) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirProduto(Produto produto) {
		entityManager.remove(entityManager.merge(produto));
	}
	
	@Override
	public boolean verificarProdutoUtilizado(Produto produto) {
		Query query =  entityManager.createQuery("SELECT ic FROM ItemContrato ic WHERE ic.produto.id not in (select p.id from Produto p where p.id =  :id)");
		query.setParameter("id", produto.getId());
		
		
		if(SimpleValidate.isNullOrEmpty(query.getResultList())){
			return true;
		}
		
		return false;
	}

	/**
	 * Mï¿½todos do cotrato 
	 */ 

	@SuppressWarnings("unchecked")
	@Override
	public List<ContratoView> buscarTodosContratos() {
		return entityManager.createQuery("Select v FROM ContratoView v").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContratoView> buscarContratosPorFiltros(String numero) {

		StringBuffer sql = new StringBuffer();

		sql.append(" Select v FROM ContratoView v WHERE 1=1 ");

		if(numero != ""){
			sql.append(" AND v.numeroObjeto like :numero ");
		}

		Query query =  entityManager.createQuery(sql.toString());

		if(numero != ""){
			query.setParameter("numero", "%"+numero+"%");
		}
		return query.getResultList();
	}

	@Override
	public Contrato buscarContratoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT c FROM Contrato c left join fetch c.itensContrato WHERE c.id = :id");
		query.setParameter("id", id);
		return (Contrato) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Contrato salvarContrato(Contrato contrato) {
		return entityManager.merge(contrato);
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirContrato(Contrato contrato) {
		entityManager.remove(entityManager.merge(contrato));
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvarItemContrato(ItemContrato item) {
		entityManager.merge(item);		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemContrato> buscarItensContratoPorIdContrato(Long id) {
		Query query =  entityManager.createQuery("SELECT ic FROM ItemContrato ic WHERE ic.contrato.id = :id " +
				" AND ic.produto is not null AND ic.produto.tipo = 'OBRA' AND ic.contrato.situacao = 'FINALIZADO' ");
		query.setParameter("id", id);
		return  query.getResultList();
	}

	

	@SuppressWarnings("unchecked")
	@Override
	public List<Traco> buscarTodosTracos() {
		return entityManager.createQuery("Select t FROM Traco t ORDER BY t.nome").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Traco> buscarTodosTracosAtivos() {
		return entityManager.createQuery("Select t FROM Traco t WHERE t.situacao = 'ATIVO' ORDER BY t.nome").getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Traco salvarTraco(Traco traco) {
		return entityManager.merge(traco);
	}

	@Override
	public Traco buscarTracoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT t FROM Traco t WHERE t.id = :id");
		query.setParameter("id", id);
		return (Traco) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemTraco> buscarItemTracoPorIdTraco(Long id) {
		Query query =  entityManager.createQuery("SELECT t FROM ItemTraco t WHERE t.traco.id = :id");
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvarItemTraco(ItemTraco itemTraco) {
		entityManager.merge(itemTraco);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirTraco(Traco traco) {
		entityManager.remove(entityManager.merge(traco));
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirItemTraco(ItemTraco itemTraco) {
		entityManager.remove(entityManager.merge(itemTraco));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Contrato> buscarContratosPorFiltros(Contrato contrato){

		StringBuilder hql = new StringBuilder();
		HashMap<String, Object> listaParametros = new HashMap<String, Object>();  

		hql.append(" select c from Contrato c fetch all properties where 1 = 1 ");

		if(!SimpleValidate.isNullOrBlank(contrato.getNomeObra())){
			hql.append(" and c.nomeObra like :nomeObra ");
			listaParametros.put("nomeObra", "%"+contrato.getNomeObra()+"%");
		}

		if(!SimpleValidate.isNullOrBlank(contrato.getNumero())){
			hql.append(" and c.numero like :numeroContrato ");
			listaParametros.put("numeroContrato", "%"+contrato.getNumero()+"%");
		}

		Query query =  entityManager.createQuery(hql.toString());

		Set<String> chaves = listaParametros.keySet();  

		for (String chave : chaves)  
		{  
			query.setParameter(chave, listaParametros.get(chave));
		}  

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemContrato> buscarItensContratoProdutoPorIdContrato(Long id) {
		Query query =  entityManager.createQuery("SELECT ic FROM ItemContrato ic WHERE ic.contrato.id = :id and ic.produto.id is not null ");
		query.setParameter("id", id);
		return  query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> buscarProdutosDisponiveisPorIdContratoEProduto(Long id, String produto) {
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT p FROM Produto p ");
		sql.append(" WHERE p.id not in (SELECT ic.produto.id FROM ItemContrato ic WHERE ic.contrato.id = :id and ic.produto.id is not null) ");
		
		sql.append(" AND p.tipo <> 'PADRAO' ");
		
		if(!SimpleValidate.isNullOrBlank(produto)){
			sql.append(" AND p.nome like :produto ");
		}
		
		sql.append(" order by p.tipo, p.nome ");
		
		Query query =  entityManager.createQuery(sql.toString());
		query.setParameter("id", id);
		if(!SimpleValidate.isNullOrBlank(produto)){
			query.setParameter("produto", "%"+produto+"%");
		}
		
		return  query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemContrato> buscarItensContratoMaterialPorIdContrato(Long id) {
		Query query =  entityManager.createQuery("SELECT ic FROM ItemContrato ic WHERE ic.contrato.id = :id and ic.material.id is not null ");
		query.setParameter("id", id);
		return  query.getResultList();
	}
	  
	@Override
	public ItemContrato buscarItemContratoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT ic FROM ItemContrato ic WHERE ic.id = :id");
		query.setParameter("id", id);
		return (ItemContrato) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirItemContrato(ItemContrato itemContrato) {
		entityManager.remove(entityManager.merge(itemContrato));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> buscarProdutosPorTipo(EnumTipoProduto tipoProduto) {
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT p FROM Produto p ");
		sql.append(" WHERE p.tipo = :tipoProduto ");
		
		Query query =  entityManager.createQuery(sql.toString());
		query.setParameter("tipoProduto", tipoProduto);
		
		return  query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemProducao> buscarTodasOPs() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT op FROM OrdemProducao op ");
		
		Query query =  entityManager.createQuery(sql.toString());
		
		return  query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public OrdemProducao salvarOP(OrdemProducao op) {
		return entityManager.merge(op);
	}

	@Override
	public OrdemProducao buscarOrdemProducaoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT op FROM OrdemProducao op WHERE op.id = :id");
		query.setParameter("id", id);
		return (OrdemProducao) query.getSingleResult();
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ProdutoProjeto salvarProdutoProjeto(ProdutoProjeto produtoProjeto) {
		if(produtoProjeto.getId() == null ){
			produtoProjeto.setDataCadastro(new Date());
		}
		return entityManager.merge(produtoProjeto);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FaseProducao> buscarTodasFases() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT f FROM FaseProducao f order by f.id");
		
		Query query =  entityManager.createQuery(sql.toString());
		
		return  query.getResultList();
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirProdutoProjeto(ProdutoProjeto produtoProjeto) {
		entityManager.remove(entityManager.merge(produtoProjeto));
	}
	
	@Override
	public ProdutoProjeto buscarProdutoProjetoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT pp FROM ProdutoProjeto pp WHERE pp.id = :id");
		query.setParameter("id", id);
		return (ProdutoProjeto) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public OrdemProducaoFase salvarOrdemProducaoFase(OrdemProducaoFase producaoFase) {
		return entityManager.merge(producaoFase);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemProducaoFase> buscarOrdemProducaoFasePorOp(OrdemProducao op) {
		Query query =  entityManager.createQuery("SELECT opf FROM OrdemProducaoFase opf WHERE opf.ordemProducao.id = :id");
		query.setParameter("id", op.getId());
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirOrdemProducaoFase(OrdemProducaoFase ordemProducaoFase) {
		entityManager.remove(entityManager.merge(ordemProducaoFase));		
	}

	@Override
	public OrdemProducaoFase buscarOrdemProducaoFasePorId(Long id) {
		Query query =  entityManager.createQuery("SELECT opf FROM OrdemProducaoFase opf WHERE opf.id = :id");
		query.setParameter("id", id);
		return (OrdemProducaoFase) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FaseProducao> buscarFasesDisponiveisParaOP(OrdemProducao op) {
		Query query =  entityManager.createQuery("SELECT fp FROM FaseProducao fp WHERE fp.id not in (select opf.faseProducao.id FROM OrdemProducaoFase opf WHERE opf.ordemProducao.id = :id)");
		query.setParameter("id", op.getId());
		return query.getResultList();
	}

	@Override
	public FaseProducao buscarFasesProducaoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT fp FROM FaseProducao fp WHERE fp.id = :id");
		query.setParameter("id", id);
		return (FaseProducao) query.getSingleResult();
	}

	@Override
	public FaseProducao buscarFaseProducaoPorOrdem(int ordem) {
		Query query =  entityManager.createQuery("SELECT fp FROM FaseProducao fp WHERE fp.ordem = :ordem");
		query.setParameter("ordem", ordem);
		return (FaseProducao) query.getSingleResult();
	}

	@Override
	public OrdemProducaoFase buscarProximaOrdemProducaoFaseParaProduzirPorOp(OrdemProducao op) {
		
		StringBuilder sql = new StringBuilder("SELECT opf FROM OrdemProducaoFase opf ");
		sql.append(" WHERE opf.dataInicio is null and opf.dataFim is null and opf.ordemProducao.id = :id ");
		sql.append(" ORDER BY opf.faseProducao.ordem ");
		
		Query query =  entityManager.createQuery(sql.toString());
		
		query.setParameter("id", op.getId());
		if(!SimpleValidate.isNullOrEmpty(query.getResultList())){
			return (OrdemProducaoFase) query.getResultList().get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FaseProducao> buscarFasesDisponiveisParaBaixarPorOp(OrdemProducao op) {
		// a fase de expedi��o s� � baixada na ordem de carregamento
		StringBuilder sql = new StringBuilder("SELECT opf.faseProducao FROM OrdemProducaoFase opf ");
		sql.append(" WHERE opf.dataFim is null and opf.ordemProducao.id = :id and opf.faseProducao.fase <> 'Expedi��o'");
		sql.append(" ORDER BY opf.faseProducao.ordem ");
		
		Query query =  entityManager.createQuery(sql.toString());
		
		query.setParameter("id", op.getId());
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemProducao> buscarOrdensProducaoPorFiltro(Long codigoPeca, Long faseAtual, EnumSituacaoOP situacao , String sigla, String nomeProduto, String nomeObra, String dimensao1) {
		StringBuilder sql = new StringBuilder("SELECT op FROM OrdemProducao op WHERE 1=1 ");
		
		if(codigoPeca != null){
			sql.append(" AND op.itemContratoProjeto.id =  :codigoPeca ");
		}
		
		if(faseAtual!=null){
			sql.append(" AND op.fase.id =  :faseAtual ");
		}
		
		if(situacao!=null){
			sql.append(" AND op.situacao =  :situacao ");
		}
		
		if(!SimpleValidate.isNullOrBlank(sigla)){
			sql.append(" AND op.itemContratoProjeto.sigla like  :sigla ");
		}
		
		if(!SimpleValidate.isNullOrBlank(nomeProduto)){
			sql.append(" AND op.itemContratoProjeto.itemContrato.produto.nome like  :nomeProduto ");
		}
		
		if(!SimpleValidate.isNullOrBlank(nomeObra)){
			sql.append(" AND op.itemContratoProjeto.itemContrato.contrato.nomeObra like  :nomeObra ");
		}
		
		
		if(!SimpleValidate.isNullOrBlank(dimensao1)){
			sql.append(" AND op.itemContratoProjeto.projeto.dimensao1 =  :dimensao1");
		}
		
		sql.append(" ORDER BY op.itemContratoProjeto.id ");
		
		Query query =  entityManager.createQuery(sql.toString());
		if(codigoPeca != null){
			query.setParameter("codigoPeca", codigoPeca);
		}
		if(faseAtual != null){
			query.setParameter("faseAtual", faseAtual);
		}
		if(situacao!=null){
			query.setParameter("situacao", situacao);
		}
		if(!SimpleValidate.isNullOrBlank(sigla)){
			query.setParameter("sigla", sigla+"%");
		}
		if(!SimpleValidate.isNullOrBlank(nomeProduto)){
			query.setParameter("nomeProduto", nomeProduto+"%");
		}
		if(!SimpleValidate.isNullOrBlank(nomeObra)){
			query.setParameter("nomeObra", nomeObra+"%");
		}
		
		if(!SimpleValidate.isNullOrBlank(dimensao1)){
			query.setParameter("dimensao1", dimensao1);
		}
		
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirOrdemProducao(OrdemProducao ordem) {
		entityManager.remove(entityManager.merge(ordem));
	}

	@Override
	public OrdemProducao buscarOrdemProducaoPorIdPeca(Long idOrdemProducao) {
		Query query =  entityManager.createQuery("SELECT op FROM OrdemProducao op WHERE op.itemContratoProjeto.id = :id");
		query.setParameter("id", idOrdemProducao);
		return (OrdemProducao) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public MaterialProjeto salvarMaterialProjeto(MaterialProjeto materialAco) {
		if(materialAco.getId() == null){
			materialAco.setDataCadastro(new Date());
		}
		return entityManager.merge(materialAco);
	}

	@Override
	public MaterialProjeto buscarMaterialProjetoPorId(Long idMaterialProjeto) {
		Query query =  entityManager.createQuery("SELECT mp FROM MaterialProjeto mp WHERE mp.id = :id");
		query.setParameter("id", idMaterialProjeto);
		return (MaterialProjeto) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirMaterialProjeto(MaterialProjeto materialProjeto) {
		entityManager.remove(entityManager.merge(materialProjeto));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemContratoProjeto> buscarItensContratoProjetoComProdutoCarregadoPorIdProjeto(Long projetoId) {
		Query query =  entityManager.createQuery("SELECT ip FROM ItemContratoProjeto ip left join fetch ip.itemContrato ic left join fetch ic.produto WHERE ip.projeto.id = :id");
		query.setParameter("id", projetoId);
		return  query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public  List<OrdemProducao> buscarOrdemProducaoDisponivelPorProjeto(Long idProjeto) {
		Query query =  entityManager.createQuery("SELECT op FROM OrdemProducao op WHERE op.itemContratoProjeto.id in (SELECT icp.id FROM ItemContratoProjeto icp WHERE icp.projeto.id = :idProjeto  ) and op.situacao = 'DISPONIVEL' ");
		query.setParameter("idProjeto", idProjeto);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemCarregamento> buscarTodasOrdensCarregamento() {
		Query query =  entityManager.createQuery("SELECT oc FROM OrdemCarregamento oc ");
		return  query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Contrato> buscarContratoParaOrdemCarregamento() {
		Query query =  entityManager.createQuery("SELECT distinct ip.itemContrato.contrato FROM ItemContratoProjeto ip  WHERE ip.id in (SELECT op.itemContratoProjeto.id FROM OrdemProducao op WHERE op.situacao = 'DISPONIVEL') ");
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	
	public OrdemCarregamento salvarOrdemCarregamento(OrdemCarregamento ordemCarregamento) {
		return entityManager.merge(ordemCarregamento);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemContratoProjeto> buscarPecasPorContratoESituacaoOP(Long id,	EnumSituacaoOP situacao) {
		Query query =  entityManager.createQuery("SELECT ip FROM ItemContratoProjeto ip  WHERE ip.id in (SELECT op.itemContratoProjeto.id FROM OrdemProducao op WHERE op.situacao = :situacao) " +
				" and ip.itemContrato.contrato.id = :contrato and ip.id is not null and  ip.id not in (SELECT ioc.itemContratoProjeto.id FROM ItemOrdemCarregamento ioc WHERE ioc.itemContratoProjeto.id IS NOT NULL )");
		query.setParameter("contrato", id);
		query.setParameter("situacao", situacao);
		return query.getResultList();
	}

	@Override
	public OrdemCarregamento buscarOrdemCarregamentoPorId(Long idOC) {
		Query query =  entityManager.createQuery("SELECT oc FROM OrdemCarregamento oc WHERE oc.id = :id");
		query.setParameter("id", idOC);
		return (OrdemCarregamento) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvarItemOrdemCarregamento(ItemOrdemCarregamento item) {
		entityManager.merge(item);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemOrdemCarregamento> buscarItensOrdemCarregamentoPorOC(Long idOC) {
		Query query =  entityManager.createQuery("SELECT ioc FROM ItemOrdemCarregamento ioc WHERE ioc.ordemCarregamento.id = :id AND ioc.itemContratoProjeto.id is not null");
		query.setParameter("id", idOC);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemOrdemCarregamento> buscarItensOrdemCarregamentoProdutoPorOC(Long idOC) {
		Query query =  entityManager.createQuery("SELECT ioc FROM ItemOrdemCarregamento ioc WHERE ioc.ordemCarregamento.id = :id AND ioc.produto.id is not null");
		query.setParameter("id", idOC);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemOrdemCarregamento> buscarItensOrdemCarregamentoMaterialPorOC(Long idOC) {
		Query query =  entityManager.createQuery("SELECT ioc FROM ItemOrdemCarregamento ioc WHERE ioc.ordemCarregamento.id = :id AND ioc.material.id is not null");
		query.setParameter("id", idOC);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> buscarProdutosPorTipoParaProducao(EnumTipoProduto tipo) {
		Query query =  entityManager.createQuery("SELECT p FROM Produto p join fetch p.projeto WHERE p.tipo = :tipo and p.projeto.situacao = 'APROVADO'");
		query.setParameter("tipo", tipo);
		
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemCarregamentoFrota> buscarOrdemCarregamentoFrotaPorIdOrdemCarregamento(Long id) {
		Query query =  entityManager.createQuery("SELECT ov FROM OrdemCarregamentoFrota ov WHERE ov.ordemCarregamento.id = :id");
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public OrdemCarregamentoFrota salvarOrdemCarregamentoFrota(	OrdemCarregamentoFrota ordemCarregamentoFrota) {
		return entityManager.merge(ordemCarregamentoFrota);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirOrdemCarregamentoFrota(OrdemCarregamentoFrota ordemCarregamentoFrota) {
		entityManager.remove(entityManager.merge(ordemCarregamentoFrota));
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirItemOrdemCarregamento(ItemOrdemCarregamento itemOrdemCarregamento) {
		entityManager.remove(entityManager.merge(itemOrdemCarregamento));
		
	}

	@Override
	public ItemOrdemCarregamento buscarItemOrdemCarregamentoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT ioc FROM ItemOrdemCarregamento ioc WHERE ioc.id = :id");
		query.setParameter("id", id);
		return (ItemOrdemCarregamento) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirOrdemCarregamento(OrdemCarregamento ordemCarregamento) {
		entityManager.remove(entityManager.merge(ordemCarregamento));
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public MaterialProj salvarMaterialProj(MaterialProj material) {
		if(material.getId() == null){
			material.setDataCadastro(new Date());
		}
		return entityManager.merge(material);
	}

	@Override
	public MaterialProj buscarMaterialProjPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT p FROM MaterialProj p WHERE p.id = :id");
		query.setParameter("id", id);
		return (MaterialProj) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirMaterialProjeto(MaterialProj material) {
		entityManager.remove(entityManager.merge(material));
	}

	@Override
	public boolean tracoUtilizadoEmProjeto(Long idTraco) {
		Query query =  entityManager.createQuery("SELECT p FROM Projeto p WHERE p.traco.id =  :id and p.situacao <> 'REPROVADO')");
		query.setParameter("id",idTraco);
		
		
		if(SimpleValidate.isNullOrEmpty(query.getResultList())){
			return false;
		}
		
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> buscarDadosRelatorioAco(Long materialFiltro, String contratoFiltro, String obraFiltro, String situacaoFiltro, String dataInicialFiltro, String dataFinalFiltro) {
		
		StringBuffer sql = new StringBuffer();
		sql.append("select  distinct p.PJ_ID, c.CT_NUMERO,  c.CT_ID, c.CT_NOME_OBRA, icp.IP_SIGLA, p.PJ_DATA_CADASTRO, pro.PR_NOME, p.PJ_REVISAO, ");  
        sql.append(" pro.PR_ID, CASE WHEN p.PJ_SITUACAO IS null THEN 'Aguardando envio' else p.PJ_SITUACAO end as situacao, ");
        sql.append(" (select COUNT(icp1.ip_id) FROM TB_ITEM_CONTRATO_PROJETO icp1 "+ 
        			" WHERE icp1.PROJETO_ID = icp.PROJETO_ID and icp1.IP_SITUACAO <> 'CANCELADO') as pecas_geradas, "+
        			" mat.MA_NOME, SUM(MP.MP_PESO) as peso_aco_utilizado, SUM(MP.MP_COMPRIMENTO) as comprimento_aco_utilizado ");
        sql.append(" FROM TB_PROJETO p ");       
        sql.append(" join TB_ITEM_CONTRATO_PROJETO icp on icp.PROJETO_ID = p.PJ_ID ");       
        sql.append(" join TB_ITEM_CONTRATO ic on icp.ITEMCONTRATO_ID = ic.IC_ID ");       
        sql.append(" join TB_CONTRATO c on ic.CONTRATO_ID = c.CT_ID ");      
        sql.append(" join TB_PRODUTO pro on pro.PR_ID = ic.PRODUTO_ID ");
        sql.append(" JOIN TB_MATERIAL_PROJETO MP on MP.PROJETO_ID = p.PJ_ID ");
        sql.append(" JOIN TB_MATERIAL mat on mat.MA_ID = MP.MATERIAL_ID ");
        sql.append(" where c.CT_ID is not null and icp.IP_SITUACAO <> 'CANCELADO' ");
        
        if(materialFiltro != null){
        	sql.append("  AND mat.ma_id = :materialId ");
        }
        
        if(!SimpleValidate.isNullOrBlank(contratoFiltro)){
        	sql.append("  AND c.CT_NUMERO = :contratoNumero ");
        }
        
        if(!SimpleValidate.isNullOrBlank(obraFiltro)){
        	sql.append("  AND c.CT_NOME_OBRA like :nomeObra ");
        }
        
        if(!SimpleValidate.isNullOrBlank(situacaoFiltro)){
        	sql.append("  AND (CASE WHEN p.PJ_SITUACAO IS null THEN 'Aguardando envio' else p.PJ_SITUACAO end) = :situacaoFiltro ");
        }
        
        if(!SimpleValidate.isNullOrBlank(dataInicialFiltro)){
        	sql.append("  AND p.PJ_DATA_CADASTRO >= :dataInicial ");
        }
        
        if(!SimpleValidate.isNullOrBlank(dataFinalFiltro)){
        	sql.append("  AND p.PJ_DATA_CADASTRO <= :dataFinalFiltro ");
        }
        
        sql.append(" GROUP BY p.PJ_ID, c.CT_NUMERO, c.CT_ID, c.CT_NOME_OBRA, icp.IP_SIGLA, p.PJ_DATA_CADASTRO, pro.PR_NOME, p.PJ_REVISAO, pro.PR_ID, p.PJ_SITUACAO, mat.MA_NOME, icp.PROJETO_ID ");
        sql.append(" ORDER BY c.CT_NUMERO, icp.IP_SIGLA ");
		
        Query query = entityManager.createNativeQuery(sql.toString());
        
        if(materialFiltro != null){
        	query.setParameter("materialId", materialFiltro);
        }
        
        if(!SimpleValidate.isNullOrBlank(contratoFiltro)){
        	query.setParameter("contratoNumero", contratoFiltro);
        }
        
        if(!SimpleValidate.isNullOrBlank(obraFiltro)){
        	query.setParameter("nomeObra", "%"+obraFiltro+"%");
        }
        
        if(!SimpleValidate.isNullOrBlank(situacaoFiltro)){
        	query.setParameter("situacaoFiltro", situacaoFiltro);
        }
        
		if (!SimpleValidate.isNullOrBlank(dataInicialFiltro)) {
			query.setParameter("dataInicial", DateFacility.ajustaDataSql(dataInicialFiltro));
		}
		
		if (!SimpleValidate.isNullOrBlank(dataFinalFiltro)) {
			query.setParameter("dataFinalFiltro", DateFacility.ajustaDataSql(dataFinalFiltro));
		}
		
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> buscarSituacoesProjetos() {
		Query query =  entityManager.createQuery("SELECT distinct CASE WHEN p.situacao IS null THEN 'Aguardando envio' else p.situacao end as situacao FROM Projeto p ");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> buscarDadosRelatorioProducao(String contratoFiltro, String obraFiltro, String situacaoFiltro, String dataInicioFiltro, 
			String dataFimFiltro, String pecaFiltro, String siglaPecaFiltro) {
		
		StringBuffer sql = new StringBuffer();
		
		sql.append(" select ");       
		sql.append(" distinct p.PJ_ID, c.CT_NUMERO, c.CT_ID,	c.CT_NOME_OBRA,	icp.IP_SIGLA,	p.PJ_DATA_CADASTRO,		pro.PR_NOME, "); 
		sql.append(" p.PJ_REVISAO,	pro.PR_ID,	CASE WHEN p.PJ_SITUACAO IS null THEN 'Aguardando envio' else p.PJ_SITUACAO end, "); 
		sql.append(" (select COUNT(icp1.ip_id) FROM TB_ITEM_CONTRATO_PROJETO icp1 WHERE icp1.PROJETO_ID = icp.PROJETO_ID) as pecas_geradas, ");
		sql.append(" (select COUNT(icp1.ip_id) FROM TB_ITEM_CONTRATO_PROJETO icp1 WHERE icp1.PROJETO_ID = icp.PROJETO_ID) * CONVERT(decimal(14,3), replace(p.PJ_VOLUME,',','.')) as volume_concreto, ");
		sql.append(" (select SUM(MP_PESO) from TB_MATERIAL_PROJETO m where m.PROJETO_ID = p.PJ_ID) as aco ");
		sql.append(" from TB_PROJETO p ");       
		sql.append(" left join TB_ITEM_CONTRATO_PROJETO icp on icp.PROJETO_ID = p.PJ_ID ");       
		sql.append(" left join TB_ITEM_CONTRATO ic on icp.ITEMCONTRATO_ID = ic.IC_ID ");       
		sql.append(" left join TB_CONTRATO c on ic.CONTRATO_ID = c.CT_ID ");      
		sql.append(" left join TB_PRODUTO pro on pro.PR_ID = ic.PRODUTO_ID ");
		sql.append(" where c.CT_ID is not null ");
		
		 
		if(!SimpleValidate.isNullOrBlank(contratoFiltro)){
        	sql.append("  AND c.CT_NUMERO = :contratoNumero ");
        }
        
        if(!SimpleValidate.isNullOrBlank(obraFiltro)){
        	sql.append("  AND c.CT_NOME_OBRA like :nomeObra ");
        }
        
        if(!SimpleValidate.isNullOrBlank(situacaoFiltro)){
        	sql.append("  AND (CASE WHEN p.PJ_SITUACAO IS null THEN 'Aguardando envio' else p.PJ_SITUACAO end) = :situacaoFiltro ");
        }
        
        if(!SimpleValidate.isNullOrBlank(siglaPecaFiltro)){
        	sql.append("  AND icp.IP_SIGLA like :pecaSigla ");
        }
        
        if(!SimpleValidate.isNullOrBlank(pecaFiltro)){
        	sql.append("  AND pro.PR_NOME like :peca ");
        }
        
        
        if(!SimpleValidate.isNullOrBlank(dataInicioFiltro)){
        	sql.append("  AND p.PJ_DATA_CADASTRO >= :dataInicial ");
        }
        
        if(!SimpleValidate.isNullOrBlank(dataFimFiltro)){
        	sql.append("  AND p.PJ_DATA_CADASTRO <= :dataFinalFiltro ");
        }
        
        sql.append(" ORDER BY c.CT_NUMERO, icp.IP_SIGLA ");
		
		Query query = entityManager.createNativeQuery(sql.toString());
		
		if(!SimpleValidate.isNullOrBlank(contratoFiltro)){
        	query.setParameter("contratoNumero", contratoFiltro);
        }
        
        if(!SimpleValidate.isNullOrBlank(obraFiltro)){
        	query.setParameter("nomeObra", "%"+obraFiltro+"%");
        }
        
        if(!SimpleValidate.isNullOrBlank(siglaPecaFiltro)){
        	query.setParameter("pecaSigla", "%"+siglaPecaFiltro+"%");
        }
        
        if(!SimpleValidate.isNullOrBlank(pecaFiltro)){
        	query.setParameter("peca", "%"+pecaFiltro+"%");
        }
        
        if(!SimpleValidate.isNullOrBlank(situacaoFiltro)){
        	query.setParameter("situacaoFiltro", situacaoFiltro);
        }
        
        if (!SimpleValidate.isNullOrBlank(dataInicioFiltro)) {
			query.setParameter("dataInicial", DateFacility.ajustaDataSql(dataInicioFiltro));
		}
		
		if (!SimpleValidate.isNullOrBlank(dataFimFiltro)) {
			query.setParameter("dataFinalFiltro", DateFacility.ajustaDataSql(dataFimFiltro));
		}
        
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemCarregamento> buscarOrdensCarregamentoPorFiltro(
			String contrato) {
		Query query =  entityManager.createQuery("SELECT oc FROM OrdemCarregamento oc ");
		return  query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Pedido> buscarTodosPedidos() {
		return entityManager.createQuery("Select v FROM Pedido v").getResultList();
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Pedido salvarPedido(Pedido pedido) {
		return entityManager.merge(pedido);
	}
	
	@Override
	public Pedido buscarPedidoPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT p FROM Pedido p WHERE p.id = :id");
		query.setParameter("id", id);
		return (Pedido) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> buscarProdutosDisponiveisEProduto(Long id, String produto) {
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT p FROM Produto p ");
		sql.append(" WHERE p.id not in (SELECT ip.produto.id FROM ItemPedido ip WHERE ip.pedido.id = :id and ip.produto.id is not null) ");
		
		sql.append(" AND p.tipo = 'PRATELEIRA' ");
		
		if(!SimpleValidate.isNullOrBlank(produto)){
			sql.append(" AND p.nome like :produto ");
		}
		
		sql.append(" order by p.tipo, p.nome ");
		
		Query query =  entityManager.createQuery(sql.toString());
		query.setParameter("id", id);
		if(!SimpleValidate.isNullOrBlank(produto)){
			query.setParameter("produto", "%"+produto+"%");
		}
		
		return  query.getResultList();
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ItemPedido salvarItemPedido(ItemPedido item) {
		return entityManager.merge(item);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemPedido> buscarItensPedidoProdutoPorIdPedido(Long id) {
		Query query =  entityManager.createQuery("SELECT ip FROM ItemPedido ip WHERE ip.pedido.id = :id and ip.produto.id is not null ");
		query.setParameter("id", id);
		return  query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemPedido> buscarItensPedidoMaterialPorIdPedido(Long id) {
		Query query =  entityManager.createQuery("SELECT ip FROM ItemPedido ip WHERE ip.pedido.id = :id and ip.material.id is not null ");
		query.setParameter("id", id);
		return  query.getResultList();
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirItemPedido(ItemPedido itemPedido) {
		entityManager.remove(entityManager.merge(itemPedido));
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirPedido(Pedido pedido) {
		entityManager.remove(entityManager.merge(pedido));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Pedido> buscarPedidosPorFiltros(String numero) {

		StringBuffer sql = new StringBuffer();

		sql.append(" Select p FROM Pedido p WHERE 1=1 ");

		if(numero != ""){
			sql.append(" AND v.numero like :numero ");
		}

		Query query =  entityManager.createQuery(sql.toString());

		if(numero != ""){
			query.setParameter("numero", "%"+numero+"%");
		}
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Pedido> buscarPedidoParaOrdemCarregamento() {
		Query query =  entityManager.createQuery(" SELECT distinct ip.pedido FROM ItemPedido ip  " +
				" WHERE ip.pedido.situacao = 'FINALIZADO' and (ip.produto.id is not null or ip.material.id is not null) and ip.id not in (SELECT ioc.itemPedido.id FROM ItemOrdemCarregamento ioc WHERE ioc.itemPedido.id is not null)  ");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> buscarPecasPorPedidoESituacaoOP(Long id) {
		
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ip.produto  FROM ItemPedido ip ");
		sql.append(" WHERE ip.pedido.id in (SELECT p.id FROM Pedido p WHERE p.situacao = 'FINALIZADO') AND ip.produto.id IS NOT NULL ");
		sql.append(" and ip.pedido.id = :pedido and ip.produto.id not in (SELECT ioc.produto.id FROM ItemOrdemCarregamento ioc where ioc.ordemCarregamento.pedido.id = :pedido1 ) ");
		
		Query query =  entityManager.createQuery(sql.toString());
		query.setParameter("pedido", id);
		query.setParameter("pedido1", id);
		return query.getResultList();
	}
	
	@Override
	public ItemPedido buscarItemPedidoPorProdutoEPedido(Long idProduto, Long idPedido) {
		Query query =  entityManager.createQuery("SELECT ip FROM ItemPedido ip  WHERE ip.pedido.id = :pedido and ip.produto.id = :produto   ");
		query.setParameter("pedido", idPedido);
		query.setParameter("produto", idProduto);
		return (ItemPedido) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> buscarProdutosPorContratoESituacaoOP(Long id) {
		
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ip.produto  FROM ItemContrato ip ");
		sql.append(" WHERE ip.contrato.id in (SELECT p.id FROM Contrato p WHERE p.situacao = 'FINALIZADO') AND ip.produto.id IS NOT NULL AND ip.produto.tipo = 'PRATELEIRA' ");
		sql.append(" and ip.contrato.id = :contrato and ip.produto.id not in (SELECT ioc.produto.id FROM ItemOrdemCarregamento ioc where ioc.ordemCarregamento.contrato.id = :contrato AND ioc.produto.id is not null ) ");
		
		Query query =  entityManager.createQuery(sql.toString());
		query.setParameter("contrato", id);
		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> buscarMateriaisPorPedidoESituacaoOP(Long id) {
		
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ip.MATERIAL_ID, m.MA_NOME, ip.IP_QUANTIDADE - "); 
		sql.append(" (SELECT sum(ioc.ip_quantidade) FROM TB_ITEM_ORDEM_CARREGAMENTO ioc ");
		sql.append(" JOIN TB_ORDEM_CARREGAMENTO oc on oc.OC_ID = ioc.ORDEMCARREGAMENTO_ID ");
		sql.append(" where oc.PEDIDO_ID = :pedido and ip.MATERIAL_ID = ioc.MATERIAL_ID and ioc.MATERIAL_ID is not null ) as quantidade ");
		sql.append(" FROM TB_ITEM_PEDIDO ip "); 
		sql.append(" JOIN TB_MATERIAL m on m.MA_ID = ip.MATERIAL_ID ");
		sql.append(" WHERE ip.PEDIDO_ID in "); 
		sql.append(" (SELECT p.PD_ID FROM TB_PEDIDO p WHERE p.PD_SITUACAO = 'FINALIZADO') ");
		sql.append(" AND ip.MATERIAL_ID IS NOT NULL ");
		sql.append(" and ip.PEDIDO_ID = :pedido "); 
		sql.append(" and ip.IP_QUANTIDADE > (SELECT sum(ioc.ip_quantidade) "); 
		sql.append(" FROM TB_ITEM_ORDEM_CARREGAMENTO ioc ");
		sql.append(" JOIN TB_ORDEM_CARREGAMENTO oc on oc.OC_ID = ioc.ORDEMCARREGAMENTO_ID ");
		sql.append(" where oc.PEDIDO_ID = :pedido and ip.MATERIAL_ID = ioc.MATERIAL_ID ");
		sql.append(" and ioc.MATERIAL_ID is not null ) ");
		
		Query query =  entityManager.createNativeQuery(sql.toString());
		query.setParameter("pedido", id);
		return query.getResultList();
	}
	
}