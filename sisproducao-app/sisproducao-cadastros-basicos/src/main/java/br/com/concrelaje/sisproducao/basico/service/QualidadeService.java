package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Checklist;
import br.com.concrelaje.sisproducao.entidades.Inspecao;
import br.com.concrelaje.sisproducao.entidades.ItemInspecao;
import br.com.concrelaje.sisproducao.entidades.RespostaInspecao;


/**
 * @author IGOR
 */
public interface QualidadeService {

	List<ItemInspecao> buscarItemInspecao(); 

	ItemInspecao salvarItemInspecao(ItemInspecao itemInspecao);

	ItemInspecao buscarItemInspecaoPorId(Long id); 

	List<Checklist> buscarTodosChecklists();

	Checklist salvarChecklist(Checklist checklist);

	List<ItemInspecao> buscarItensDisponiveisPorChecklist(Long idChecklist);

	Checklist buscarChecklistPorId(Long id);

	List<Checklist> buscarTodosChecklistsAtivo();

	Inspecao salvarInspecao(Inspecao inspecao);

	Inspecao buscarInspecaoPorId(Long id);

	void salvarRespostaInspecao(RespostaInspecao respostaInspecao);

	List<RespostaInspecao> buscarRespostaInspecaoPorIdInspecao(Long id);

	RespostaInspecao buscarRespostaInspecaoPorId(Long id);

	List<Object> buscarInpecoesPorFiltro(Long idPeca, Long idChecklist, String nomeObra, String numeroContrato);

	void excluirItemInspecao(ItemInspecao itemInspecao);

	List<RespostaInspecao> buscarRespostaInspecaoPorIdItemInspecao(Long idItemInspecao);

	void excluirChecklist(Checklist checklist);

	List<Object> buscarDadosRelatorioQualidadePorTipo(String filtroTipoRelatorio);

}

