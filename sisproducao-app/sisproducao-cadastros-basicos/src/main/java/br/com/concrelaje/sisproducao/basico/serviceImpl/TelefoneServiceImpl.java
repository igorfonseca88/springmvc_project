package br.com.concrelaje.sisproducao.basico.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.service.TelefoneService;
import br.com.concrelaje.sisproducao.entidades.Telefone;

@Service("telefoneService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class TelefoneServiceImpl implements TelefoneService{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluir(Telefone telefone) {
		entityManager.remove(entityManager.merge(telefone));
	}
	
	@Override
	public Telefone buscarPorId(Long id) {
		Query query = entityManager.createQuery("select t from Telefone t where t.id = :id");
		query.setParameter("id", id);
		return (Telefone) query.getSingleResult();
	}
}
