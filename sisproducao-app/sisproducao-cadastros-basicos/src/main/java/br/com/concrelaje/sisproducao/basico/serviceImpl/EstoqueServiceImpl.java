package br.com.concrelaje.sisproducao.basico.serviceImpl;



import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrelaje.sisproducao.basico.control.util.SimpleValidate;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.basico.service.UsuarioService;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoSimNao;
import br.com.concrelaje.sisproducao.entidades.EnumSituacaoMovimentacao;
import br.com.concrelaje.sisproducao.entidades.EnumModalidade;
import br.com.concrelaje.sisproducao.entidades.EnumTipoMovimentacao;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.ItemMovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Setor;
import br.com.concrelaje.sisproducao.entidades.Usuario;

@Service("estoqueService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class EstoqueServiceImpl implements EstoqueService{
    
	@PersistenceContext
    private EntityManager entityManager;
	 
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ProducaoService producaoService;

	@SuppressWarnings("unchecked")
	@Override
	public List<MovimentacaoEstoque> buscarTodasMovimentacoes() {
		Query query =  entityManager.createQuery("FROM MovimentacaoEstoque m");
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public MovimentacaoEstoque salvarMovimentacao(MovimentacaoEstoque movimentacaoEstoque) {
		return entityManager.merge(movimentacaoEstoque);
	}

	@Override
	public MovimentacaoEstoque buscarPorId(Long id) {
		Query query = entityManager.createQuery("select m from MovimentacaoEstoque m where m.id = :id");
		query.setParameter("id", id);
		return (MovimentacaoEstoque) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvarItemMovimentacaoEstoque(ItemMovimentacaoEstoque item) {
		entityManager.merge(item);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemMovimentacaoEstoque> buscarItensMovimentacaoEstoquePorIdMovimentacaoEstoque(Long id) {
		Query query = entityManager.createQuery("select im from ItemMovimentacaoEstoque im where im.movimentacaoEstoque.id = :id");
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public MovimentacaoEstoque buscarMovimentacaoEstoqueComItensPorIdMovimentacaoEstoque(Long id) {

		try{
			StringBuilder hql = new StringBuilder("");

			hql.append("select e from MovimentacaoEstoque e left join fetch e.itensMovimentacaoEstoque where e.id = :id ");
			
			Query query = entityManager.createQuery(hql.toString());
			query.setParameter("id", id);
			return (MovimentacaoEstoque) query.getSingleResult();

		}catch (NoResultException exception){
			return  null;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirItemMovimentacaoEstoque(ItemMovimentacaoEstoque itemMovimentacaoEstoque) {
		entityManager.remove(entityManager.merge(itemMovimentacaoEstoque));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Material> buscarTodosMateriais() { 
		return entityManager.createQuery("Select m FROM Material m ORDER BY m.nome").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Material> buscarTodosMateriaisComEstoque() {
		return entityManager.createQuery("Select m FROM Material m where m.estoque > 0 ").getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Material salvarMaterial(Material material) {
		return entityManager.merge(material);
	}

	@Override
	public Material buscarMaterialPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT m FROM Material m WHERE m.id = :id");
		query.setParameter("id", id);
		return (Material) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirMaterial(Material material) {
		entityManager.remove(entityManager.merge(material));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Material> buscarMateriaisDisponiveisPorIdContratoENumero(Long id, String numero) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT m FROM Material m ");
		//sql.append(" WHERE m.id not in (SELECT ic.material.id FROM ItemContrato ic WHERE ic.contrato.id = :id and ic.material.id is not null) AND m.estoque > 0 ");
		sql.append(" WHERE m.id not in (SELECT ic.material.id FROM ItemContrato ic WHERE ic.contrato.id = :id and ic.material.id is not null)  ");
		if(!SimpleValidate.isNullOrBlank(numero)){
			sql.append(" and m.numero like :numero ");
		}
		
		Query query =  entityManager.createQuery(sql.toString());
		query.setParameter("id", id);
		
		if(!SimpleValidate.isNullOrBlank(numero)){
			query.setParameter("numero", "%"+numero+"%");
		}
		
		return  query.getResultList();
	}


	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void movimentarEstoque(ItemContrato item, Material material) {
		
		producaoService.salvarItemContrato(item);
		
		MovimentacaoEstoque movimentacaoEstoque = new MovimentacaoEstoque();
		movimentacaoEstoque.setDataCadastro(new Date());
		movimentacaoEstoque.setSituacao(EnumSituacaoMovimentacao.FINALIZADA);
		Usuario usuario = usuarioService.buscarUsuarioPorLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		movimentacaoEstoque.setUsuario(usuario);
		movimentacaoEstoque.setTipoMovimentacao(EnumTipoMovimentacao.SAIDA);
		//movimentacaoEstoque.setModalidade(EnumModalidade.VENDA);
		movimentacaoEstoque = this.salvarMovimentacao(movimentacaoEstoque);
		
		ItemMovimentacaoEstoque itemEstoque = new ItemMovimentacaoEstoque();
		itemEstoque.setMaterial(material);
		itemEstoque.setQuantidade(new BigDecimal(item.getQuantidade()));
		itemEstoque.setMovimentacaoEstoque(movimentacaoEstoque);
		this.salvarItemMovimentacaoEstoque(itemEstoque);
		
		material.setEstoque(material.getEstoque().subtract(new BigDecimal(item.getQuantidade())));

		this.salvarMaterial(material);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Material> buscarMateriaisAco(EnumLogicoSimNao acoSimNao) {
		Query query =  entityManager.createQuery("SELECT m FROM Material m WHERE m.aco = :acoSimNao ORDER BY m.nome asc");
		query.setParameter("acoSimNao", acoSimNao);
		return query.getResultList();
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Setor salvarSetor(Setor setor) {
		return entityManager.merge(setor);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Setor> buscarTodosSetores() {
		return entityManager.createQuery("Select s FROM Setor s").getResultList();
	}

	@Override
	public Setor buscarSetorPorId(Long id) {
		Query query =  entityManager.createQuery("SELECT s FROM Setor s WHERE s.id = :id");
		query.setParameter("id", id);
		return (Setor) query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluirSetor(Setor setor) {
		entityManager.remove(entityManager.merge(setor));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Material> buscarMateriaisDisponiveisPorIdPedidoENumero(Long id, String numero) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT m FROM Material m ");
		sql.append(" WHERE m.id not in (SELECT ip.material.id FROM ItemPedido ip WHERE ip.pedido.id = :id and ip.material.id is not null)  ");
		if(!SimpleValidate.isNullOrBlank(numero)){
			sql.append(" and m.numero like :numero ");
		}
		
		Query query =  entityManager.createQuery(sql.toString());
		query.setParameter("id", id);
		
		if(!SimpleValidate.isNullOrBlank(numero)){
			query.setParameter("numero", "%"+numero+"%");
		}
		
		return  query.getResultList();
	}
	
}

