package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Cliente;

/**
 * @author WILLIAM
 */
public interface ClienteService {
 
	/**
	 * Salva um cliente. 
	 * @param cliente
	 * @throws Exception
	 */
	Cliente salvarCliente(Cliente cliente);
	
	/**
	 * Exclui um cliente.
	 * @param cliente
	 * @throws Exception
	 */
	void excluirCliente(Cliente cliente);
	
	/**
	 * Busca todas os clientes.
	 * @return
	 */
	List<Cliente> buscarTodosClientes();
	
	/**
	 * Busca cliente por id
	 * @param id
	 * @return
	 */
	
	Cliente buscarPorId(Long id);

	List<Cliente> buscarClientePorNome(String nome);
}

