package br.com.concrelaje.sisproducao.basico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.concrelaje.sisproducao.basico.service.ClienteService;
import br.com.concrelaje.sisproducao.basico.service.DocumentoAnexoService;
import br.com.concrelaje.sisproducao.basico.service.EstoqueService;
import br.com.concrelaje.sisproducao.basico.service.ProducaoService;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.wrapper.PedidoWrapper;

@Controller
@RequestMapping(value = "pedido/pedidoList/**")
@SessionAttributes("pedidoWrapper")
public class ListarPedidos {
    
	 @Autowired
	 private ProducaoService producaoService;
	
	 @Autowired
	 private EstoqueService estoqueService;
	
	 @Autowired
	 private ClienteService clienteService;
	
	 @Autowired
     private MessageSource messageSource;
	 
	 @Autowired
	 private DocumentoAnexoService documentoAnexoService;

    @RequestMapping(value="/init", method = RequestMethod.GET)
    public String listarPedidos(ModelMap modelMap) {
    	modelMap.addAttribute("listaPedidos", producaoService.buscarTodosPedidos());
    	return "producao/pedido/PedidoList";
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET) 
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	try{
    		Pedido pedido =  producaoService.buscarPedidoPorId(new Long(id));
    		PedidoWrapper pedidoWrapper = new PedidoWrapper();
    		pedidoWrapper.setPedido(pedido);
    		pedidoWrapper.setListaItemPedidoProduto(producaoService.buscarItensPedidoProdutoPorIdPedido(pedido.getId()));
    		pedidoWrapper.setListaItemPedidoMaterial(producaoService.buscarItensPedidoMaterialPorIdPedido(pedido.getId()));
    		pedidoWrapper.setListaDocumentoAnexo(documentoAnexoService.buscarDocumentosAnexoPorIdPedido(pedidoWrapper.getPedido().getId()));
    		pedidoWrapper.setCliente(pedido.getCliente());
    		pedidoWrapper.setListaProdutos(producaoService.buscarTodosProdutos());
    		pedidoWrapper.setListaTipoDocumento(documentoAnexoService.buscarTodosTiposDocumento());
    		
    		modelMap.addAttribute("listaItens", pedidoWrapper.getItemPedido());
    		modelMap.addAttribute("listaClientes", clienteService.buscarTodosClientes());
    		modelMap.addAttribute("pedidoWrapper", pedidoWrapper);
    		
    	}catch(Exception e){
    		modelMap.addAttribute("error", "Erro ao carregar contrato.");
    		e.printStackTrace();
    	}
    	return "producao/pedido/PedidoForm";
    }
    
    @RequestMapping(value="/excluir")
    public String excluir(Pedido pedido, ModelMap modelMap) {
 	   try{
    		pedido = producaoService.buscarPedidoPorId(pedido.getId());
    		producaoService.excluirPedido(pedido);
    		
    		modelMap.addAttribute("success", "Pedido removido com sucesso!");
    		
    		}catch(Exception e){
    		e.printStackTrace();
    		modelMap.addAttribute("error", "Não foi possível excluir o contrato!");
    		}
    		return listarPedidos(modelMap);
    }
    
    @RequestMapping(value="/pesquisar", method = RequestMethod.POST)
    public String pesquisar(@RequestParam("numero") String numero,ModelMap modelMap) {
    	modelMap.addAttribute("listaPedidos", producaoService.buscarPedidosPorFiltros(numero));
        return "producao/pedido/PedidoList";
    }
    
}
