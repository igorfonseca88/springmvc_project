package br.com.concrelaje.sisproducao.basico.service;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.EnumTipoPessoa;
import br.com.concrelaje.sisproducao.entidades.Pessoa;

/**
 * Service para entidade Pessoa
 * @author WILLIAM
 *
 */
public interface PessoaService {
 

	/**
	 * Salva uma pessoa. 
	 * @param pessoa
	 * @throws Exception
	 */
	Pessoa salvar(Pessoa pessoa) throws Exception;

	/**
	 * Busca todas as pessoas.
	 * @return
	 */
	List<Pessoa> buscarTodos();

	/**
	 * Exclui uma pessoa.
	 * @param pessoa
	 */
	void excluir(Pessoa pessoa);
	
	/**
	 * Busca pessoa por cpf ou cnpj dependendo do tipo de pessoa informada.
	 * @param cpfCnpj
	 * @param tipoPessoa
	 * @return
	 */
	
	Pessoa buscarPessoaPorCpfCnpj(String cpfCnpj, EnumTipoPessoa tipoPessoa);
	
	/**
	 * Busca pessoa com as lista de telefones carregada.
	 * @param idPessoa
	 * @return
	 */
	Pessoa buscarPessoaComTelefonePorIdPessoa(Long idPessoa);

	/**
	 * Busca pessoa com as lista de endere�os carregada.
	 * @param idPessoa
	 * @return
	 */
	Pessoa buscarPessoaComEnderecoPorIdPessoa(Long idPessoa);
}
