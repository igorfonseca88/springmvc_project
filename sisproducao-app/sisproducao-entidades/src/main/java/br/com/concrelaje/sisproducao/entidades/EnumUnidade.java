package br.com.concrelaje.sisproducao.entidades;

public enum EnumUnidade {
	PEÇA,
	UNIDADE,
	PACOTE,
	LITRO,
	TONELADA,
	METRO_CUBICO,
	CHAPA,
	KILOGRAMA,
	CAIXA,
	ROLO,
	PAR,
	CENTO,
	GALAO,
	METRO,
	HORA,
	KILOMETRO,
	BARRA
	
}
