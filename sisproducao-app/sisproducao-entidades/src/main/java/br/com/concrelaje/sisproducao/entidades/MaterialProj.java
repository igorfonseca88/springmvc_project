package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_MATERIAL_PROJ")
public class MaterialProj implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "MA_ID")
	@SequenceGenerator(name="SEQ_MATERIAL_PROJ", sequenceName="SEQ_MATERIAL_PROJ", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_MATERIAL_PROJ" )
	private Long id;
	
	@Column(name = "MA_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "MA_QUANTIDADE", length = 50)
	private BigDecimal quantidade;
	
	@ManyToOne
	@ForeignKey(name = "FK_MATERIALPROJ_PROJETO")
	@JoinColumn(name = "PROJETO_ID")
	private Projeto projeto;
	
	@ManyToOne
	@ForeignKey(name = "FK_MATERIALPROJ_MATERIAL")
	@JoinColumn(name = "MATERIAL_ID")
	private Material material;
	
	@Transient
	private String quantidadeString;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	
	

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MaterialProj other = (MaterialProj) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public String getQuantidadeString() {
		return quantidadeString;
	}

	public void setQuantidadeString(String quantidadeString) {
		this.quantidadeString = quantidadeString;
	}

	


	
	
}
