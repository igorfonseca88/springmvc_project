package br.com.concrelaje.sisproducao.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PESSOA_JURIDICA")
@ForeignKey(name="FK_PESSOAJURIDICA_PESSOA")
public class PessoaJuridica extends Pessoa{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "PJ_CNPJ", nullable = false, length = 20)
	private String cnpj;
	
	@Column(name = "PJ_RAZAO_SOCIAL", nullable = false, length = 100)
	private String razaoSocial;

	@Column(name = "PJ_NOME_FANTASIA", nullable=false, length = 100)
	private String nomeFantasia;
	
	@Column(name = "PJ_INSC_ESTADUAL", nullable=true, length = 100)
	private String inscricaoEstadual;
	
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	@Override
	public String getNomePessoaFisicaJuridica() {
		return this.nomeFantasia;
	}
	
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	@Override
	public String getCpfCnpj() {
		return this.cnpj;
	}

}
