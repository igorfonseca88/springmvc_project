package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.ItemMovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MovimentacaoEstoque;
import br.com.concrelaje.sisproducao.entidades.Usuario;


public class MovimentacaoWrapper {
	
	private MovimentacaoEstoque movimentacaoEstoque;
	
	private ItemMovimentacaoEstoque itemMovimentacaoEstoque;
	
	private Material material;
	
	private Usuario usuario;
	
	
	private List<ItemMovimentacaoEstoque> listaItemEstoques;


	public MovimentacaoEstoque getMovimentacaoEstoque() {
		return movimentacaoEstoque;
	}


	public void setMovimentacaoEstoque(MovimentacaoEstoque movimentacaoEstoque) {
		this.movimentacaoEstoque = movimentacaoEstoque;
	}


	public ItemMovimentacaoEstoque getItemMovimentacaoEstoque() {
		return itemMovimentacaoEstoque;
	}


	public void setItemMovimentacaoEstoque(
			ItemMovimentacaoEstoque itemMovimentacaoEstoque) {
		this.itemMovimentacaoEstoque = itemMovimentacaoEstoque;
	}


	public Material getMaterial() {
		return material;
	}


	public void setMaterial(Material material) {
		this.material = material;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public List<ItemMovimentacaoEstoque> getListaItemEstoques() {
		return listaItemEstoques;
	}


	public void setListaItemEstoques(List<ItemMovimentacaoEstoque> listaItemEstoques) {
		this.listaItemEstoques = listaItemEstoques;
	}
	
	

		
}
