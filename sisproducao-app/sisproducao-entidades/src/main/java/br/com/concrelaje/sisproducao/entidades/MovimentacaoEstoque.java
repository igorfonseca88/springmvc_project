package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_MOVIMENTACAO_ESTOQUE")
public class MovimentacaoEstoque implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ME_ID")
	@SequenceGenerator(name="SEQ_MOVIMENTACAO_ESTOQUE", sequenceName="SEQ_MOVIMENTACAO_ESTOQUE", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_MOVIMENTACAO_ESTOQUE" )
	private Long id;
	
	@Column(name = "ME_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@OneToOne( optional = false, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "USUARIO_ID",  nullable = false)
	@ForeignKey(name = "FK_USUARIO_MOVIMENTACAOESTOQUE")
	private Usuario usuario;
	
	@Column(name = "ME_SITUACAO", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoMovimentacao situacao;
	
	@Column(name = "ME_TIPO_MOVIMENTACAO", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private EnumTipoMovimentacao tipoMovimentacao;
	
	@Column(name = "ME_MODALIDADE", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private EnumModalidade modalidade;
	
	@Column(name = "ME_DATA_FINALIZACAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFinalizacao;
	
	@OneToMany(mappedBy = "movimentacaoEstoque", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	private List<ItemMovimentacaoEstoque> itensMovimentacaoEstoque;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public EnumSituacaoMovimentacao getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoMovimentacao situacao) {
		this.situacao = situacao;
	}

	public EnumTipoMovimentacao getTipoMovimentacao() {
		return tipoMovimentacao;
	}

	public void setTipoMovimentacao(EnumTipoMovimentacao tipoMovimentacao) {
		this.tipoMovimentacao = tipoMovimentacao;
	}

	public EnumModalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(EnumModalidade modalidade) {
		this.modalidade = modalidade;
	}

	public Date getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(Date dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public List<ItemMovimentacaoEstoque> getItensMovimentacaoEstoque() {
		return itensMovimentacaoEstoque;
	}

	public void setItensMovimentacaoEstoque(
			List<ItemMovimentacaoEstoque> itensMovimentacaoEstoque) {
		this.itensMovimentacaoEstoque = itensMovimentacaoEstoque;
	}

	
	
}
