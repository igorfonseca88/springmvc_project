package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_PESSOA")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Pessoa implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PE_ID")
	@SequenceGenerator(name="SEQ_PESSOA", sequenceName="SEQ_PESSOA", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_PESSOA" )
	private Long id;

	@Column(name = "PE_TIPO_PESSOA", nullable = false, length = 10)
	@Enumerated(value = EnumType.STRING)
	private EnumTipoPessoa tipoPessoa;
	
	@Column(name = "PE_EMAIL", nullable = false, length = 100)
	private String email;
	
	@OneToMany(mappedBy = "pessoa", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	private List<Telefone> telefones;
	
	@OneToMany(mappedBy = "pessoa", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	private List<Endereco> enderecos;
	
	public Long getId() {	
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EnumTipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(EnumTipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public abstract String getNomePessoaFisicaJuridica();
	
	public abstract String getCpfCnpj();

}
