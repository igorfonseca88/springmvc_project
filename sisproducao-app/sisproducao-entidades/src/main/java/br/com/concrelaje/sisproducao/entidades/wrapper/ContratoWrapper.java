package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Cliente;
import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;




public class ContratoWrapper {
	
	private Contrato contrato;
	
	private ItemContrato itemContrato;
	
	private Produto produto;
	
	private DocumentoAnexo documentoAnexo;
	
	private Cliente cliente;
	
	private List<ItemContrato> listaItemContratoProduto;
	
	private List<ItemContrato> listaItemContratoMaterial;
	
	private List<DocumentoAnexo> listaDocumentoAnexo;
	
	private Material material;
	
	private List<Material> listaMateriais;
	
	private List<Produto> listaProdutos;
	
	private List<TipoDocumento> listaTipoDocumento;
	
	
	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public ItemContrato getItemContrato() {
		return itemContrato;
	}

	public void setItemContrato(ItemContrato itemContrato) {
		this.itemContrato = itemContrato;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	public List<DocumentoAnexo> getListaDocumentoAnexo() {
		return listaDocumentoAnexo;
	}

	public void setListaDocumentoAnexo(List<DocumentoAnexo> listaDocumentoAnexo) {
		this.listaDocumentoAnexo = listaDocumentoAnexo;
	}

	public DocumentoAnexo getDocumentoAnexo() {
		return documentoAnexo;
	}

	public void setDocumentoAnexo(DocumentoAnexo documentoAnexo) {
		this.documentoAnexo = documentoAnexo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public List<Material> getListaMateriais() {
		return listaMateriais;
	}

	public void setListaMateriais(List<Material> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}

	public List<ItemContrato> getListaItemContratoProduto() {
		return listaItemContratoProduto;
	}

	public void setListaItemContratoProduto(
			List<ItemContrato> listaItemContratoProduto) {
		this.listaItemContratoProduto = listaItemContratoProduto;
	}

	public List<ItemContrato> getListaItemContratoMaterial() {
		return listaItemContratoMaterial;
	}

	public void setListaItemContratoMaterial(
			List<ItemContrato> listaItemContratoMaterial) {
		this.listaItemContratoMaterial = listaItemContratoMaterial;
	}

	public List<Produto> getListaProdutos() {
		return listaProdutos;
	}

	public void setListaProdutos(List<Produto> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}

	public List<TipoDocumento> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}
	
	
}
