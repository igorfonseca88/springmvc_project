package br.com.concrelaje.sisproducao.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ENDERECO")
public class Endereco {
	
	
	@Id
	@Column(name = "EN_ID")
	@SequenceGenerator(name="SEQ_ENDERECO", sequenceName="SEQ_ENDERECO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ENDERECO" )
	private Long id;

	@Column(name = "EN_CEP", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private String CEP;

	@Column(name = "EN_CIDADE", nullable = false, length = 50)
	private String cidade;

	@Column(name = "EN_UF", nullable = false, length = 2)
	private String UF;

	@Column(name = "EN_LOGRADOURO", nullable = false, length = 50)
	private String logradouro;
	
	@Column(name = "EN_BAIRRO", nullable = false, length = 50)
	private String bairro;
	
	@Column(name = "EN_NUMERO", nullable = false, length = 6)
	private Long numero;
	
	@Column(name = "EN_COMPLEMENTO", nullable = true, length = 50)
	private String complemento;

	@Column(name = "EN_TIPO_ENDERECO", nullable = true, length = 50)
	@Enumerated(value = EnumType.STRING)
	private EnumTipoEndereco tipoEndereco;
	
	@ManyToOne
	@ForeignKey(name = "FK_PESSOA_ENDERECO")
	@JoinColumn(name = "PESSOA_ID", nullable = false)
	private Pessoa pessoa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUF() {
		return UF;
	}

	public void setUF(String uF) {
		UF = uF;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}
	
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public EnumTipoEndereco getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(EnumTipoEndereco tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
