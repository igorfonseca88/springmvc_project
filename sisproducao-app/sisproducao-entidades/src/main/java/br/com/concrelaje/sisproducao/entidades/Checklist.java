package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_CHECKLIST")
public class Checklist implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CH_ID")
	@SequenceGenerator(name="SEQ_CHECKLIST", sequenceName="SEQ_CHECKLIST", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_CHECKLIST" )
	private Long id;
	
	@Column(name = "CH_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@OneToOne
	@JoinColumn(name = "USUARIO_ID",  nullable = false)
	@ForeignKey(name = "FK_USUARIO_CHECKLIST")
	private Usuario usuario;
	
	@Column(name = "CH_SITUACAO", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private EnumLogicoAtivoInativo situacao;
	
	@OneToMany(mappedBy = "checklist", fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	private List<ItemInspecao> itensInspecao;
	
	//@ManyToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	 //@JoinTable(name="TB_CHECKLIST_ITEMINSPECAO", joinColumns={@JoinColumn(name="CH_ID")}, inverseJoinColumns={@JoinColumn(name="II_ID")})  
	//private List<ItemInspecao> itensInspecao;
	
	@Column(name="CH_NOME", nullable = false, length = 100)
	private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public EnumLogicoAtivoInativo getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumLogicoAtivoInativo situacao) {
		this.situacao = situacao;
	}

	public List<ItemInspecao> getItensInspecao() {
		return itensInspecao;
	}

	public void setItensInspecao(List<ItemInspecao> itensInspecao) {
		this.itensInspecao = itensInspecao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
