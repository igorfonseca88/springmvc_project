package br.com.concrelaje.sisproducao.entidades;

public enum EnumTipoRelatorioQualidade {
	NUMERO_REPROVACOES_POR_OBRA,
	NUMERO_REPROVACOES_POR_PECA,
	NUMERO_REPROVACOES_POR_GAP;
	
}
