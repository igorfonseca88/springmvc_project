package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ORDEMCARREGAMENTO_FROTA")
public class OrdemCarregamentoFrota implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "OV_ID")
	@SequenceGenerator(name="SEQ_ORDEMCARREGAMENTO_VEICULO", sequenceName="SEQ_ORDEMCARREGAMENTO_VEICULO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ORDEMCARREGAMENTO_VEICULO" )
	private Long id;
	
	
	@ManyToOne
	@ForeignKey(name = "FK_ORDEMCARREGAMENTO_ORDEMCARREGAMENTOVEICULO")
	@JoinColumn(name = "ORDEMCARREGAMENTO_ID")
	private OrdemCarregamento ordemCarregamento;
	
	@ManyToOne
	@ForeignKey(name = "FK_FROTA_ORDEMCARREGAMENTOVEICULO")
	@JoinColumn(name = "VEICULO_ID")
	private Frota frota;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrdemCarregamento getOrdemCarregamento() {
		return ordemCarregamento;
	}

	public void setOrdemCarregamento(OrdemCarregamento ordemCarregamento) {
		this.ordemCarregamento = ordemCarregamento;
	}

	public Frota getFrota() {
		return frota;
	}

	public void setFrota(Frota frota) {
		this.frota = frota;
	}

	
	
		
}
