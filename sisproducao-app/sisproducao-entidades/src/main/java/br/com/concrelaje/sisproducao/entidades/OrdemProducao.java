package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ORDEM_PRODUCAO")
public class OrdemProducao implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "OP_ID")
	@SequenceGenerator(name="SEQ_ORDEM_PRODUCAO", sequenceName="SEQ_ORDEM_PRODUCAO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ORDEM_PRODUCAO" )
	private Long id;

	@Column(name = "OP_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@OneToOne
	@JoinColumn(name = "ITEMCONTRATOPROJETO_ID")
	@ForeignKey(name = "FK_ITEMCONTRATOPROJETO")
	private ItemContratoProjeto itemContratoProjeto;
	
	@OneToOne
	@JoinColumn(name = "PRODUTO_ID")
	@ForeignKey(name = "FK_ORDEMPRODUCAO_PRODUTO")
	private Produto produto;

	@OneToOne
	@JoinColumn(name = "USUARIO_ID")
	@ForeignKey(name = "FK_USUARIO_OP")
	private Usuario usuario;
	
	
	@Column(name = "OP_SITUACAO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoOP situacao;
	
	@OneToOne
	@JoinColumn(name = "FASE_ID",  nullable = true)
	@ForeignKey(name = "FK_FASE_PRODUCAO")
	private FaseProducao fase;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public ItemContratoProjeto getItemContratoProjeto() {
		return itemContratoProjeto;
	}

	public void setItemContratoProjeto(ItemContratoProjeto itemContratoProjeto) {
		this.itemContratoProjeto = itemContratoProjeto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public EnumSituacaoOP getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoOP situacao) {
		this.situacao = situacao;
	}

	public FaseProducao getFase() {
		return fase;
	}

	public void setFase(FaseProducao fase) {
		this.fase = fase;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	
	
}
