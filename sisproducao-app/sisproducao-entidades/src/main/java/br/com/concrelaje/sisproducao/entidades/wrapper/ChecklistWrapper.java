package br.com.concrelaje.sisproducao.entidades.wrapper;


import br.com.concrelaje.sisproducao.entidades.Checklist;
import br.com.concrelaje.sisproducao.entidades.EnumLogicoAtivoInativo;
import br.com.concrelaje.sisproducao.entidades.ItemInspecao;


public class ChecklistWrapper {
	
	private Checklist checklist;
	
	private ItemInspecao inspecao;
	
	public Checklist getChecklist() {
		return checklist;
	}

	public void setChecklist(Checklist checklist) {
		this.checklist = checklist;
	}

	public ItemInspecao getInspecao() {
		return inspecao;
	}

	public void setInspecao(ItemInspecao inspecao) {
		this.inspecao = inspecao;
	}
	
	public EnumLogicoAtivoInativo[] getListaAtivoInativos() {
		return EnumLogicoAtivoInativo.values();
	}
}
