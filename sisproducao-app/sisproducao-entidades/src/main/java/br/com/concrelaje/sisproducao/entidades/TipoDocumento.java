package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TIPO_DOCUMENTO")
public class TipoDocumento implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "TD_ID")
	@SequenceGenerator(name="SEQ_TIPO_DOCUMENTO", sequenceName="SEQ_TIPO_DOCUMENTO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_TIPO_DOCUMENTO" )
	private Long id;
	
	@Column(name = "TD_DESCRICAO", nullable = false, length = 500)
	private String descricao;
	
	@Column(name = "TD_SITUACAO", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private EnumLogicoAtivoInativo situacao;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public EnumLogicoAtivoInativo getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumLogicoAtivoInativo situacao) {
		this.situacao = situacao;
	}

	
}
