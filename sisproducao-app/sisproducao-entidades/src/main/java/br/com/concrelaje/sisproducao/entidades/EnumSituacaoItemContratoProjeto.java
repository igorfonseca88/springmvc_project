package br.com.concrelaje.sisproducao.entidades;

public enum EnumSituacaoItemContratoProjeto {
	A_PRODUZIR,
	EM_PRODUCAO,
	PRODUZIDO,
	CANCELADO
}
