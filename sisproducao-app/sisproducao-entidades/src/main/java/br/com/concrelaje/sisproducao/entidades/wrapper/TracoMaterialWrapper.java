package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.EnumLogicoAtivoInativo;
import br.com.concrelaje.sisproducao.entidades.ItemTraco;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Traco;


public class TracoMaterialWrapper {
	
	private Traco traco;
	
	private ItemTraco itemTraco;
	
	private Material material;
	
	private List<ItemTraco> listaItemTraco;
	
	private List<Material> listaMateriais;
	
	private boolean podeEditarTraco;

	public Traco getTraco() {
		return traco;
	}

	public void setTraco(Traco traco) {
		this.traco = traco;
	}

	public ItemTraco getItemTraco() {
		return itemTraco;
	}

	public void setItemTraco(ItemTraco itemTraco) {
		this.itemTraco = itemTraco;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public List<ItemTraco> getListaItemTraco() {
		return listaItemTraco;
	}

	public void setListaItemTraco(List<ItemTraco> listaItemTraco) {
		this.listaItemTraco = listaItemTraco;
	}

	public List<Material> getListaMateriais() {
		return listaMateriais;
	}

	public void setListaMateriais(List<Material> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}
	
	public EnumLogicoAtivoInativo[] getListaAtivoInativos() {
		return EnumLogicoAtivoInativo.values();
	}

	public boolean isPodeEditarTraco() {
		return podeEditarTraco;
	}

	public void setPodeEditarTraco(boolean podeEditarTraco) {
		this.podeEditarTraco = podeEditarTraco;
	}
	
	
	
}
