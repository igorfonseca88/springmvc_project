package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ORDEM_CARREGAMENTO")
public class OrdemCarregamento implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "OC_ID")
	@SequenceGenerator(name="SEQ_ORDEM_CARREGAMENTO", sequenceName="SEQ_ORDEM_CARREGAMENTO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ORDEM_CARREGAMENTO" )
	private Long id;
	
	@Column(name = "OC_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "OC_NUMERO")
	private Integer numero;
	
	@Column(name = "OC_SITUACAO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoOC situacao;
	
	
	@ManyToOne
	@ForeignKey(name = "FK_CONTRATO_ORDEM_CARREGAMENTO")
	@JoinColumn(name = "CONTRATO_ID", nullable = true)
	private Contrato contrato;
	
	@ManyToOne
	@ForeignKey(name = "FK_PEDIDO_ORDEM_CARREGAMENTO")
	@JoinColumn(name = "PEDIDO_ID", nullable = true)
	private Pedido pedido;
	
	@ManyToOne
	@ForeignKey(name = "FK_FROTA_ORDEM_CARREGAMENTO")
	@JoinColumn(name = "FROTA_ID")
	private Frota veiculo;
	
	@ManyToOne
	@ForeignKey(name = "FK_MOTORISTA_ORDEM_CARREGAMENTO")
	@JoinColumn(name = "MOTORISTA_ID")
	private Motorista motorista;
	
	@Column(name = "OC_DATA_CARREGAMENTO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCarregamento;
	
	@Column(name = "OC_DATA_PREVISTA_SAIDA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPrevSaida;
	
	@OneToMany(mappedBy = "ordemCarregamento", fetch = FetchType.LAZY )
	private List<ItemOrdemCarregamento> itens;
	
	@Column(name = "OC_DATA_INICIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;
	
	@Column(name = "OC_DATA_FIM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;
	
	@Column(name = "OC_TIPO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumModalidade tipo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Frota getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Frota veiculo) {
		this.veiculo = veiculo;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public Date getDataCarregamento() {
		return dataCarregamento;
	}

	public void setDataCarregamento(Date dataCarregamento) {
		this.dataCarregamento = dataCarregamento;
	}

	public Date getDataPrevSaida() {
		return dataPrevSaida;
	}

	public void setDataPrevSaida(Date dataPrevSaida) {
		this.dataPrevSaida = dataPrevSaida;
	}

	public List<ItemOrdemCarregamento> getItens() {
		return itens;
	}

	public void setItens(List<ItemOrdemCarregamento> itens) {
		this.itens = itens;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public EnumSituacaoOC getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoOC situacao) {
		this.situacao = situacao;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public EnumModalidade getTipo() {
		return tipo;
	}

	public void setTipo(EnumModalidade tipo) {
		this.tipo = tipo;
	}
	
	
	
	
	
}
