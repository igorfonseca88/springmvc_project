package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PEDIDO")
public class Pedido implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PD_ID")
	@SequenceGenerator(name="SEQ_PEDIDO", sequenceName="SEQ_PEDIDO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_PEDIDO" )
	private Long id;
	
	@Column(name = "PD_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@ManyToOne
	@ForeignKey(name = "PD_CLIENTE_CONTRATO")
	@JoinColumn(name = "CLIENTE_ID")
	private Cliente cliente;
	
	@Column(name = "PD_NUMERO", nullable = false, length = 100)
	private String numero;
	
	@Column(name = "PD_DATA_CONTRATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPedido;
	
	@Column(name = "PD_OBSERVACAO", length = 4000)
	private String observacao;
	
	@OneToMany(mappedBy = "pedido", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	private List<ItemPedido> itensPedido;
	
	@Column(name = "PD_SITUACAO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoContrato situacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<ItemPedido> getItensPedido() {
		return itensPedido;
	}

	public void setItensPedido(List<ItemPedido> itensPedido) {
		this.itensPedido = itensPedido;
	}

	public EnumSituacaoContrato getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoContrato situacao) {
		this.situacao = situacao;
	}
	
	
	
}