package br.com.concrelaje.sisproducao.entidades;

public enum EnumLegenda {
    EM_BRANCO,
    APROVADO,
    REPROVADO,
    APROVADO_APOS_REINSPECAO,
    NAO_APLICAVEL;
}
