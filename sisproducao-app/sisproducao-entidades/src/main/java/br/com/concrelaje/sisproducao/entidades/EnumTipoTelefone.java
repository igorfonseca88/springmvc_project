package br.com.concrelaje.sisproducao.entidades;

public enum EnumTipoTelefone {
    RESIDENCIAL,
    COMERCIAL,
    CELULAR;
}
