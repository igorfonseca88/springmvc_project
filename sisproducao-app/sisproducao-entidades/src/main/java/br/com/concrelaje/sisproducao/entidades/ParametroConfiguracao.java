package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "TB_PARAMETRO_CONFIGURACAO")
public class ParametroConfiguracao implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "PC_ID")
	@SequenceGenerator(name="SEQ_PARAMETRO_CONFIGURACAO", sequenceName="SEQ_PARAMETRO_CONFIGURACAO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_PARAMETRO_CONFIGURACAO" )
	private Long id;
	
	@Column(name = "PC_URL")
	private String url;
	
	@Column(name = "PC_SMTP_HOST")
	private String smtpHost;
	
	@Column(name = "PC_SMTP_PORT")
	private String smtpPort;
	
	@Column(name = "PC_SMTP_USER")
	private String smtpUser;
	
	@Column(name = "PC_SMTP_PASS")
	private String smtpPass;
	
	@Column(name = "PC_RECIPIENTE_NF")
	private String recipienteNF;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUser() {
		return smtpUser;
	}

	public void setSmtpUser(String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public String getSmtpPass() {
		return smtpPass;
	}

	public void setSmtpPass(String smtpPass) {
		this.smtpPass = smtpPass;
	}

	public String getRecipienteNF() {
		return recipienteNF;
	}

	public void setRecipienteNF(String recipienteNF) {
		this.recipienteNF = recipienteNF;
	}
	
	
	}
