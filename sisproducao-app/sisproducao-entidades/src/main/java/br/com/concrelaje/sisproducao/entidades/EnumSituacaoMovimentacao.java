package br.com.concrelaje.sisproducao.entidades;

public enum EnumSituacaoMovimentacao {
    EM_ELABORACAO,
    FINALIZADA,
    CANCELADA;
}
