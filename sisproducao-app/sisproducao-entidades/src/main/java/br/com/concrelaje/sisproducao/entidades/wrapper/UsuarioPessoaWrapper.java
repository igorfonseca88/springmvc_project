package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Papel;
import br.com.concrelaje.sisproducao.entidades.PessoaFisica;
import br.com.concrelaje.sisproducao.entidades.Usuario;


public class UsuarioPessoaWrapper {
	
	private PessoaFisica pessoaFisica;
	
	private Usuario usuario;
	
	private Papel papelSelecionado;
	
	private List<Papel> listaPapeis;
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Papel getPapelSelecionado() {
		return papelSelecionado;
	}

	public void setPapelSelecionado(Papel papelSelecionado) {
		this.papelSelecionado = papelSelecionado;
	}

	public List<Papel> getListaPapeis() {
		return listaPapeis;
	}

	public void setListaPapeis(List<Papel> listaPapeis) {
		this.listaPapeis = listaPapeis;
	}
	
}
