package br.com.concrelaje.sisproducao.entidades;

public enum EnumSituacaoProjeto {
	APROVADO,
	REPROVADO,
	AGUARDANDO_APROVACAO
}
