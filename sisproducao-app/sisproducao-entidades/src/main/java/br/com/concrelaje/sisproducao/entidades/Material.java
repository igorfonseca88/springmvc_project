package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_MATERIAL")
public class Material implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "MA_ID")
	@SequenceGenerator(name="SEQ_MATERIAL", sequenceName="SEQ_MATERIAL", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_MATERIAL" )
	private Long id;
	
	@Column(name = "MA_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "MA_NOME", nullable = false, length = 500)
	private String nome;
	
	@Column(name = "MA_NUMERO", nullable = false, length = 50)
	private String numero;
	
	@Column(name = "MA_UNIDADE", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private EnumUnidade unidade;
	
	@Column(name = "MA_ESTOQUE", length = 50)
	private BigDecimal estoque;
	
	@Column(name = "MA_ACO")
	@Enumerated(value = EnumType.STRING)
	private EnumLogicoSimNao aco;
	
	@ManyToOne
	@ForeignKey(name = "FK_MATERIAL_SETOR")
	@JoinColumn(name = "SETOR_ID")
	private Setor setor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EnumUnidade getUnidade() {
		return unidade;
	}

	public void setUnidade(EnumUnidade unidade) {
		this.unidade = unidade;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public BigDecimal getEstoque() {
		return estoque;
	}

	public void setEstoque(BigDecimal estoque) {
		this.estoque = estoque;
	}

	public EnumLogicoSimNao getAco() {
		return aco;
	}

	public void setAco(EnumLogicoSimNao aco) {
		this.aco = aco;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	
	
	
}
