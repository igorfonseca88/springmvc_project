package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_DOCUMENTO_ANEXO")
public class DocumentoAnexo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DA_ID")
	@SequenceGenerator(name = "SEQ_DOCUMENTO_ANEXO", sequenceName = "SEQ_DOCUMENTO_ANEXO", allocationSize = 1, initialValue = 0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_DOCUMENTO_ANEXO")
	private Long id;

	@Column(name = "DA_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@ManyToOne
	@ForeignKey(name = "FK_CONTRATO_DOCUMENTOANEXO")
	@JoinColumn(name = "CONTRATO_ID", nullable = true)
	private Contrato contrato;

	@Column(name = "DA_DESCRICAO", nullable = false, length = 100)
	private String descricao;

	
	@Column(name = "DA_TYPE", nullable = false, length = 100)
	private String type;
	
	@Column(name = "DA_FILENAME", nullable = false, length = 100)
	private String filename;
	
	@Lob  
    @Column(name = "DA_IMAGEM")  
    private byte[] imagem;
	
	@OneToOne
	@JoinColumn(name = "TD_ID",  nullable = true)
	@ForeignKey(name = "FK_TIPO_DOCUMENTO")
	private TipoDocumento tipoDocumento;
	
	@ManyToOne
	@ForeignKey(name = "FK_PROJETO_DOCUMENTOANEXO")
	@JoinColumn(name = "PROJETO_ID", nullable = true)
	private Projeto projeto;
	
	@ManyToOne
	@ForeignKey(name = "FK_ORDEMCARREGAMANETO_DOCUMENTOANEXO")
	@JoinColumn(name = "ORDEMCARREGAMANETO_ID", nullable = true)
	private OrdemCarregamento ordemCarregamento;
	
	@ManyToOne
	@ForeignKey(name = "FK_PEDIDO_DOCUMENTOANEXO")
	@JoinColumn(name = "PEDIDO_ID", nullable = true)
	private Pedido pedido;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public OrdemCarregamento getOrdemCarregamento() {
		return ordemCarregamento;
	}

	public void setOrdemCarregamento(OrdemCarregamento ordemCarregamento) {
		this.ordemCarregamento = ordemCarregamento;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	

}
