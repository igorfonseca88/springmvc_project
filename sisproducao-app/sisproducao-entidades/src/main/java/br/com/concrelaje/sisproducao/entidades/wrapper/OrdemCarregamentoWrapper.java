package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.EnumModalidade;
import br.com.concrelaje.sisproducao.entidades.Frota;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.ItemOrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Motorista;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamento;
import br.com.concrelaje.sisproducao.entidades.OrdemCarregamentoFrota;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;


public class OrdemCarregamentoWrapper {
	
	
	private List<Contrato> listaContrato;
	
	private List<Pedido> listaPedido;
	
	private OrdemCarregamento ordemCarregamento;
	
	private String dataCarregamentoString;
	
	private String dataPrevistaSaidaString;
	
	private String horarioInicial;
	
	private String horarioFinal;

	private List<Frota> listaVeiculo;
	
	private List<Motorista> listaMotorista;
	
	private List<ItemContratoProjeto> listaPecasDisponiveis;
	
	private List<Produto> listaPecasProdutoDisponiveis;
	
	private ItemOrdemCarregamento itemOrdemCarregamento;
	
	private List<ItemOrdemCarregamento> listaCarregamento;
	
	private List<ItemOrdemCarregamento> listaCarregamentoProduto;
	
	private List<ItemOrdemCarregamento> listaCarregamentoMaterial;
	
	private List<DocumentoAnexo> listaDocumentoAnexo;
	
	private List<TipoDocumento> listaTipoDocumento;

	private DocumentoAnexo documentoAnexo;
	
	private List<OrdemCarregamentoFrota> listaOrdemCarregamentoFrota;
	
	private Frota veiculo;
	
	private EnumModalidade tipo;
	
	private List<Object> listaMateriaisDisponiveis;
	
	public OrdemCarregamento getOrdemCarregamento() {
		return ordemCarregamento;
	}

	public void setOrdemCarregamento(OrdemCarregamento ordemCarregamento) {
		this.ordemCarregamento = ordemCarregamento;
	}

	public String getDataCarregamentoString() {
		return dataCarregamentoString;
	}

	public void setDataCarregamentoString(String dataCarregamentoString) {
		this.dataCarregamentoString = dataCarregamentoString;
	}

	public String getDataPrevistaSaidaString() {
		return dataPrevistaSaidaString;
	}

	public void setDataPrevistaSaidaString(String dataPrevistaSaidaString) {
		this.dataPrevistaSaidaString = dataPrevistaSaidaString;
	}

	public String getHorarioInicial() {
		return horarioInicial;
	}

	public void setHorarioInicial(String horarioInicial) {
		this.horarioInicial = horarioInicial;
	}

	public String getHorarioFinal() {
		return horarioFinal;
	}

	public void setHorarioFinal(String horarioFinal) {
		this.horarioFinal = horarioFinal;
	}


	public List<Contrato> getListaContrato() {
		return listaContrato;
	}

	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}

	public List<Frota> getListaVeiculo() {
		return listaVeiculo;
	}

	public void setListaVeiculo(List<Frota> listaVeiculo) {
		this.listaVeiculo = listaVeiculo;
	}

	public List<Motorista> getListaMotorista() {
		return listaMotorista;
	}

	public void setListaMotorista(List<Motorista> listaMotorista) {
		this.listaMotorista = listaMotorista;
	}

	public List<ItemContratoProjeto> getListaPecasDisponiveis() {
		return listaPecasDisponiveis;
	}

	public void setListaPecasDisponiveis(
			List<ItemContratoProjeto> listaPecasDisponiveis) {
		this.listaPecasDisponiveis = listaPecasDisponiveis;
	}

	public ItemOrdemCarregamento getItemOrdemCarregamento() {
		return itemOrdemCarregamento;
	}

	public void setItemOrdemCarregamento(ItemOrdemCarregamento itemOrdemCarregamento) {
		this.itemOrdemCarregamento = itemOrdemCarregamento;
	}

	public List<ItemOrdemCarregamento> getListaCarregamento() {
		return listaCarregamento;
	}

	public void setListaCarregamento(List<ItemOrdemCarregamento> listaCarregamento) {
		this.listaCarregamento = listaCarregamento;
	}

	public List<DocumentoAnexo> getListaDocumentoAnexo() {
		return listaDocumentoAnexo;
	}

	public void setListaDocumentoAnexo(List<DocumentoAnexo> listaDocumentoAnexo) {
		this.listaDocumentoAnexo = listaDocumentoAnexo;
	}

	public List<TipoDocumento> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}

	public DocumentoAnexo getDocumentoAnexo() {
		return documentoAnexo;
	}

	public void setDocumentoAnexo(DocumentoAnexo documentoAnexo) {
		this.documentoAnexo = documentoAnexo;
	}

	public List<OrdemCarregamentoFrota> getListaOrdemCarregamentoFrota() {
		return listaOrdemCarregamentoFrota;
	}

	public void setListaOrdemCarregamentoFrota(
			List<OrdemCarregamentoFrota> listaOrdemCarregamentoFrota) {
		this.listaOrdemCarregamentoFrota = listaOrdemCarregamentoFrota;
	}

	public Frota getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Frota veiculo) {
		this.veiculo = veiculo;
	}

	public EnumModalidade getTipo() {
		return tipo;
	}

	public void setTipo(EnumModalidade tipo) {
		this.tipo = tipo;
	}

	public List<Pedido> getListaPedido() {
		return listaPedido;
	}

	public void setListaPedido(List<Pedido> listaPedido) {
		this.listaPedido = listaPedido;
	}

	public List<Produto> getListaPecasProdutoDisponiveis() {
		return listaPecasProdutoDisponiveis;
	}

	public void setListaPecasProdutoDisponiveis(
			List<Produto> listaPecasProdutoDisponiveis) {
		this.listaPecasProdutoDisponiveis = listaPecasProdutoDisponiveis;
	}

	public List<ItemOrdemCarregamento> getListaCarregamentoProduto() {
		return listaCarregamentoProduto;
	}

	public void setListaCarregamentoProduto(
			List<ItemOrdemCarregamento> listaCarregamentoProduto) {
		this.listaCarregamentoProduto = listaCarregamentoProduto;
	}

	public List<Object> getListaMateriaisDisponiveis() {
		return listaMateriaisDisponiveis;
	}

	public void setListaMateriaisDisponiveis(
			List<Object> listaMateriaisDisponiveis) {
		this.listaMateriaisDisponiveis = listaMateriaisDisponiveis;
	}

	public List<ItemOrdemCarregamento> getListaCarregamentoMaterial() {
		return listaCarregamentoMaterial;
	}

	public void setListaCarregamentoMaterial(
			List<ItemOrdemCarregamento> listaCarregamentoMaterial) {
		this.listaCarregamentoMaterial = listaCarregamentoMaterial;
	}


	
	
}
