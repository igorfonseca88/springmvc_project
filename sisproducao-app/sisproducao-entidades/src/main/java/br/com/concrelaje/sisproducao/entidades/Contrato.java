package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_CONTRATO")
public class Contrato implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CT_ID")
	@SequenceGenerator(name="SEQ_CONTRATO", sequenceName="SEQ_CONTRATO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_CONTRATO" )
	private Long id;
	
	@Column(name = "CT_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@ManyToOne
	@ForeignKey(name = "FK_CLIENTE_CONTRATO")
	@JoinColumn(name = "CLIENTE_ID")
	private Cliente cliente;
	
	@Column(name = "CT_NUMERO", nullable = false, length = 100)
	private String numero;
	
	@Column(name = "CT_OBJETO", length = 100)
	private String objeto;
	
	@Column(name = "CT_DATA_CONTRATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataContrato;
	
	@Column(name = "CT_NOME_OBRA", length = 100)
	private String nomeObra;
	
	@OneToMany(mappedBy = "contrato", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	private List<ItemContrato> itensContrato;
	
	@Column(name = "CT_SITUACAO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoContrato situacao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDataContrato() {
		return dataContrato;
	}

	public void setDataContrato(Date dataContrato) {
		this.dataContrato = dataContrato;
	}

	public String getNomeObra() {
		return nomeObra;
	}

	public void setNomeObra(String nomeObra) {
		this.nomeObra = nomeObra;
	}

	public List<ItemContrato> getItensContrato() {
		return itensContrato;
	}

	public void setItensContrato(List<ItemContrato> itensContrato) {
		this.itensContrato = itensContrato;
	}

	

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public EnumSituacaoContrato getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoContrato situacao) {
		this.situacao = situacao;
	}

	
}