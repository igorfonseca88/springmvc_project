package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_RESPOSTA_INSPECAO")
public class RespostaInspecao implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "RE_ID")
	@SequenceGenerator(name="SEQ_RESPOSTA_INSPECAO", sequenceName="SEQ_RESPOSTA_INSPECAO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_RESPOSTA_INSPECAO" )
	private Long id;
	
	@ManyToOne
	@ForeignKey(name = "FK_ITEMINSPECAO_RESPOSTAINSPECAO")
	@JoinColumn(name = "II_ID")
	private ItemInspecao itemInspecao;
	
	@OneToOne( optional = false, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "INSPECAO_ID",  nullable = false)
	@ForeignKey(name = "FK_INSPECAO_RESPOSTAINSPECAO")
	private Inspecao inspecao;
	
	@Column(name = "II_DESCRICAO_PROBLEMA",length= 4000)
	private String descricaoProblema;
	
	@Column(name="II_LEGENDA")
	@Enumerated(EnumType.STRING)
	private EnumLegenda legenda;
	
	@Column(name="II_SOLUCAO_PROPOSTA", length = 1000)
	private String solucaoProposta;
	
	@Column(name="II_REINSPECAO", length = 1000)
	private String reinspecao;
	
	@Column(name = "II_DATA_REINSPECAO", length = 12)
	private String dataReinspecao;
	
	
	@OneToOne
	@JoinColumn(name = "USUARIO_ID")
	@ForeignKey(name = "FK_USUARIO_RESPOSTAINSPECAO")
	private Usuario usuario;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ItemInspecao getItemInspecao() {
		return itemInspecao;
	}

	public void setItemInspecao(ItemInspecao itemInspecao) {
		this.itemInspecao = itemInspecao;
	}

	public Inspecao getInspecao() {
		return inspecao;
	}

	public void setInspecao(Inspecao inspecao) {
		this.inspecao = inspecao;
	}

	public String getDescricaoProblema() {
		return descricaoProblema;
	}

	public void setDescricaoProblema(String descricaoProblema) {
		this.descricaoProblema = descricaoProblema;
	}

	public EnumLegenda getLegenda() {
		return legenda;
	}

	public void setLegenda(EnumLegenda legenda) {
		this.legenda = legenda;
	}

	public String getSolucaoProposta() {
		return solucaoProposta;
	}

	public void setSolucaoProposta(String solucaoProposta) {
		this.solucaoProposta = solucaoProposta;
	}

	public String getReinspecao() {
		return reinspecao;
	}

	public void setReinspecao(String reinspecao) {
		this.reinspecao = reinspecao;
	}


	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDataReinspecao() {
		return dataReinspecao;
	}

	public void setDataReinspecao(String dataReinspecao) {
		this.dataReinspecao = dataReinspecao;
	}

	
		
}