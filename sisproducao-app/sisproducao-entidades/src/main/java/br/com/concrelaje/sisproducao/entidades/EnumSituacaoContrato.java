package br.com.concrelaje.sisproducao.entidades;

public enum EnumSituacaoContrato {
	EM_ELABORACAO,
	FINALIZADO,
	CANCELADO,
	REABERTO
}
