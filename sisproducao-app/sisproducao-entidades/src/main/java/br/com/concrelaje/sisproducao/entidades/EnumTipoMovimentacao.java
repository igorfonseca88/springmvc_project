package br.com.concrelaje.sisproducao.entidades;

public enum EnumTipoMovimentacao {
	/** O atributo ATIVO. */
    ENTRADA,

    /** O atributo INATIVO. */
    SAIDA;
}
