package br.com.concrelaje.sisproducao.entidades.view;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.concrelaje.sisproducao.entidades.EnumSituacaoProjeto;

@Entity
@Table(name = "VI_PROJETO")
public class ProjetoView implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PJ_ID")
	private Long id;
	
	@Column(name="CT_NUMERO")
	private String numero;
	
	@Column(name="CT_NOME_OBRA")
	private String nomeObra;
	
	@Column(name="CT_ID")
	private Long idContrato;
	
	@Column(name = "IP_SIGLA")
	private String sigla;
	
	@Column(name = "PJ_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "PR_NOME")
	private String nomeProduto;
	 
	@Column(name = "PJ_REVISAO")
	private int revisao;
	
	@Column(name = "PR_ID")
	private Long idProduto;
	
	@Column(name = "CT_OBJETO")
	private String objeto;
	
	@Column(name = "PJ_SITUACAO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoProjeto situacao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNomeObra() {
		return nomeObra;
	}

	public void setNomeObra(String nomeObra) {
		this.nomeObra = nomeObra;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public int getRevisao() {
		return revisao;
	}

	public void setRevisao(int revisao) {
		this.revisao = revisao;
	}

	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	public EnumSituacaoProjeto getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoProjeto situacao) {
		this.situacao = situacao;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}
	
	

}
