package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name = "TB_MOTORISTA")
public class Motorista implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "MO_ID")
	@SequenceGenerator(name="SEQ_MOTORISTA", sequenceName="SEQ_MOTORISTA", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_MOTORISTA" )
	private Long id;
	
	@Column(name = "MO_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "MO_MOTORISTA", nullable = false, length = 200)
	private String motorista;
	
	@Column(name = "MO_NRO_REGISTRO", nullable = false, length = 20)
	private String numeroRegistro;
	
	@Column(name = "MO_VALIDADE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validade;
	
	@Column(name = "MO_TIPO", nullable = false, length = 3)
	private String tipo;
	
	@Column(name="MO_CPF",length=14)
	private String cpf;
	
	@Column(name = "MO_TIPO_PROPRIOTERCEIRO")
	@Enumerated(value = EnumType.STRING)
	private EnumTipoProprioTerceiro tipoProprioTerceiro;
	
	@Column(name="MO_TELEFONE",length=20)
	private String telefone;
	
	@Transient
	private String dataString;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getMotorista() {
		return motorista;
	}

	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}

	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public Date getValidade() {
		return validade;
	}

	public void setValidade(Date validade) {
		this.validade = validade;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public EnumTipoProprioTerceiro getTipoProprioTerceiro() {
		return tipoProprioTerceiro;
	}

	public void setTipoProprioTerceiro(EnumTipoProprioTerceiro tipoProprioTerceiro) {
		this.tipoProprioTerceiro = tipoProprioTerceiro;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	
	
		
}
