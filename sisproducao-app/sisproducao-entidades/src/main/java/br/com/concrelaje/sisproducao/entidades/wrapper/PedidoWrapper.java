package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Cliente;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.ItemPedido;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.Pedido;
import br.com.concrelaje.sisproducao.entidades.Produto;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;


public class PedidoWrapper {
	
	private Pedido pedido;
	
	private ItemPedido itemPedido;
	
	private Produto produto;
	
	private DocumentoAnexo documentoAnexo;
	
	private Cliente cliente;
	
	private List<ItemPedido> listaItemPedidoProduto;
	
	private List<ItemPedido> listaItemPedidoMaterial;
	
	private List<DocumentoAnexo> listaDocumentoAnexo;
	
	private Material material;
	
	private List<Material> listaMateriais;
	
	private List<Produto> listaProdutos;

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public ItemPedido getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(ItemPedido itemPedido) {
		this.itemPedido = itemPedido;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemPedido> getListaItemPedidoProduto() {
		return listaItemPedidoProduto;
	}

	public void setListaItemPedidoProduto(List<ItemPedido> listaItemPedidoProduto) {
		this.listaItemPedidoProduto = listaItemPedidoProduto;
	}

	public List<ItemPedido> getListaItemPedidoMaterial() {
		return listaItemPedidoMaterial;
	}

	public void setListaItemPedidoMaterial(List<ItemPedido> listaItemPedidoMaterial) {
		this.listaItemPedidoMaterial = listaItemPedidoMaterial;
	}

	public List<DocumentoAnexo> getListaDocumentoAnexo() {
		return listaDocumentoAnexo;
	}

	public void setListaDocumentoAnexo(List<DocumentoAnexo> listaDocumentoAnexo) {
		this.listaDocumentoAnexo = listaDocumentoAnexo;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public List<Material> getListaMateriais() {
		return listaMateriais;
	}

	public void setListaMateriais(List<Material> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}

	public List<Produto> getListaProdutos() {
		return listaProdutos;
	}

	public void setListaProdutos(List<Produto> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}
	
	private List<TipoDocumento> listaTipoDocumento;

	public DocumentoAnexo getDocumentoAnexo() {
		return documentoAnexo;
	}

	public void setDocumentoAnexo(DocumentoAnexo documentoAnexo) {
		this.documentoAnexo = documentoAnexo;
	}

	public List<TipoDocumento> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}
	
	
		
}
