package br.com.concrelaje.sisproducao.entidades;

public enum EnumTipoPessoa{
	/** O atributo FISICA. */
    FISICA,

    /** O atributo JURIDICA. */
    JURIDICA;
}
