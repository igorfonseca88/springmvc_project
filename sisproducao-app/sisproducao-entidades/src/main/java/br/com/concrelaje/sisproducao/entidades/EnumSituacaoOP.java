package br.com.concrelaje.sisproducao.entidades;

public enum EnumSituacaoOP {
	EM_ELABORACAO,
	EM_PRODUCAO,
	PRODUZIDO,
	CANCELADO,
	DISPONIVEL,
	EXPEDIDA;
}
