package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PRODUTO")
public class Produto implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "PR_ID")
	@SequenceGenerator(name="SEQ_PRODUTO", sequenceName="SEQ_PRODUTO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_PRODUTO" )
	private Long id;
	
	@Column(name = "PR_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "PR_NOME", nullable = false, length = 500)
	private String nome;
	
	@Column(name = "PR_TIPO_PRODUTO", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private EnumTipoProduto tipo;
	
	@ManyToOne
	@ForeignKey(name = "FK_PROJETO_PRODUTO")
	@JoinColumn(name = "PROJETO_ID")
	private Projeto projeto;
	
	@ManyToOne
	@ForeignKey(name = "FK_PRODUTO_SETOR")
	@JoinColumn(name = "SETOR_ID")
	private Setor setor;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EnumTipoProduto getTipo() {
		return tipo;
	}

	public void setTipo(EnumTipoProduto tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	
	
	
}
