package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ORDEMPRODUCAO_FASE")
public class OrdemProducaoFase implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "OF_ID")
	@SequenceGenerator(name="SEQ_ORDEMPRODUCAO_FASE", sequenceName="SEQ_ORDEMPRODUCAO_FASE", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ORDEMPRODUCAO_FASE" )
	private Long id; 
	
	
	@Column(name = "OF_DATA_INICIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;
	
	@Column(name = "OF_DATA_FIM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;
	
	@OneToOne
	@JoinColumn(name = "USUARIO_ID",  nullable = true)
	@ForeignKey(name = "FK_USUARIO")
	private Usuario usuario;
	
	@ManyToOne
	@ForeignKey(name = "FK_ORDEM_PRODUCAO")
	@JoinColumn(name = "OP_ID")
	private OrdemProducao ordemProducao;
	
	@ManyToOne
	@ForeignKey(name = "FK_FASE")
	@JoinColumn(name = "FASE_ID")
	private FaseProducao faseProducao;
	
	@Column(name = "OF_REVISAO")
	private Integer revisao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public OrdemProducao getOrdemProducao() {
		return ordemProducao;
	}

	public void setOrdemProducao(OrdemProducao ordemProducao) {
		this.ordemProducao = ordemProducao;
	}

	public FaseProducao getFaseProducao() {
		return faseProducao;
	}

	public void setFaseProducao(FaseProducao faseProducao) {
		this.faseProducao = faseProducao;
	}

	public Integer getRevisao() {
		return revisao;
	}

	public void setRevisao(Integer revisao) {
		this.revisao = revisao;
	}



	
		
}
