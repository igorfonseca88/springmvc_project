package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ITEM_INSPECAO")
public class ItemInspecao implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "II_ID")
	@SequenceGenerator(name="SEQ_ITEM_INSPECAO", sequenceName="SEQ_ITEM_INSPECAO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ITEM_INSPECAO" )
	private Long id;
	
	@Column(name = "II_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "II_ITEM", nullable = false, length = 500)
	private String item;
	
	@Column(name = "II_METODO", nullable = false, length = 100)
	private String metodo;
	
	@Column(name = "II_TOLERANCIA", nullable = false, length = 100)
	private String tolerancia;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "II_SITUACAO")
	private EnumLogicoAtivoInativo situacao;
	
	@ManyToOne
	@ForeignKey(name = "FK_CHECKLIST_ITEMINSPECAO")
	@JoinColumn(name = "CH_ID", nullable = false)
	private Checklist checklist;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getTolerancia() {
		return tolerancia;
	}

	public void setTolerancia(String tolerancia) {
		this.tolerancia = tolerancia;
	}

	public EnumLogicoAtivoInativo getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumLogicoAtivoInativo situacao) {
		this.situacao = situacao;
	}

	public Checklist getChecklist() {
		return checklist;
	}

	public void setChecklist(Checklist checklist) {
		this.checklist = checklist;
	}
	

}