package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_SETOR")
public class Setor implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "SE_ID")
	@SequenceGenerator(name="SEQ_SETOR", sequenceName="SEQ_SETOR", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_SETOR" )
	private Long id;
	
	@Column(name = "SE_DESCRICAO", nullable = false, length = 500)
	private String descricao;
	
	@Column(name = "SE_CODIGO", nullable = false)
	private Long codigo;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	
	
		
}
