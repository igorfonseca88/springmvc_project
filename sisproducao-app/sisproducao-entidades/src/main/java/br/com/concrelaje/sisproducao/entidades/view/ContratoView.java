package br.com.concrelaje.sisproducao.entidades.view;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.concrelaje.sisproducao.entidades.EnumSituacaoContrato;

@Entity
@Table(name = "VI_CONTRATO")
public class ContratoView implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CT_ID")
	private Long id;
	
	@Column(name="CT_NUMERO")
	private String numero;
	
	@Column(name="CT_NOME_OBRA")
	private String nomeObra;
	
	@Column(name="CT_DATA_CADASTRO")
	private Date dataCadastro;
	
	@Column(name="CLIENTE")
	private String cliente;
	
	@Column(name="DOCUMENTO")
	private String documento;
	
	@Column(name="CT_OBJETO")
	private String objeto;
	
	@Column(name="NUMERO_OBJETO")
	private String numeroObjeto;
	
	@Column(name="CT_SITUACAO")
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoContrato situacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNomeObra() {
		return nomeObra;
	}

	public void setNomeObra(String nomeObra) {
		this.nomeObra = nomeObra;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public String getNumeroObjeto() {
		return numeroObjeto;
	}

	public void setNumeroObjeto(String numeroObjeto) {
		this.numeroObjeto = numeroObjeto;
	}

	public EnumSituacaoContrato getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoContrato situacao) {
		this.situacao = situacao;
	}
	
	
}
