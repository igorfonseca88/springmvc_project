package br.com.concrelaje.sisproducao.entidades.wrapper;


import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Checklist;
import br.com.concrelaje.sisproducao.entidades.EnumLegenda;
import br.com.concrelaje.sisproducao.entidades.Inspecao;
import br.com.concrelaje.sisproducao.entidades.RespostaInspecao;
import br.com.concrelaje.sisproducao.entidades.Usuario;


public class InspecaoWrapper {
	
	private Checklist checklist;
	
	private List<Checklist> listaChecklist;
	
	private Inspecao inspecao;
	
	private List<Usuario> listaUsuarios;
	
	private String dataString;
	
	private String horarioInicial;
	
	private String horarioFinal;
	
	private List<RespostaInspecao> respostaInspecaos;
	
	private RespostaInspecao[] respostaInspecaos2;
	
	private String codigoPecaFiltro;
	
	private String nomeObraFiltro;
	
	private String numeroContratoFiltro;
	
	public Checklist getChecklist() {
		return checklist;
	}

	public void setChecklist(Checklist checklist) {
		this.checklist = checklist;
	}

	public List<Checklist> getListaChecklist() {
		return listaChecklist;
	}

	public void setListaChecklist(List<Checklist> listaChecklist) {
		this.listaChecklist = listaChecklist;
	}

	public Inspecao getInspecao() {
		return inspecao;
	}

	public void setInspecao(Inspecao inspecao) {
		this.inspecao = inspecao;
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}

	public String getHorarioInicial() {
		return horarioInicial;
	}

	public void setHorarioInicial(String horarioInicial) {
		this.horarioInicial = horarioInicial;
	}

	public String getHorarioFinal() {
		return horarioFinal;
	}

	public void setHorarioFinal(String horarioFinal) {
		this.horarioFinal = horarioFinal;
	}

	public List<RespostaInspecao> getRespostaInspecaos() {
		return respostaInspecaos;
	}

	public void setRespostaInspecaos(List<RespostaInspecao> respostaInspecaos) {
		this.respostaInspecaos = respostaInspecaos;
	}
	
	public EnumLegenda[] getLegendas() {
		return EnumLegenda.values();
	}

	public RespostaInspecao[] getRespostaInspecaos2() {
		return respostaInspecaos2;
	}

	public void setRespostaInspecaos2(RespostaInspecao[] respostaInspecaos2) {
		this.respostaInspecaos2 = respostaInspecaos2;
	}

	public String getCodigoPecaFiltro() {
		return codigoPecaFiltro;
	}

	public void setCodigoPecaFiltro(String codigoPecaFiltro) {
		this.codigoPecaFiltro = codigoPecaFiltro;
	}

	public String getNomeObraFiltro() {
		return nomeObraFiltro;
	}

	public void setNomeObraFiltro(String nomeObraFiltro) {
		this.nomeObraFiltro = nomeObraFiltro;
	}

	public String getNumeroContratoFiltro() {
		return numeroContratoFiltro;
	}

	public void setNumeroContratoFiltro(String numeroContratoFiltro) {
		this.numeroContratoFiltro = numeroContratoFiltro;
	}
	
	


}
