package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ITEM_ORDEM_CARREGAMENTO")
public class ItemOrdemCarregamento implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "IP_ID")
	@SequenceGenerator(name="SEQ_ITEM_ORDEM_CARREGAMENTO", sequenceName="SEQ_ITEM_ORDEM_CARREGAMENTO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ITEM_ORDEM_CARREGAMENTO" )
	private Long id;

	@Column(name = "IP_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@ManyToOne
	@ForeignKey(name = "FK_ORDEMCARREGAMENTO_ITEMORDEMCARREGAMENTO")
	@JoinColumn(name = "ORDEMCARREGAMENTO_ID", nullable = false)
	private OrdemCarregamento ordemCarregamento;

	@OneToOne
	@JoinColumn(name = "ORDEMPRODUCAO_ID")
	@ForeignKey(name = "FK_ORDEMPRODUCAO_ITEMORDEMCARREGAMENTO")
	private OrdemProducao ordemProducao;
	
	@ManyToOne
	@JoinColumn(name = "ITEMCONTRATOPROJETO_ID", nullable = true)
	@ForeignKey(name = "FK_ITEMCONTRATOPROJETO_ITEMORDEMCARREGAMENTO")
	private ItemContratoProjeto itemContratoProjeto;
	
	@ManyToOne
	@JoinColumn(name = "PRODUTO_ID", nullable = true)
	@ForeignKey(name = "FK_PRODUTO_ITEMORDEMCARREGAMENTO")
	private Produto produto;
	
	@ManyToOne
	@JoinColumn(name = "MATERIAL_ID", nullable = true)
	@ForeignKey(name = "FK_MATERIAL_ITEMORDEMCARREGAMENTO")
	private Material material;
	
	@ManyToOne
	@JoinColumn(name = "ITEMPEDIDO_ID", nullable = true)
	@ForeignKey(name = "FK_ITEMPEDIDO_ITEMORDEMCARREGAMENTO")
	private ItemPedido itemPedido;
	
	@Column(name="IP_QUANTIDADE", nullable = true) 
	private Integer quantidade;
	
	@Transient
	private  Boolean check;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public OrdemCarregamento getOrdemCarregamento() {
		return ordemCarregamento;
	}

	public void setOrdemCarregamento(OrdemCarregamento ordemCarregamento) {
		this.ordemCarregamento = ordemCarregamento;
	}

	public ItemContratoProjeto getItemContratoProjeto() {
		return itemContratoProjeto;
	}

	public void setItemContratoProjeto(ItemContratoProjeto itemContratoProjeto) {
		this.itemContratoProjeto = itemContratoProjeto;
	}

	public OrdemProducao getOrdemProducao() {
		return ordemProducao;
	}

	public void setOrdemProducao(OrdemProducao ordemProducao) {
		this.ordemProducao = ordemProducao;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public ItemPedido getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(ItemPedido itemPedido) {
		this.itemPedido = itemPedido;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	
	
	
}
