package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ITEM_CONTRATO_PROJETO")
public class ItemContratoProjeto implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "IP_ID")
	@SequenceGenerator(name="SEQ_ITEM_CONTRATO_PROJETO", sequenceName="SEQ_ITEM_CONTRATO_PROJETO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ITEM_CONTRATO_PROJETO" )
	private Long id;

	@Column(name = "IP_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@ManyToOne
	@ForeignKey(name = "FK_ITEMCONTRATO_ITEMCONTRATOPROJETO")
	@JoinColumn(name = "ITEMCONTRATO_ID", nullable = false)
	private ItemContrato itemContrato;

	@ManyToOne
	@ForeignKey(name = "FK_PROJETO_ITEMCONTRATOPROJETO")
	@JoinColumn(name = "PROJETO_ID")
	private Projeto projeto;

	@Column(name = "IP_SIGLA", nullable = false, length = 10)
	private String sigla;

	@Column(name = "IP_SEQUENCIAL", nullable = false, length = 10)
	private Long sequencial;

	@Column(name = "IP_URL", length = 100)
	private String url;

	@Column(name = "IP_SITUACAO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoItemContratoProjeto situacao;

	@Transient
	private  Boolean check;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public ItemContrato getItemContrato() {
		return itemContrato;
	}

	public void setItemContrato(ItemContrato itemContrato) {
		this.itemContrato = itemContrato;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Long getSequencial() {
		return sequencial;
	}

	public void setSequencial(Long sequencial) {
		this.sequencial = sequencial;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public EnumSituacaoItemContratoProjeto getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoItemContratoProjeto situacao) {
		this.situacao = situacao;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}
}
