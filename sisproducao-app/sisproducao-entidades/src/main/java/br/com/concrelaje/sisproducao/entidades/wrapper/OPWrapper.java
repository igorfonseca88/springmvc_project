package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.EnumTipoProduto;
import br.com.concrelaje.sisproducao.entidades.FaseProducao;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.OrdemProducao;
import br.com.concrelaje.sisproducao.entidades.OrdemProducaoFase;
import br.com.concrelaje.sisproducao.entidades.Produto;


public class OPWrapper {

	private OrdemProducao op;
	
	private String tipoBusca;

	private String buscarPor;
	
	private List<Contrato> listaContratos;
	
	private LinkedHashMap<String, String> listaTiposBusca;
	
	private List<ItemContratoProjeto> listaContratoProjeto;
	
	private ItemContratoProjeto itemContratoProjeto;
	
	private List<FaseProducao> listaFaseProducao;
	
	private List<OrdemProducaoFase> producaoFases ;
	
	private Long idFaseSelecionada;
	
	private Date dataInicio;
	
	private String dataString;
	
	private String dataBaixaString;
	
	private List<FaseProducao> listaFasesDisponiveisParaBaixar;
	
	private int revisaoProjeto;
	
	private List<Produto> listaProdutoProjeto;
	
	
	/*CRIADOS PARA INTEGRACAO*/
	private Long idItemContratoProjeto;
	
	@Enumerated(value = EnumType.STRING)
	private EnumTipoProduto tipoPecaProduto;
	
	private Long idProduto;
	
	/****************************/
	
	public Long getIdItemContratoProjeto() {
		return idItemContratoProjeto;
	}

	public void setIdItemContratoProjeto(Long idItemContratoProjeto) {
		this.idItemContratoProjeto = idItemContratoProjeto;
	}

	public String getTipoBusca() {
		return tipoBusca;
	}

	public void setTipoBusca(String tipoBusca) {
		this.tipoBusca = tipoBusca;
	}

	public String getBuscarPor() {
		return buscarPor;
	}

	public void setBuscarPor(String buscarPor) {
		this.buscarPor = buscarPor;
	}

	public List<Contrato> getListaContratos() {
		return listaContratos;
	}

	public void setListaContratos(List<Contrato> listaContratos) {
		this.listaContratos = listaContratos;
	}

	public LinkedHashMap<String, String> getListaTiposBusca(){
		listaTiposBusca = new LinkedHashMap<String,String>();
		listaTiposBusca.put("TODOS_OBRA", "Listar todos os produtos OBRA");
		listaTiposBusca.put("TODOS_PRATELEIRA", "Listar todos os produtos PRATELEIRA");
		//listaTiposBusca.put("TODOS_PADRAO", "Todos os produtos PADR�O");
		listaTiposBusca.put("CONTRATO", "Buscar por N�mero do contrato");
		listaTiposBusca.put("OBRA", "Buscar por Obra");
		return listaTiposBusca;
	}

	public void setListaTiposBusca(LinkedHashMap<String, String> listaTiposBusca) {
		this.listaTiposBusca = listaTiposBusca;
	}

		public ItemContratoProjeto getItemContratoProjeto() {
		return itemContratoProjeto;
	}

	public void setItemContratoProjeto(ItemContratoProjeto itemContratoProjeto) {
		this.itemContratoProjeto = itemContratoProjeto;
	}

	public OrdemProducao getOp() {
		return op;
	}

	public void setOp(OrdemProducao op) {
		this.op = op;
	}

	public List<ItemContratoProjeto> getListaContratoProjeto() {
		return listaContratoProjeto;
	}

	public void setListaContratoProjeto(
			List<ItemContratoProjeto> listaContratoProjeto) {
		this.listaContratoProjeto = listaContratoProjeto;
	}

	public List<FaseProducao> getListaFaseProducao() {
		return listaFaseProducao;
	}

	public void setListaFaseProducao(List<FaseProducao> listaFaseProducao) {
		this.listaFaseProducao = listaFaseProducao;
	}

	public List<OrdemProducaoFase> getProducaoFases() {
		return producaoFases;
	}

	public void setProducaoFases(List<OrdemProducaoFase> producaoFases) {
		this.producaoFases = producaoFases;
	}

	public Long getIdFaseSelecionada() {
		return idFaseSelecionada;
	}

	public void setIdFaseSelecionada(Long idFaseSelecionada) {
		this.idFaseSelecionada = idFaseSelecionada;
	}

	public List<FaseProducao> getListaFasesDisponiveisParaBaixar() {
		return listaFasesDisponiveisParaBaixar;
	}

	public void setListaFasesDisponiveisParaBaixar(
			List<FaseProducao> listaFasesDisponiveisParaBaixar) {
		this.listaFasesDisponiveisParaBaixar = listaFasesDisponiveisParaBaixar;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}

	public String getDataBaixaString() {
		return dataBaixaString;
	}

	public void setDataBaixaString(String dataBaixaString) {
		this.dataBaixaString = dataBaixaString;
	}

	public int getRevisaoProjeto() {
		return revisaoProjeto;
	}

	public void setRevisaoProjeto(int revisaoProjeto) {
		this.revisaoProjeto = revisaoProjeto;
	}

	public List<Produto> getListaProdutoProjeto() {
		return listaProdutoProjeto;
	}

	public void setListaProdutoProjeto(List<Produto> listaProdutoProjeto) {
		this.listaProdutoProjeto = listaProdutoProjeto;
	}

	public EnumTipoProduto getTipoPecaProduto() {
		return tipoPecaProduto;
	}

	public void setTipoPecaProduto(EnumTipoProduto tipoPecaProduto) {
		this.tipoPecaProduto = tipoPecaProduto;
	}

	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	
				
}
