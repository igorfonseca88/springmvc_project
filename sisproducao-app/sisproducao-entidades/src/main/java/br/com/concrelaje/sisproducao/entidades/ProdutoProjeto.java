package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PRODUTO_PROJETO")
public class ProdutoProjeto implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "PP_ID")
	@SequenceGenerator(name="SEQ_PRODUTO_PROJETO", sequenceName="SEQ_PRODUTO_PROJETO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_PRODUTO_PROJETO" )
	private Long id;
	
	@Column(name = "PP_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "PP_QUANTIDADE", nullable = false)
	private Integer quantidade;
	
	@ManyToOne
	@ForeignKey(name = "FK_PROJETO_PROD_PROJETO")
	@JoinColumn(name = "PROJETO_ID")
	private Projeto projeto;
	
	@ManyToOne
	@ForeignKey(name = "FK_PRODUTO_PROD_PROJETO")
	@JoinColumn(name = "PRODUTO_ID")
	private Produto produto;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoProjeto other = (ProdutoProjeto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
