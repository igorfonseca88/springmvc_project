package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ITEM_TRACO")
public class ItemTraco implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "IT_ID")
	@SequenceGenerator(name="SEQ_ITEM_TRACO", sequenceName="SEQ_ITEM_TRACO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ITEM_TRACO" )
	private Long id;
	
	@Column(name = "IT_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@ManyToOne
	@ForeignKey(name = "FK_TRACO_ITEM_TRACO")
	@JoinColumn(name = "TRACO_ID", nullable = false)
	private Traco traco;
	
	@ManyToOne
	@ForeignKey(name = "FK_MATERIAL_ITEM_MATERIAL")
	@JoinColumn(name = "MATERIAL_ID", nullable = false)
	private Material material;
	
	@Column(name = "IT_QUANTIDADE")
	private BigDecimal quantidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Traco getTraco() {
		return traco;
	}

	public void setTraco(Traco traco) {
		this.traco = traco;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	
	
}
