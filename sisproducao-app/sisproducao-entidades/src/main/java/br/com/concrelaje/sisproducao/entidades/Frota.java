package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "TB_FROTA")
public class Frota implements Serializable{

	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name = "FR_ID")
	@SequenceGenerator(name="SEQ_FROTA", sequenceName="SEQ_FROTA", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_FROTA" )
	private Long id;
	
	@Column(name = "FR_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "FR_VEICULO", nullable = false, length = 500)
	private String veiculo;
	
	@Column(name = "FR_PLACA", nullable = false, length = 25)
	private String placa;
	
	@Column(name = "FR_MARCA", nullable = false, length = 50)
	private String marca;
	
	@Column(name = "FR_MODELO", nullable = false, length = 50)
	private String modelo;
	
	@Column(name = "FR_COR", nullable = false, length = 50)
	private String cor;

	@Column(name = "FR_CHASSI", nullable = false, length = 50)
	private String chassi;
	
	@Column(name = "FR_RENAVAM", nullable = false, length = 50)
	private String renavam;
	
	@Column(name = "FR_ESTADO", length = 25)
	private String estado;
	
	
	@Column(name = "FR_TIPO")
	@Enumerated(value = EnumType.STRING)
	private EnumTipoProprioTerceiro tipo;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}
	
	

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

	public EnumTipoProprioTerceiro getTipo() {
		return tipo;
	}

	public void setTipo(EnumTipoProprioTerceiro tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Frota [id=" + id + ", dataCadastro=" + dataCadastro
				+ ", veiculo=" + veiculo + ", placa=" + placa + ", marca="
				+ marca + ", modelo=" + modelo + ", cor=" + cor + ", chassi="
				+ chassi + ", renavam=" + renavam + ", estado=" + estado + "]";
	}

	
	

		
}
