package br.com.concrelaje.sisproducao.entidades.wrapper;

import br.com.concrelaje.sisproducao.entidades.EnumTipoRelatorioQualidade;


public class RelatorioQualidadeWrapper {
	
	private String filtroTipoRelatorio;
	
	public String getFiltroTipoRelatorio() {
		return filtroTipoRelatorio;
	}

	public void setFiltroTipoRelatorio(String filtroTipoRelatorio) {
		this.filtroTipoRelatorio = filtroTipoRelatorio;
	}
	
	public EnumTipoRelatorioQualidade[] getListaTipoRelatorioQualidade() {
		return EnumTipoRelatorioQualidade.values();
	}
		
}
