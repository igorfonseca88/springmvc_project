package br.com.concrelaje.sisproducao.entidades;

public enum EnumLogicoAtivoInativo {
	/** O atributo ATIVO. */
    ATIVO,

    /** O atributo INATIVO. */
    INATIVO;
}
