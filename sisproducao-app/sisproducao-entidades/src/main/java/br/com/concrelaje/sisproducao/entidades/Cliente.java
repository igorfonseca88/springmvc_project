package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_CLIENTE")
public class Cliente implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CL_ID")
	@SequenceGenerator(name="SEQ_CLIENTE", sequenceName="SEQ_CLIENTE", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_CLIENTE" )
	private Long id;
	
	@Column(name = "CL_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@OneToOne( optional = false, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "PESSOA_ID",  nullable = false)
	@ForeignKey(name = "FK_PESSOA_CLIENTE")
	private Pessoa pessoa;
	
	@Column(name = "CL_SITUACAO", nullable = false, length = 15)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoCliente situacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public EnumSituacaoCliente getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoCliente situacao) {
		this.situacao = situacao;
	}
	
}
