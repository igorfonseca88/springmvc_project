package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_ITEM_CONTRATO")
public class ItemContrato implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "IC_ID")
	@SequenceGenerator(name="SEQ_ITEM_CONTRATO", sequenceName="SEQ_ITEM_CONTRATO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_ITEM_CONTRATO" )
	private Long id;
	
	@Column(name = "IC_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@ManyToOne
	@ForeignKey(name = "FK_CONTRATO_ITEM_CONTRATO")
	@JoinColumn(name = "CONTRATO_ID", nullable = false)
	private Contrato contrato;
	
	@ManyToOne
	@ForeignKey(name = "FK_PRODUTO_ITEM_CONTRATO")
	@JoinColumn(name = "PRODUTO_ID")
	private Produto produto;
	
	@Column(name="IC_QUANTIDADE", nullable = false) 
	private Integer quantidade;
	
	@ManyToOne
	@ForeignKey(name = "FK_MATERIAL_ITEM_CONTRATO")
	@JoinColumn(name = "MATERIAL_ID")
	private Material material;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	
	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	
	
	
}
