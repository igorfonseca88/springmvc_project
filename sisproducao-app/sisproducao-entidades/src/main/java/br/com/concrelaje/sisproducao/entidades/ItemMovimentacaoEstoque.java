package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_MOVIMENTACAO_ESTOQUE_ITEM")
public class ItemMovimentacaoEstoque implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "MI_ID")
	@SequenceGenerator(name="SEQ_MOVIMENTACAO_ESTOQUE_ITEM", sequenceName="SEQ_MOVIMENTACAO_ESTOQUE_ITEM", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_MOVIMENTACAO_ESTOQUE_ITEM" )
	private Long id;
	
	@Column(name = "MI_FORNECEDOR",  length = 100)
	private String fornecedor;
	
	@Column(name = "MI_QUANTIDADE", nullable = false)
	private BigDecimal quantidade;
	
	@ManyToOne
	@ForeignKey(name = "FK_MOVESTOQUE_ITEM_MOV")
	@JoinColumn(name = "MOVESTOQUE_ID", nullable = false)
	private MovimentacaoEstoque movimentacaoEstoque;
	
	
	@ManyToOne
	@ForeignKey(name = "FK_MATERIAL_ITEM_MOVESTOQUE")
	@JoinColumn(name = "MATERIAL_ID")
	private Material material;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getFornecedor() {
		return fornecedor;
	}


	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}


	
	public BigDecimal getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}


	public MovimentacaoEstoque getMovimentacaoEstoque() {
		return movimentacaoEstoque;
	}


	public void setMovimentacaoEstoque(MovimentacaoEstoque movimentacaoEstoque) {
		this.movimentacaoEstoque = movimentacaoEstoque;
	}


	public Material getMaterial() {
		return material;
	}


	public void setMaterial(Material material) {
		this.material = material;
	}
	
	
		
}
