package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_INSPECAO")
public class Inspecao implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "IN_ID")
	@SequenceGenerator(name="SEQ_INSPECAO", sequenceName="SEQ_INSPECAO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_INSPECAO" )
	private Long id;
	
	@Column(name = "IN_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@OneToOne
	@JoinColumn(name = "USUARIO_ID",  nullable = false)
	@ForeignKey(name = "FK_USUARIO_INSPECAO")
	private Usuario usuario;
	
	@ManyToOne
	@ForeignKey(name = "FK_ITEMCONTRATOPROJETO_INSPECAO")
	@JoinColumn(name = "IP_ID")
	private ItemContratoProjeto peca;
	
	@Column(name = "IN_DATA_INICIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;
	
	@Column(name = "IN_DATA_FIM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;
	
	
	@OneToOne
	@JoinColumn(name = "CHECKLIST_ID",  nullable = false)
	@ForeignKey(name = "FK_CHECKLIST_INSPECAO")
	private Checklist checklist;
	
	@Column(name = "IN_GAP")
	private String gap;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ItemContratoProjeto getPeca() {
		return peca;
	}

	public void setPeca(ItemContratoProjeto peca) {
		this.peca = peca;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Checklist getChecklist() {
		return checklist;
	}

	public void setChecklist(Checklist checklist) {
		this.checklist = checklist;
	}

	public String getGap() {
		return gap;
	}

	public void setGap(String gap) {
		this.gap = gap;
	}

	
		
}