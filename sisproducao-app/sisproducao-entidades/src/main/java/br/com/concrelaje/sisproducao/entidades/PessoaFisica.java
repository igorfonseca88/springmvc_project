package br.com.concrelaje.sisproducao.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PESSOA_FISICA")
@ForeignKey(name="FK_PESSOAFISICA_PESSOA")
public class PessoaFisica extends Pessoa{
	
	private static final long serialVersionUID = 1L;


	@Column(name="PF_CPF", nullable=false, length=15)
	private String cpf;
	

	@Column(name="PF_NOME", nullable=false, length=100)
	private String nome;
	
	@Column(name="PF_RG", length=100)
	private String rg;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String getNomePessoaFisicaJuridica() {
		return this.nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	@Override
	public String getCpfCnpj() {
		return this.cpf;
	}
	
}
