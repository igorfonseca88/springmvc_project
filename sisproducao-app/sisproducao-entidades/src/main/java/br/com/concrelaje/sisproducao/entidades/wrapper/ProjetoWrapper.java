package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.LinkedHashMap;
import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Contrato;
import br.com.concrelaje.sisproducao.entidades.DocumentoAnexo;
import br.com.concrelaje.sisproducao.entidades.ItemContrato;
import br.com.concrelaje.sisproducao.entidades.ItemContratoProjeto;
import br.com.concrelaje.sisproducao.entidades.Material;
import br.com.concrelaje.sisproducao.entidades.MaterialProj;
import br.com.concrelaje.sisproducao.entidades.MaterialProjeto;
import br.com.concrelaje.sisproducao.entidades.ProdutoProjeto;
import br.com.concrelaje.sisproducao.entidades.Projeto;
import br.com.concrelaje.sisproducao.entidades.TipoDocumento;
import br.com.concrelaje.sisproducao.entidades.view.ProjetoView;


public class ProjetoWrapper {

	private Projeto projeto;
	
	private ProjetoView projetoView;

	private String tipoBusca;

	private String buscarPor;
	
	private List<Contrato> listaContratos;
	
	private LinkedHashMap<String, String> listaTiposBusca;
	
	private List<ItemContrato> listaItemContratos;
	
	private Integer quantidadeItemSelecionado;
	
	private String quantidadeRestante;
	
	private ItemContratoProjeto itemContratoProjeto;
	
	private List<ProjetoView> listaProjetosGerados;
	
	private ProdutoProjeto produtoProjeto;
	
	private Integer countItensContratoProjeto;
	
	private Boolean habilitarAprovarReprovar;
	
	private List<DocumentoAnexo> listaDocumentoAnexo;
	
	private DocumentoAnexo documentoAnexo;
	
	private List<TipoDocumento> listaTipoDocumento;
	
	private boolean mostrarMotivoReprovacao;
	
	private List<Material> listaMateriaisAco;
	
	private MaterialProjeto materialProjeto;
	
	private List<ItemContratoProjeto> listaItensContratoProjeto;
	
	private boolean visualizar = true;
	
	private boolean visualizarAcessorios = true;
	
	private List<Material> listaMateriais;
	
	private MaterialProj materialProj;
	
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public String getTipoBusca() {
		return tipoBusca;
	}

	public void setTipoBusca(String tipoBusca) {
		this.tipoBusca = tipoBusca;
	}

	public String getBuscarPor() {
		return buscarPor;
	}

	public void setBuscarPor(String buscarPor) {
		this.buscarPor = buscarPor;
	}

	public List<Contrato> getListaContratos() {
		return listaContratos;
	}

	public void setListaContratos(List<Contrato> listaContratos) {
		this.listaContratos = listaContratos;
	}

	public LinkedHashMap<String, String> getListaTiposBusca(){
		listaTiposBusca = new LinkedHashMap<String,String>();
		listaTiposBusca.put("CONTRATO", "N�mero do Contrato");
		listaTiposBusca.put("OBRA", "Obra");
		return listaTiposBusca;
	}

	public List<ItemContrato> getListaItemContratos() {
		return listaItemContratos;
	}

	public void setListaItemContratos(List<ItemContrato> listaItemContratos) {
		this.listaItemContratos = listaItemContratos;
	}

	public void setListaTiposBusca(LinkedHashMap<String, String> listaTiposBusca) {
		this.listaTiposBusca = listaTiposBusca;
	}

	public Integer getQuantidadeItemSelecionado() {
		return quantidadeItemSelecionado;
	}

	public void setQuantidadeItemSelecionado(Integer quantidadeItemSelecionado) {
		this.quantidadeItemSelecionado = quantidadeItemSelecionado;
	}

	public ItemContratoProjeto getItemContratoProjeto() {
		return itemContratoProjeto;
	}

	public void setItemContratoProjeto(ItemContratoProjeto itemContratoProjeto) {
		this.itemContratoProjeto = itemContratoProjeto;
	}

	public String getQuantidadeRestante() {
		return quantidadeRestante;
	}

	public void setQuantidadeRestante(String quantidadeRestante) {
		this.quantidadeRestante = quantidadeRestante;
	}

	public List<ProjetoView> getListaProjetosGerados() {
		return listaProjetosGerados;
	}

	public void setListaProjetosGerados(List<ProjetoView> listaProjetosGerados) {
		this.listaProjetosGerados = listaProjetosGerados;
	}

	public ProjetoView getProjetoView() {
		return projetoView;
	}

	public void setProjetoView(ProjetoView projetoView) {
		this.projetoView = projetoView;
	}

	public ProdutoProjeto getProdutoProjeto() {
		return produtoProjeto;
	}

	public void setProdutoProjeto(ProdutoProjeto produtoProjeto) {
		this.produtoProjeto = produtoProjeto;
	}

	public Integer getCountItensContratoProjeto() {
		return countItensContratoProjeto;
	}

	public void setCountItensContratoProjeto(Integer countItensContratoProjeto) {
		this.countItensContratoProjeto = countItensContratoProjeto;
	}

	public Boolean getHabilitarAprovarReprovar() {
		return habilitarAprovarReprovar;
	}

	public void setHabilitarAprovarReprovar(Boolean habilitarAprovarReprovar) {
		this.habilitarAprovarReprovar = habilitarAprovarReprovar;
	}

	public List<DocumentoAnexo> getListaDocumentoAnexo() {
		return listaDocumentoAnexo;
	}

	public void setListaDocumentoAnexo(List<DocumentoAnexo> listaDocumentoAnexo) {
		this.listaDocumentoAnexo = listaDocumentoAnexo;
	}

	public DocumentoAnexo getDocumentoAnexo() {
		return documentoAnexo;
	}

	public void setDocumentoAnexo(DocumentoAnexo documentoAnexo) {
		this.documentoAnexo = documentoAnexo;
	}

	public List<TipoDocumento> getListaTipoDocumento() {
		return listaTipoDocumento;
	}

	public void setListaTipoDocumento(List<TipoDocumento> listaTipoDocumento) {
		this.listaTipoDocumento = listaTipoDocumento;
	}

	public boolean isMostrarMotivoReprovacao() {
		return mostrarMotivoReprovacao;
	}

	public void setMostrarMotivoReprovacao(boolean mostrarMotivoReprovacao) {
		this.mostrarMotivoReprovacao = mostrarMotivoReprovacao;
	}

	public List<Material> getListaMateriaisAco() {
		return listaMateriaisAco;
	}

	public void setListaMateriaisAco(List<Material> listaMateriaisAco) {
		this.listaMateriaisAco = listaMateriaisAco;
	}

	public MaterialProjeto getMaterialProjeto() {
		return materialProjeto;
	}

	public void setMaterialProjeto(MaterialProjeto materialProjeto) {
		this.materialProjeto = materialProjeto;
	}

	public List<ItemContratoProjeto> getListaItensContratoProjeto() {
		return listaItensContratoProjeto;
	}

	public void setListaItensContratoProjeto(
			List<ItemContratoProjeto> listaItensContratoProjeto) {
		this.listaItensContratoProjeto = listaItensContratoProjeto;
	}

	public boolean isVisualizar() {
		return visualizar;
	}

	public void setVisualizar(boolean visualizar) {
		this.visualizar = visualizar;
	}

	public List<Material> getListaMateriais() {
		return listaMateriais;
	}

	public void setListaMateriais(List<Material> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}

	public MaterialProj getMaterialProj() {
		return materialProj;
	}

	public void setMaterialProj(MaterialProj materialProj) {
		this.materialProj = materialProj;
	}

	public boolean isVisualizarAcessorios() {
		return visualizarAcessorios;
	}

	public void setVisualizarAcessorios(boolean visualizarAcessorios) {
		this.visualizarAcessorios = visualizarAcessorios;
	}
	
	
	
	
}
