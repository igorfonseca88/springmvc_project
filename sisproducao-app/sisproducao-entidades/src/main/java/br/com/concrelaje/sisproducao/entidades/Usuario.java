package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_USUARIO")
public class Usuario implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "US_ID")
    @SequenceGenerator(name="SEQ_USUARIO", sequenceName="SEQ_USUARIO", allocationSize = 1, initialValue=0)
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_USUARIO")
    private Long id;

    @Column(name = "US_USERNAME")
    private String username;

    @Column(name = "US_PASSWORD")
    private String password;

    @Column(name = "US_DT_CADASTRO")
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "US_HABILITADO", columnDefinition = "INTEGER")
    private Boolean habilitado;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "TB_USUARIO_PAPEL", joinColumns = @JoinColumn(name = "US_ID"), inverseJoinColumns = @JoinColumn(name = "PA_ID"))
    private List<Papel> papeis;
    
    
	@OneToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "PESSOA_ID",  nullable = false)
	@ForeignKey(name = "FK_PESSOA_USUARIO")
	private Pessoa pessoa;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Papel> getPapeis() {
        return papeis;
    }

    public void setPapeis(List<Papel> papeis) {
        this.papeis = papeis;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}

