package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.math.BigDecimal;
import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Material;

public class RelatorioProducaoWrapper {

	private List<Material> listaMaterialAcoFiltro;
	
	private Long materialFiltro;
	
	private String contratoFitro;

	private String obraFiltro;
	
	private String situacaoProjetoFiltro;
	
	private List<String> listaSituacaoProjetoFiltro;
	
	private String dataInicioFiltro;
	
	private String dataFimFiltro;
	
	private BigDecimal totalComprimentoAco;
	
	private BigDecimal totalPesoAco;
	
	private List<Object> listaPesquisaAco;
	
	private String pecaFiltro;
	
	private String siglaPecaFiltro;
	
	public List<Material> getListaMaterialAcoFiltro() {
		return listaMaterialAcoFiltro;
	}

	public void setListaMaterialAcoFiltro(List<Material> listaMaterialAcoFiltro) {
		this.listaMaterialAcoFiltro = listaMaterialAcoFiltro;
	}

	public Long getMaterialFiltro() {
		return materialFiltro;
	}

	public void setMaterialFiltro(Long materialFiltro) {
		this.materialFiltro = materialFiltro;
	}

	public String getContratoFitro() {
		return contratoFitro;
	}

	public void setContratoFitro(String contratoFitro) {
		this.contratoFitro = contratoFitro;
	}

	public String getObraFiltro() {
		return obraFiltro;
	}

	public void setObraFiltro(String obraFiltro) {
		this.obraFiltro = obraFiltro;
	}

	public String getSituacaoProjetoFiltro() {
		return situacaoProjetoFiltro;
	}

	public void setSituacaoProjetoFiltro(String situacaoProjetoFiltro) {
		this.situacaoProjetoFiltro = situacaoProjetoFiltro;
	}

	public List<String> getListaSituacaoProjetoFiltro() {
		return listaSituacaoProjetoFiltro;
	}

	public void setListaSituacaoProjetoFiltro(
			List<String> listaSituacaoProjetoFiltro) {
		this.listaSituacaoProjetoFiltro = listaSituacaoProjetoFiltro;
	}

	public String getDataInicioFiltro() {
		return dataInicioFiltro;
	}

	public void setDataInicioFiltro(String dataInicioFiltro) {
		this.dataInicioFiltro = dataInicioFiltro;
	}

	public String getDataFimFiltro() {
		return dataFimFiltro;
	}

	public void setDataFimFiltro(String dataFimFiltro) {
		this.dataFimFiltro = dataFimFiltro;
	}

	public BigDecimal getTotalComprimentoAco() {
		return totalComprimentoAco;
	}

	public void setTotalComprimentoAco(BigDecimal totalComprimentoAco) {
		this.totalComprimentoAco = totalComprimentoAco;
	}

	public BigDecimal getTotalPesoAco() {
		return totalPesoAco;
	}

	public void setTotalPesoAco(BigDecimal totalPesoAco) {
		this.totalPesoAco = totalPesoAco;
	}

	public List<Object> getListaPesquisaAco() {
		return listaPesquisaAco;
	}

	public void setListaPesquisaAco(List<Object> listaPesquisaAco) {
		this.listaPesquisaAco = listaPesquisaAco;
	}

	public String getPecaFiltro() {
		return pecaFiltro;
	}

	public void setPecaFiltro(String pecaFiltro) {
		this.pecaFiltro = pecaFiltro;
	}

	public String getSiglaPecaFiltro() {
		return siglaPecaFiltro;
	}

	public void setSiglaPecaFiltro(String siglaPecaFiltro) {
		this.siglaPecaFiltro = siglaPecaFiltro;
	}
	
	

	
}
