package br.com.concrelaje.sisproducao.entidades;

public enum EnumSituacaoOC {
	EM_ELABORACAO,
	A_CARREGAR,
	GERAR_NOTA,
	FINALIZADO;
	
}
