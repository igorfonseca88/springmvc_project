package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PROJETO")
public class Projeto implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PJ_ID")
	@SequenceGenerator(name="SEQ_PROJETO", sequenceName="SEQ_PROJETO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_PROJETO" )
	private Long id;

	@Column(name = "PJ_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@OneToMany(mappedBy = "projeto", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE}, orphanRemoval = true )
	private List<ItemContratoProjeto> itensContratoProjeto;
	
	@ManyToOne
	@ForeignKey(name = "FK_TRACO_PROJETO")
	@JoinColumn(name = "TRACO_ID")
	private Traco traco;

	@Column(name = "PJ_NOTA")
	private String nota;
	
	/* CARACTERISTICAS */
	@Column(name = "PJ_ACO")
	private String aco;
	
	@Column(name = "PJ_VOLUME")
	private String volume;
	
	@Column(name = "PJ_PESO")
	private String peso;
	
	@Column(name = "PJ_COMPRIMENTO")
	private String comprimento;
	
	@Column(name = "PJ_FCK")
	private String fck;
	
	@Column(name = "PJ_FCJ_DESFORMA")
	private String fcjDesforma;
	
	@Column(name = "PJ_FCJ_TRANSPORTE")
	private String fcjTransporte;
	
	@Column(name = "PJ_CLASSE_AGRESSIVIDADE")
	private String classeAgressividade;
	
	@OneToMany(mappedBy = "projeto", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE}, orphanRemoval = true )
	private List<ProdutoProjeto> acessorios;
	
	@Column(name = "PJ_SITUACAO", length = 100)
	@Enumerated(value = EnumType.STRING)
	private EnumSituacaoProjeto situacao;
	
	@Column(name = "PJ_MOTIVO_REPROVACAO")
	private String motivoReprovacao;
	
	@OneToMany(mappedBy = "projeto", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE}, orphanRemoval = true )
	private Set<MaterialProjeto> materiais;
	
	@OneToMany(mappedBy = "projeto", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE}, orphanRemoval = true )
	private Set<MaterialProj> materiaisProjeto;
	
	@Column(name = "PJ_REVISAO")
	private int revisao;
	
	@Column(name = "PJ_D1")
	private String dimensao1;
	
	@Column(name = "PJ_D2")
	private String dimensao2;
	
	@Column(name = "PJ_D3")
	private String dimensao3;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public List<ItemContratoProjeto> getItensContratoProjeto() {
		return itensContratoProjeto;
	}

	public void setItensContratoProjeto(
			List<ItemContratoProjeto> itensContratoProjeto) {
		this.itensContratoProjeto = itensContratoProjeto;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getAco() {
		return aco;
	}

	public void setAco(String aco) {
		this.aco = aco;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getComprimento() {
		return comprimento;
	}

	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}

	public String getFck() {
		return fck;
	}

	public void setFck(String fck) {
		this.fck = fck;
	}

	public String getFcjDesforma() {
		return fcjDesforma;
	}

	public void setFcjDesforma(String fcjDesforma) {
		this.fcjDesforma = fcjDesforma;
	}

	public String getFcjTransporte() {
		return fcjTransporte;
	}

	public void setFcjTransporte(String fcjTransporte) {
		this.fcjTransporte = fcjTransporte;
	}

	public String getClasseAgressividade() {
		return classeAgressividade;
	}

	public void setClasseAgressividade(String classeAgressividade) {
		this.classeAgressividade = classeAgressividade;
	}
	
	public Traco getTraco() {
		return traco;
	}

	public void setTraco(Traco traco) {
		this.traco = traco;
	}

	public List<ProdutoProjeto> getAcessorios() {
		return acessorios;
	}

	public void setAcessorios(List<ProdutoProjeto> acessorios) {
		this.acessorios = acessorios;
	}

	public EnumSituacaoProjeto getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumSituacaoProjeto situacao) {
		this.situacao = situacao;
	}
	
	public String getMotivoReprovacao() {
		return motivoReprovacao;
	}

	public void setMotivoReprovacao(String motivoReprovacao) {
		this.motivoReprovacao = motivoReprovacao;
	}
	

	public Set<MaterialProjeto> getMateriais() {
		return materiais;
	}

	public void setMateriais(Set<MaterialProjeto> materiais) {
		this.materiais = materiais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projeto other = (Projeto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public int getRevisao() {
		return revisao;
	}

	public void setRevisao(int revisao) {
		this.revisao = revisao;
	}

	public String getDimensao1() {
		return dimensao1;
	}

	public void setDimensao1(String dimensao1) {
		this.dimensao1 = dimensao1;
	}

	public String getDimensao2() {
		return dimensao2;
	}

	public void setDimensao2(String dimensao2) {
		this.dimensao2 = dimensao2;
	}

	public String getDimensao3() {
		return dimensao3;
	}

	public void setDimensao3(String dimensao3) {
		this.dimensao3 = dimensao3;
	}

	public Set<MaterialProj> getMateriaisProjeto() {
		return materiaisProjeto;
	}

	public void setMateriaisProjeto(Set<MaterialProj> materiaisProjeto) {
		this.materiaisProjeto = materiaisProjeto;
	}



}
