package br.com.concrelaje.sisproducao.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "TB_TRACO")
public class Traco implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "TR_ID")
	@SequenceGenerator(name="SEQ_TRACO", sequenceName="SEQ_TRACO", allocationSize = 1, initialValue=0)
	@GeneratedValue(strategy = GenerationType.AUTO, generator="SEQ_TRACO" )
	private Long id;
	
	@Column(name = "TR_DATA_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Column(name = "TR_NOME", nullable = false, length = 100)
	private String nome;
	
	@Column(name = "TR_NUMERO", nullable = false, length = 100)
	private String numero;
	
	@Column(name = "TR_UTILIZACAO", nullable = false, length = 100)
	private String utilizacao;
	
	@Column(name = "TR_RESISTENCIA", nullable = false, length = 100)
	private String resistencia;
	
	@OneToMany(mappedBy = "traco", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	private List<ItemTraco> itensTraco;
	
	@Column(name = "TR_SITUACAO")
	@Enumerated(value = EnumType.STRING)
	private EnumLogicoAtivoInativo situacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getUtilizacao() {
		return utilizacao;
	}

	public void setUtilizacao(String utilizacao) {
		this.utilizacao = utilizacao;
	}

	public String getResistencia() {
		return resistencia;
	}

	public void setResistencia(String resistencia) {
		this.resistencia = resistencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	

	public EnumLogicoAtivoInativo getSituacao() {
		return situacao;
	}

	public void setSituacao(EnumLogicoAtivoInativo situacao) {
		this.situacao = situacao;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Traco other = (Traco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
