package br.com.concrelaje.sisproducao.entidades.wrapper;

import java.util.List;

import br.com.concrelaje.sisproducao.entidades.Cliente;
import br.com.concrelaje.sisproducao.entidades.Endereco;
import br.com.concrelaje.sisproducao.entidades.PessoaFisica;
import br.com.concrelaje.sisproducao.entidades.PessoaJuridica;
import br.com.concrelaje.sisproducao.entidades.Telefone;


public class ClientePessoaWrapper {
	
	private PessoaFisica pessoaFisica;
	
	private PessoaJuridica pessoaJuridica;
	
	private String tipoPessoa;
	
	private Telefone telefone;
	
	private Endereco endereco;
	
	private Cliente cliente;
	
	private List<Telefone> listaTelefonesCliente;
	
	private List<Endereco> listaEnderecosCliente;
	
	private String validator;
	
	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Telefone> getListaTelefonesCliente() {
		return listaTelefonesCliente;
	}

	public void setListaTelefonesCliente(List<Telefone> listaTelefonesCliente) {
		this.listaTelefonesCliente = listaTelefonesCliente;
	}

	public List<Endereco> getListaEnderecosCliente() {
		return listaEnderecosCliente;
	}

	public void setListaEnderecosCliente(List<Endereco> listaEnderecosCliente) {
		this.listaEnderecosCliente = listaEnderecosCliente;
	}

	public String getValidator() {
		return validator;
	}

	public void setValidator(String validator) {
		this.validator = validator;
	}
}
