<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	<div >
		<h3><fmt:message key="list.motorista.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/basico/motoristaForm/init.html"/>"><fmt:message key="list.motorista.novo"/></a>
		</p>
	</div>

<c:if test="${!empty listaMotoristas}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th>Tipo</th>
			<th><fmt:message key="list.motorista.nome"/></th>
			<th>Telefone</th>
			<th><fmt:message key="list.motorista.nroRegistro"/></th>
			<th>Categoria</th>
			<th><fmt:message key="list.motorista.validade"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaMotoristas}" var="item">
				<tr>
					<td>${item.tipo == 'PROPRIO' ? 'Pr�prio' : 'Terceiro'}</td>
					<td>${item.motorista}</td>
					<td>${item.telefone}</td>
					<td>${item.numeroRegistro}</td>
					<td>${item.tipo}</td>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.validade}" /></td>
					<td>
						<a href="<c:url value="/basico/motoristaList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/basico/motoristaList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaMotoristas}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>

