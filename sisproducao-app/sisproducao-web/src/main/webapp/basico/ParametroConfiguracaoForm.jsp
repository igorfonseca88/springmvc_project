<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form cssClass="form-horizontal" modelAttribute="parametroConfiguracao"	action="${pageContext.request.contextPath}/basico/parametroConfiguracaoForm/acoesForm.html">

	<form:hidden path="id" />

	<legend>
		Par�metro de Configura��es Gerais
	</legend>

	


	<div class="control-group">
		<label class="control-label" for="url">
			URL aplica��o
		</label>
		<div class="controls">
			<form:input path="url" cssStyle="width:200px" id="url"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="smtpHost">
			SMTP - Host
		</label>
		<div class="controls">
			<form:input path="smtpHost" cssStyle="width:200px" id="smtpHost"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="smtpPort">
			SMTP - Porta
		</label>
		<div class="controls">
			<form:input path="smtpPort" cssStyle="width:200px" id="smtpPort"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="smtpUser">
			SMTP - Usu�rio
		</label>
		<div class="controls">
			<form:input path="smtpUser" cssStyle="width:200px" id="smtpUser"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="smtpPass">
			SMTP - Senha
		</label>
		<div class="controls">
			<form:password showPassword="true" path="smtpPass" cssStyle="width:200px" id="smtpPass"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="recipienteNF">
			E-mail destinat�rio (nota fiscal)
		</label>
		<div class="controls">
			<form:input path="recipienteNF" cssStyle="width:200px" id="recipienteNF"/>
		</div>
	</div>
	
	<div class="control-group">
		<div class="controls">
		
			
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>


</form:form>
