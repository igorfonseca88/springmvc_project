<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	<div >
		<h3>Listagem de Estoques</h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/basico/setorForm/init.html"/>">Novo Estoque</a>
		</p>
	</div>

<c:if test="${!empty listaSetor}">
	<table class="table table-striped">
		<thead>
			<th>C�digo</th>
			<th>Descri��o</th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaSetor}" var="item">
				<tr>
					<td>${item.codigo}</td>
					<td>${item.descricao}</td>
			
					<td>
						<a href="<c:url value="/basico/setorList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/basico/setorList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaSetor}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


