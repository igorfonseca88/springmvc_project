<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	<div >
		<h3><fmt:message key="list.tipoDocumento.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/basico/tipoDocumentoForm/init.html"/>"><fmt:message key="list.tipoDocumento.novo"/></a>
		</p>
	</div>

<c:if test="${!empty listaTipoDocumento}">
	<table class="table table-striped">
		<thead>
			<th><fmt:message key="list.tipoDocumento.codigo"/></th>
			<th><fmt:message key="list.tipoDocumento.descricao"/></th>
			<th><fmt:message key="list.tipoDocumento.situacao"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaTipoDocumento}" var="item">
				<tr>
					<td>${item.id}</td>
					<td>${item.descricao}</td>
					<td>${item.situacao}</td>
					<td>
						<a href="<c:url value="/basico/tipoDocumentoList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/basico/tipoDocumentoList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaTipoDocumento}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


