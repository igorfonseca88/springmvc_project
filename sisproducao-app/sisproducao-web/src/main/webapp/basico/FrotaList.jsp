<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	<div >
		<h3><fmt:message key="list.frota.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/basico/frotaForm/init.html"/>"><fmt:message key="list.frota.novo"/></a>
		</p>
	</div>

<c:if test="${!empty listaFrotas}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th>Tipo</th>
			<th><fmt:message key="list.frota.veiculo"/></th>
			<th><fmt:message key="list.frota.placa"/></th>
			<th><fmt:message key="list.frota.marcaModelo"/></th>
			<th><fmt:message key="list.frota.chassi"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaFrotas}" var="item">
				<tr>
					<td>${item.tipo == 'PROPRIO' ? 'Pr�prio' : 'Terceiro'}</td>
					<td>${item.veiculo}</td>
					<td>${item.placa}</td>
					<td>${item.marca}/${item.modelo}</td>
					<td>${item.chassi}</td>
					<td>
						<a href="<c:url value="/basico/frotaList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/basico/frotaList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaFrotas}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>


