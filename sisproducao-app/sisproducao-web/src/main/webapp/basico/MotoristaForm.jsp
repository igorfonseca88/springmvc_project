<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form cssClass="form-horizontal" modelAttribute="motorista"	action="${pageContext.request.contextPath}/basico/motoristaForm/acoesForm.html">

	<form:hidden path="id" />

	<legend>
		<fmt:message key="form.motorista.headerForm" />
	</legend>

	<legend>
		<fmt:message key="form.motorista.dados" />
	</legend>
	
	<div class="control-group">
		<label class="control-label requiredField" for="tipoProprioTerceiro">
			Tipo
		</label>
		<div class="controls">
			<form:radiobutton path="tipoProprioTerceiro"  value="PROPRIO" />
			Pr�prio

			<form:radiobutton path="tipoProprioTerceiro"	value="TERCEIRO"/>
			Terceiro
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="nome">
			<fmt:message	key="form.motorista.nome" />
		</label>
		<div class="controls">
			<form:input path="motorista" cssStyle="width:200px" id="motorista"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="cpf">
			CPF
		</label>
		<div class="controls">
			<form:input path="cpf" cssStyle="width:200px" id="cpf"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="telefone">
			Telefone
		</label>
		<div class="controls">
			<form:input path="telefone" cssStyle="width:200px" id="telefone"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="nroRegistro">
			<fmt:message	key="form.motorista.nroRegistro" />
		</label>
		<div class="controls">
			<form:input path="numeroRegistro" cssStyle="width:200px" id="numeroRegistro"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="tipo">
			Categoria
		</label>
		<div class="controls">
			<form:input path="tipo" cssStyle="width:200px" id="tipo"/>
		</div>
	</div>
	
	

	<div class="control-group">
					<label class="control-label requiredField"> <fmt:message	key="form.motorista.validade" /> </label>
					<div class="controls">
						<form:input cssClass="date" type="text" id="datepicker"
							path="dataString" />
					</div>
				</div>

	
	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/basico/motoristaList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>


</form:form>
