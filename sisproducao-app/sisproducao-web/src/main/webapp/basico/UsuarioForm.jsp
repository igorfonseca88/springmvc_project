<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form cssClass="form-horizontal" modelAttribute="usuarioPessoaWrapper"	action="${pageContext.request.contextPath}/basico/usuarioForm/acoesForm.html">

	<form:hidden path="usuario.id" />
	<form:hidden path="pessoaFisica.id" />

	<legend>
		<fmt:message key="form.usuario.header" />
	</legend>

	<legend>
		<fmt:message key="form.usuario.dadosPessoais" />
	</legend>

	<div class="control-group">
		<label class="control-label requiredField" for="cpf">
			<fmt:message	key="form.usuario.cpf" />
		</label>
		<div class="controls">
			<form:input path="pessoaFisica.cpf" cssStyle="width:200px" id="cpf"/>
			<form:errors path="pessoaFisica.cpf" cssClass="label label-important" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="nome">
			<fmt:message key="form.usuario.nome" />
		</label>
		<div class="controls">
			<form:input path="pessoaFisica.nome" cssStyle="width:200px" id="nome" />
			<form:errors path="pessoaFisica.nome" cssClass="label label-important" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="email">
			<fmt:message key="form.usuario.email" />
		</label>
		<div class="controls">
			<form:input path="pessoaFisica.email" cssStyle="width:200px" id="email" />
			<form:errors path="pessoaFisica.email" cssClass="label label-important" />
		</div>
	</div>

	<legend>
		<fmt:message key="form.usuario.dadosUsuario" />
	</legend>
	<div class="control-group">
		<label class="control-label requiredField" for="login">
			<fmt:message key="form.usuario.login" />
		</label>
		<div class="controls">
			<form:input path="usuario.username" cssStyle="width:200px" id="username" autocomplete="off" />
			<form:errors path="usuario.username" cssClass="label label-important" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="senha">
			<fmt:message key="form.usuario.senha" />
		</label>
		<div class="controls">
			<form:password path="usuario.password" cssStyle="width:200px" id="password" autocomplete="off" showPassword="true" />
			<form:errors path="usuario.password" cssClass="label label-important" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="habilitado">
			<fmt:message key="form.usuario.habilitado" />
		</label>
		<div class="controls">
			<form:checkbox path="usuario.habilitado" cssStyle="width:200px"	id="habilitado" />
			<br />
			<form:errors path="usuario.habilitado" cssClass="label label-important" />
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/basico/usuarioList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>

<c:if test="${usuarioPessoaWrapper.usuario.id != null}">
	<div class="accordion" id="accordion2">
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
					<fmt:message key="form.usuario.papeis" />
				</a>
			</div>
			<div id="collapseTwo" class="accordion-body collapse">
				<div class="accordion-inner">
					<div>
						<form:select path="papelSelecionado.id">
							<form:option value="0">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${usuarioPessoaWrapper.listaPapeis}" itemValue="id" itemLabel="nome" />
						</form:select>

						<button type="submit" name="incluir" class="btn">
							<i class="icon-plus-sign"></i>
						</button>
					</div>

					<c:if test="${!empty usuarioPessoaWrapper.usuario.papeis}">
						<table class="table table-condensed">
							<br />
							<thead>
								<th><fmt:message key="form.papel.nome" /></th>
								<th><fmt:message key="form.papel.descricao" /></th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${usuarioPessoaWrapper.usuario.papeis}"
									var="item">
									<tr>
										<td>${item.nome}</td>
										<td>${item.descricao}</td>
										<td>
											<a href="<c:url value="/basico/usuarioForm/excluirPapel.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
											</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</c:if>
</form:form>
