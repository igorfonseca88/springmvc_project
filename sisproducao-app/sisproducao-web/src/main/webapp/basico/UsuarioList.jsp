<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	<div >
		<h3><fmt:message key="list.usuario.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/basico/usuarioForm/init.html"/>"><fmt:message key="list.usuario.novoUsuario"/></a>
		</p>
	</div>

<c:if test="${!empty listaUsuarios}">
	<table class="table table-striped">
		<thead>
			<th><fmt:message key="list.usuario.nome"/></th>
			<th><fmt:message key="list.usuario.login"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaUsuarios}" var="item">
				<tr>
					<td>${item.pessoa.nomePessoaFisicaJuridica}</td>
					<td>${item.username}</td>
					<td>
						<a href="<c:url value="/basico/usuarioList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/basico/usuarioList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaUsuarios}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


