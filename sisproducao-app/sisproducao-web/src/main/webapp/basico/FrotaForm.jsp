<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form name="frotaForm" cssClass="form-horizontal" modelAttribute="frota"	action="${pageContext.request.contextPath}/basico/frotaForm/acoesForm.html">

	<form:hidden path="id" />

	<legend>
		<fmt:message key="form.frota.headerForm" />
	</legend>

	<legend>
		<fmt:message key="form.frota.dados" />
	</legend>
	
	<div class="control-group">
		<label class="control-label requiredField" for="tipoProprioTerceiro">
			Tipo
		</label>
		<div class="controls">
			<form:radiobutton path="tipo"  value="PROPRIO" />
			Pr�prio

			<form:radiobutton path="tipo"	value="TERCEIRO"/>
			Terceiro
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="veiculo">
			<fmt:message	key="form.frota.veiculo" />
		</label>
		<div class="controls">
			<form:input path="veiculo" cssStyle="width:200px" id="veiculo"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="placa">
			<fmt:message	key="form.frota.placa" />
		</label>
		<div class="controls">
			<form:input path="placa" cssStyle="width:200px" id="placa"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="marca">
			<fmt:message	key="form.frota.marca" />
		</label>
		<div class="controls">
			<form:input path="marca" cssStyle="width:200px" id="marca"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="modelo">
			<fmt:message	key="form.frota.modelo" />
		</label>
		<div class="controls">
			<form:input path="modelo" cssStyle="width:200px" id="modelo"/>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="chassi">
			<fmt:message	key="form.frota.chassi" />
		</label>
		<div class="controls">
			<form:input path="chassi" cssStyle="width:200px" id="chassi"/>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="renavam">
			<fmt:message	key="form.frota.renavam" />
		</label>
		<div class="controls">
			<form:input path="renavam" cssStyle="width:200px" id="renavam"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="cor">
			<fmt:message	key="form.frota.cor" />
		</label>
		<div class="controls">
			<form:input path="cor" cssStyle="width:200px" id="cor"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label requiredField" for="estado">
			Estado
		</label>
		<div class="controls">
			<form:input path="estado" cssStyle="width:200px" id="estado"/>
		</div>
	</div>

	
	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/basico/frotaList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>


</form:form>
