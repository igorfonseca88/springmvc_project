<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form cssClass="form-horizontal" modelAttribute="tipoDocumento"	action="${pageContext.request.contextPath}/basico/tipoDocumentoForm/acoesForm.html">

	<form:hidden path="id" />

	<legend>
		<fmt:message key="form.tipoDocumento.headerForm" />
	</legend>

	<legend>
		<fmt:message key="form.tipoDocumento.dados" />
	</legend>

	<div class="control-group">
		<label class="control-label requiredField" for="descricao">
			<fmt:message	key="form.tipoDocumento.descricao" />
		</label>
		<div class="controls">
			<form:input path="descricao" cssStyle="width:200px" id="descricao"/>
		</div>
	</div>



	<div class="control-group">
		<label class="control-label" for="situacao"><fmt:message
				key="form.tipoDocumento.situacao" /></label>

		<div class="controls">
			<form:select path="situacao" id="situacao">
				<form:option value="">
					<fmt:message key="sistema.msg.textoComboInicial" />
				</form:option>
				<form:options items="${listaAtivoInativos}" />
			</form:select>
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/basico/tipoDocumentoList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>


</form:form>
