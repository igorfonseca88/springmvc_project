<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form cssClass="form-horizontal" modelAttribute="setor"	action="${pageContext.request.contextPath}/basico/setorForm/acoesForm.html">

	<form:hidden path="id" />

	<legend>
		Cadastro de Estoque
	</legend>

	<legend>
		Dados
	</legend>
	
	<div class="control-group">
		<label class="control-label requiredField" for="codigo">
			C�digo
		</label>
		<div class="controls">
			<form:input path="codigo" cssStyle="width:200px" id="codigo"/>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="descricao">
			Descri��o
		</label>
		<div class="controls">
			<form:input path="descricao" cssStyle="width:200px" id="descricao"/>
		</div>
	</div>



	

	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/basico/setorList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>


</form:form>
