/* Mascara de Telefone, CEP, Date, CPF e CNPJ*/
$(document).ready(function() {
	$(function() {
		$("#telefone").mask("(99) 9999-9999");
		$("#cep").mask("99999-999");
		$("#data").mask("99/99/9999");
		$("#cpf").mask("999.999.999-99");
		$("#cnpj").mask("99.999.999/9999-99");

		
		// aciona o metodo JSON buscar()
		$('#itemContrato').change(function() {
			buscarQuantidade();
		});

	});

});

$(document).ready(function() {
    
	   
	   $(".maskMoney").maskMoney({
	       showSymbol:true, 
	       symbol:"", 
	       decimal:",", 
	       precision: 3,
	       thousands:""
	   });
	   
	   
	  
	});

$(document).ready(function() {
	$(".date").datepicker({
		dateFormat : 'dd/mm/yy',
		changeMonth : true,
		changeYear : true
	});

	$("#horarioInicial").mask("99:99");
	$("#horarioFinal").mask("99:99");
});

function submitForm(idForm){
	$('#'+idForm).submit();
}

/*Limpa os campos passados por parametro*/
var limparCampos = function() {
	for ( var i = 0; i < arguments.length; i++) {
		$(arguments[i]).val('');
	}
}

function findCEP() {
	if($.trim($("#cep").val()) != ""){
		$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+$("#cep").val(), function(){
			if(resultadoCEP["resultado"] == 1){
				$("#logradouro").val(unescape(resultadoCEP["tipo_logradouro"])+" "+unescape(resultadoCEP["logradouro"]));
				$("#bairro").val(unescape(resultadoCEP["bairro"]));
				$("#cidade").val(unescape(resultadoCEP["cidade"]));
				$("#uf").val(unescape(resultadoCEP["uf"]));
				$("#numero").focus();

				ScriptDiv.innerHTML = '';
			}else{
				ScriptDiv.innerHTML = 'CEP n�o encontrado.';

				$("#logradouro").val("");
				$("#bairro").val("");
				$("#cidade").val("");
				$("#uf").val("");
				$("#complemento").val("");
				$("#numero").val("");

				return false;
			}
		});
	}
}

function buscarQuantidade() {
	$.getJSON("buscarQuantidade.html", { itemContrato: $('#itemContrato').val()}, function(resultado) {
		$('#quantidadeRestante').val(resultado);
	});
}

function displayInline(id){
	document.getElementById(id).style.display = "block";
}

function toggleChecked(status) {
	$(".checkbox").each( function() {
		$(this).attr("checked",status);
	})
}

function soNumeroeVirgula(evtKeyPress, campo)
{
	var nTecla = 0;

	if (document.all) {
		nTecla = evtKeyPress.keyCode;
	} else {
		nTecla = evtKeyPress.which;
	}
	if(nTecla == 44){
		campo.value += '.';
		return false;
	}

	if ((nTecla> 47 && nTecla <58)
			|| nTecla == 45 
			|| nTecla == 8 || nTecla == 127
			|| nTecla == 0 || nTecla == 9  // 0 == Tab
			|| nTecla == 13 || nTecla == 46 ) { // 13 == Enter 44 == ,
		return true;
	} else {
		return false;
	}
}

function soNumeroePonto(evtKeyPress, campo)
{
	var nTecla = 0;

	if (document.all) {
		nTecla = evtKeyPress.keyCode;
	} else {
		nTecla = evtKeyPress.which;
	}
	if(nTecla == 44){
		campo.value += ',';
		return false;
	}

	if ((nTecla> 47 && nTecla <58)
			|| nTecla == 45 
			|| nTecla == 8 || nTecla == 127
			|| nTecla == 0 || nTecla == 9  // 0 == Tab
			|| nTecla == 13 || nTecla == 46 ) { // 13 == Enter 44 == ,
		return true;
	} else {
		return false;
	}
}

function soNumero(evtKeyPress, campo) {
	var nTecla = 0;

	if (document.all) {
		nTecla = evtKeyPress.keyCode;
	} else {
		nTecla = evtKeyPress.which;
	}

	if ((nTecla> 47 && nTecla <58)
			|| nTecla == 45 
			|| nTecla == 8 || nTecla == 127
			|| nTecla == 0 || nTecla == 9  // 0 == Tab
			|| nTecla == 13 || nTecla == 46 ) { // 13 == Enter 44 == ,
		return true;
	} else {
		return false;
	}
}

function FormataValor(campo,tammax,teclapres) {
	var tecla = teclapres.keyCode;
	var vr = campo.value;
	vr = vr.replace( "/", "" );
	vr = vr.replace( "/", "" );
	vr = vr.replace( ",", "" );
	vr = vr.replace( ".", "" );
	vr = vr.replace( ".", "" );
	vr = vr.replace( ".", "" );
	vr = vr.replace( ".", "" );
	tam = vr.length;

	if (tam < tammax && tecla != 8){ tam = vr.length + 1; }

	if (tecla == 8 ){    tam = tam - 1; }

	if ( tecla == 8 || (tecla >= 48 && tecla <= 57) || (tecla >= 96 && tecla <= 105) ){
		if ( tam <= 2 ){
			campo.value = vr; }
		tam = tam - 1;
		if ( (tam > 2) && (tam <= 5) ){
			campo.value = vr.substr( 0, tam - 2 ) + '.' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 6) && (tam <= 8) ){
			campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + '.' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 9) && (tam <= 11) ){
			campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + '.' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 12) && (tam <= 14) ){
			campo.value = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + '.' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 15) && (tam <= 17) ){
			campo.value = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + '.' + vr.substr( tam - 2, tam );}
	}
}

function FormataMedida(campo,tammax,teclapres) {
	var tecla = teclapres.keyCode;
	var vr = campo.value;
	vr = vr.replace( "/", "" );
	vr = vr.replace( "/", "" );
	vr = vr.replace( ",", "" );
	vr = vr.replace( ".", "" );
	vr = vr.replace( ".", "" );
	vr = vr.replace( ".", "" );
	vr = vr.replace( ".", "" );
	tam = vr.length;

	if (tam < tammax && tecla != 8){ tam = vr.length + 1; }

	if (tecla == 8 ){    tam = tam - 1; }

	if ( tecla == 8 || (tecla >= 48 && tecla <= 57) || (tecla >= 96 && tecla <= 105) ){
		if ( tam <= 2 ){
			campo.value = vr; }
		tam = tam - 1;
		if ( (tam > 2) && (tam <= 5) ){
			campo.value = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 6) && (tam <= 8) ){
			campo.value = vr.substr( 0, tam - 5 ) + '' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 9) && (tam <= 11) ){
			campo.value = vr.substr( 0, tam - 8 ) + '' + vr.substr( tam - 8, 3 ) + '' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 12) && (tam <= 14) ){
			campo.value = vr.substr( 0, tam - 11 ) + '' + vr.substr( tam - 11, 3 ) + '' + vr.substr( tam - 8, 3 ) + '' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ); }
		if ( (tam >= 15) && (tam <= 17) ){
			campo.value = vr.substr( 0, tam - 14 ) + '' + vr.substr( tam - 14, 3 ) + '' + vr.substr( tam - 11, 3 ) + '' + vr.substr( tam - 8, 3 ) + '' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam );}
	}
}
/*
 * TIPO= OBRA
 * */
function iniciarIntegracao(peca){
	alert($("#idFaseSelecionada").val());
	if($("#idFaseSelecionada").val() == "1"){
	    $(document).ready(function () {
	       $.post("http://192.169.0.39/sisproducao_integracao/principal/enviarDadosAcoParaController/"+peca, 
	       
	    	    function (result) {
					alert(result);
					if(result == 'Sucesso'){
						//$("#formBaixa").submit();
						alert('Integração com o Controller realizada com sucesso.');
						$("#btnSalvar").click();
					}
					else{
						$("#resulAlert").html("<span style='color:red'>Erro ao realizar a Integração com o Controller. "+result+"</span>");
						
					}
	       		})
	       		.fail(function(data) {
	       			$("#resulAlert").html("<span style='color:red'>Erro ao realizar a Integração com o Controller. Tente novamente e caso o problema persista, informar o Administrador do sistema.</span>");
	       			alert(data);
	       		});
	      
				
			
	   });
	}else  if($("#idFaseSelecionada").val() == "2"){
		    $(document).ready(function () {
		       $.post("http://192.169.0.39/sisproducao_integracao/principal/enviarDadosMaterialParaController/"+peca, 
		       
		    	    function (result) {
						alert(result);
						if(result == 'Sucesso'){
							//$("#formBaixa").submit();
							alert('Integração com o Controller realizada com sucesso.');
							$("#btnSalvar").click();
						}
						else{
							$("#resulAlert").html("<span style='color:red'>Erro ao realizar a Integração com o Controller. "+result+"</span>");
						}
		       		})
		       		.fail(function(data) {
		       			$("#resulAlert").html("<span style='color:red'>Erro ao realizar a Integração com o Controller. Tente novamente e caso o problema persista, informar o Administrador do sistema.</span>");
		       			alert(data);
		       		});
		      
					
				
		   });
	    }else{
	    	$("#btnSalvar").click();
	    }
}

/*
 * TIPO= PRATELEIRA
 * FASE = 2 - Concretagem
 * */
function iniciarIntegracaoPrateleira(op){
	alert($("#idFaseSelecionada").val());
	if($("#idFaseSelecionada").val() == "2"){
	    $(document).ready(function () {
	       $.post("http://192.169.0.39/sisproducao_integracao/principal/enviarDadosPrateleiraParaController/"+op, 
	       
	    	    function (result) {
					alert(result);
					if(result == 'Sucesso'){
						//$("#formBaixa").submit();
						alert('Integração com o Controller realizada com sucesso.');
						$("#btnSalvar").click();
					}
					else{
						$("#resulAlert").html("<span style='color:red'>Erro ao realizar a Integração com o Controller. "+result+"</span>");
						
					}
	       		})
	       		.fail(function(data) {
	       			$("#resulAlert").html("<span style='color:red'>Erro ao realizar a Integração com o Controller. Tente novamente e caso o problema persista, informar o Administrador do sistema.</span>");
	       			alert(data);
	       		});
	      
				
			
	   });
	}else{
	    	$("#btnSalvar").click();
	    }
}