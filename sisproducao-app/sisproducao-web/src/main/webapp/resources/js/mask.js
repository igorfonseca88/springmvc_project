jQuery(function($) {
    $(".cpf").mask("999.999.999-99");
	
    $(".rg").mask("99.999.999-a");
	
    $(".cep").mask("99999-999");
	
    $(".telefone").mask("(99) 9999-9999");
	
    $(".cnpj").mask("999999999-99");
	
    $(".monetario").maskMoney({
        symbol: "",
        decimal: ",",
        precision: 3,
        thousands: ".",
        showSymbol: true
    });
	
    $(".numeros").keypress(function(event){
        var tecla = (window.event) ? event.keyCode : event.which;
        if ((tecla > 47 && tecla < 58)) 
            return true;
        else {
            if (tecla != 8) 
                return false;
            else 
                return true;
        }
    });
});