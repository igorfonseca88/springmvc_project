<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<form:form cssClass="form-horizontal" modelAttribute="inspecaoWrapper"
	action="${pageContext.request.contextPath}/qualidade/inspecaoForm/acoesForm.html">

	<form:hidden path="inspecao.id" />

	<legend> Cadastro de Inspe��o </legend>

	<legend> Dados da Inspe��o </legend>

	<table>

		<tr>
			<td>

				<div class="control-group">
					<label class="control-label requiredField"> Data: </label>
					<div class="controls">
						<form:input cssClass="date" type="text" id="datepicker"
							path="dataString" />
					</div>
				</div>
			</td>
			<td>
				<div class="control-group">
					<label class="control-label requiredField" for="situacao">Inspecionado
						por</label>

					<div class="controls">
						<form:select path="inspecao.usuario.id" id="usuario">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${inspecaoWrapper.listaUsuarios}"
								itemValue="id" itemLabel="username" />
						</form:select>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label requiredField">Hor�rio inicial </label>
					<div class="controls">
						<label class="control-label"> <form:input
								path="horarioInicial" cssStyle="width:200px" id="horarioInicial" />
						</label>
					</div>
				</div>
			</td>
			<td>
				<div class="control-group">
					<label class="control-label">Hor�rio final </label>
					<div class="controls">
						<label class="control-label"> <form:input
								path="horarioFinal" cssStyle="width:200px" id="horarioFinal" />
						</label>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label requiredField" for="situacao">Checklist</label>

					<div class="controls">
						<form:select disabled="${inspecaoWrapper.inspecao.id != null}" path="inspecao.checklist.id" id="checklist">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${inspecaoWrapper.listaChecklist}"
								itemValue="id" itemLabel="nome" />
						</form:select>
					</div>
				</div>
			</td>
			<td>
				<div class="control-group">
					<label class="control-label requiredField">C�digo da Pe�a </label>
					<div class="controls">
						<label class="control-label"> <form:input
								path="inspecao.peca.id" cssStyle="width:200px" id="peca" disabled="${inspecaoWrapper.inspecao.id == null and inspecaoWrapper.inspecao.peca.id != null}"/>
						</label>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label requiredField">GAP </label>
					<div class="controls">
						<label class="control-label"> <form:input
								path="inspecao.gap" cssStyle="width:200px" id="gap" />
						</label>
					</div>
				</div>

			</td>
		</tr>
	</table>

	<div class="control-group">
		<div class="controls">

			<button type="button"
				onclick="location.href='<c:url value="/qualidade/inspecaoList/init.html"/>'"
				name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>

			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>

		</div>
	</div>


	<c:if test="${!empty inspecaoWrapper.respostaInspecaos}">
	
	<legend> Respostas do Checklist </legend>
	
		<table class="table table-striped" style="font-size: 10px;">
			<thead>
				<th><fmt:message key="list.itemInspecao.item" /></th>
				<th><fmt:message key="list.itemInspecao.metodo" /></th>
				<th><fmt:message key="list.itemInspecao.tolerancia" /></th>
				<th>Resposta</th>
				<th>Descri��o do problema</th>
				<th>Solu��o proposta</th>
				<th>Data Reinspe��o</th>
				<th>Respons�vel</th>
			</thead>
			<tbody>
				<c:forEach items="${inspecaoWrapper.respostaInspecaos}" var="item">
					<tr>
						<td>${item.itemInspecao.item}</td>
						<td>${item.itemInspecao.metodo}</td>
						<td>${item.itemInspecao.tolerancia}</td>
						<td><select style="font-size: 10px;" width="150px"  id="respostaInspecaos2[${item.id}].legenda"
							name="respostaInspecaos2[${item.id}].legenda">
								<option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</option>
								<c:forEach items="${inspecaoWrapper.legendas}" var="itemLegendaCombo">
									<option value="${itemLegendaCombo}" <c:if test="${ item.legenda ==  itemLegendaCombo }">selected</c:if>  >${itemLegendaCombo}</option>
								</c:forEach>
						</select></td>
						<td><input type="hidden"
							id="respostaInspecaos2[${item.id}].id"
							name="respostaInspecaos2[${item.id}].id" value="${item.id}" /> 
							<textarea rows="7" style="width: 100px;" id="respostaInspecaos2[${item.id}].descricaoProblema"
								name="respostaInspecaos2[${item.id}].descricaoProblema">${item.descricaoProblema}</textarea>
						</td>
						<td><textarea rows="7" style="width: 100px;"
								id="respostaInspecaos2[${item.id}].solucaoProposta"
								name="respostaInspecaos2[${item.id}].solucaoProposta">${item.solucaoProposta}</textarea>
						</td>
						<td>
							<input class="date" type="text" id="respostaInspecaos2[${item.id}].dataReinspecao" name="respostaInspecaos2[${item.id}].dataReinspecao"
							value="${item.dataReinspecao}" />
						</td>
						
						<td>
						<select style="font-size: 10px;" width="150px" id="respostaInspecaos2[${item.id}].usuario.id"
							name="respostaInspecaos2[${item.id}].usuario.id">
								<option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</option>
								<c:forEach items="${inspecaoWrapper.listaUsuarios}" var="itemUsuarioCombo">
									<option value="${itemUsuarioCombo.id}" <c:if test="${ item.usuario.id ==  itemUsuarioCombo.id }">selected</c:if> >${itemUsuarioCombo.username}</option>
								</c:forEach>
						</select>
						</td>
						
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<div class="control-group">
			<div class="controls">


				<button type="submit" name="salvarCheck" class="btn">
					Salvar Respostas
				</button>

			</div>
		</div>

	</c:if>

</form:form>
