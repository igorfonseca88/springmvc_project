<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	<div >
		<h3><fmt:message key="list.checklist.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/qualidade/checklistForm/init.html"/>"><fmt:message key="list.checklist.novo"/></a>
		</p>
	</div>

<c:if test="${!empty listaChecklist}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th><fmt:message key="list.checklist.nome"/></th>
			<th><fmt:message key="list.checklist.usuario"/></th>
			<th><fmt:message key="list.checklist.situacao"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaChecklist}" var="item">
				<tr>
					<td>${item.nome}</td>
					<td>${item.usuario.username}</td>
					<td>${item.situacao}</td>
					<td>
						<a href="<c:url value="/qualidade/checklistList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/qualidade/checklistList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaChecklist}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>

