<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form cssClass="form-horizontal" modelAttribute="checklistWrapper"	action="${pageContext.request.contextPath}/qualidade/checklistForm/acoesForm.html">

	<form:hidden path="checklist.id" />

	<legend>
		Cadastro de Checklist
	</legend>

	<legend>
		Dados do Checklist
	</legend>

	<div class="control-group">
		<label class="control-label requiredField" for="nome">Checklist</label>
		<div class="controls">
			<form:textarea path="checklist.nome" rows="3" />
		</div>
	</div>

	
	<div class="control-group">
		<label class="control-label" for="situacao"><fmt:message
				key="form.itemInspecao.situacao" /></label>

		<div class="controls">
			<form:select path="checklist.situacao" id="situacao">
				<form:option value="">
					<fmt:message key="sistema.msg.textoComboInicial" />
				</form:option>
				<form:options items="${checklistWrapper.listaAtivoInativos}" />
			</form:select>
		</div>
	</div>
	
	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/qualidade/checklistList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>
	
	<c:if test="${!empty checklistWrapper.checklist.id}">

			<legend>
		     Item do Checklist
			</legend>

		<table>

			<tr>
				<td>
					<div class="control-group">
						<label class="control-label"> <fmt:message
								key="form.itemInspecao.item" />
						</label>
						<div class="controls">
							<label class="control-label"> <form:input
									path="inspecao.item" cssStyle="width:200px" id="item"
									 />
							</label>
						</div>
					</div>
				</td>

				<td><label style="width: 20px;"> </label></td>

				<td>
					<div class="control-group">
						<label class="control-label"> <fmt:message
								key="form.itemInspecao.metodo" />
						</label>
						<div class="controls">
							<label class="control-label"> <form:input
									path="inspecao.metodo" cssStyle="width:200px"
									id="metodo" />
							</label>
						</div>
					</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="control-group">
						<label class="control-label"> <fmt:message
								key="form.itemInspecao.tolerancia" />
						</label>
						<div class="controls">
							<label class="control-label"> <form:input
									path="inspecao.tolerancia" cssStyle="width:200px" id="item"
									 />
							</label>
						</div>
					</div>
				</td>

				<td><label style="width: 20px;"> </label></td>

				<td>
					<div class="control-group">
						<label class="control-label"> <fmt:message
								key="form.itemInspecao.situacao" />
						</label>
						<div class="controls">
							<form:select path="inspecao.situacao" id="situacao">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
								<form:options items="${checklistWrapper.listaAtivoInativos}" />
							</form:select>
						</div>
					</div>
				</td>
			</tr>
		</table>
		
		<div class="control-group">
		<div class="controls">
		
			<button type="submit" name="salvarItemInspecao" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>

		<c:if test="${!empty checklistWrapper.checklist.itensInspecao}">
						<table class="table table-striped">
							<thead>
								<th><fmt:message key="list.itemInspecao.item" /></th>
								<th><fmt:message key="list.itemInspecao.metodo" /></th>
								<th><fmt:message key="list.itemInspecao.tolerancia" /></th>
								<th><fmt:message key="list.itemInspecao.situacao" /></th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${checklistWrapper.checklist.itensInspecao}"
									var="item">
									<tr>
										<td>${item.item}</td>
										<td>${item.metodo}</td>
										<td>${item.tolerancia}</td>
										<td>${item.situacao}</td>
										<td>
											<a href="<c:url value="/qualidade/checklistForm/editarItem.html?id=${item.id}"/>"><i
													class="icon-edit" title="Editar"></i></a> | 
											<a href="<c:url value="/qualidade/checklistForm/excluirItem.html?id=${item.id}"/>"><i
													class="icon-remove"></i></a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
	</c:if>



</form:form>
