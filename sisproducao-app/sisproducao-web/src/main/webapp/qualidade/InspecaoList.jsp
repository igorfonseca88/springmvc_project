<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



	
	<form:form id="formInspecaoList" name="formInspecaoList" modelAttribute="inspecaoWrapper"  method="post" action="${pageContext.request.contextPath}/qualidade/inspecaoList/acoesForm.html">
	<div>
		<h3><fmt:message key="list.inspecao.header"/></h3>
		
		<legend>Filtros</legend>

		<table>
			<tr>
				<td>
					<label class="control-label" for="numero">N� da pe�a</label> 
					<form:input  type="text" id="codigoPecaFiltro" cssStyle="width:200px" path="codigoPecaFiltro"/>
				</td>
				
				
				
				<td>
					<label class="control-label" for="nomeObra">Obra</label> 
					<form:input  type="text" id="nomeObraFiltro" cssStyle="width:200px" path="nomeObraFiltro"/>
				</td>
				
				<td>
					<label class="control-label" for="nomeObra">Contrato</label> 
					<form:input  type="text" id="numeroContratoFiltro" cssStyle="width:200px" path="numeroContratoFiltro"/>
				</td>
				
			</tr>
			
			<tr>
				
				
				
				<td>
					<button type="submit" name="pesquisar" class="btn">	<i class="icon-search"></i>	</button>
					<button type="submit" name="limpar" class="btn"><fmt:message key="sistema.botao.limpar" /></button>
				</td>
				
			</tr>
			
			
			<tr>
				<td>
					<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/qualidade/inspecaoForm/init.html"/>">Nova Inspe��o</a>
		</p>
				</td>
			</tr>
		</table>

		
	</div>
    </form:form>
	
	

<c:if test="${!empty listaInspecao}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th>Data</th>
			<th>Hora in�cio</th>
			<th>Hora fim</th>
			<th>Inspecionado por</th>
			<th>Checklist</th>
			<th>N� da pe�a</th>
			<th>Pe�a</th>
			<th>Contrato</th>
			<th>Obra</th>
			<th>Existe Pend�ncia</th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaInspecao}" var="item">
				<tr>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item[1].dataCadastro}" /></td>
					<td><fmt:formatDate pattern="hh:mm" value="${item[1].dataInicio}" /></td>
					<td><fmt:formatDate pattern="hh:mm" value="${item[1].dataFim}" /></td>
					<td>${item[1].usuario.username}</td>
					<td>${item[1].checklist.nome}</td>
					<td>${item[1].peca.id}</td>
					<td>${item[1].peca.itemContrato.produto.nome}</td>
					<td>${item[1].peca.itemContrato.contrato.numero}</td>
					<td>${item[1].peca.itemContrato.contrato.nomeObra}</td>
					<td>${item[0] > 0 ? 'Sim' : 'N�o'}</td>
					<td>
						<a href="<c:url value="/qualidade/inspecaoList/editar.html?id=${item[1].id}"/>"><i class="icon-pencil"></i></a>|
					<c:if test="${item[1].dataFim == null}">	
						<a href="<c:url value="/qualidade/inspecaoList/excluir.html?id=${item[1].id}"/>"><i class="icon-remove"></i></a>
					</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaInspecao}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>
