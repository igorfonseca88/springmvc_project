<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



	<form:form id="formRelQualidade" name="formRelQualidade" modelAttribute="relatorioQualidadeWrapper"  method="post" 
	action="${pageContext.request.contextPath}/qualidade/relatoriosQualidade/pesquisar.html">
	<div>
		<h3>Relat�rios - Qualidade</h3>
		
		<legend>Filtros</legend>

		<table>
			<tr>
				<td>
					<div class="controls">
						
						<label class="control-label"> Tipo de relat�rio	</label> 
					<form:select path="filtroTipoRelatorio" id="filtroTipoRelatorio">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<option value="NUMERO_REPROVACOES_POR_OBRA"> N� de reprova��es /obra </option>
							<option value="NUMERO_REPROVACOES_POR_PECA"> N� de reprova��es / pe�a</option>
							<option value="NUMERO_REPROVACOES_POR_GAP"> N� de reprova��es / gap</option>
						</form:select>
					</div>
				</td>
				
				
			</tr>
			
			<tr>
				<td>
					<button type="submit" name="pesquisar" class="btn">	<i class="icon-search"></i>	</button>
					<button type="submit" name="limpar" class="btn"><fmt:message key="sistema.botao.limpar" /></button>
				</td>
				
			</tr>
			
			
		</table>

		
	</div>
	</form:form>

	<table class="table table-striped">
		<thead>
		  <c:if test="${ relatorioQualidadeWrapper.filtroTipoRelatorio != null and relatorioQualidadeWrapper.filtroTipoRelatorio == 'NUMERO_REPROVACOES_POR_OBRA'  }">
			<th>N� de reprova��es</th>
			<th>Obra</th>
		  </c:if>	
		  
		  <c:if test="${ relatorioQualidadeWrapper.filtroTipoRelatorio != null and relatorioQualidadeWrapper.filtroTipoRelatorio == 'NUMERO_REPROVACOES_POR_PECA'  }">
			<th>N� de reprova��es</th>
			<th>Pe�a</th>
		  </c:if>
		  
		  <c:if test="${ relatorioQualidadeWrapper.filtroTipoRelatorio != null and relatorioQualidadeWrapper.filtroTipoRelatorio == 'NUMERO_REPROVACOES_POR_GAP'  }">
			<th>N� de reprova��es</th>
			<th>Gap</th>
		  </c:if>
		  
		</thead>	
		<tbody>
				<c:forEach items="${listaDataModel}" var="item">
				<tr>
					
						<td>${item[0]}</td>
						<td>${item[1]}</td>
					
				</tr>
			</c:forEach>
		</tbody>
	</table>


