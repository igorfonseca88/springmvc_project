<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span>
			</a> 
			<a class="brand" href="#"><fmt:message key="sistema.sigla"/></a>
			<div class="nav-collapse collapse">
				
				<p class="navbar-text pull-right">
				
				
					<a href="<c:url value="/home.html"/>" title="Home" ><i class="icon-home icon-white"></i> Home </a> | 
					<a href="<c:url value="/logout"/>"  title="Sair do sistema" ><i class="icon-share-alt icon-white"></i> Logout </a> |
					<i class="icon-user icon-white" title="Usu�rio Logado"  ></i> Usu�rio: <%= request.getSession().getAttribute("usuario")%>
				</p>
			</div>
		</div>
	</div>
</div>