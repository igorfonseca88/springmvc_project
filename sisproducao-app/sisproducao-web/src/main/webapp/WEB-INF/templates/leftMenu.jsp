<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<% if( String.valueOf( request.getSession().getAttribute("idPecaSessao") ).equals( "null" ) ) { %>  
<div class="span3">
	<div class="well sidebar-nav">
		<ul class="nav nav-list">
		
			<li class="nav-header">Dados b�sicos</li>
			<li><a href="<c:url value="/basico/parametroConfiguracaoForm/init.html"/>">Par�metros de configura��o</a></li>
			<sec:authorize ifAnyGranted="menu.usuarios, ROLE_ADMIN">
				<li><a href="<c:url value="/basico/usuarioList/init.html"/>"><fmt:message key="menu.left.usuario"/></a></li>
			</sec:authorize>
			
			<sec:authorize ifAnyGranted="menu.tipoDocumento">
				<li><a href="<c:url value="/basico/tipoDocumentoList/init.html"/>"><fmt:message key="menu.left.tipoDocumento"/></a></li>
			</sec:authorize>
			
			
			<li><a href="<c:url value="/basico/setorList/init.html"/>">Local de Armazenamento</a></li>
						
			<sec:authorize ifAnyGranted="menu.clientes">
				<li><a href="<c:url value="/cliente/clienteList/init.html"/>"><fmt:message key="menu.left.cliente"/></a></li>
			</sec:authorize>
			
			<li><a href="<c:url value="/material/materialList/init.html"/>"><fmt:message key="menu.left.materiais"/></a></li>
			<li><a href="<c:url value="/traco/tracoList/init.html"/>"><fmt:message key="menu.left.traco"/></a></li>
			<li><a href="<c:url value="/produto/produtoList/init.html"/>"><fmt:message key="menu.left.produto"/></a></li>
			<li><a href="<c:url value="/basico/frotaList/init.html"/>"><fmt:message key="menu.left.frota"/></a></li>
			<li><a href="<c:url value="/basico/motoristaList/init.html"/>"><fmt:message key="menu.left.motorista"/></a></li>
			<li class="nav-header">Qualidade</li>
			<li><a href="<c:url value="/qualidade/checklistList/init.html"/>">Checklists</a></li>
			<li><a href="<c:url value="/qualidade/inspecaoList/init.html"/>">Inspe��o</a></li>
			<li><a href="<c:url value="/qualidade/relatoriosQualidade/init.html"/>">Relat�rios</a></li>
			<li class="nav-header">Produ��o</li>
			<li><a href="<c:url value="/contrato/contratoList/init.html"/>"><fmt:message key="menu.left.contratos"/></a></li>
			<li><a href="<c:url value="/pedido/pedidoList/init.html"/>">Pedidos</a></li>
			<li><a href="<c:url value="/projeto/projetoList/init.html"/>"><fmt:message key="menu.left.projeto"/></a></li>
			<li><a href="<c:url value="/op/opList/init.html"/>"><fmt:message key="menu.left.ordemProducao"/></a></li>
			<li><a href="<c:url value="/ordemCarregamento/ordemCarregamentoList/init.html"/>">Ordem de Carregamento</a></li>
			<li><a href="<c:url value="/producao/relatorios/relatoriosProducao/initRelProducao.html"/>">Relat�rios de Produ��o</a></li>
			<li><a href="<c:url value="/producao/relatorios/relatoriosProducao/initRelAco.html"/>">Relat�rios de A�o</a></li>
		</ul>
	</div>
</div>
<%} else{ %>
<div class="span3">
	<div class="well sidebar-nav">
		<ul class="nav nav-list">
			<li class="nav-header">Menu</li>
			<li><a href="<c:url value="/projeto/projetoList/visualizarProjeto.html"/>"><fmt:message key="menu.left.projeto"/></a></li>
			<li><a href="<c:url value="/op/opList/visualizarOP.html"/>"><fmt:message key="menu.left.ordemProducao"/></a></li>
			<li><a href="<c:url value="/qualidade/inspecaoList/init.html"/>">Inspe��o</a></li>
		</ul>
	</div>
</div>
<%}%>