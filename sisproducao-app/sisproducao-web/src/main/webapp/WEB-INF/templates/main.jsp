<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<head>
	<fmt:setLocale value="pt_BR"/>
	
	<title><fmt:message key="sistema.descricao"/></title>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.maskedinput-1.2.2.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.maskMoney.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/util.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-ui-1-8.min.js"></script>
	
	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-responsive.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/EstilosConcrelaje.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/jquery.datepick.css" media="screen" />
	
	<!-- Core CSS - Include with every page -->
    
    

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="${pageContext.request.contextPath}/resources/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>

    <!-- SB Admin CSS - Include with every page -->
    
    
    <script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>
    
	
	


</head>

<body>
	
	<tiles:insertAttribute name="header" />

	<div class="container-fluid">
		<div class="row-fluid">

			<tiles:insertAttribute name="leftMenu" />

			<div class="span9">

				<c:if test="${success != null}">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Sucesso! </strong>${success}
					</div>
				</c:if>
				<c:if test="${error != null}">
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Erro! </strong>${error}
					</div>
				</c:if>
				<c:if test="${alert != null}">
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Alerta! </strong>${alert}
					</div>
				</c:if>
				<tiles:insertAttribute name="body" />
			</div>


			<tiles:insertAttribute name="footer" />
		</div>
	</div>

</body>
</html>


