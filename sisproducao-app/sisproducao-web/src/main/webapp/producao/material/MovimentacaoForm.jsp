<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<form:form  cssClass="form-horizontal" method="post" modelAttribute="movimentacaoWrapper" action="${pageContext.request.contextPath}/material/movimentacaoForm/acoesForm.html">
		<legend><fmt:message key="form.movimentacao.headerForm"/></legend>
		
		<legend>
			<fmt:message key="form.movimentacao.dadosCadastro" />
		</legend>
               
               <form:hidden path="movimentacaoEstoque.id" />
               
               
               <div class="control-group">
					<label class="control-label" for="codigo"><fmt:message key="form.movimentacao.codigo" /></label>
					<div class="controls">
						${movimentacaoWrapper.movimentacaoEstoque.id}
					</div>
				</div>
               
               <div class="control-group">
					<label class="control-label" for="dataCadastro"><fmt:message key="form.movimentacao.dataCadastro" /></label>
					<div class="controls">
						
						<fmt:formatDate pattern="dd/MM/yyyy" value="${movimentacaoWrapper.movimentacaoEstoque.dataCadastro}" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="situacao"><fmt:message key="form.movimentacao.situacao" /></label>
					<div class="controls">
						<form:hidden path="movimentacaoEstoque.situacao" />
						${movimentacaoWrapper.movimentacaoEstoque.situacao}
					</div>
				</div>
               
				<div class="control-group">	
					<label class="control-label requiredField" for="tipoMovimentacao"><fmt:message key="form.movimentacao.tipoMovimentacao"/></label>
					<div class="controls"> 
						<form:select path="movimentacaoEstoque.tipoMovimentacao" id="tipoMovimentacao">
							<form:option value="0">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${listaTipoMovimentacao}"/>
						</form:select>
					</div>
				</div>
							
							
				<div class="control-group">	
					<label class="control-label requiredField" for="modalidade"><fmt:message key="form.movimentacao.modalidade"/></label>
					<div class="controls"> 
						<form:select path="movimentacaoEstoque.modalidade" id="modalidade">
							<form:option value="0">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${listaModalidade}"/>
						</form:select>
					</div>
				</div>			
	

		<div class="control-group">
		
			<div class="controls">
			<c:if test="${movimentacaoWrapper.movimentacaoEstoque.situacao != 'FINALIZADA'}">
				<button type="submit" name="salvar" class="btn"><fmt:message key="sistema.botao.salvar"/></button>
				<c:if test="${movimentacaoWrapper.movimentacaoEstoque.id != null and !empty movimentacaoWrapper.listaItemEstoques}">	
					<button type="submit" name="finalizar" class="btn"><fmt:message key="sistema.botao.finalizar"/></button>
				</c:if>	
			</c:if>
				<button type="button" onclick="location.href='<c:url value="/material/movimentacaoList/init.html"/>'" name="voltar" class="btn"><fmt:message key="sistema.botao.voltar"/></button>
			</div>
		</div>
		
		
		<c:if test="${movimentacaoWrapper.movimentacaoEstoque.id != null}">
		
		<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
						<fmt:message key="form.traco.materiais" /> 
					</a>
				</div>
				<div id="collapseTwo" class="accordion-body collapse">
				   <c:if test="${movimentacaoWrapper.movimentacaoEstoque.situacao != 'FINALIZADA'}">
					<div class="control-group">
						<label class="control-label requiredField">
							<fmt:message key="form.movimentacao.material" />
						</label>
						
						<div class="controls">
							<form:hidden path="material.id" />
							<form:input path="material.nome" cssStyle="width:200px"	id="nomeMaterial" />
							<a href="#myModal" role="button" class="btn" data-toggle="modal"><i	class="icon-search"></i></a>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label requiredField" for="qtdeMaterial">
							<fmt:message key="form.movimentacao.fornecedor" />
						</label>
						<div class="controls">
							<form:input path="itemMovimentacaoEstoque.fornecedor" cssStyle="width:200px"	id="fornecedor" />
						</div>
					</div>	
					
					<div class="control-group">
						<label class="control-label requiredField" for="qtdeMaterial">
							<fmt:message key="form.movimentacao.quantidade" />
						</label>
						<div class="controls">
							<form:input path="itemMovimentacaoEstoque.quantidade" cssStyle="width:200px"	id="qtdeMaterial" />
						</div>
					</div>
					
					
					<div class="control-group">
						<div class="controls" >
							<button type="submit" name="incluirItem" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
						</div>
					</div>
					</c:if>
					<c:if test="${!empty movimentacaoWrapper.listaItemEstoques}">
						<table class="table table-condensed">
							<br />
							<thead>
								<th><fmt:message key="form.movimentacao.nomeMaterial" /></th>
								<th><fmt:message key="form.movimentacao.unidadeMaterial" /></th>
								<th><fmt:message key="form.movimentacao.qtdeMaterial" /></th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${movimentacaoWrapper.listaItemEstoques}"
									var="item">
									<tr>
										<td>${item.material.nome}</td>
										<td>${item.fornecedor}</td>
										<td>${item.quantidade}</td>
										<td>
											<a href="<c:url value="/material/movimentacaoForm/excluirItem.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
											</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>						
					
				</div>
			</div>
		</div>
		
				<!-- Modal -->
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">X�</button>
				<h3 id="myModalLabel">Pesquisa de Materiais</h3>
			</div>
			<div class="modal-body">
				<p>
					<c:if test="${!empty listaMateriais}">
						<table class="table table-striped">
							<thead>
								<th><fmt:message key="list.material.numero"/></th>
								<th><fmt:message key="list.material.nome"/></th>
								<th><fmt:message key="list.material.unidade"/></th>
								<th><fmt:message key="sistema.dataTable.acoes"/></th>
							</thead>	
							<tbody>
								<c:forEach items="${listaMateriais}" var="item">
									<tr>
										<td>${item.numero}</td>
										<td>${item.nome}</td>
										<td>${item.unidade}</td>
										<td>
										<c:if test="${movimentacaoWrapper.movimentacaoEstoque.situacao != 'FINALIZADA'}">
											<a href="<c:url value="/material/movimentacaoForm/selecionarMaterial.html?id=${item.id}"/>"><i class="icon-plus-sign"></i></a>
										</c:if>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
				</p>
			</div>
		</div>
		
		</c:if>
		
	</form:form>
	
	
