<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div >
		<h3><fmt:message key="list.movimentacao.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/material/movimentacaoForm/init.html"/>"><fmt:message key="list.movimentacao.novaMovimentacao"/></a>
		</p>
	</div>

<c:if test="${!empty listaMovimentacoes}">
	<table class="table table-striped">
		<thead>
			<th><fmt:message key="list.movimentacao.numero"/></th>
			<th><fmt:message key="list.movimentacao.dataCadastro"/></th>
			<th><fmt:message key="list.movimentacao.tipoMovimentacao"/></th>
			<th><fmt:message key="list.movimentacao.modalidade"/></th>
			<th><fmt:message key="list.movimentacao.situacao"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>
		<tbody>
			<c:forEach items="${listaMovimentacoes}" var="item">
				<tr>
					<td>${item.id}</td>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataCadastro}" /></td>
					<td>${item.tipoMovimentacao}</td>
					<td>${item.modalidade}</td>
					<td>${item.situacao}</td>
					<td>
						<a href="<c:url value="/material/movimentacaoList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<c:if test="${item.situacao != 'FINALIZADA'}">
							<a href="<c:url value="/material/materialList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaMovimentacoes}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


