<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<form:form  cssClass="form-horizontal" method="post" modelAttribute="material" action="${pageContext.request.contextPath}/material/materialForm/acoesForm.html">
		<legend><fmt:message key="form.material.headerForm"/></legend>
		
		<legend>
			<fmt:message key="form.material.dadosCadastro" />
		</legend>
               
               <form:hidden path="id" />
               
               <div class="control-group">
					<label class="control-label" for="numero"><fmt:message key="form.material.numero" /></label>
					<div class="controls">
						<form:input path="numero" cssStyle="width:200px" id="numero" />
					</div>
				</div>
               
               <div class="control-group">
					<label class="control-label" for="nome"><fmt:message key="form.material.nome" /></label>
					<div class="controls">
						<form:input path="nome" cssStyle="width:200px" id="nome" />
					</div>
				</div>
				<div class="control-group">	
					<label class="control-label" for="unidade"><fmt:message key="form.material.unidade"/></label>
					<div class="controls"> 
						<form:select path="unidade" id="unidade">
							<form:options items="${listaUnidadesMaterial}"/>
						</form:select>
					</div>
				</div>
				
				<div class="control-group">	
					<label class="control-label" for="aco">O material � um a�o ?</label>
					<div class="controls"> 
						<form:select path="aco" id="aco">
							<form:options items="${listaEnumLogicoSimNao}"/>
						</form:select>
					</div>
				</div>
				
				<div class="control-group">	
					<label class="control-label" for="setor">Local de Armazenamento</label>
					<div class="controls"> 
						<form:select path="setor.id">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${listaSetor}" itemValue="id" itemLabel="descricao"/>
						</form:select>
					</div>
				</div>
				
				<!--  <div class="control-group">
					<label class="control-label" for="estoque"><fmt:message key="form.material.estoque" /></label>
					<div class="controls">
						${material.estoque == null ? 0 : material.estoque}
					</div>
				</div> -->
							
	

		<div class="control-group">
			<div class="controls">
				<button type="button" onclick="location.href='<c:url value="/material/materialList/init.html"/>'" name="voltar" class="btn"><fmt:message key="sistema.botao.voltar"/></button>
				<button type="submit" name="salvar" class="btn"><fmt:message key="sistema.botao.salvar"/></button>
			</div>
		</div>
	</form:form>
