<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div >
		<h3><fmt:message key="list.material.header" /></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/material/materialForm/init.html"/>"><fmt:message key="list.material.novoMaterial"/></a>
		</p>
	</div>
	
<c:if test="${!empty listaMateriais}">

	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th><fmt:message key="list.material.numero"/></th>
			<th><fmt:message key="list.material.nome"/></th>
			<th><fmt:message key="list.material.unidade"/></th>
			<!-- <th><fmt:message key="list.material.estoque"/></th> -->
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>
		<tbody>
			<c:forEach items="${listaMateriais}" var="item">
				<tr>
					<td>${item.numero}</td>
					<td>${item.nome}</td>
					<td>${item.unidade}</td>
					<!--<td>${item.estoque}</td>-->
					<td>
						<a href="<c:url value="/material/materialList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/material/materialList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaMateriais}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

