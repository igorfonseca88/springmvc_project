<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

	
	<form:form id="formProduto" cssClass="form-horizontal"
	modelAttribute="produto"
	action="${pageContext.request.contextPath}/produto/produtoForm/acoesForm.html">
			
		<h3><fmt:message key="form.produto.headerForm" /></h3>
               
               <form:hidden path="id" />
               
               <div class="control-group">
					<label class="control-label" for="nome">C�digo:</label>
					<div class="controls">
						<form:input path="id" cssStyle="width:200px" id="codigo" disabled="true" />
					</div>
				</div>
               
               <div class="control-group">
					<label class="control-label requiredField" for="nome">Produto:</label>
					<div class="controls">
						<form:input path="nome" cssStyle="width:200px" id="nome" />
					</div>
				</div>

				<div class="control-group">	
					<label class="control-label requiredField" for="tipo">Tipo:</label>

					<div class="controls"> 
						<form:select path="tipo" id="tipo">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${listaTiposProduto}"/>
						</form:select>
					</div>
				</div>
				
				<div class="control-group">	
					<label class="control-label" for="setor">Local de Armazenamento:</label>
					<div class="controls"> 
						<form:select path="setor.id">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${listaSetor}" itemValue="id" itemLabel="descricao"/>
						</form:select>
					</div>
				</div>

		<div class="control-group">
			<div class="controls">
				<button type="button" onclick="location.href='<c:url value="/produto/produtoList/init.html"/>'" name="voltar" class="btn"><fmt:message key="sistema.botao.voltar"/></button>
				<button type="submit" name="salvar" class="btn"><fmt:message key="sistema.botao.salvar"/></button>
			</div>
		</div>
	</form:form>
