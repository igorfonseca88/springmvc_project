<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div >
		<h3>Listagem de Produtos</h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/produto/produtoForm/init.html"/>"><fmt:message key="list.produto.novoProduto"/></a>
		</p>
	</div>

<c:if test="${!empty listaProdutos}">

	<table class="table table-striped table-bordered table-hover" id="dataTables-example">

		<thead>
		<th>C�digo</th>
			<th><fmt:message key="list.produto.nome"/></th>
			<th><fmt:message key="list.produto.tipo"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>
		<tbody>
			<c:forEach items="${listaProdutos}" var="item">
			<tr class="odd gradeX">
				<td>${item.id}</td>
					<td>${item.nome}</td>
					<td>${item.tipo}</td>
					<td>
					<c:if test="${item.tipo eq 'PADRAO' or item.tipo eq 'PRATELEIRA'}">
						<a href="<c:url value="/produto/produtoList/complementarProjeto.html?id=${item.id}"/>" title="Complementar Projeto" ><i class="icon-refresh"></i></a>|
					</c:if>
						<a href="<c:url value="/produto/produtoList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/produto/produtoList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaProdutos}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

   <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>


