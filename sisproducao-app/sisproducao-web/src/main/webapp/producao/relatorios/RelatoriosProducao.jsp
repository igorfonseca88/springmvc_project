<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



	<form:form id="formRelQualidade" name="formRelQualidade" modelAttribute="relatorioProducaoWrapper"  method="post" 
	action="${pageContext.request.contextPath}/producao/relatorios/relatoriosProducao/pesquisarProducao.html">
	<div>
		<h3>Relat�rios de Produ��o</h3>
		
		<legend>Filtros</legend>

		<table>
			<tr>
				
				
				<td>
					<label class="control-label" for="contrato">Contrato</label> 
					<form:input path="contratoFitro" id="contratoFiltro" name="contratoFiltro"/>
				</td>
				
				<td>
					<label class="control-label" for="contrato">Obra</label> 
					<form:input path="obraFiltro" id="obraFiltro" name="obraFiltro"/>
				</td>
				
				<td>
					<div class="controls">

						<label class="control-label"> Situa��o </label>
						<form:select path="situacaoProjetoFiltro" id="situacaoProjetoFiltro">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options
								items="${relatorioProducaoWrapper.listaSituacaoProjetoFiltro}" />
						</form:select>
					</div>
				</td>
				
				<td>
					<label class="control-label" for="peca">Pe�a</label> 
					<form:input path="pecaFiltro" id="pecaFiltro" name="pecaFiltro"/>
				</td>
				
			</tr>
			
			<tr>
				<td>
					<label class="control-label" for="siglaPeca">Sigla</label> 
					<form:input path="siglaPecaFiltro" id="siglaPecaFiltro" name="siglaPecaFiltro"/>
				</td>
				
				<td>

				<div class="control-group">
					<label class="control-label"> Data In�cio: </label>
					<div class="controls">
						<form:input cssClass="date" type="text" id="datepicker" path="dataInicioFiltro" />
					</div>
				</div>
			</td>
			
			<td>

				<div class="control-group">
					<label class="control-label"> Data Fim: </label>
					<div class="controls">
						<form:input cssClass="date" type="text" id="datepicker2" path="dataFimFiltro" />
					</div>
				</div>
			</td>
			
				<td>
					<button type="submit" name="btPesquisarProducao" class="btn">	<i class="icon-search"></i>	</button>
					<button type="submit" name="limpar" class="btn"><fmt:message key="sistema.botao.limpar" /></button>
					<!--  <button type="submit" name="excel" class="btn">Exportar para excel</button> -->
				</td>
				
			</tr>
			
			
		</table>

		
	</div>
	</form:form>

	<table class="table table-striped">
		<thead>
		 
			<th>Id Projeto</th>
			<th>Contrato</th>
			<th>Obra</th>
			<th>Sigla</th>
			<th>Data do Cadastro</th>
			<th>Pe�a</th>
			<th>Revis�o</th>
			<th>Situa��o</th>
			<th>Qtde de pe�as</th>
			<th>Volume de Concreto</th>
			<th>Peso do A�o</th>
		  
		</thead>	
		<tbody>
				<c:forEach items="${listaDataModel}" var="item">
				<tr>
					
						<td>${item[0]}</td>
						<td>${item[1]}</td>
						<td>${item[3]}</td>
						<td>${item[4]}</td>
						<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item[5]}" /></td>
						<td>${item[6]}</td>
						<td>${item[7]}</td>
						<td>${item[9]}</td>
						
						<td>${item[10]}</td>
						
						
						<td>${item[11]}</td>
						
						<td>${item[12]}</td>
				</tr>
			</c:forEach>
			    <tr>
			    	<td colspan="9">Total</td>
			    	<td>${relatorioProducaoWrapper.totalPesoAco}</td>
			    	<td>${relatorioProducaoWrapper.totalComprimentoAco}</td>
			    </tr>
		</tbody>
	</table>


