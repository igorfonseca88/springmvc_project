<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<div >
		<h3>Listagem de Contratos</h3>
		
		<legend>Filtros</legend>
		<form:form cssClass="" method="post" action="${pageContext.request.contextPath}/contrato/contratoList/pesquisar.html"> 
			<div class="control-group">
				<label class="control-label" for="numero">N�mero:</label>
				<div class="controls">
					<input type="text" value="" style="width:200px" name="numero" id="numero"/>
					<button type="submit" name="pesquisar" class="btn"><i
				class="icon-search"></i></button>
				</div>
			</div>
		</form:form>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/contrato/contratoForm/init.html"/>"><fmt:message key="list.contrato.novoContrato"/></a>
		</p>
	</div>
	
<c:if test="${!empty listaContratos}">

	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th><fmt:message key="list.contrato.dataCadastro"/></th>
			<th><fmt:message key="list.contrato.numero"/></th>
			<th><fmt:message key="list.contrato.nomeObra"/></th>
			<th><fmt:message key="list.contrato.cliente"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>
		<tbody>
			<c:forEach items="${listaContratos}" var="item">
				<tr>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataCadastro}" /></td>
					<td>${item.numeroObjeto}</td>
					<td>${item.nomeObra}</td>
					<td>${item.documento} - ${item.cliente}</td>
					<td>
						<a href="<c:url value="/contrato/contratoList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<c:if test="${item.situacao != 'FINALIZADO'}">	
							<a href="<c:url value="/contrato/contratoList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaContratos}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>
