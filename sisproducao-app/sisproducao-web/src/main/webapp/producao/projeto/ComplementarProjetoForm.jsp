<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<form:form id="complementarProjetoForm" cssClass="form-horizontal" enctype="multipart/form-data" modelAttribute="projetoWrapper" action="${pageContext.request.contextPath}/projeto/complementarProjetoForm/acoesForm.html">
	<form:hidden path="projeto.id" />

	<legend>
		<fmt:message key="form.complementarProjeto.headerForm" />
	</legend>

<c:if test="${projetoWrapper.projetoView.numero != null}">
	<legend>
		<fmt:message key="form.projeto.dadosContrato" />
	</legend>
	
	<table>
	
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label">
						<fmt:message key="form.contrato.numero" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projetoView.numero" cssStyle="width:200px" id="numero" disabled="true"/>
						</label>
					</div>
				</div>
			</td>
			
			<td>
				<label style="width: 20px;">
				</label>
			</td>
			
			<td>
				<div class="control-group">
					<label class="control-label">
						<fmt:message key="form.contrato.nomeObra"/>
					</label>
					<div class="controls">
					<label class="control-label">
						<form:input path="projetoView.nomeObra" cssStyle="width:200px" id="nomeObra" disabled="true"/>
					</label>
					</div>
				</div>
			</td>
		</tr>
	</table>
	</c:if>
	
	<legend>
		<fmt:message key="form.complementarProjeto.dadosProjeto" />
	</legend>
	
	<table>
	<c:if test="${projetoWrapper.projetoView.numero != null}">
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="list.projeto.siglaItemContratoProjeto" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projetoView.sigla" cssStyle="width:200px" id="sigla" disabled="true"/>
						</label>
					</div>
				</div>
			</td>
			
			<td>
				<label style="width: 20px;">
				</label>
			</td>
			
			<td>
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="form.complementarProjeto.qtdItensContrato" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="countItensContratoProjeto" cssStyle="width:200px" id="countItensContratoProjeto" disabled="true"/>
						</label>
					</div>
				</div>
			</td>
		</tr>
		</c:if>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label" >
						Revis�o
					</label>
					<div class="controls">
						<label class="control-label">
							<select name="revisao">
								<option> Selecione </option>
								<option value="1" <c:if test="${projetoWrapper.projetoView.revisao == 1 or projetoWrapper.projetoView.revisao == 0}"> selected</c:if> > 1 </option>
								<option value="2" <c:if test="${projetoWrapper.projetoView.revisao == 2}"> selected</c:if> > 2 </option>
								<option value="3" <c:if test="${projetoWrapper.projetoView.revisao == 3}"> selected</c:if> > 3 </option>
								<option value="4" <c:if test="${projetoWrapper.projetoView.revisao == 4}"> selected</c:if> > 4 </option>
								<option value="5" <c:if test="${projetoWrapper.projetoView.revisao == 5}"> selected</c:if> > 5 </option>
								<option value="6" <c:if test="${projetoWrapper.projetoView.revisao == 6}"> selected</c:if> > 6 </option>
								<option value="7" <c:if test="${projetoWrapper.projetoView.revisao == 7}"> selected</c:if> > 7 </option>
								<option value="8" <c:if test="${projetoWrapper.projetoView.revisao == 8}"> selected</c:if> > 8 </option>
								<option value="9" <c:if test="${projetoWrapper.projetoView.revisao == 9}"> selected</c:if> > 9 </option>
								<option value="10" <c:if test="${projetoWrapper.projetoView.revisao == 10}"> selected</c:if> > 10 </option>
							</select>
							
							
						</label>
					</div>
					
					
					
					
				</div>
			</td>
			
			<td>
				<label style="width: 20px;">
				</label>
			</td>
			
			<td>
				<label style="width: 20px;">
				</label>
			</td>
		</tr>
	</table>
	
	
	<legend>
		<fmt:message key="form.projeto.caracteristica" />
	</legend>
	
	<table>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="form.complementarProjeto.aco" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.aco" cssStyle="width:200px" id="aco"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
			 </td>
			 <td>	
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="form.complementarProjeto.fck" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.fck" cssStyle="width:200px" id="fck"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
				
			</td>
		</tr>
		
		<tr>
			<td>	
				
				<div class="control-group">
					<label class="control-label">
						<fmt:message key="form.complementarProjeto.volume" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.volume" cssStyle="width:200px" id="volume"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
				
			</td>
			
			<td>	
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="form.complementarProjeto.fcjDesforma" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.fcjDesforma" cssStyle="width:200px" id="fcjDesforma"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
				
			
			</td>
		</tr>	
		
		<tr>
		  <td>	
				<div class="control-group">
					<label class="control-label">
						<fmt:message key="form.complementarProjeto.peso" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.peso" cssStyle="width:200px" id="peso" 
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
			</td>
			
			<td>	
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="form.complementarProjeto.fcjTransporte" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.fcjTransporte" cssStyle="width:200px" id="fcjTransporte"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
				
			</td>
		</tr>
		
		<tr>
		   <td>		
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="form.complementarProjeto.comprimento" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.comprimento" cssStyle="width:200px" id="comprimento"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
			</td>
			
			<td>
				<div class="control-group">
					<label class="control-label" >
						<fmt:message key="form.complementarProjeto.agressividade" />
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.classeAgressividade" cssStyle="width:200px" id="agressividade"/>
						</label>
					</div>
				</div>
			</td>	
		</tr>		
				
		<tr>
		    <td>
		    	<div class="control-group">
					<label class="control-label" for="traco">
						<fmt:message key="form.complementarProjeto.traco" />
					</label>
					<div class="controls">
						<form:select path="projeto.traco.id">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options items="${listaTracos}" itemValue="id" itemLabel="nome"/>
						</form:select>
					</div>
				</div>
			</td>
			<td>
				<div class="control-group">
					<label class="control-label" >
						D1 (cm)
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.dimensao1"  cssStyle="width:200px" id="dimensao1"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
			</td>
		</tr>		
		<tr>
			<td>
			   <div class="control-group">
					<label class="control-label" >
						D2 (cm)
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.dimensao2" cssStyle="width:200px" id="dimensao2"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
			  </td>
			  <td> 		
				
				<div class="control-group">
					<label class="control-label" >
						D3 (cm)
					</label>
					<div class="controls">
						<label class="control-label">
							<form:input path="projeto.dimensao3" cssStyle="width:200px" id="dimensao3"
								onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
						</label>
					</div>
				</div>
				
			</td>
		</tr>
	</table>
	
	
	
	<div class="accordion-group" style="${projetoWrapper.projeto.motivoReprovacao == null ? 'display: none;' : 'display: block;' }" id="divMotivo">
			<legend>
				<fmt:message key="form.complementarProjeto.motivoReprovacao" />
			</legend>
			<form:textarea path="projeto.motivoReprovacao" cssStyle="width: 95%; height: 87px;"/>
			<br>
			<div class="control-group">
				<button type="submit" name="okReprovar" class="btn">
					<fmt:message key="sistema.botao.ok" />
				</button>
				
				<button type="submit" name="cancelarReprovacao" class="btn" style="${projetoWrapper.projeto.motivoReprovacao != null ? 'display: none;' : 'display: inline;' }">
					<fmt:message key="sistema.botao.cancelar" />
				</button>
			</div>
		</div>
		
		<div class="control-group">
			<div class="controls">
			
				<button type="button" onclick="location.href='<c:url value="/projeto/projetoList/init.html"/>'" name="voltar" class="btn">
					<fmt:message key="sistema.botao.voltar" />
				</button>
				
				<c:if test="${projetoWrapper.visualizar == true}">
				<button type="submit" name="salvar" class="btn">
					<fmt:message key="sistema.botao.salvar" />
				</button>
				</c:if>
				
				<button type="submit" name="cancelarReprovacaoProjeto" class="btn" style="${projetoWrapper.projeto.motivoReprovacao == null ? 'display: none;' : 'display: inline;' }">
					<fmt:message key="sistema.botao.cancelarReprovacao" />
				</button>

				<c:if test="${projetoWrapper.projeto.situacao == null and projetoWrapper.visualizar == true}">
				<sec:authorize ifAnyGranted="complementarProjeto.enviar">
					<button type="submit" name="enviar" class="btn">
						<fmt:message key="sistema.botao.enviar" />
					</button>
				</sec:authorize>
				</c:if>
				
				<c:if test="${projetoWrapper.habilitarAprovarReprovar and projetoWrapper.visualizar == true}">
					<sec:authorize ifAnyGranted="complementarProjeto.aprovar">
					<button type="submit" name="aprovar" class="btn">
						<fmt:message key="sistema.botao.aprovar" />
					</button>
					</sec:authorize>
					
					<sec:authorize ifAnyGranted="complementarProjeto.reprovar">
						<button type="button" name="reprovar" onclick="displayInline('divMotivo');" class="btn">
							<fmt:message key="sistema.botao.reprovar" />
						</button>
					</sec:authorize>
				</c:if>
				
			</div>
		</div>
	
	
	<c:if test="${projetoWrapper.visualizarAcessorios == true }">
		<div class="accordion" id="accordion1">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"> 
						<fmt:message key="form.complementarProjeto.acessorios" />
					</a>
				</div>
				
				<div id="collapseOne"  class="accordion-body collapse">
					<div class="accordion-inner">

					<div class="control-group">
						<label class="control-label">
							<fmt:message key="form.complementarProjeto.acessorios" />
						</label>
						
						<div class="controls">
							<form:select path="produtoProjeto.produto.id">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
							<form:options items="${listaProdutos}" itemValue="id" itemLabel="nome"/>
							</form:select>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" >
							<fmt:message key="form.complementarProjeto.quantidade" />
						</label>
						
						<div class="controls">
							<label class="control-label">
								<form:input path="produtoProjeto.quantidade" cssStyle="width:200px" id="quantidade" 
									onkeypress="return soNumero(event, this);" />
							</label>
						</div>
					</div>

					<div class="control-group">
						<div class="controls" >
						<c:if test="${projetoWrapper.visualizarAcessorios == true and projetoWrapper.projeto.situacao != 'APROVADO' }">
							<button type="submit" name="incluirAcessorio" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
							</c:if>
						</div>
					</div>
						
						<c:if test="${!empty projetoWrapper.projeto.acessorios}">
							<table class="table table-striped">
								<thead>
									<th><fmt:message key="form.complementarProjeto.acessorios"/></th>
									<th><fmt:message key="form.complementarProjeto.quantidade"/></th>
									<th><fmt:message key="sistema.dataTable.acoes"/></th>
								</thead>
								<tbody>
									<c:forEach items="${projetoWrapper.projeto.acessorios}" var="item">
										<tr>
											<td>${item.produto.nome}</td>
											<td>${item.quantidade}</td>
											<td>
											<c:if test="${projetoWrapper.visualizar == true}">
												<a href="<c:url value="/projeto/complementarProjetoForm/excluirAcessorio.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		
	</c:if>
		
		<div class="accordion" id="accordion3">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree"> 
						Tabela de A�o (Quantitativo Unit�rio)
					</a>
				</div>
				
				<div id="collapseThree"  class="accordion-body collapse">
					<div class="accordion-inner">

					<div class="control-group">
						<label class="control-label">
							A�o
						</label>
						
						<div class="controls">
							<form:select path="materialProjeto.material.id">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
							<form:options items="${listaMateriaisAco}" itemValue="id" itemLabel="nome"/>
							</form:select>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" >
							C.Total(m), m�
						</label>
						
						<div class="controls">
							<label class="control-label">
								<form:input path="materialProjeto.comprimentoString" cssStyle="width:200px" id="comprimento" 
									onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
							</label>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" >
							Peso (kg)
						</label>
						
						<div class="controls">
							<label class="control-label">
								<form:input path="materialProjeto.pesoString" cssStyle="width:200px" id="peso"
									onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
							</label>
						</div>
					</div>

					<div class="control-group">
						<div class="controls" >
						<c:if test="${projetoWrapper.visualizar == true and projetoWrapper.projeto.situacao != 'APROVADO' }">
							<button type="submit" name="incluirMaterialAco" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
							</c:if>
						</div>
					</div>
						
						<c:if test="${!empty projetoWrapper.projeto.materiais}">
							<table class="table table-striped">
								<thead>
									<th>A�o</th>
									<th>C.Total(m), m�</th>
									<th>Peso (kg)</th>
									<th><fmt:message key="sistema.dataTable.acoes"/></th>
								</thead>
								<tbody>
									<c:forEach items="${projetoWrapper.projeto.materiais}" var="item">
										<tr>
											<td>${item.material.nome}</td>
											<td>${item.comprimento}</td>
											<td>${item.peso}</td>
											<td>
											<c:if test="${projetoWrapper.visualizar == true}">
												<a href="<c:url value="/projeto/complementarProjetoForm/excluirMaterialAco.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="accordion" id="accordion5">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion5" href="#collapseFive"> 
						Materiais
					</a>
				</div>
				
				<div id="collapseFive"  class="accordion-body collapse">
					<div class="accordion-inner">

					<div class="control-group">
						<label class="control-label">
							Material
						</label>
						
						<div class="controls">
							<form:select path="materialProj.material.id">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
							<form:options items="${listaMateriais}" itemValue="id" itemLabel="nome"/>
							</form:select>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" >
							Quantidade
						</label>
						
						<div class="controls">
							<label class="control-label">
								<form:input path="materialProj.quantidadeString" cssStyle="width:200px" id="quantidade" 
									onkeypress="return soNumeroeVirgula(event, this);" cssClass="maskMoney" />
							</label>
						</div>
					</div>
					
				

					<div class="control-group">
						<div class="controls" >
						<c:if test="${projetoWrapper.visualizar == true and projetoWrapper.projeto.situacao != 'APROVADO' }">
							<button type="submit" name="incluirMaterial" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
							</c:if>
						</div>
					</div>
						
						<c:if test="${!empty projetoWrapper.projeto.materiaisProjeto}">
							<table class="table table-striped">
								<thead>
									<th>Material</th>
									<th>Unidade</th>
									<th>Quantidade</th>
									<th><fmt:message key="sistema.dataTable.acoes"/></th>
								</thead>
								<tbody>
									<c:forEach items="${projetoWrapper.projeto.materiaisProjeto}" var="item">
										<tr>
											<td>${item.material.nome}</td>
											<td>${item.material.unidade}</td>
											<td>${item.quantidade}</td>
											<td>
											<c:if test="${projetoWrapper.visualizar == true}">
												<a href="<c:url value="/projeto/complementarProjetoForm/excluirMaterial.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		
		
	
	
		<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"> 
						<fmt:message key="form.complementarProjeto.documentos" />
					</a>
				</div>
				
				<div id="collapseTwo" class="accordion-body collapse">
					<div class="accordion-inner">

						<div class="control-group">
							<label class="control-label" for="tipoDocumento">
								<fmt:message key="form.complementarProjeto.tipoDocumento" />
							</label>
							
							<div class="controls">
								<form:select path="documentoAnexo.tipoDocumento.id" id="tipoDocumento">
									<form:option value="">
										<fmt:message key="sistema.msg.textoComboInicial" />
									</form:option>
									<form:options items="${projetoWrapper.listaTipoDocumento}" itemValue="id" itemLabel="descricao" />
								</form:select>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="descricao">
								<fmt:message key="form.complementarProjeto.descricaoArquivo" />
							</label>
							<div class="controls">
								<form:textarea path="documentoAnexo.descricao" rows="3" />
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<label for="file">
									<fmt:message key="sistema.msg.upload" />
								</label> 
								<input type="file" name="file" />
								
								<c:if test="${projetoWrapper.visualizar == true}">
								<button type="submit" name="upload" class="btn">
									<i class="icon-upload"></i>
								</button>
								</c:if>
							</div>
						</div>

						

						<c:if test="${!empty projetoWrapper.listaDocumentoAnexo}">
							<table class="table table-striped">
								<thead>
									<th><fmt:message key="form.complementarProjeto.nomeArquivo"/></th>
									<th><fmt:message key="form.complementarProjeto.descricaoArquivo"/></th>
									<th><fmt:message key="sistema.dataTable.acoes"/></th>
								</thead>
								<tbody>
									<c:forEach items="${projetoWrapper.listaDocumentoAnexo}" var="item">
										<tr>
											<td>${item.filename}</td>
											<td>${item.descricao}</td>
											<td>
												<a href="<c:url value="/projeto/complementarProjetoForm/download.html?id=${item.id}"/>">
													<i class="icon-download"></i>
												</a>|
											    <c:if test="${projetoWrapper.visualizar == true}">	
												<a href="<c:url value="/projeto/complementarProjetoForm/excluirDocumento.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
		
</form:form>

<c:if test="${projetoWrapper.projeto.situacao == 'APROVADO'}">

<form:form id="pecasForm" cssClass="form-horizontal" modelAttribute="projetoWrapper" action="${pageContext.request.contextPath}/projeto/complementarProjetoForm/cancelarProducao.html">	
		  <div class="accordion" id="accordion4">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour"> 
						<fmt:message key="form.complementarProjeto.produtosProducao" />
					</a>
				</div>
						
				<div id="collapseFour" class="accordion-body collapse">
					<div class="accordion-inner">
						<c:if test="${!empty projetoWrapper.listaItensContratoProjeto}">
							<table class="table table-striped">
								<thead>
									<th></th>
									<th>C�digo da Pe�a</th>
									<th><fmt:message key="form.complementarProjeto.nomeProduto"/></th>
									<th>Situa��o</th>
								</thead>
								<tbody>
									<c:forEach items="${projetoWrapper.listaItensContratoProjeto}" var="item">
										<tr>
										    <td><input type="checkbox" <c:if test="${item.situacao == 'PRODUZIDO' || item.situacao == 'CANCELADO' }">disabled</c:if> name="ckPecas" id="ckPecas${item.id}" value="${item.id}"/></td>
											<td>${item.id}</td>
											<td>${item.itemContrato.produto.nome}</td>
											<td>${item.situacao}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							
							<input type="hidden" name="checados" id="checados" value=""/>
							
							<div class="control-group">
								<div class="controls" >
									<button type="button" onclick="verificarChecados();" name="cancelarProducao" class="btn">Cancelar produ��o</button>
								</div>
							</div>
							
						</c:if>
					</div>
				</div>
			</div>
		</div> 
</form:form>		
		
</c:if>		
		



<script type="text/javascript">

function verificarChecados(){
	
	var check = document.getElementsByName("ckPecas");
	var len = check.length;
	var checados = "";
	
	for(i=0; i<len; i++ ){
		if(check[i].checked){
			checados+="@"+check[i].value;
		}
		
	}
	document.getElementById('checados').value = checados;
	document.getElementById('pecasForm').submit();
}



</script>

