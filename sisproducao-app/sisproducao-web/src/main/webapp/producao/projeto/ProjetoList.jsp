<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div >
		<h3><fmt:message key="list.projeto.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/projeto/projetoForm/init.html"/>"><fmt:message key="list.projeto.novoProjeto"/></a>
		</p>
	</div>

<c:if test="${!empty listaProjetos}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th><fmt:message key="list.projeto.nomeObraContrato"/></th>
			<th><fmt:message key="list.projeto.produto"/></th>
			<th><fmt:message key="list.projeto.siglaItemContratoProjeto"/></th>
			<th><fmt:message key="list.projeto.numeroContrato"/></th>
			<th>Objeto</th>
			<th><fmt:message key="list.projeto.dataCriacao"/></th>
			<th>Situa��o</th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaProjetos}" var="item">
			<c:if test="${item.nomeObra != null}">
				<tr>
					<td>${item.nomeObra}</td>
					<td>${item.nomeProduto}</td>
					<td>${item.sigla}</td>
					<td>${item.numero}</td>
					<td>${item.objeto}</td>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataCadastro}" /></td>
					<td>${item.situacao}</td>
					<td>
						<a href="<c:url value="/projeto/projetoList/complementarProjeto.html?id=${item.id}"/>" title="Complementar Projeto" ><i class="icon-refresh"></i></a>|
						<a href="<c:url value="/projeto/projetoList/excluir.html?id=${item.id}"/>" title="Excluir Projeto"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:if>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaProjetos}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>
