<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form id="projetoForm" cssClass="form-horizontal" modelAttribute="projetoWrapper" action="${pageContext.request.contextPath}/projeto/projetoForm/acoesForm.html">
	<form:hidden path="projeto.id" />

	<legend>
		<fmt:message key="form.projeto.headerForm" />
	</legend>

	<legend>
		<fmt:message key="form.projeto.dadosContrato" />
	</legend>

	<div class="control-group">
		<div>
			<form:select path="tipoBusca" id="tipoBusca">
				<form:option value="0">
					<fmt:message key="sistema.msg.textoComboPesquisa" />
				</form:option>
				<form:options items="${projetoWrapper.listaTiposBusca}"/>
			</form:select>
			
			<form:input path="buscarPor" cssStyle="width:200px" id="buscarPor"/>
			
			<button type="submit" name="buscar" class="btn"><i class="icon-search" ></i></button>
		</div>
	</div>
		
	<c:if test="${!empty projetoWrapper.listaContratos}">
	
		<table class="table table-striped">
			<thead>
				<th><fmt:message key="list.contrato.numero"/></th>
				<th><fmt:message key="list.contrato.nomeObra"/></th>
			</thead>
			<tbody>
				<c:forEach items="${projetoWrapper.listaContratos}" var="item">
					<tr>
						<td>${item.numero}</td>
						<td>${item.nomeObra}</td>
						<td>
							<a href="<c:url value="/projeto/projetoForm/sequenciarItemContrato.html?id=${item.id}"/>"><fmt:message key="form.projeto.acao.sequenciar"/></a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
	
	<c:if test="${!empty projetoWrapper.listaItemContratos}">
		<legend>
			<fmt:message key="form.projeto.sequenciarItens" />
		</legend>
		
		
		<table class="table table-striped">
			<thead>
				<th><fmt:message key="form.projeto.itemContrato"/></th>
				<th><fmt:message key="form.projeto.quantidadeItem"/></th>
				<th><fmt:message key="form.projeto.quantidade"/></th>
				<th><fmt:message key="form.projeto.siglaSequencial"/></th>
			</thead>
			<tbody>
				<tr>
					<td>
						<form:select path="itemContratoProjeto.itemContrato.id" id="itemContrato">
							<form:option value="0">
								<fmt:message key="sistema.msg.textoComboPesquisa" />
							</form:option>
							<form:options items="${projetoWrapper.listaItemContratos}" itemValue="id" itemLabel="produto.nome"/>
						</form:select>	
					</td>
					<td>
						<form:input path="quantidadeRestante" cssStyle="width:100px" id="quantidadeRestante" disabled="true"/>
					</td>
					<td>
						<form:input path="quantidadeItemSelecionado" cssStyle="width:100px" id="quantidade"/>
					</td>
					<td>
						<form:input path="itemContratoProjeto.sigla" cssStyle="width:100px" id="sequencial"/>
					</td>
				</tr>
			</tbody>
		</table>
	</c:if>
	
	<c:if test="${!empty projetoWrapper.listaProjetosGerados}">
	
	<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
						<fmt:message key="form.projeto.projetosGerados" /> 
					</a>
				</div>

				<div id="collapseTwo" class="accordion-body collapse">
				
					<table class="table table-striped">
						<thead>
							<th><fmt:message key="list.projeto.produto"/></th>
							<th><fmt:message key="list.projeto.siglaItemContratoProjeto"/></th>
							<th><fmt:message key="list.projeto.dataCriacao"/></th>
							<th><fmt:message key="sistema.dataTable.acoes"/></th>
						</thead>	
						<tbody>
							<c:forEach items="${projetoWrapper.listaProjetosGerados}" var="item">
								<tr>
									<td>${item.nomeProduto}</td>
									<td>${item.sigla}</td>
									<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataCadastro}" /></td>
									<td>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
	</div>
	
</c:if>
		
	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/projeto/projetoList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<c:if test="${!empty projetoWrapper.listaItemContratos}">
				<button type="submit" name="gerarProjetoItemContrato" class="btn">
					<fmt:message key="form.projeto.gerarProjeto" />
				</button>
			</c:if>
			
		</div>
	</div>

</form:form>

