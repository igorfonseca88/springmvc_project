<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<form:form cssClass="form-horizontal" modelAttribute="ordemCarregamentoWrapper" enctype="multipart/form-data"
	action="${pageContext.request.contextPath}/ordemCarregamento/ordemCarregamentoForm/acoesForm.html">

	<form:hidden path="ordemCarregamento.id" />

	<legend> Cadastro de Ordem de Carregamento </legend>

	<legend> Dados da Ordem de Carregamento </legend>

	<table>

		<tr>
		
		    <td>

				<div class="control-group">
					<label class="control-label requiredField"> N�mero Ordem: </label>
					<div class="controls">
						<form:input  type="text" id="id" cssStyle="width:200px" path="ordemCarregamento.id" disabled="true" />
					</div>
				</div>
			</td>
		 </tr>
		 
		 <tr>   
		    <td>
		    <div class="control-group">
				<label class="control-label" for="situacao">Tipo</label>
		
				<div class="controls">
					<form:select path="tipo" id="tipo" onchange="esconderCampo()">
						<form:option value="">
							<fmt:message key="sistema.msg.textoComboInicial" />
						</form:option>
						<form:options items="${listaModalidade}" />
					</form:select>
				</div>
			</div>
		    </td>
		
			<td>
				<div class="control-group" id="divContrato" style="${ordemCarregamentoWrapper.ordemCarregamento.contrato.id != NULL ? 'display:block' : 'display:none'}" >
					<label class="control-label requiredField" for="contrato">Contrato</label>

					<div class="controls">
						<form:select path="ordemCarregamento.contrato.id" id="contrato" cssStyle="width:300px">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							
							<c:forEach items="${ordemCarregamentoWrapper.listaContrato}" var="item">
								<option value="${item.id}" <c:if test="${ordemCarregamentoWrapper.ordemCarregamento.contrato.id == item.id}"> selected </c:if>>${item.numero} - ${item.nomeObra} </option>
							</c:forEach>
							
						</form:select>
						
						
					</div>
				</div>
				
				<div class="control-group" id="divPedido" style="${ordemCarregamentoWrapper.ordemCarregamento.pedido.id != NULL ? 'display:block' : 'display:none'}">
					<label class="control-label requiredField" for="pedido">Pedido</label>

					<div class="controls">
						<form:select path="ordemCarregamento.pedido.id" id="pedido" cssStyle="width:300px">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							
							<c:forEach items="${ordemCarregamentoWrapper.listaPedido}" var="item">
								<option value="${item.id}" <c:if test="${ordemCarregamentoWrapper.ordemCarregamento.pedido.id == item.id}"> selected </c:if>>${item.numero}</option>
							</c:forEach>
							
						</form:select>
						
						
					</div>
				</div>
				
			</td>
		
						
		</tr>
		
		
		<tr>
		
		    <td>
				<div class="control-group">
					<label class="control-label requiredField"> Data Carregamento: </label>
					<div class="controls">
						<form:input cssClass="date" type="text" id="datepicker"
							path="dataCarregamentoString" />
					</div>
				</div>
			</td>
		
		
			<td>
				<div class="control-group">
					<label class="control-label requiredField"> Data Prevista Sa�da: </label>
					<div class="controls">
						<form:input cssClass="date" type="text" id="datepicker2"
							path="dataPrevistaSaidaString" />
					</div>
				</div>
			</td>
		
			
			
		</tr>
		
		<tr>
		
		    <td>
				<div class="control-group">
					<label class="control-label">Hor�rio inicial </label>
					<div class="controls">
						<label class="control-label"> <form:input
								path="horarioInicial" cssStyle="width:200px" id="horarioInicial" />
						</label>
					</div>
				</div>
			</td>
		
			<td>
				<div class="control-group">
					<label class="control-label">Hor�rio final </label>
					<div class="controls">
						<label class="control-label"> <form:input
								path="horarioFinal" cssStyle="width:200px" id="horarioFinal" />
						</label>
					</div>
				</div>
			</td>
			
			
			
		</tr>
		
		<tr>
		
		
		
			<td>
				<div class="control-group">
					<label class="control-label requiredField" for="motorista">Motorista</label>

					<div class="controls">
						<form:select path="ordemCarregamento.motorista.id" id="motorista" cssStyle="width:300px">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<c:forEach items="${ordemCarregamentoWrapper.listaMotorista}" var="item">
								<option value="${item.id}" <c:if test="${ordemCarregamentoWrapper.ordemCarregamento.motorista.id == item.id}"> selected </c:if>>${item.cpf} - ${item.motorista} </option>
							</c:forEach>
						</form:select>
					</div>
				</div>
			</td>
			
		</tr>
		
		
	</table>

	<div class="control-group">
		<div class="controls">

			<button type="button"
				onclick="location.href='<c:url value="/ordemCarregamento/ordemCarregamentoList/init.html"/>'"
				name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO'}">
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			</c:if>
			
			<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao == 'GERAR_NOTA' and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'FINALIZADO' }">
			<button type="submit" name="finalizar" class="btn">
				<fmt:message key="sistema.botao.finalizar" />
			</button>
			</c:if>
			
			<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO'}">
			<button type="submit" name="carregar" class="btn">
				Carregar
			</button>
			</c:if>
			
			<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao == 'A_CARREGAR'}">
			<button type="submit" name="gerarNotaFiscal" class="btn">
				Enviar para nota fiscal
			</button>
			</c:if>

		</div>
	</div>


<c:if test="${!empty ordemCarregamentoWrapper.ordemCarregamento.id}">



	<div class="accordion" id="accordion3">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree"> 
						Ve�culos
					</a>
				</div>
				
				<div id="collapseThree"  class="accordion-body collapse in">
					<div class="accordion-inner">

					<div class="control-group">
						<label class="control-label">
							Ve�culos
						</label>
						
						<div class="controls">
							<form:select path="veiculo.id" id="veiculo" cssStyle="width:300px">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<c:forEach items="${ordemCarregamentoWrapper.listaVeiculo}" var="item">
								<option value="${item.id}">${item.veiculo} - ${item.placa} </option>
							</c:forEach>
						</form:select>
						</div>
					</div>
					
					
					<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO'}">
					<div class="control-group">
						<div class="controls" >
							<button type="submit" name="incluirVeiculo" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
						</div>
					</div>
					</c:if>
						
							<table class="table table-striped">
								<thead>
									<th>Ve�culo</th>
									<th>Placa</th>
									<th>A��es</th>
								</thead>
								<tbody>
									<c:forEach items="${ordemCarregamentoWrapper.listaOrdemCarregamentoFrota}" var="item">
										<tr>
											<td>${item.frota.veiculo}</td>
											<td>${item.frota.placa}</td>
											<td>
											<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'GERAR_NOTA' and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'FINALIZADO' }">
											<a href="<c:url value="/ordemCarregamento/ordemCarregamentoForm/excluirVeiculo.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
											</c:if>	
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>

	
	
	
		
		
		
		<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"> 
						<fmt:message key="form.complementarProjeto.documentos" />
					</a>
				</div>
				
				<div id="collapseTwo" class="accordion-body collapse">
					<div class="accordion-inner">

						<div class="control-group">
							<label class="control-label" for="tipoDocumento">
								<fmt:message key="form.complementarProjeto.tipoDocumento" />
							</label>
							
							<div class="controls">
								<form:select path="documentoAnexo.tipoDocumento.id" id="tipoDocumento">
									<form:option value="">
										<fmt:message key="sistema.msg.textoComboInicial" />
									</form:option>
									<form:options items="${ordemCarregamentoWrapper.listaTipoDocumento}" itemValue="id" itemLabel="descricao" />
								</form:select>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="descricao">
								<fmt:message key="form.complementarProjeto.descricaoArquivo" />
							</label>
							<div class="controls">
								<form:textarea path="documentoAnexo.descricao" rows="3" />
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<label for="file">
									<fmt:message key="sistema.msg.upload" />
								</label> 
								<input type="file" name="file" />
								
								<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'GERAR_NOTA' and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'FINALIZADO' }">
								<button type="submit" name="upload" class="btn">
									<i class="icon-upload"></i>
								</button>
								</c:if>
							</div>
						</div>

						

						<c:if test="${!empty ordemCarregamentoWrapper.listaDocumentoAnexo}">
							<table class="table table-striped">
								<thead>
									<th><fmt:message key="form.complementarProjeto.nomeArquivo"/></th>
									<th><fmt:message key="form.complementarProjeto.descricaoArquivo"/></th>
									<th><fmt:message key="sistema.dataTable.acoes"/></th>
								</thead>
								<tbody>
									<c:forEach items="${ordemCarregamentoWrapper.listaDocumentoAnexo}" var="item">
										<tr>
											<td>${item.filename}</td>
											<td>${item.descricao}</td>
											<td>
												<a href="<c:url value="/ordemCarregamento/ordemCarregamentoForm/download.html?id=${item.id}"/>">
													<i class="icon-download"></i>
												</a>|
											<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'GERAR_NOTA' and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'FINALIZADO' }">	
												<a href="<c:url value="/ordemCarregamento/ordemCarregamentoForm/excluirDocumento.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
											</c:if>	
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</c:if>	
	</form:form>
<c:if test="${!empty ordemCarregamentoWrapper.ordemCarregamento.id}">	
	
	<form:form id="pecasForm" cssClass="form-horizontal" modelAttribute="ordemCarregamentoWrapper" action="${pageContext.request.contextPath}/ordemCarregamento/ordemCarregamentoForm/acoesFormPecas.html">	
		<input type="hidden" name="checados" id="checados" value=""/>
		<div class="accordion" id="accordion1">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"> 
						Pe�as
					</a>
				</div>
				
				<div id="collapseOne"  class="accordion-body collapse in">
					<div class="accordion-inner">

					<div class="control-group">
						<label class="control-label">
							Pe�as produzidas
						</label>
						
						<div class="controls">
							<form:select path="itemOrdemCarregamento.itemContratoProjeto.id">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
							<c:forEach items="${ordemCarregamentoWrapper.listaPecasDisponiveis}" var="item">
								<option value="${item.id}"> ${item.sigla} - ${item.id}  </option>
							</c:forEach>
							</form:select>
						</div>
					</div>
					
					
					
					<div class="control-group">
						<div class="controls" >
						<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO'}">
							<button type="submit" name="incluirPeca" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
						</c:if>		
						
						<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO' or ordemCarregamentoWrapper.ordemCarregamento.situacao == 'A_CARREGAR'}">	
							<button type="button" name="excluirPecasChecadas" onclick="verificarChecados()" class="btn"  title="Excluir selecionados">
								Excluir selecionados
							</button>
						</c:if>	
						</div>
					</div>
					
						
							<table class="table table-striped">
								<thead>
									<th></th>
									<th>C�digo</th>
									<th>Pe�a</th>
									<th>Produto</th>
									<th>D1 (cm)</th>
									<th>D2 (cm)</th>
									<th>D3 (cm)</th>
									<th>Peso</th>
									<th>A��es</th>
								</thead>
								<tbody>
									<c:forEach items="${ordemCarregamentoWrapper.listaCarregamento}" var="item">
										<tr>
											<td><input type="checkbox"  name="ckPecas" id="ckPecas${item.id}" value="${item.id}"/></td>
											<td>${item.itemContratoProjeto.id}</td>
											<td>${item.itemContratoProjeto.sigla}</td>
											<td>${item.itemContratoProjeto.itemContrato.produto.nome}</td>
											<td>${item.itemContratoProjeto.projeto.dimensao1}</td>
											<td>${item.itemContratoProjeto.projeto.dimensao2}</td>
											<td>${item.itemContratoProjeto.projeto.dimensao3}</td>
											<td>${item.itemContratoProjeto.projeto.peso}</td>
											<td>
											<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'GERAR_NOTA' and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'FINALIZADO' }">
											<a href="<c:url value="/ordemCarregamento/ordemCarregamentoForm/excluirPeca.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
											</c:if>	
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>
		
	
</form:form>
</c:if>



<c:if test="${!empty ordemCarregamentoWrapper.ordemCarregamento.id}">	
	
	<form:form id="pecasForm" cssClass="form-horizontal" modelAttribute="ordemCarregamentoWrapper" action="${pageContext.request.contextPath}/ordemCarregamento/ordemCarregamentoForm/acoesFormPecas.html">	
		<input type="hidden" name="checados" id="checados" value=""/>
		<div class="accordion" id="accordion3">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree"> 
						Produtos de Prateleira
					</a>
				</div>
				
				<div id="collapseThree"  class="accordion-body collapse in">
					<div class="accordion-inner">

					<div class="control-group">
						<label class="control-label">
							Produtos produzidos
						</label>
						
						<div class="controls">
							<form:select path="itemOrdemCarregamento.produto.id">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
							<c:forEach items="${ordemCarregamentoWrapper.listaPecasProdutoDisponiveis}" var="item">
								<option value="${item.id}"> ${item.nome} - ${item.id}  </option>
							</c:forEach>
							</form:select>
						</div>
					</div>
					
				
					
					
					<div class="control-group">
						<div class="controls" >
						<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO'}">
							<button type="submit" name="incluirPecaPedido" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
						</c:if>		
						
						<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO' or ordemCarregamentoWrapper.ordemCarregamento.situacao == 'A_CARREGAR'}">	
							<button type="button" name="excluirPecasChecadas" onclick="verificarChecados()" class="btn"  title="Excluir selecionados">
								Excluir selecionados
							</button>
						</c:if>	
						</div>
					</div>
					
						
							<table class="table table-striped">
								<thead>
									<th></th>
									<th>C�digo</th>
									<th>Produto</th>
									<th>A��es</th>
								</thead>
								<tbody>
									<c:forEach items="${ordemCarregamentoWrapper.listaCarregamentoProduto}" var="item">
										<tr>
											<td><input type="checkbox"  name="ckPecas" id="ckPecas${item.id}" value="${item.id}"/></td>
											<td>${item.produto.id}</td>
											<td>${item.produto.nome}</td>
											<td>
											<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'GERAR_NOTA' and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'FINALIZADO' }">
											<a href="<c:url value="/ordemCarregamento/ordemCarregamentoForm/excluirPeca.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
											</c:if>	
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>
		
	
</form:form>
</c:if>


<c:if test="${!empty ordemCarregamentoWrapper.ordemCarregamento.id}">	
	
	<form:form id="materiaisForm" cssClass="form-horizontal" modelAttribute="ordemCarregamentoWrapper" action="${pageContext.request.contextPath}/ordemCarregamento/ordemCarregamentoForm/acoesFormPecas.html">	
		<input type="hidden" name="checados" id="checados" value=""/>
		<div class="accordion" id="accordion4">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour"> 
						Materiais
					</a>
				</div>
				
				<div id="collapseFour"  class="accordion-body collapse in">
					<div class="accordion-inner">

					<div class="control-group">
						<label class="control-label">
							Materiais
						</label>
						
						<div class="controls">
							<form:select path="itemOrdemCarregamento.material.id">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
							<c:forEach items="${ordemCarregamentoWrapper.listaMateriaisDisponiveis}" var="item">
								<option value="${item[0]}"> ${item[1]} - QTD: ${item[2]}  </option>
							</c:forEach>
							</form:select>
						</div>
						
						<div class="control-group">
						<label class="control-label" >
							Quantidade
						</label>
						
						<div class="controls">
							<label class="control-label">
								<form:input path="itemOrdemCarregamento.quantidade" cssStyle="width:200px" id="quantidade" 
									onkeypress="return return soNumero(event, this);" />
							</label>
						</div>
					</div>
						
					</div>
					
				
					
					
					<div class="control-group">
						<div class="controls" >
						<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO'}">
							<button type="submit" name="incluirMaterial" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
						</c:if>		
						
						<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.situacao == 'EM_ELABORACAO' or ordemCarregamentoWrapper.ordemCarregamento.situacao == 'A_CARREGAR'}">	
							<button type="button" name="excluirPecasChecadas" onclick="verificarChecados()" class="btn"  title="Excluir selecionados">
								Excluir selecionados
							</button>
						</c:if>	
						</div>
					</div>
					
						
							<table class="table table-striped">
								<thead>
									<th></th>
									<th>C�digo</th>
									<th>Material</th>
									<th>Quantidade</th>
									<th>A��es</th>
								</thead>
								<tbody>
									<c:forEach items="${ordemCarregamentoWrapper.listaCarregamentoMaterial}" var="item">
										<tr>
											<td><input type="checkbox"  name="ckPecas" id="ckPecas${item.id}" value="${item.id}"/></td>
											<td>${item.material.id}</td>
											<td>${item.material.nome}</td>
											<td>${item.quantidade}</td>
											<td>
											<c:if test="${ordemCarregamentoWrapper.ordemCarregamento.id != null and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'GERAR_NOTA' and ordemCarregamentoWrapper.ordemCarregamento.situacao != 'FINALIZADO' }">
											<a href="<c:url value="/ordemCarregamento/ordemCarregamentoForm/excluirPeca.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
												</a>
											</c:if>	
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>
		
	
</form:form>
</c:if>





<script type="text/javascript">

function verificarChecados(){
	
	var check = document.getElementsByName("ckPecas");
	var len = check.length;
	var checados = "";
	
	
		for(i=0; i<len; i++ ){
			if(check[i].checked){
				checados+="@"+check[i].value;
			}
			
		}
		
		
		if(checados.length == 0){
			alert('Selecione pelo menos uma pe�a para exclus�o');
		}
		else{	
			
			var r=confirm("Confirma a exclus�o da Ordem de Carregamento das pe�as selecionadas ? ")
			if (r==true)
			  {
				document.getElementById('checados').value = checados;
				document.getElementById('pecasForm').submit();
			  }
		}
	
}

function esconderCampo(){
	var tipo = document.getElementById('tipo').value;
	if(tipo == 'CONTRATO'){
		document.getElementById('divContrato').style.display = "block";
		document.getElementById('divPedido').style.display = "none";
	}
	else if(tipo == 'PEDIDO'){
		document.getElementById('divContrato').style.display = "none";
		document.getElementById('divPedido').style.display = "block";
	}
	else{
		document.getElementById('divContrato').style.display = "none";
		document.getElementById('divPedido').style.display = "none";
	}
}



</script>
