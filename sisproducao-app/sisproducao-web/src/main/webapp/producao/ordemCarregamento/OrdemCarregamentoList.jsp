<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	
	<form:form id="formOcList" name="formOcList" modelAttribute="ordemCarregamentoWrapper"  method="post" action="${pageContext.request.contextPath}/ordemCarregamento/ordemCarregamentoList/acoesForm.html">
	<div>
		<h3>Listagem de Ordens de Carregamento</h3>
		
		<legend>Filtros</legend>

		<table>
			<tr>
				<td>
					<label class="control-label" for="contrato">Contrato</label> 
					<input type="text" value="" style="width: 200px" name="contrato" id="contrato" />
				</td>
				
				<td>
					<div class="controls">
						
						<label class="control-label"> <fmt:message	key="list.op.situacao" />	</label> 
					<select id="comboSituacao" name="comboSituacao">
						<option value="">
							<fmt:message key="sistema.msg.textoComboInicial" />
						</option>
						<c:forEach var="item" items="${listaSitacaoOC}">
							<option value="${item}">${item}</option>
						</c:forEach>
					</select>
					</div>
				</td>
				<td>
					<button type="submit" name="pesquisar" class="btn">	<i class="icon-search"></i>	</button>
					<button type="submit" name="limpar" class="btn"><fmt:message key="sistema.botao.limpar" /></button>
				</td>
			</tr>
			<tr>
				<td>
					<p>
						<a class="btn btn-primary btn-small" href="<c:url value="/ordemCarregamento/ordemCarregamentoForm/init.html"/>">Nova Ordem</a>
					</p>
				</td>
			</tr>
		</table>

		
	</div>
    </form:form>	
	
	
<c:if test="${!empty listaOC}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th>N�mero</th>
			<th>Contrato/Pedido</th>
			<th>Obra</th>
			<th>Data Carregamento</th>
			<th>Data Prevista Sa�da</th>
			<th>Motorista</th>
			<th>Situa��o</th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>
		<tbody>
			<c:forEach items="${listaOC}" var="item">
				<tr>
					<td>${item.id}</td>
					<td>${ item.contrato.numero != NULL ? item.contrato.numero : item.pedido.numero}</td>
					<td>${item.contrato.nomeObra}</td>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataCarregamento}" /></td>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataPrevSaida}" /></td>
					<td>${item.motorista.motorista}</td>
					<td>${item.situacao}</td>
					<td>
						<a href="<c:url value="/ordemCarregamento/ordemCarregamentoList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
					<c:if test="${item.situacao == 'EM_ELABORACAO'}">							
						<a href="<c:url value="/ordemCarregamento/ordemCarregamentoList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</c:if>	
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaOC}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>
