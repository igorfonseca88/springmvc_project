<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<form:form cssClass="form-horizontal" method="post" id="formPesqMateriais"  action="${pageContext.request.contextPath}/modal/adicionarMaterial/filtrar.html">
	<div>
		<h3>Adicionar Materiais</h3>
		
		<legend>Filtros</legend>
		 
			<div class="control-group">
				<label class="control-label" for="numero">N�mero:</label>
				<div class="controls">
					<input type="text" value="" style="width:200px" name="numero" id="numero"/>
					<button type="submit" name="filtrar" class="btn"><i class="icon-search"></i></button>
					<button type="submit" name="filtrar" class="btn">Limpar</button>
				</div>
			</div>
			
		
	</div>
</form:form>

<form:form cssClass="form-horizontal" method="post" id="formIncluirMateriais"  action="${pageContext.request.contextPath}/modal/adicionarMaterial/incluirMaterial.html">	
<c:if test="${!empty listaMateriais}">

			<button type="button" name="incluirMaterial" onclick="verificarChecados();" class="btn">Enviar</button>
			<input type="hidden" name="checados" id="checados" value=""/>
			
			<table class="table table-striped">
				<thead>
					<th></th>
					<th><fmt:message key="list.material.numero"/></th>
					<th><fmt:message key="list.material.nome"/></th>
					<!--  <th><fmt:message key="list.material.estoque"/></th> -->
					<th><fmt:message key="form.contrato.quantidade"/></th>
					
				</thead>	
				<tbody>
					<c:forEach items="${listaMateriais}" var="item">
						<tr>
							<td><input type="checkbox" name="ckMateriais" id="ckMateriais${item.id}" value="${item.id}"/></td>
							<td>${item.numero}</td>
							<td>${item.nome}</td>
							<!--<td>${item.estoque}</td>-->
							<td><input type="text" name="quantidadeMaterial${item.id}" id="quantidadeMaterial${item.id}" value=""></td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
</c:if>
	
</form:form>

<c:if test="${empty listaMateriais}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


<script type="text/javascript">

function verificarChecados(){
	
	var check = document.getElementsByName("ckMateriais");
	var len = check.length;
	var checados = "";
	
	for(i=0; i<len; i++ ){
		if(check[i].checked){
			if(document.getElementById('quantidadeMaterial'+check[i].value).value == "" || document.getElementById('quantidadeMaterial'+check[i].value).value == "0"){
				alert("Informe a quantidade.");
				//return false;
			}
			else{
				checados+="@"+check[i].value+"#"+document.getElementById('quantidadeMaterial'+check[i].value).value;
			}
		}
		
	}
	document.getElementById('checados').value = checados;
	document.getElementById('formIncluirMateriais').submit();
}



</script>


