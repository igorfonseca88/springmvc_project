<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<form:form cssClass="form-horizontal" method="post" id="formIncluirFases"  action="${pageContext.request.contextPath}/modal/adicionarFase/incluirFase.html">

<div>
		<h3>Fases da Produ��o</h3>
	</div>
		

			<button type="button" name="incluirFase" onclick="verificarChecados();" class="btn">Enviar</button>
			<input type="hidden" name="checados" id="checados" value=""/>
			
			<table class="table table-striped">
				<thead>
					<th></th>
					<th><fmt:message key="list.op.fase"/></th>
					
				</thead>	
				<tbody>
					<c:forEach items="${opWrapper.listaFaseProducao}" var="item">
						<tr>
							<td><input type="checkbox" name="ckFases" id="ckFases${item.id}" value="${item.id}"/></td>
							<td>${item.fase}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
	
</form:form>

<c:if test="${empty opWrapper.listaFaseProducao}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


<script type="text/javascript">

function verificarChecados(){
	
	var check = document.getElementsByName("ckFases");
	var len = check.length;
	var checados = "";
	
	for(i=0; i<len; i++ ){
		if(check[i].checked){
				checados+="@"+check[i].value;
		}
		
	}
	document.getElementById('checados').value = checados;
	document.getElementById('formIncluirFases').submit();
}



</script>


