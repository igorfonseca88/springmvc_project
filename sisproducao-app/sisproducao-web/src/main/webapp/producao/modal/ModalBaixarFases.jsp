<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script>
$(document).ready(function() {
        $( ".date" ).datepicker({
           dateFormat: 'dd/mm/yy', 
           changeMonth: true, 
           changeYear: true
       });
	});
  </script>
  
<form:form cssClass="form-horizontal"  method="post" modelAttribute="opWrapper"	id="formBaixa"	action="${pageContext.request.contextPath}/modal/modalBaixa/acoesForm.html">
	<div>
		<h3>Baixar Fases da Produ��o</h3>
	</div>
	
	<div id="resulAlert">
		
	</div>

	<table>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.fase" />
					</label>
					<div class="controls">
						<form:select path="idFaseSelecionada">
							<form:option value="">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<form:options
								items="${opWrapper.listaFasesDisponiveisParaBaixar}"
								itemValue="id" itemLabel="fase" />
						</form:select>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="control-group">
				   <label class="control-label"> Data In�cio:
					</label>
					<div class="controls">
							<form:input cssClass="date" type="text" id="datepicker" path="dataString"  />
					</div>
				</div>
			</td>
		</tr>

		<tr>
			<td>
				<div class="control-group">
				   <label class="control-label"> Data Baixa:
					</label>
					<div class="controls">
							<form:input cssClass="date" type="text" id="datepicker2" path="dataBaixaString"  />
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="control-group">
					<label class="control-label"> Revis�o </label>
					<div class="controls">
						<label class="control-label"> <select name="revisao">
								<option>Selecione</option>
								<option value="1"
									<c:if test="${opWrapper.revisaoProjeto == 1}"> selected</c:if>>
									1</option>
								<option value="2"
									<c:if test="${opWrapper.revisaoProjeto == 2}"> selected</c:if>>
									2</option>
								<option value="3"
									<c:if test="${opWrapper.revisaoProjeto == 3}"> selected</c:if>>
									3</option>
								<option value="4"
									<c:if test="${opWrapper.revisaoProjeto == 4}"> selected</c:if>>
									4</option>
								<option value="5"
									<c:if test="${opWrapper.revisaoProjeto == 5}"> selected</c:if>>
									5</option>
								<option value="6"
									<c:if test="${opWrapper.revisaoProjeto == 6}"> selected</c:if>>
									6</option>
								<option value="7"
									<c:if test="${opWrapper.revisaoProjeto == 7}"> selected</c:if>>
									7</option>
								<option value="8"
									<c:if test="${opWrapper.revisaoProjeto == 8}"> selected</c:if>>
									8</option>
								<option value="9"
									<c:if test="${opWrapper.revisaoProjeto == 9}"> selected</c:if>>
									9</option>
								<option value="10"
									<c:if test="${opWrapper.revisaoProjeto == 10}"> selected</c:if>>
									10</option>
						</select>


						</label>
					</div>
			</td>
		</tr>

	</table>

	<c:if test="${opWrapper.tipoPecaProduto == 'OBRA'}">

	<div class="control-group">
		<div class="controls">
			<button type="button" onclick="iniciarIntegracao('${opWrapper.idItemContratoProjeto}')"  name="integrar" class="btn">
					Enviar para controller
			</button>
		</div>
	</div>
	</c:if>

	<c:if test="${opWrapper.tipoPecaProduto == 'PRATELEIRA'}">

	<div class="control-group">
		<div class="controls">
			<button type="button" onclick="iniciarIntegracaoPrateleira('${opWrapper.op.id}')"  name="integrar" class="btn">
					Enviar prateleira controller
			</button>
		</div>
	</div>
	</c:if>
	
	<div class="control-group">
		<div class="controls" style="display: none;">
			<button type="submit"  name="salvar" id="btnSalvar" class="btn">
					<fmt:message key="sistema.botao.salvar" />
			</button>
		</div>
	</div>

	

</form:form>

	


