<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<form:form cssClass="form-horizontal" method="post" id="formPesqProdutos"  action="${pageContext.request.contextPath}/modal/pesquisaProdutoPrateleira/filtrar.html">
	<div>
		<h3>Listagem de Produtos</h3>
		
		<legend>Filtros</legend>
		 
			<div class="control-group">
				<label class="control-label" for="numero">Produto:</label>
				<div class="controls">
					<input type="text" value="" style="width:200px" name="produto" id="produto"/>
					<button type="submit" name="filtrar" class="btn"><i class="icon-search"></i></button>
					<button type="submit" name="filtrar" class="btn">Limpar</button>
				</div>
			</div>
			
		
	</div>
</form:form>

<form:form cssClass="form-horizontal" method="post" id="formIncluirProdutos"  action="${pageContext.request.contextPath}/modal/pesquisaProdutoPrateleira/incluirProduto.html">	
<c:if test="${!empty listaProdutos}">

			<button type="button" name="incluirProduto" onclick="verificarChecados();" class="btn">Enviar</button>
			<input type="hidden" name="checados" id="checados" value=""/>
			
			<table class="table table-striped">
				<thead>
					<th></th>
					<th><fmt:message key="form.contrato.produto"/></th>
					<th><fmt:message key="form.contrato.tipo"/></th>
					<th><fmt:message key="form.contrato.quantidade"/></th>
					
				</thead>	
				<tbody>
					<c:forEach items="${listaProdutos}" var="item">
						<tr>
							<td><input type="checkbox" name="ckProdutos" id="ckProdutos${item.id}" value="${item.id}"/></td>
							<td>${item.nome}</td>
							<td>${item.tipo}</td>
							<td><input type="text" name="quantidadeProduto${item.id}" id="quantidadeProduto${item.id}" value=""></td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
</c:if>
	
</form:form>

<c:if test="${empty listaProdutos}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


<script type="text/javascript">

function verificarChecados(){
	
	var check = document.getElementsByName("ckProdutos");
	var len = check.length;
	var checados = "";
	
	for(i=0; i<len; i++ ){
		if(check[i].checked){
			if(document.getElementById('quantidadeProduto'+check[i].value).value == "" || document.getElementById('quantidadeProduto'+check[i].value).value == "0"){
				alert("Informe a quantidade.");
				//return false;
			}
			else{
				checados+="@"+check[i].value+"#"+document.getElementById('quantidadeProduto'+check[i].value).value;
			}
		}
		
	}
	document.getElementById('checados').value = checados;
	document.getElementById('formIncluirProdutos').submit();
}



</script>


