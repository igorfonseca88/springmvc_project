<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form id="tracoForm" cssClass="form-horizontal"
	modelAttribute="tracoMaterialWrapper"
	action="${pageContext.request.contextPath}/traco/tracoForm/acoesForm.html">

	<form:hidden path="traco.id" />

	<legend>
		<fmt:message key="form.traco.header" />
	</legend>

	<legend>
		<fmt:message key="form.traco.dadosCadastro" />
	</legend>

	<div class="control-group">
		<label class="control-label requiredField" for="codigo"> C�digo
		</label>
		<div class="controls">
			<form:input path="traco.id" cssStyle="width:200px" id="codigo" disabled="true" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="numero"> <fmt:message
				key="form.traco.numero" />
		</label>
		<div class="controls">
			<form:input path="traco.numero" cssStyle="width:200px" id="numero" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="nome"> <fmt:message
				key="form.traco.nome" />
		</label>
		<div class="controls">
			<form:input path="traco.nome" cssStyle="width:200px" id="nome" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="utilizacao"> <fmt:message
				key="form.traco.utilizacao" />
		</label>
		<div class="controls">
			<form:input path="traco.utilizacao" cssStyle="width:200px" id="utilizacao" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="resistencia">
			<fmt:message key="form.traco.resistencia" />
		</label>
		<div class="controls">
			<form:input path="traco.resistencia" cssStyle="width:200px"
				id="resistencia" />MPA
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="situacao">Situa��o</label>

		<div class="controls">
			<form:select path="traco.situacao" id="situacao">
				<form:option value="">
					<fmt:message key="sistema.msg.textoComboInicial" />
				</form:option>
				<form:options items="${tracoMaterialWrapper.listaAtivoInativos}" />
			</form:select>
		</div>
	</div>


	<div class="control-group">
		<div class="controls">

			<button type="button"
				onclick="location.href='<c:url value="/traco/tracoList/init.html"/>'"
				name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>

			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>

		</div>
	</div>
	
	<c:if test="${tracoMaterialWrapper.traco.id != null}">
	
		<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
						<fmt:message key="form.traco.materiais" /> 
					</a>
				</div>
				<div id="collapseTwo" class="${!empty tracoMaterialWrapper.listaItemTraco ? 'accordion-body collapse in' : 'accordion-body collapse'}">
				
				<c:if test="${tracoMaterialWrapper.podeEditarTraco}">	
					<div class="control-group">
						<label class="control-label requiredField">
							<fmt:message key="form.traco.materiais" />
						</label>
						<div class="controls">
						<form:select path="itemTraco.material.id">
							<form:option value="0">
								<fmt:message key="sistema.msg.textoComboInicial" />
							</form:option>
							<c:forEach items="${tracoMaterialWrapper.listaMateriais}" var="item">
								<option value="${item.id}">${item.nome}</option>
							</c:forEach>
						</form:select>
						</div>
					</div>	
					
				
					<div class="control-group">
						<label class="control-label requiredField" for="qtdeMaterial">
							<fmt:message key="form.traco.qtdeMaterial" />
						</label>
						<div class="controls">
							<form:input path="itemTraco.quantidade" cssStyle="width:200px" id="qtdeMaterial" />
						</div>
					</div>
					
					
					<div class="control-group">
						<div class="controls" >
							<button type="submit" name="incluirItem" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
						</div>
					</div>
				
				</c:if>		
					<c:if test="${!empty tracoMaterialWrapper.listaItemTraco}">
						<table class="table table-condensed">
							<br />
							<thead>
								<th><fmt:message key="form.traco.nomeMaterial" /></th>
								<th><fmt:message key="form.traco.unidadeMaterial" /></th>
								<th><fmt:message key="form.traco.qtdeMaterial" /></th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${tracoMaterialWrapper.listaItemTraco}"
									var="item">
									<tr>
										<td>${item.material.nome}</td>
										<td>${item.material.unidade}</td>
										<td>${item.quantidade}</td>
										<td>
										<c:if test="${tracoMaterialWrapper.podeEditarTraco}">
											<a href="<c:url value="/traco/tracoForm/excluirItem.html?id=${item.id}"/>">
													<i class="icon-remove"></i>
											</a>
										</c:if>	
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>						
					
				</div>
			</div>
		</div>
		
	</c:if>
	


</form:form>
