<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div >
		<h3><fmt:message key="list.traco.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/traco/tracoForm/init.html"/>"><fmt:message key="list.traco.novoTraco"/></a>
		</p>
	</div>

<c:if test="${!empty listaTracos}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
		<th>C�digo</th>
			<th><fmt:message key="list.traco.nome"/></th>
			<th><fmt:message key="list.traco.numero"/></th>
			<th><fmt:message key="list.traco.utilizacao"/></th>
			<th><fmt:message key="list.traco.resistencia"/></th>
			<th>Situa��o</th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaTracos}" var="item">
				<tr>
				<td>${item.id}</td>
					<td>${item.nome}</td>
					<td>${item.numero}</td>
					<td>${item.utilizacao}</td>
					<td>${item.resistencia} MPA </td>
					<td>${item.situacao} </td>
					<td>
						<a href="<c:url value="/traco/tracoList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/traco/tracoList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaTracos}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>


