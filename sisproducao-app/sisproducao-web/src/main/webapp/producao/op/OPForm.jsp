<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form id="opForm" cssClass="form-horizontal" modelAttribute="opWrapper" action="${pageContext.request.contextPath}/op/opForm/acoesForm.html">
	

	<legend>
		<fmt:message key="form.op.headerForm" />
	</legend>

	<legend>
		<fmt:message key="form.op.dadosContrato" />
	</legend>

	<div class="control-group">
		<div>
			<form:select path="tipoBusca" id="tipoBusca">
				<form:option value="0">
					<fmt:message key="sistema.msg.textoComboPesquisa" />
				</form:option>
				<form:options items="${opWrapper.listaTiposBusca}"/>
			</form:select>
			
			<form:input path="buscarPor" cssStyle="width:200px" id="buscarPor"/>
			
			<button type="submit" name="buscar" class="btn"><i class="icon-search" ></i></button>
		</div>
	</div>
</form:form>
	
<c:if test="${!empty opWrapper.listaContratoProjeto}">	
<form:form id="opFormProducao" cssClass="form-horizontal" modelAttribute="opWrapper" action="${pageContext.request.contextPath}/op/opForm/produzirItens.html">	
	
	
		<input type="hidden" name="checados" id="checados" value=""/>
		<table class="table table-striped">
			<thead>
				<th></th>
				<th><fmt:message key="list.op.contrato"/></th>
				<th><fmt:message key="list.op.obra"/></th>
				<th><fmt:message key="list.op.peca"/></th>
				<th><fmt:message key="list.op.sigla"/></th>
				<th><fmt:message key="list.op.codigoPeca"/></th>
				
				
			</thead>
			<tbody>
				<c:forEach items="${opWrapper.listaContratoProjeto}" var="item">
					<tr>
						<td><input type="checkbox" name="ckItens" id="ckItens${item.id}" value="${item.id}"/></td>
						<td>${item.itemContrato.contrato.numero}</td>
						<td>${item.itemContrato.contrato.nomeObra}</td>
						<td>${item.itemContrato.produto.nome}</td>
						<td>${item.sigla}</td>
						<td>${item.id}</td>			
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/op/opList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<c:if test="${!empty opWrapper.listaContratoProjeto}">
				<button type="button" name="btProduzirItens" class="btn" onclick="verificarChecados()">
					Gerar OP
				</button>
			</c:if>
			
		</div>
	</div>
	
</form:form>
</c:if>			

<c:if test="${!empty opWrapper.listaProdutoProjeto}">

<form:form id="opFormProducao" cssClass="form-horizontal" modelAttribute="opWrapper" action="${pageContext.request.contextPath}/op/opForm/produzirItensProduto.html">	
	
	
	
		<input type="hidden" name="checados" id="checados" value=""/>
		<table class="table table-striped">
			<thead>
				<th></th>
			    <th>C�digo do produto</th>
				<th>Produto</th>

				
				
			</thead>
			<tbody>
				<c:forEach items="${opWrapper.listaProdutoProjeto}" var="item">
					<tr>
						<td><input type="checkbox" name="ckItens" id="ckItens${item.id}" value="${item.id}"/></td>
						<td>${item.id}</td>
						<td>${item.nome}</td>
			
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/op/opList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<c:if test="${!empty opWrapper.listaProdutoProjeto}">
				<button type="button" name="btProduzirItensProduto" class="btn" onclick="verificarChecados()">
					Gerar OP
				</button>
			</c:if>
			
		</div>
	</div>

</form:form>
</c:if>			

<script type="text/javascript">

function verificarChecados(){
	
	var check = document.getElementsByName("ckItens");
	var len = check.length;
	var checados = "";
	
	for(i=0; i<len; i++ ){
		if(check[i].checked){
			checados+="@"+check[i].value;
			
		}
		
	}
	document.getElementById('checados').value = checados;
	document.getElementById('opFormProducao').submit();
}



</script>



