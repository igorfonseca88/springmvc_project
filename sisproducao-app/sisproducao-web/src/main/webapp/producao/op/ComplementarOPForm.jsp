<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form id="complementarProjetoForm" cssClass="form-horizontal" 	modelAttribute="opWrapper" 	action="${pageContext.request.contextPath}/op/opForm/acoesForm.html">
	<form:hidden path="op.id" />

	<legend>
		<fmt:message key="form.complementarOP.headerForm" /> ${opWrapper.op.id}
	</legend>

	<legend>
		<fmt:message key="form.op.dadosOP" /> 
	</legend>

	<table>

		<c:if test="${opWrapper.op.itemContratoProjeto != null}">
		<tr>
			<td>

				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.codigoPeca" />
					</label>
					<div class="controls">
						${opWrapper.op.itemContratoProjeto.id}</div>
				</div>
			</td>
			<td>
				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.obra" />
					</label>
					<div class="controls">
						${opWrapper.op.itemContratoProjeto.itemContrato.contrato.nomeObra}
					</div>
				</div>
			</td>
			
			<td>
				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.sigla" />
					</label>
					<div class="controls">
						${opWrapper.op.itemContratoProjeto.sigla}</div>
				</div>
			</td>
		</tr>
		</c:if>
		
		
		<c:if test="${opWrapper.op.produto != null}">
		<tr>
			<td>

				<div class="control-group">
					<label class="control-label">C�digo do produto
					</label>
					<div class="controls">
						${opWrapper.op.produto.id}</div>
				</div>
			</td>
			
			<td>
				<div class="control-group">
					<label class="control-label"> Produto
					</label>
					<div class="controls">
						${opWrapper.op.produto.nome}</div>
				</div>
			</td>
		</tr>
		</c:if>
		
		<tr>
		<c:if test="${opWrapper.op.itemContratoProjeto != null}">
			<td>
				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.peca" />
					</label>
					<div class="controls">
						${opWrapper.op.itemContratoProjeto.itemContrato.produto.nome}</div>
				</div>
			</td>
	   </c:if>		
			<td>
				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.usuario" />
					</label>
					<div class="controls">${opWrapper.op.usuario.username}</div>
				</div>
			</td>

			<td>
				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.situacao" />
					</label>
					<div class="controls">${opWrapper.op.situacao}</div>
				</div>
			</td>
		</tr>
		
		<tr>
			<td colspan="3">
				<div class="control-group">
					<label class="control-label"> <fmt:message
							key="list.op.url" />
					</label>
					<div class="controls">
						${opWrapper.op.itemContratoProjeto.url}</div>
				</div>
			</td>
			
			
		</tr>

	</table>
	
	<legend>
		<fmt:message key="list.op.fasesOP" />
	</legend>
	
	<table class="table table-striped">
				<thead>
					<th><fmt:message key="list.op.fase"/></th>
					<th><fmt:message key="list.op.dataInicio"/></th>
					<th><fmt:message key="list.op.dataFim"/></th>
					<th><fmt:message key="list.op.usuarioBaixa"/></th>
					<th><fmt:message key="list.op.revisao"/></th>
					<th><fmt:message key="sistema.dataTable.acoes"/></th>
					
				</thead>	
				<tbody>
					<c:forEach items="${opWrapper.producaoFases}" var="item">
						<tr>
							<td>${item.faseProducao.fase}</td>
							<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataInicio}" /></td>
							<td><fmt:formatDate pattern="dd/MM/yyyy" value="${item.dataFim}" /></td>
							<td>${item.usuario.username}</td>
							<td>${item.revisao}</td>
							<td>
							<c:if test="${opWrapper.op.situacao == 'EM_ELABORACAO' and item.faseProducao.fase != 'Confer�ncia'}">
								<a href="<c:url value="/op/opForm/excluirFase.html?id=${item.id}"/>" title="Excluir Fase"><i class="icon-remove"></i></a>
						    </c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>


	<div class="control-group">
		<div class="controls">

			<button type="button"
				onclick="location.href='<c:url value="/op/opList/init.html"/>'"
				name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			<c:if test="${opWrapper.op.situacao == 'EM_ELABORACAO'}">		
				<button type="button" onclick="javascript:InserirItem('<c:url value="/modal/adicionarFase/init.html?idOp=${opWrapper.op.id}"/>');" name="btnAdicionarFases" class="btn">
					<fmt:message key="form.op.acao.addFase" />
				</button>
				
				<button type="submit" name="produzir" class="btn">
					<fmt:message key="form.op.acao.produzir" />
				</button>
			</c:if>
			
			<c:if test="${opWrapper.op.situacao == 'EM_PRODUCAO'}">		
				<button type="button" onclick="javascript:InserirItem('<c:url value="/modal/modalBaixa/init.html?idOp=${opWrapper.op.id}"/>');" name="btnBaixarFases" class="btn">
					<fmt:message key="form.op.acao.baixar" />
				</button>

				<button type="submit"  name="cancelar" class="btn">
					<fmt:message key="form.op.acao.cancelarOP" />
				</button>
			</c:if>
		</div>
	</div>
	
	
<!-- Modal -->
<div id="myModalFases" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="fechaAtualiza('<c:url value="/op/opList/complementarOP.html?id=${opWrapper.op.id}"/>');">X�</button>
	</div>
	<div class="modal-body">
		<iframe name="itemFrame" id="itemFrame" width="600px" height="450px" scrolling="YES"  frameborder="0" src=""></iframe>
	</div>
</div>

<!-- Modal -->
<div id="myModalBaixa" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="fechaAtualiza('<c:url value="/op/opList/complementarOP.html?id=${opWrapper.op.id}"/>');">X�</button>
	</div>
	<div class="modal-body">
		<iframe name="itemFrame" id="itemFrame" width="600px" height="450px" scrolling="YES"  frameborder="0" src=""></iframe>
	</div>
</div>
	
	
<script>
	
	function InserirItem(url) {
        document.getElementById("itemFrame").src = url;
        abrePopoup();
    }
	
	function abrePopoup(){
		$(document).ready(function(){
	        $('#myModalFases').modal('show');
	    });
	}
	
	function fechaAtualiza(url)
    {
        window.location.href=url;
    }

</script>	

</form:form>

