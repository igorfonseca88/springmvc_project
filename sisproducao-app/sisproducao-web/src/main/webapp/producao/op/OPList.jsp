<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	
	<form:form id="formOpList" name="formOpList" modelAttribute="ordemProducao"  method="post" action="${pageContext.request.contextPath}/op/opList/acoesForm.html">
	<div>
		<h3>Listagem de Ordens de Produ��o</h3>
		
		<legend>Filtros</legend>

		<table>
			<tr>
				<td>
					<label class="control-label" for="numero"><fmt:message	key="list.op.codigoPeca" /></label> 
					<input type="text" value="" style="width: 200px" name="codigoPeca" id="codigoPeca" />
				</td>
				<td>
					<label class="control-label"> <fmt:message	key="list.op.fase" />	</label> 
					<select id="comboFases" name="comboFases">
						<option value="">
							<fmt:message key="sistema.msg.textoComboInicial" />
						</option>
						<c:forEach var="item" items="${listaFases}">
							<option value="${item.id}">${item.fase}</option>
						</c:forEach>
					</select>
				</td>
				<td>
					<div class="controls">
						
						<label class="control-label"> <fmt:message	key="list.op.situacao" />	</label> 
					<select id="comboSituacao" name="comboSituacao">
						<option value="">
							<fmt:message key="sistema.msg.textoComboInicial" />
						</option>
						<c:forEach var="item" items="${listaSituacaoOPs}">
							<option value="${item}">${item}</option>
						</c:forEach>
					</select>
					</div>
				</td>
				
				<td>
					<label class="control-label" for="sigla">Sigla</label> 
					<input type="text" value="" style="width: 200px" name="sigla" id="sigla" />
				</td>
				
				
			</tr>
			
			<tr>
				<td>
					<label class="control-label" for="nomeProduto">Pe�a/Produto</label> 
					<input type="text" value="" style="width: 200px" name="nomeProduto" id="nomeProduto" />
				</td>
				
				<td>
					<label class="control-label" for="nomeObra">Obra</label> 
					<input type="text" value="" style="width: 200px" name="nomeObra" id="nomeObra" />
				</td>
				<td>
					<button type="submit" name="pesquisar" class="btn">	<i class="icon-search"></i>	</button>
					<button type="submit" name="limpar" class="btn"><fmt:message key="sistema.botao.limpar" /></button>
				</td>
				
			</tr>
			
			
			<tr>
				<td>
					<p>
						<a class="btn btn-primary btn-small" href="<c:url value="/op/opForm/init.html"/>"><fmt:message key="list.op.novaOP"/></a>
					</p>
				</td>
			</tr>
		</table>

		
	</div>
    </form:form>	
	
	
<c:if test="${!empty listaOP}">
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<th><fmt:message key="list.op.codigoPeca"/>/ Produto</th>
			<th><fmt:message key="list.op.sigla"/></th>
			<th><fmt:message key="list.op.peca"/>/ Produto</th>
			<th>Dimens�es (D1xD2xD3)</th>		
			<th><fmt:message key="list.op.contrato"/></th>
			<th><fmt:message key="list.op.obra"/></th>
			<th><fmt:message key="list.op.usuario"/></th>
			<th><fmt:message key="list.op.situacao"/></th>
			<th><fmt:message key="list.op.faseAtual"/></th>
			<th><fmt:message key="list.op.anexoProjeto"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>
		<tbody>
			<c:forEach items="${listaOP}" var="item">
				<tr>
					<c:if test="${item.itemContratoProjeto != null}">
					
						<td>${item.itemContratoProjeto.id}</td>
						<td>${item.itemContratoProjeto.sigla}</td>
						<td>${item.itemContratoProjeto.itemContrato.produto.nome}</td>
						<td>${item.itemContratoProjeto.projeto.dimensao1}x${item.itemContratoProjeto.projeto.dimensao2}x${item.itemContratoProjeto.projeto.dimensao3}</td>
						<td>${item.itemContratoProjeto.itemContrato.contrato.numero}</td>
						<td>${item.itemContratoProjeto.itemContrato.contrato.nomeObra}</td>
					
					</c:if>
					
					<c:if test="${item.produto != null}">
						
						<td>${item.produto.id}</td>
						<td>&nbsp;</td>
						<td>${item.produto.nome}</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					
					</c:if>
					
					<td>${item.usuario.username}</td>
					<td>${item.situacao}</td>
					<td>${item.fase.fase}</td>
					<c:if test="${item.produto == null}">
					<td><a href="<c:url value="/op/opList/download.html?id=${item.itemContratoProjeto.projeto.id}"/>">Download</a></td>
					</c:if>
					<c:if test="${item.produto != null}">
					<td><a href="<c:url value="/op/opList/download.html?id=${item.produto.projeto.id}"/>">Download</a></td>
					</c:if>
					<td>
						<a href="<c:url value="/op/opList/complementarOP.html?id=${item.id}"/>" title="Complementar OP" ><i class="icon-refresh"></i></a>|
						<c:if test="${item.situacao == 'EM_ELABORACAO'}">
							<a href="<c:url value="/op/opList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
						</c:if>
						
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaOP}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>


