<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form cssClass="form-horizontal" method="post"
	action="${pageContext.request.contextPath}/pedido/pedidoForm/acoesForm.html" modelAttribute="pedidoWrapper" enctype="multipart/form-data">


	<form:hidden path="pedido.id" />

	<legend>
		Cadastro de Pedido
	</legend>

	<legend>
		Dados do Cadastro
	</legend>
	<c:if test="${!empty pedidoWrapper.pedido.id}">
	<div class="control-group">
		<label class="control-label" for="numero">N�mero</label> 
 		<div class="controls"> 
			
				${pedidoWrapper.pedido.numero}
		</div> 
	</div>
	</c:if>
<c:if test="${!empty pedidoWrapper.pedido.id}">	
	<div class="control-group">
		<label class="control-label" for="situacao">Situa��o</label>
		<div class="controls">
				<form:hidden path="pedido.situacao" />
			${pedidoWrapper.pedido.situacao} 
		</div>
	</div>
</c:if>	

	<div class="control-group">
		<label class="control-label" for="cliente">Cliente:</label>
		<div class="controls">
			<input type="hidden" name="cliente.idCliente" id="cliente.idCliente" value="${pedidoWrapper.cliente.id}" /> 
			<input type="text" id="nomeCliente"	name="nomeCliente" value="${pedidoWrapper.cliente.pessoa.nomePessoaFisicaJuridica}" readonly />

		<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">	
			<a href="#myModal" role="button" id="linkModal" class="btn" data-toggle="modal"><i class="icon-search"></i></a>
		</c:if>		
		</div>
	</div>

	<div class="control-group">
		<label class="control-label requiredField" for="numeropedido">N�mero</label>
		<div class="controls">
			<form:input path="pedido.numero" maxlength="100" cssStyle="width:200px"
				id="numeropedido" />
		</div>
	</div>


	

	

	<div class="control-group">
		<div class="controls">

			<button type="button"
				onclick="location.href='<c:url value="/pedido/pedidoList/init.html"/>'"
				name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
	<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
	</c:if>
	<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO' and !empty pedidoWrapper.listaItemPedidoProduto}">		
			<button type="submit" name="finalizar" class="btn">
				<fmt:message key="sistema.botao.finalizar" />
			</button>
	</c:if>
	
	<c:if test="${pedidoWrapper.pedido.situacao == 'FINALIZADO'}">
		<button type="submit" name="reabrir" class="btn">
				Reabrir
			</button>
	</c:if>	
		</div>
	</div>

	<c:if test="${!empty pedidoWrapper.pedido.id}">
		<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse"
						data-parent="#accordion2" href="#collapseTwo"> Documentos
					</a>
				</div>
				<div id="collapseTwo" class="accordion-body collapse">
					<div class="accordion-inner">

						<div class="control-group">
							<label class="control-label" for="situacao">Tipo</label>
							<div class="controls">
								<form:select path="documentoAnexo.tipoDocumento.id" id="idTD">
									<form:option value="">
										<fmt:message key="sistema.msg.textoComboInicial" />
									</form:option>
									<form:options items="${pedidoWrapper.listaTipoDocumento}" itemValue="id" itemLabel="descricao" />
								</form:select>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="descricaoDoc"><fmt:message key="form.tipoDocumento.descricao" /></label>
							<div class="controls">
								<form:textarea path="documentoAnexo.descricao" rows="3" />
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<label for="file">
									<fmt:message key="sistema.msg.upload" />
								</label>
								<input type="file" name="file" />
								<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">
									<button type="submit" name="upload" class="btn">
										<i class="icon-upload"></i>
									</button>
								</c:if>
							</div>
						</div>


						<c:if test="${!empty pedidoWrapper.listaDocumentoAnexo}">
							<table class="table table-striped">
								<thead>
									<th>Descri��o</th>
									<th>Nome arquivo</th>
									<th><fmt:message key="sistema.dataTable.acoes" /></th>
								</thead>
								<tbody>
									<c:forEach items="${pedidoWrapper.listaDocumentoAnexo}"
										var="item">
										<tr>
											<td>${item.descricao}</td>
											<td>${item.filename}</td>
											<td><a
												href="<c:url value="/pedido/pedidoForm/download.html?id=${item.id}"/>">Download</a>
											<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">	
												<a
												href="<c:url value="/pedido/pedidoForm/excluirDocumento.html?id=${item.id}"/>"><i
													class="icon-remove"></i></a>
											</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>


					</div>
				</div>
			</div>
		</div>
	</c:if>


	<c:if test="${!empty pedidoWrapper.pedido.id}">

		<fieldset><legend>Produtos de Prateleira</legend>
		<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">
			<p>
				<a href="javascript:InserirItem('<c:url value="/modal/pesquisaProdutoPrateleira/init.html?idPedido=${pedidoWrapper.pedido.id}"/>');" class="btn btn-primary btn-small">Adicionar produto</a>
			</p>
		</c:if>
		<c:if test="${!empty pedidoWrapper.listaItemPedidoProduto}">
						<table class="table table-striped">
							<thead>
								<th>C�digo</th>
								<th>Produto</th>
								<th>Tipo</th>
								<th>Quantidade</th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${pedidoWrapper.listaItemPedidoProduto}"
									var="item">
									<tr>
										<td>${item.id}</td>
										<td>${item.produto.nome}</td>
										<td>${item.produto.tipo}</td>
										<td>${item.quantidade}</td>
										<td>
										<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">
											<a href="<c:url value="/pedido/pedidoForm/excluirProduto.html?id=${item.id}"/>"><i
													class="icon-remove"></i></a>
										</c:if>		
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>

		</fieldset>
	</c:if>
	
	<c:if test="${!empty pedidoWrapper.pedido.id}">

	

		<fieldset><legend>Materiais</legend>
		<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">
			<p>
				<a href="javascript:InserirItem('<c:url value="/modal/adicionarMaterialPedido/init.html?idPedido=${pedidoWrapper.pedido.id}"/>');" class="btn btn-primary btn-small">Adicionar material</a>
			</p>
		</c:if>

					<c:if test="${!empty pedidoWrapper.listaItemPedidoMaterial}">
						<table class="table table-striped">
							<thead>
								<th>C�digo</th>
								<th>Material</th>
								<th>Unidade</th>
								<th>Quantidade</th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${pedidoWrapper.listaItemPedidoMaterial}"
									var="item">
									<tr>
										<td>${item.id}</td>
										<td>${item.material.nome}</td>
										<td>${item.material.unidade}</td>
										<td>${item.quantidade}</td>
										<td>
										<c:if test="${pedidoWrapper.pedido.situacao != 'FINALIZADO'}">
											<a href="<c:url value="/pedido/pedidoForm/excluirMaterial.html?id=${item.id}"/>"><i
													class="icon-remove"></i></a>
										</c:if>		
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>

				



	</c:if>
</form:form>

<c:if test="${openModal == 'sim'}">
	<script type="text/javascript">
	    $(document).ready(function(){
	        $('#myModal').modal('show');
	    });
	</script>
</c:if>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">X�</button>
		<h3 id="myModalLabel">Pesquisa de Clientes</h3>
	</div>
	<div class="modal-body">
		<div >
		<h3><fmt:message key="list.cliente.header"/></h3>
	</div>

    <form:form  cssClass="form-horizontal" method="post" id="formPesquisaCliente" action="${pageContext.request.contextPath}/pedido/pedidoForm/acoesForm.html">
    
			<div class="control-group">
				<label class="control-label" for="numero">Cliente:</label>
				<div class="controls">
					<input type="text" value="" style="width:200px" name="cliente" id="cliente"/>
					<button type="submit" name="pesquisarCliente" class="btn"><i class="icon-search"></i></button>
					<button type="submit" name="pesquisarCliente" class="btn">Limpar</button>
				</div>
			</div>
			
    
		<c:if test="${!empty listaClientes}">
			<table class="table table-striped">
				<thead>
					<th><fmt:message key="list.cliente.nome"/></th>
					<th><fmt:message key="list.cliente.cpfCnpj"/></th>
					<th><fmt:message key="list.cliente.tipoPessoa"/></th>
					<th><fmt:message key="sistema.dataTable.acoes"/></th>
				</thead>	
				<tbody>
					<c:forEach items="${listaClientes}" var="item">
						<tr>
							<td>${item.pessoa.nomePessoaFisicaJuridica}</td>
							<td>${item.pessoa.cpfCnpj}</td>
							<td>${item.pessoa.tipoPessoa}</td>
							<td>
								<a href="<c:url value="/pedido/pedidoForm/selecionarCliente.html?id=${item.id}"/>"><i class="icon-plus-sign"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</form:form>
	</div>
</div>


<!-- Modal -->
<div id="myModalProdutos" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="fechaAtualiza('<c:url value="/pedido/pedidoList/editar.html?id=${pedidoWrapper.pedido.id}"/>');">X�</button>
	</div>
	<div class="modal-body">
		<iframe name="itemFrame" id="itemFrame" width="600px" height="450px" scrolling="YES"  frameborder="0" src=""></iframe>
	</div>
</div>

<!-- Modal -->
<div id="myModalMateriais" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="fechaAtualiza('<c:url value="/pedido/pedidoList/editar.html?id=${pedidoWrapper.pedido.id}"/>');">X�</button>
	</div>
	<div class="modal-body">
		<iframe name="itemFrameMateriais" id="itemFrameMateriais" width="600px" height="450px" scrolling="YES"  frameborder="0" src=""></iframe>
	</div>
</div>


<script>
	
	function InserirItem(url) {
        document.getElementById("itemFrame").src = url;
        abrePopoup();
    }
	
	function abrePopoup(){
		$(document).ready(function(){
	        $('#myModalProdutos').modal('show');
	    });
	}
	
	function fechaAtualiza(url)
    {
        window.location.href=url;
    }

</script>