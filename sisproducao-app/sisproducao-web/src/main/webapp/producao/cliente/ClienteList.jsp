<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div >
		<h3><fmt:message key="list.cliente.header"/></h3>
		<p>
			<a class="btn btn-primary btn-small" href="<c:url value="/cliente/clienteForm/init.html"/>"><fmt:message key="list.cliente.novoCliente"/></a>
		</p>
	</div>

<c:if test="${!empty listaClientes}">
	<table class="table table-striped">
		<thead>
			<th><fmt:message key="list.cliente.nome"/></th>
			<th><fmt:message key="list.cliente.cpfCnpj"/></th>
			<th><fmt:message key="list.cliente.tipoPessoa"/></th>
			<th><fmt:message key="sistema.dataTable.acoes"/></th>
		</thead>	
		<tbody>
			<c:forEach items="${listaClientes}" var="item">
				<tr>
					<td>${item.pessoa.nomePessoaFisicaJuridica}</td>
					<td>${item.pessoa.cpfCnpj}</td>
					<td>${item.pessoa.tipoPessoa}</td>
					<td>
						<a href="<c:url value="/cliente/clienteList/editar.html?id=${item.id}"/>"><i class="icon-pencil"></i></a>|
						<a href="<c:url value="/cliente/clienteList/excluir.html?id=${item.id}"/>"><i class="icon-remove"></i></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<c:if test="${empty listaClientes}">
	<div >
		<p>
			<fmt:message key="sistema.msg.semRegistro"/>
		</p>
	</div>
</c:if>


