<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form id="clienteForm" cssClass="form-horizontal" modelAttribute="clientePessoaWrapper" action="${pageContext.request.contextPath}/cliente/clienteForm/acoesForm.html">

	<form:hidden path="cliente.id" />

	<legend>
		<fmt:message key="form.cliente.header" />
	</legend>

	<legend>
		<fmt:message key="form.cliente.dadosPessoais" />
	</legend>

	<div class="control-group">
		<label class="control-label requiredField" for="tipoPessoa">
			<fmt:message key="form.cliente.tipoPessoa" />
		</label>
		<div class="controls">
			<form:radiobutton path="tipoPessoa" onchange="submitForm('clienteForm');" value="FISICA" disabled="${clientePessoaWrapper.cliente.id != null}" />
			<fmt:message key="sistema.tipoPessoa.fisica" />

			<form:radiobutton path="tipoPessoa"	onchange="submitForm('clienteForm');" value="JURIDICA" disabled="${clientePessoaWrapper.cliente.id != null}" />
			<fmt:message key="sistema.tipoPessoa.juridica" />
		</div>
	</div>
	
	<c:if test="${clientePessoaWrapper.tipoPessoa eq 'FISICA'}">
	
		<div class="control-group">
			<label class="control-label requiredField" for="nome">
				<fmt:message key="form.cliente.nome" />
			</label>
			<div class="controls">
				<form:input path="pessoaFisica.nome" cssStyle="width:200px" id="nome" />
				<form:errors path="pessoaFisica.nome" cssClass="label label-important" />
			</div>
		</div>

		<div class="control-group">
			<label class="control-label requiredField" for="rg">
				<fmt:message key="form.cliente.rg" />
			</label>
			<div class="controls">
				<form:input path="pessoaFisica.rg" cssStyle="width:200px" id="rg" />
				<form:errors path="pessoaFisica.rg" cssClass="label label-important" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label requiredField" for="cpf">
				<fmt:message key="form.cliente.cpf" />
			</label>
			<div class="controls">
				<form:input path="pessoaFisica.cpf" cssStyle="width:200px" id="cpf" />
				<form:errors path="pessoaFisica.cpf" cssClass="label label-important" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label requiredField" for="emailFisica">
				<fmt:message key="form.cliente.email" />
			</label>
			<div class="controls">
				<form:input path="pessoaFisica.email" cssStyle="width:200px" id="emailFisica" />
				<form:errors path="pessoaFisica.email" cssClass="label label-important" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label requiredField" for="situacao">
				<fmt:message key="form.cliente.situacao" />
			</label>
			<div class="controls">
				<form:select path="cliente.situacao">
					<form:option value="">
						<fmt:message key="sistema.msg.textoComboInicial" />
					</form:option>
					<form:options items="${listaSituacoesCliente}"/>
				</form:select>
				<form:errors path="cliente.situacao" cssClass="label label-important" />
			</div>
		</div>

	</c:if>

	<c:if test="${clientePessoaWrapper.tipoPessoa eq 'JURIDICA' }">
		
		<div class="control-group">
			<label class="control-label requiredField" for="nomeFantasia">
				<fmt:message key="form.cliente.nomeFantasia" />
			</label>
			<div class="controls">
				<form:input path="pessoaJuridica.nomeFantasia" cssStyle="width:200px" id="nomeFantasia" />
				<form:errors path="pessoaJuridica.nomeFantasia" cssClass="label label-important" />
			</div>
		</div>

		<div class="control-group">
			<label class="control-label requiredField" for="razaoSocial">
				<fmt:message key="form.cliente.razaoSocial" />
			</label>
			<div class="controls">
				<form:input path="pessoaJuridica.razaoSocial" cssStyle="width:200px" id="razaoSocial" />
				<form:errors path="pessoaJuridica.razaoSocial" cssClass="label label-important" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label requiredField" for="cnpj">
				<fmt:message key="form.cliente.cnpj" />
			</label>
			<div class="controls">
				<form:input path="pessoaJuridica.cnpj" cssStyle="width:200px" id="cnpj" />
				<form:errors path="pessoaJuridica.cnpj" cssClass="label label-important" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label requiredField" for="inscricaoEstadual">
				<fmt:message key="form.cliente.inscricaoEstadual" />
			</label>
			<div class="controls">
				<form:input path="pessoaJuridica.inscricaoEstadual" cssStyle="width:200px" id="inscricaoEstadual" />
				<form:errors path="pessoaJuridica.inscricaoEstadual" cssClass="label label-important" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label requiredField" for="emailJuridica">
				<fmt:message key="form.cliente.email" />
			</label>
			<div class="controls">
				<form:input path="pessoaJuridica.email" cssStyle="width:200px" id="emailJuridica" />
				<form:errors path="pessoaJuridica.email" cssClass="label label-important" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label requiredField" for="situacao">
				<fmt:message key="form.cliente.situacao" />
			</label>
			<div class="controls">
				<form:select path="cliente.situacao">
					<form:option value="">
						<fmt:message key="sistema.msg.textoComboInicial" />
					</form:option>
					<form:options items="${listaSituacoesCliente}"/>
				</form:select>
				<form:errors path="cliente.situacao" cssClass="label label-important" />
			</div>
		</div>
		
	</c:if>
	
	<div class="control-group">
		<div class="controls">
		
			<button type="button" onclick="location.href='<c:url value="/cliente/clienteList/init.html"/>'" name="voltar" class="btn">
				<fmt:message key="sistema.botao.voltar" />
			</button>
			
			<button type="submit" name="salvar" class="btn">
				<fmt:message key="sistema.botao.salvar" />
			</button>
			
		</div>
	</div>


	<c:if test="${clientePessoaWrapper.cliente.id != null}">
	
		<div class="accordion" id="accordion2">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
						<fmt:message key="form.cliente.telefone" /> 
					</a>
				</div>

				<div id="collapseTwo" class="${!empty clientePessoaWrapper.listaTelefonesCliente ? 'accordion-body collapse in' : 'accordion-body collapse'}">
				
					<div class="control-group">
						<label class="control-label requiredField">
							<fmt:message key="form.cliente.tipoTelefone" />
						</label>
						<div class="controls">
							<form:select path="telefone.tipoTelefone" id="tipoTelefone">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
								<form:options items="${listaTiposTelefone}"/>
							</form:select>
							<form:errors path="telefone.tipoTelefone" cssClass="label label-important" />
						</div>
					</div>	
					
					<div class="control-group">
						<label class="control-label requiredField" for="telefone">
							<fmt:message key="form.cliente.numeroTelefone" />
						</label>
						<div class="controls">
							<form:input path="telefone.numero" cssStyle="width:200px" id="telefone"  />
							<form:errors path="telefone.numero" cssClass="label label-important" />
						</div>
					</div>
					
					<div class="control-group">
						<div class="controls" >
							<button type="submit" name="incluirTelefone" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
							
						</div>
					</div>
					
					<c:if test="${!empty clientePessoaWrapper.listaTelefonesCliente}">
						<table class="table table-condensed">
							<br />
							<thead>
								<th><fmt:message key="form.cliente.tipoTelefone" /></th>
								<th><fmt:message key="form.cliente.numeroTelefone" /></th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${clientePessoaWrapper.listaTelefonesCliente}" var="item">
									<tr>
										<td>${item.tipoTelefone}</td>
										<td>${item.numero}</td>
										<td>
											<a href="<c:url value="/cliente/clienteForm/excluirTelefone.html?id=${item.id}"/>">
												<i class="icon-remove"></i>
											</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>			
					
				</div>
			</div>
		</div>
		
	</c:if>
	
	<c:if test="${clientePessoaWrapper.cliente.id != null}">
	
		<div class="accordion" id="accordion3">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseTree">
						<fmt:message key="form.cliente.endereco" /> 
					</a>
				</div>
				<div id="collapseTree" class="${!empty clientePessoaWrapper.listaEnderecosCliente ? 'accordion-body collapse in' : 'accordion-body collapse'}">
				
					<div class="control-group">
						<label class="control-label requiredField" for="cep">
							<fmt:message key="form.cliente.cep" />
						</label>
						<div class="controls">
							<div style="float: left; width: 280px;">
								<form:input path="endereco.CEP" cssStyle="width:100px" id="cep" onblur="return findCEP();"/>
								<form:errors path="endereco.CEP" cssClass="label label-important" />
							</div>
							<div id="ScriptDiv" style="float: left; color: #FF0000; width: 200px; margin-top: 5px;"></div>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label requiredField" for="logradouro">
							<fmt:message key="form.cliente.logradouro" />
						</label>
						<div class="controls">
							<form:input path="endereco.logradouro" cssStyle="width:400px" id="logradouro" />
							<form:errors path="endereco.logradouro" cssClass="label label-important" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label requiredField" for="bairro">
							<fmt:message key="form.cliente.bairro" />
						</label>
						<div class="controls">
							<form:input path="endereco.bairro" cssStyle="width:400px" id="bairro" />
							<form:errors path="endereco.bairro" cssClass="label label-important" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label requiredField" for="numero">
							<fmt:message key="form.cliente.numero" />
						</label>
						<div class="controls">
							<form:input path="endereco.numero" cssStyle="width:100px" id="numero" />
							<form:errors path="endereco.numero" cssClass="label label-important" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="complemento">
							<fmt:message key="form.cliente.complemento" />
						</label>
						<div class="controls">
							<form:input path="endereco.complemento" cssStyle="width:400px" id="complemento" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label requiredField" for="cidade">
							<fmt:message key="form.cliente.cidade" />
						</label>
						<div class="controls">
							<form:input path="endereco.cidade" cssStyle="width:400px" id="cidade" />
							<form:errors path="endereco.cidade" cssClass="label label-important" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label requiredField" for="uf">
							<fmt:message key="form.cliente.uf" />
						</label>
						<div class="controls">
							<form:input path="endereco.UF" cssStyle="width:100px" id="uf" />
							<form:errors path="endereco.UF" cssClass="label label-important" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label requiredField">
							<fmt:message key="form.cliente.tipoEndereco" />
						</label>
						<div class="controls">
							<form:select path="endereco.tipoEndereco" id="tipoEndereco">
								<form:option value="">
									<fmt:message key="sistema.msg.textoComboInicial" />
								</form:option>
								<form:options items="${listaTiposEndereco}"/>
							</form:select>
							<form:errors path="endereco.tipoEndereco" cssClass="label label-important" />
						</div>
					</div>
					
					<div class="control-group">
						<div class="controls" >
							<button type="submit" name="incluirEndereco" class="btn"  title="<fmt:message key="sistema.botao.adicionar" />">
								<i class="icon-plus-sign"></i>
							</button>
							
						</div>
					</div>
					
					<c:if test="${!empty clientePessoaWrapper.listaEnderecosCliente}">
						<table class="table table-condensed">
							<br />
							<thead>
								<th><fmt:message key="form.cliente.cep" /></th>
								<th><fmt:message key="form.cliente.logradouro" /></th>
								<th><fmt:message key="form.cliente.bairro" /></th>
								<th><fmt:message key="form.cliente.numero" /></th>
								<th><fmt:message key="form.cliente.complemento" /></th>
								<th><fmt:message key="form.cliente.cidade" /></th>
								<th><fmt:message key="form.cliente.uf" /></th>
								<th><fmt:message key="form.cliente.tipoEndereco" /></th>
								<th><fmt:message key="sistema.dataTable.acoes" /></th>
							</thead>
							<tbody>
								<c:forEach items="${clientePessoaWrapper.listaEnderecosCliente}"
									var="item">
									<tr>
										<td>${item.CEP}</td>
										<td>${item.logradouro}</td>
										<td>${item.bairro}</td>
										<td>${item.numero}</td>
										<td>${item.complemento}</td>
										<td>${item.cidade}</td>
										<td>${item.UF}</td>
										<td>${item.tipoEndereco}</td>
										<td>
											<a href="<c:url value="/cliente/clienteForm/excluirEndereco.html?id=${item.id}"/>">
												<i class="icon-remove"></i>
											</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>	
				
				</div>
			</div>
		</div>
	
	</c:if>
</form:form>
