<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>

<html>

<head>
<title>Concrelaje - SisProducao | Login</title>
<link rel="stylesheet" type="text/css" href="/sisproducao/resources/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/sisproducao/resources/css/theme.css" />
<script type="text/javascript" src="/sisproducao/resources/bootstrap/js/bootstrap.js"></script>
</head>

<body>

    <div class="container">

        <c:if test="${not empty param['login_error']}">
            <div class="message-container">
                <div class="alert alert-error">Falha na autentica��o. O usu�rio ou a senha podem estar errados.</div>
            </div>
        </c:if>
        
        <div class="login-container">
            <div class="login-header">SisProdu��o :: Login</div>

            <spring:url var="authUrl" value="/j_spring_security_check" />
            
            <form class="form-horizontal login-form" action="${authUrl}" method="post">
                <div class="control-group">
                    <label class="control-label" for="j_username">Usu�rio</label>
                    <div class="controls">
                        <input type="text" id="j_username" name="j_username" placeholder="Usu�rio">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="j_password">Senha</label>
                    <div class="controls">
                        <input type="password" id="j_password" name="j_password" placeholder="Senha">
                    </div>
                </div>
                
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary">Ok</button>
                    </div>
                </div>
            </form>
        </div>
        
    </div>

</body>
</html>


