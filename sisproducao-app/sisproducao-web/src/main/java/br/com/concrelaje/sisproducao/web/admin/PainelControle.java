package br.com.concrelaje.sisproducao.web.admin;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@Controller
public class PainelControle {
    @RequestMapping(value = "/admin/PainelControle", method = RequestMethod.GET)
    public String abrirPainelControle(@RequestParam("id") String id, HttpServletRequest request, ModelMap modelMap) {
    	   request.getSession().setAttribute("idPecaSessao",id);
    	   System.out.println(request.getSession().getAttribute("idPecaSessao"));
    	   
    	   RequestContextHolder.getRequestAttributes();
		RequestContextHolder.currentRequestAttributes().setAttribute("idPecaSessao", id, RequestAttributes.SCOPE_SESSION);
    	   System.out.println(RequestContextHolder.currentRequestAttributes().getAttribute("idPecaSessao",RequestAttributes.SCOPE_SESSION));
    	   
        return "admin/PainelControle";
    }
    
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") String id, ModelMap modelMap) {
    	System.out.println("id");
    	return "producao/material/MaterialForm";
    }
}
