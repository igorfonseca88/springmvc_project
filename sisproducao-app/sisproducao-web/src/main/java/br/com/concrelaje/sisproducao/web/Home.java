package br.com.concrelaje.sisproducao.web;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("usuario")
public class Home {
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String index(ModelMap modelMap) {
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")){
			return "redirect:login.jsp";
		}else{
			modelMap.addAttribute("usuario",SecurityContextHolder.getContext().getAuthentication().getName());
		}
		return "home";
	}
}

